﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="NewExecutive.aspx.cs" Inherits="NewExecutive" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                return false;
            }
            status = "";
            return true;
        }
   
        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>
    <%--<div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblemail" runat="server" Text="Email:"></asp:Label>

                    <asp:TextBox ID="txtemail" runat="server" ></asp:TextBox><span class="error">*</span>
                   <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                   ControlToValidate="txtemail" ForeColor="Red" ErrorMessage="Invalid email address." />
   
                    </td>
              
                </tr>
                <tr>
                <td>
                    <asp:Label ID="lblpassword" runat="server" Text="Password:"></asp:Label>

                    <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox><span class="error1">*</span>
                    </td>
                </tr>
                <tr>
                <td>
                    <asp:Label ID="lblname" runat="server" Text="Name:"></asp:Label>

                    <asp:TextBox ID="txtname" runat="server"></asp:TextBox><span class="error2">*</span>
                    </td>
                </tr>
                <tr>
                <td>
                    <asp:Label ID="lblmobile" runat="server" Text="Mobile:"></asp:Label>

                    <asp:TextBox ID="txtmobile" runat="server"></asp:TextBox><span class="error3">*</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ForeColor="Red" ErrorMessage="Enter valid Phone number" ControlToValidate="txtmobile" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator> 
                    </td>
                </tr>
                <tr>
                <td>
                    <asp:Label ID="lblrole_type" runat="server" Text="Role:"></asp:Label>
                    <asp:DropDownList ID="ddlRole_type" runat="server" Height="30px" Width="120px"></asp:DropDownList><span class="error4">*</span>
                   
                    </td>
                </tr>

              <tr>
                <td>
                    <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
                    </td>
                <td>
                    <asp:Button ID="Cancel" runat="server" Text="Cancel" OnClick="Cancel_Click" />                  
                    </td>
              </tr>

            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
        </table>
        </div>

 
      <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="user_id"
        OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting"  >

        <Columns>
            <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("counter") %>'></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("user_id") %>'></asp:Label>
                </ItemTemplate>
               
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Password" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblpwd" runat="server" Text='<%# Eval("password") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Role_Name" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblroleid" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                </ItemTemplate>
              
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblroletype" runat="server" Text='<%# Eval("role_type") %>'></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
             <asp:TemplateField HeaderText="Name" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
             <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblemailid" runat="server" Text='<%# Eval("Email_id") %>'></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
             <asp:TemplateField HeaderText="Mobile_no" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblmobileno" runat="server" Text='<%# Eval("mobile_no") %>'></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
             <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                </ItemTemplate>
                   <EditItemTemplate>
              
                    <asp:DropDownList ID="ddlstatus" runat="server" width ="150px" SelectedValue='<%# Eval("status").ToString() %>'>
                          <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                         <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                   </asp:DropDownList>
              
                </EditItemTemplate>

            </asp:TemplateField>

            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150" />
        </Columns>

        <HeaderStyle BackColor="Gray" ForeColor="#ffffff" />
        <RowStyle BackColor="#e7ceb6" />
    </asp:GridView>--%>
   
      <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
       <script src="validation.js"></script>
   


    <script type="text/javascript">
        $(document).ready(function () {


            $('#<%=Submit.ClientID%>').click(function (event) {

                var returntypp = true;
                if ($.trim($("#<%=txtemail.ClientID%>").val()) == "") {

                    $("#<%=txtemail.ClientID%>").focus();
                    $('.error').show();
                    returntypp = false;
                }
                else {
                    $('.error').hide();
                }
                if ($.trim($("#<%=txtpassword.ClientID%>").val()) == "") {

                    $("#<%=txtpassword.ClientID%>").focus();
                    $('.error1').show();
                    returntypp = false;
                }
                else {
                    $('.error1').hide();
                }
                if ($.trim($("#<%=txtname.ClientID%>").val()) == "") {

                    $("#<%=txtname.ClientID%>").focus();
                    $('.error2').show();
                    returntypp = false;
                }
                else {
                    $('.error2').hide();
                }

                if ($.trim($("#<%=txtmobile.ClientID%>").val()) == "") {

                    $("#<%=txtmobile.ClientID%>").focus();
                    $('.error3').show();
                    returntypp = false;
                }
                else {
                    $('.error3').hide();
                }

                if ($.trim($("#<%=ddlRole_type.ClientID%>").val()) == "0") {

                    $("#<%=ddlRole_type.ClientID%>").focus();
                    $('.error4').show();
                    returntypp = false;
                }
                else {
                    $('.error4').hide();
                }
                return returntypp;
            });

        });
        //function confirmUpdate(thisObj) {
           

           
        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl02_lbtnEdit").val()) == "") {
                
        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl02_lbtnEdit").focus();
        //        return false;
        //    }
        //    else {
        //        alert("hi");
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>

   

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
   <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > New Executive</h3>
                    </div>
                    <div class="panel-body">


                          <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1" id="lblemail" runat="server">Email:</label>
                                <asp:TextBox CssClass="form-control" runat="server" ID="txtemail" MaxLength="50"></asp:TextBox>                              
                                <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="txtemail" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
               

                                 
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1" id="lblpassword" runat="server">Password :</label>
                                <asp:TextBox CssClass="form-control" runat="server" ID="txtpassword" MaxLength="40" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFVMK1" runat="server" ControlToValidate="txtpassword" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="Regex2" runat="server" ControlToValidate="txtpassword" ValidationGroup="group1" Display="dynamic"  ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{5,}$" ErrorMessage="Minimum 5 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />                                         
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1" id="lblname" runat="server">Name :</label>
                                <asp:TextBox CssClass="form-control" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');" runat="server" ID="txtname" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFVMK2" runat="server" ControlToValidate="txtname" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                 <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtname" ID="RegularExpressionValidator1" ValidationExpression = "^[\s\S]{5,30}$" runat="server" ErrorMessage="Minimum 5 and Maximum 20 characters required."></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtname" ValidationGroup="group1"
                           ValidationExpression="[a-zA-Z ]*$" ErrorMessage="*Valid characters: Alphabets and space." />

                                 </div>
                        </div>
                              </div>


                         <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1" id="lblmobile" runat="server">Mobile :</label>
                                <asp:TextBox CssClass="form-control"  onkeypress="return checkitt(event)" runat="server" ID="txtmobile" MaxLength="10"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtmobile" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="group1"
                                 ControlToValidate="txtmobile" ErrorMessage="Please Fill valid 10 digit mobile no." 
                                ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                        </div>


                     
                             <div class="col-md-4">
                                 <div class="form-group">
                                     <label for="exampleInputEmail1" id="lblrole_type">Role :</label>
                                     <asp:DropDownList CssClass="form-control" ID="ddlRole_type" runat="server">
                                     </asp:DropDownList>
                                 </div>

                             </div>
                         
                              <div class="col-md-4">
                                 <div class="form-group">
                                     <label for="exampleInputEmail1" id="lblBranch">Branch :</label>
                                     <asp:DropDownList CssClass="form-control" ID="DD_Branch" runat="server">
                                     </asp:DropDownList>
                                 </div>

                             </div>

                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" ValidationGroup="group1" />

                                    <label for="exampleInputPassword1" id="Label1" runat="server" style="color: red;"></label>

                                </div>
                            </div>
                        </div>


                       <%-- <div class="form-group">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="button buttonBlue" OnClick="Cancel_Click" />

                            </div>
                        </div>--%>
                           <%--<div class="row">
                               <div class="col-md-8">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                        </div>
                                   </div>
                               </div>--%>

                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="user_id"
                            OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                            OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                            CssClass="table" GridLines="None" Width="100%">

                            <Columns>
                                <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("counter") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("user_id") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Password" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpwd" runat="server" Text='<%# Eval("password") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                         <asp:TextBox ID="txtExecPwd" runat="server" Text='<%#Eval("password") %>' Width="100px" BackColor="#ffff66" MaxLength="49"></asp:TextBox>                                                       
                                     </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Role_Name" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblroleid" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlRole_Name" runat="server" Width="150px"></asp:DropDownList>
                                        <asp:Label ID="lblRoleNameHidden" runat="server" Text='<%# Eval("Role") %>' Visible="false"></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblroletype" runat="server" Text='<%# Eval("role_type") %>'></asp:Label>
                                    </ItemTemplate>
                                    <%--<EditItemTemplate>
                                        <asp:DropDownList ID="ddlRole_Type" runat="server" Width="150px"></asp:DropDownList>
                                        <asp:Label ID="lblRoleTypeHidden" runat="server" Text='<%# Eval("role_type") %>' Visible="false"></asp:Label>
                                    </EditItemTemplate>--%>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblemailid" runat="server" Text='<%# Eval("Email_id") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile_no" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmobileno" runat="server" Text='<%# Eval("mobile_no") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>

                                        <asp:DropDownList ID="ddlstatus" runat="server" Width="150px" SelectedValue='<%# Eval("status").ToString() %>'>
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>

                                    </EditItemTemplate>

                                </asp:TemplateField>

                              
                                   <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                   <EditItemTemplate>
                                <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update" ></asp:LinkButton>
                                <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                  </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("user_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>


                            </Columns>

                        </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>



</asp:Content>

