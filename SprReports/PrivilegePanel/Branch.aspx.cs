﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_PrivilegePanel_Branch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.BindGridview();
            //RoleType.Items.Insert(0, new ListItem("--Select RoleType--", "0"));
        }

    }
    protected void Submit_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();


        try
        {
            SqlCommand cmd = new SqlCommand("Sp_Branch_Opertion", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Branch", Branchtxt.Text.Trim());
            cmd.Parameters.AddWithValue("@CreatedBy", Session["UID"].ToString());
            cmd.Parameters.AddWithValue("@Action", "insert");

            string result = "";
            result = cmd.ExecuteScalar().ToString();

            if (result.ToLower() == "insert")
            {
                BindGridview();
                string message = "Insert Record Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);


            }
            else
            {
                BindGridview();
                string message = "Data Already Exists";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

            }

        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }

    }
    //private static DataTable GetData(string query)
    //{

    //    string constr = ConfigurationManager.ConnectionStrings["reg_connection"].ConnectionString;
    //    SqlConnection con = new SqlConnection(constr);
    //    con.Open();

    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = query;

    //    SqlDataAdapter sda = new SqlDataAdapter();

    //    cmd.Connection = con;
    //    sda.SelectCommand = cmd;
    //    DataTable dt = new DataTable();

    //    sda.Fill(dt);
    //    con.Close();

    //    return dt;
    //}
    protected void BindGridview()
    {

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("Sp_Branch_Opertion");
        cmd.Parameters.AddWithValue("@Action", "select");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.Connection = con;
        sda.SelectCommand = cmd;

        DataTable dt = new DataTable();
        sda.Fill(dt);
        GridView1.DataSource = dt;
        con.Close();



        GridView1.DataBind();
        //  RoleType.Items.Clear();
        // RoleType.ClearSelection();
        //RoleType.Items.Insert(0, new ListItem("--Select Role--", "0"));
    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();
    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int BranchID = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Sp_Branch_Opertion"))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Branch_id", BranchID);
                cmd.Parameters.AddWithValue("@Action", "delete");

                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();
    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        int BranchID = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);

        string Branch = (row.FindControl("txtBranch") as TextBox).Text;
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Sp_Branch_Opertion"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Branch", Branch.Trim());
                cmd.Parameters.AddWithValue("@Branch_ID", BranchID);
                cmd.Parameters.AddWithValue("@UpdateBy", Session["UID"]);
                cmd.Parameters.AddWithValue("@Action","update");
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();
    }


}