﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SprReports_Accounts_fixedDepartureStatus
    Inherits System.Web.UI.Page


    Dim con As New SqlConnection
    Dim adp As SqlDataAdapter
    Public ds As New DataSet
    Public dst As New DataSet
    Private ST As New SqlTransaction
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        'If Session("User_Type").ToString().ToUpper() <> "ACC" Then
        '    Response.Redirect("~/Login.aspx")
        'End If
        If Not Page.IsPostBack Then
            Try
                If con.State = ConnectionState.Open Then con.Close()
                con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
                adp = New SqlDataAdapter("SP_FDSTSTATUS", con)
                adp.SelectCommand.CommandType = CommandType.StoredProcedure
                adp.SelectCommand.Parameters.AddWithValue("@action", "SELECT")
                adp.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim fd As Boolean = False
                    fd = Convert.ToBoolean(ds.Tables(0).Rows(0)(0))
                    If (fd = True) Then
                        Fdstatus.Checked = True
                    End If


                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        End If
    End Sub
    Protected Sub Fdstatus_CheckedChanged(sender As Object, e As EventArgs) Handles Fdstatus.CheckedChanged

        Dim fd As Boolean = False
        Dim a As Integer = 0
        Dim b As Integer = 0
        If (Fdstatus.Checked) Then
            b = 1
        End If

        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim cmd As New SqlCommand("SP_FDSTSTATUS", con)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Status", b)
        cmd.Parameters.AddWithValue("@action", "MODI")

        con.Open()
        a = cmd.ExecuteNonQuery()

        If (a > 0) Then
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Updated Successfully.')", True)

        Else

            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Not Completed')", True)
        End If

        con.Close()

    End Sub
End Class

