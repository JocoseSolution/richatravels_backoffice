﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="BillingReport.aspx.vb" Inherits="SprReports_Cargo_BillingReport" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
       var UrlBase3 = '<%=ResolveUrl("http://admin.b2brichatravels.in")%>';
        //var UrlBase3 = 'http://admin.b2brichatravels.in/SprReports/Cargo/BillingReport.aspx';
    </script>

    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css?V=1")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    
    
    


    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">SG Cargo Express > API Billing Report </h3>
                    </div>


                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlShipper" runat="server" class="form-control">
                                        <asp:ListItem>Select Shipper</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" runat="server" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" runat="server" name="To" placeholder="To Date" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txt_PNR" placeholder="AWT Number" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txt_OrderId" placeholder="Enter Order Id" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="btn_result" runat="server" class="btn btn-danger" value="Search Result" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="btn_export" runat="server" class="btn btn-danger" Text="Export" />

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <label style="background: #000; color: #fff; border-radius: 4px; padding: 2px 9px; display: none;">
                                    Total Ticket Sale :
                                    <span class="fa fa-inr"></span>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <label style="background: #000; color: #fff; border-radius: 4px; padding: 2px 9px; margin-left: -35px; float: right;">
                                    Booking Summary :
                              <span class="fa fa-inr"></span>
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                    (<asp:Label ID="lbl_counttkt" runat="server"></asp:Label>)</label>
                            </div>
                        </div>
                        <br />
                        <div class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">
                                <div id="dtvs" runat="server">
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
	<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
	<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/SGXpress/SGXpressDetails.js?v=1.1")%>"></script>
</asp:Content>


