﻿Imports System.Data
Imports System.Linq
Imports SG_API
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Web.Services
Partial Class SprReports_Cargo_BookingReport
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Private ST As New SqlTransaction()
    Private CllInsSelectFlt As New clsInsertSelectedFlight()
    Dim AgencyDDLDS, grdds, fltds As New DataSet()
    Private sttusobj As New Status()
    Dim con As New SqlConnection()
    Dim PaxType As String
    Dim clsCorp As New ClsCorporate()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If


            If Not IsPostBack Then
                GridViewDataBind()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub


    Protected Sub GridViewDataBind()
        Dim dt As New DataTable()
        Try

            Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
            Dim con As New SqlConnection(constr)
            Dim cmd As New SqlCommand("GetBookingDetails_SG_Cargo_Express")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            con.Open()
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            ticket_grdview.DataSource = dt
            ticket_grdview.DataBind()
            cmd.Dispose()

            con.Close()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub



    Protected Sub btn_result_Click(sender As Object, e As EventArgs)
        'GridViewDataBind()
        Dim dt As New DataTable()

        Dim FromDate As String
        Dim ToDate As String
        If [String].IsNullOrEmpty(Request("From")) Then
            FromDate = ""
        Else
            'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)
            FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
            FromDate = FromDate + " " + "12:00:00 AM"
        End If
        If [String].IsNullOrEmpty(Request("To")) Then
            ToDate = ""
        Else
            ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
            ToDate = ToDate & " " & "11:59:59 PM"
        End If

        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("GetDataBookingDetailsOnSearching")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@frmdate", FromDate)
        cmd.Parameters.AddWithValue("@todate", ToDate)
        cmd.Parameters.AddWithValue("@awt_no", txt_awt_no.Text.ToString)
        cmd.Connection = con
        Dim da As New SqlDataAdapter(cmd)

        da.Fill(dt)
        ticket_grdview.DataSource = dt
        ticket_grdview.DataBind()
        cmd.Dispose()

        con.Close()


    End Sub
End Class
