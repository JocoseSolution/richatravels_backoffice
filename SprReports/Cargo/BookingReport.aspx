﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="BookingReport.aspx.vb" Inherits="SprReports_Cargo_BookingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


     <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%-- <link href="<%=ResolveUrl("~/CSS/main2.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/style.css")%>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <div class="row">

        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">SG Cargo Express > Booking Report</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" placeholder="To Date" class="form-control" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">AWT NO</label>
                                    <asp:TextBox ID="txt_awt_no" runat="server" placeholder="Enter Awt No" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" CssClass="button buttonBlue" OnClick="btn_result_Click" Text="Search Result" />
                                </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                                    </div>
                        </div>
                    </div>
                         <div class="row">
                            <%-- style="height:200px;overflow:scroll;"--%>

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff;  overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                                    <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        PageSize="30">
                                        <Columns>

                                            
                                            <asp:TemplateField HeaderText="Invoice No" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="INVno" runat="server" Text='<%# Eval("InvoiceNo")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            
                                             

                                            <asp:TemplateField HeaderText="Agency Name" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="agname" runat="server" Text='<%# Eval("AgencyName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="AWT No" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="tackid" runat="server" Text='<%# Eval("awb_number")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Booking Type" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="bookingtype" runat="server" Text='<%# Eval("BookingType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Amount" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="bookingtype" runat="server" Text='<%# Eval("Debit")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                             <asp:TemplateField HeaderText="ORIGIN" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="origin" runat="server" Text='<%# Eval("customer_city")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="DESTINATION" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="destination" runat="server" Text='<%# Eval("shipper_city")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CONSIGNEE Name" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="consigneename" runat="server" Text='<%# Eval("shipper_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                           <asp:TemplateField HeaderText="Customer Name" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="customername" runat="server" Text='<%# Eval("customer_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Created Date" FooterStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="cd" runat="server" Text='<%# Eval("Createddate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 11px; font-weight: 500; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        function closethis() {
            $("#outerdiv").hide();
        }
    </script>
</asp:Content>
