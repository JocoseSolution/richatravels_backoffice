﻿Imports System.Data
Imports System.Linq
Imports SG_API
Partial Class SprReports_Cargo_BillingReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If


            If Not IsPostBack Then
                ddlbind()
		    End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    'Protected Sub btn_result_Click(sender As Object, e As EventArgs) Handles btn_result.Click
    '    Dim objTrackingRequest As TasklistRQ = New TasklistRQ()
    '    Dim objGetTrackRequestResponse As TasklistRS = New TasklistRS()
    '    Dim objTrackingResponse As GetTasklist = New GetTasklist()
    '    objTrackingRequest.search = New Search()
    '    ''  From.Value = System.DateTime.Now().ToString("dd-MM-yyyy")
    '    ''  [To].Value = System.DateTime.Now().ToString("dd-MM-yyyy")
    '    objTrackingRequest.search.from = From.Value
    '    objTrackingRequest.search.to = [To].Value
    '    Dim strShiper As String = ""
    '    strShiper = ddlShipper.SelectedItem.Text
    '    If strShiper = "Select Shipper" Then
    '        strShiper = ""
    '    End If
    '    objGetTrackRequestResponse = objTrackingResponse.TasklistRSResponse(strShiper, objTrackingRequest)
    '    Dim strlayout2 As String = ""
    '    strlayout2 += "<h3> Status and Scan </h3> "
    '    strlayout2 += "<table class='table'>"

    '    strlayout2 += "<tr>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Waybill Number"
    '    strlayout2 += "</th>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Created Date"
    '    strlayout2 += "</th>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Origin"
    '    strlayout2 += "</th>"

    '    strlayout2 += "<th>"
    '    strlayout2 += "Destination"
    '    strlayout2 += "</th>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Created Date"
    '    strlayout2 += "</th>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Total weight"
    '    strlayout2 += "</th>"

    '    strlayout2 += "<th>"
    '    strlayout2 += "Total item"
    '    strlayout2 += "</th>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Status"
    '    strlayout2 += "</th>"
    '    strlayout2 += "<th>"
    '    strlayout2 += "Consignee Name"
    '    strlayout2 += "</th>"
    '    strlayout2 += "</tr>"
    '    If String.IsNullOrEmpty(objGetTrackRequestResponse.status) = False Then
    '        For index = 0 To objGetTrackRequestResponse.result.data.ToList().Count - 1
    '            strlayout2 += "<tr>"
    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).awb_number
    '            strlayout2 += "</td>"
    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).created_at
    '            strlayout2 += "</td>"
    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).origin_city
    '            strlayout2 += "</td>"

    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).destination_city
    '            strlayout2 += "</td>"
    '            strlayout2 += "<td>"
    '            strlayout2 += Convert.ToString(objGetTrackRequestResponse.result.data(index).client_status_list.Count)
    '            strlayout2 += "</td>"
    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).by_flight
    '            strlayout2 += "</td>"

    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).client_status
    '            strlayout2 += "</td>"
    '            strlayout2 += "<td>"
    '            strlayout2 += objGetTrackRequestResponse.result.data(index).delvery_date
    '            strlayout2 += "</td>"
    '            strlayout2 += "<td>"
    '            strlayout2 += "RICHA WORLD TRAVELS"
    '            strlayout2 += "</td>"
    '            strlayout2 += "</tr>"
    '        Next

    '    End If

    '    strlayout2 += "</table>"
    '    dtvs.InnerHtml = strlayout2
    'End Sub
    Public Sub ddlbind()

        Dim objCustomerlist As Customerlist = New Customerlist()
        Dim objresponse2 As CustomerRS = New CustomerRS()
        objresponse2 = objCustomerlist.CustomerlistResponse()
        Dim ii As Integer = 0
        If objresponse2.status <> "error" Then
            For Each item In objresponse2.result
                ddlShipper.Items.Add(New ListItem(Convert.ToString(item.code), item.id))
            Next
        End If
    End Sub
End Class
