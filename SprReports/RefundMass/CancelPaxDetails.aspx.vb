﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SprReports_RefundMass_CancelPaxDetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("paxdetails_can", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@Status", Request.QueryString("Status").ToString)
            adap.SelectCommand.Parameters.AddWithValue("@PNRNO", Request.QueryString("PNRNO").ToString)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            ''ViewState("Crd_limit") = dt2.Rows(0)("Crd_Limit").ToString()
            PAXDETAILS.DataSource = dt2
            PAXDETAILS.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class
