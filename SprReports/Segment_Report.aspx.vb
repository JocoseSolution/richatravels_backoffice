﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class SprReports_Segment_Report
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If

            If Not IsPostBack Then
                CheckEmptyValue()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Sub CheckEmptyValue()
        Try
            Dim thisDate As Date
            thisDate = Today

            Dim strDate As String = thisDate.ToString("yyyy-MM-dd")

            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = strDate + " " + "00:00:00"
            Else
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)  'Date Format 2017-04-09 12:00:00 AM'
                FromDate = FromDate + " " + "00:00:00"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = strDate + " " + "23:59:59"
            Else
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2) & "-" & Left((Request("To")).Split(" ")(0), 2)
                ToDate = ToDate + " " + "23:59:59"

            End If

            Dim AgencyId As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")), "", Request("hidtxtAgencyName"))
            Dim AirlineCode As String = If([String].IsNullOrEmpty(Request("aircode")), "", Request("aircode"))
            Dim AirlineId As String = If([String].IsNullOrEmpty(Request("hdnairlineid")), "", Request("hdnairlineid"))
            Dim Sector As String = ddlSector.SelectedValue

            Dim dtSegment As New DataTable()
            dtSegment = USP_GetSegment(FromDate, ToDate, AgencyId, AirlineCode, AirlineId, Sector).Tables(0)

            ticket_grdview.DataSource = dtSegment
            ticket_grdview.DataBind()
            If dtSegment.Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If

            'ticket_grdview.Columns(5).Visible = True
            'ticket_grdview.Columns(4).Visible = True

            'If Sector = "D" Then
            '    ticket_grdview.Columns(5).Visible = False
            'Else
            '    ticket_grdview.Columns(4).Visible = False
            'End If

            lbl_Total.Text = dtSegment.Rows.Count
            Session("Grdds") = dtSegment

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            CheckEmptyValue()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub ticket_grdview_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ticket_grdview.PageIndexChanging
        Try
            ticket_grdview.PageIndex = e.NewPageIndex
            BindGrid(Session("Grdds"))

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Private Sub BindGrid(ByVal Gds As DataTable)
        Try
            Session("Grdds") = Gds
            ticket_grdview.DataSource = Gds
            ticket_grdview.DataBind()
            If Gds.Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try

            Dim thisDate As Date
            thisDate = Today

            Dim strDate As String = thisDate.ToString("yyyy-MM-dd")

            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = strDate + " " + "00:00:00"
            Else
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)  'Date Format 2017-04-09 12:00:00 AM'
                FromDate = FromDate + " " + "00:00:00.001"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = strDate + " " + "23:59:59"
            Else
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2) & "-" & Left((Request("To")).Split(" ")(0), 2)
                ToDate = ToDate + " " + "23:59:59.999"

            End If

            Dim AgencyId As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")), "", Request("hidtxtAgencyName"))
            Dim AirlineCode As String = If([String].IsNullOrEmpty(Request("aircode")), "", Request("aircode"))
            Dim AirlineId As String = If([String].IsNullOrEmpty(Request("AirlineId")), "", Request("AirlineId"))
            Dim Sector As String = ddlSector.SelectedValue

            Dim dsSegment As New DataSet()
            dsSegment = USP_GetSegment(FromDate, ToDate, AgencyId, AirlineCode, AirlineId, Sector)

            STDom.ExportData(dsSegment)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function USP_GetSegment(ByVal FromDate As String, ByVal ToDate As String, ByVal AgencyId As String, ByVal AirlineCode As String, ByVal AirlineId As String, ByVal Sector As String) As DataSet
        Dim cmd As New SqlCommand()
        Dim ds As New DataSet()
        Try
            Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            cmd.CommandText = "sp_GetSegmentDetails"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@fromdate", SqlDbType.VarChar).Value = FromDate
            cmd.Parameters.Add("@todate", SqlDbType.VarChar).Value = ToDate
            cmd.Parameters.Add("@agencyid", SqlDbType.VarChar).Value = AgencyId
            cmd.Parameters.Add("@airlinecode", SqlDbType.VarChar).Value = AirlineCode
            cmd.Parameters.Add("@airlineid", SqlDbType.VarChar).Value = AirlineId
            cmd.Parameters.Add("@sector", SqlDbType.VarChar).Value = Sector
            cmd.Connection = con1
            con1.Open()
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            cmd.Dispose()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return ds
    End Function
End Class
