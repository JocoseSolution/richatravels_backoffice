﻿Imports System.Data

Imports System.Configuration.ConfigurationManager
Partial Class SprReports_Hotel_HoldHotelAccept
    Inherits System.Web.UI.Page

    Private SqlTrans As New HotelDAL.HotelDA()
    Private STDom As New SqlTransactionDom
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", True)
            End If
            If Not IsPostBack Then
                If Session("User_Type") = "EXEC" Then
                    BindGrid()
                End If
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    Protected Sub btnHoldReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHoldReject.Click
        Dim STDom As New SqlTransactionDom()
        Dim SqlT As New SqlTransaction()
        Dim HoldGrdds As New DataSet()
        Try
            HoldGrdds = Session("HoldHotelInProcessDS")
            For Each strow In HoldGrdds.Tables(0).Rows
                If strow.Item("OrderID").ToString() = Request("OrderIDS").Trim() Then
                    Dim Refundfare As Decimal = Convert.ToDecimal(strow.Item("NetCost"))
                    'Update Header table  After Reject
                    Dim i As Integer = SqlTrans.RejectHoldBooking(StatusClass.Rejected.ToString(), Request("OrderIDS").ToString(), Refundfare, Session("UID").ToString(), Request("txtRemark").ToString())
                    If i > 0 Then

                        If HoldGrdds IsNot Nothing Then
                            If HoldGrdds.Tables.Count > 0 Then
                                If HoldGrdds.Tables(0).Rows.Count > 0 Then
                                    RefundAmountAndLadgerEntry(Request("OrderIDS"), Refundfare, strow.Item("AgentID").ToString(), "", strow.Item("Provider"), strow.Item("HotelName").ToString(), strow.Item("AgencyName").ToString(), strow.Item("TripType").ToString())
                                    BindGrid()
                                    Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Reject Successfully.'" & "); ", True)
                                    Try
                                        ''''Send Mail and SMS Start
                                        Dim HtlDetailsDs As New DataSet()
                                        HtlDetailsDs = SqlTrans.htlintsummary(Request("OrderIDS"), "Ticket")

                                        Dim objmail As New HotelBAL.HotelSendMail_Log()
                                        ''objmail.SendEmailForCancelAndReject(Request("OrderIDS"), HtlDetailsDs.Tables(0).Rows(0)("BookingID").ToString(), strow.Item("HotelName").ToString(), HtlDetailsDs.Tables(0).Rows(0)("LoginID").ToString(), HotelStatus.HOTEL_REJECT.ToSting(), StatusClass.Rejected.ToString())

                                        Dim objSMSAPI As New SMSAPI.SMS
                                        Dim objSqlNew As New SqlTransactionNew
                                        Dim smstext As String = ""
                                        Dim SmsCrd As DataTable
                                        Dim objDA As New SqlTransaction
                                        SmsCrd = objDA.SmsCredential(SMS.HOTELBOOKING.ToString()).Tables(0)
                                        Dim smsStatus As String = ""
                                        Dim smsMsg As String = ""
                                        If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                                            smsStatus = objSMSAPI.SendHotelSms(Request("OrderIDS"), "", HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), strow.Item("HotelName").ToString(), "", "", "Reject", smstext, SmsCrd)
                                            objSqlNew.SmsLogDetails(Request("OrderIDS"), HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), smstext, smsStatus)
                                        End If
                                        ''''Send Mail and SMS End
                                    Catch ex As Exception
                                        HotelDAL.HotelDA.InsertHotelErrorLog(ex, " Send SMS Support Hold Request")
                                    End Try
                                End If
                                End If
                        End If
                        Exit For
                    Else
                        Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Try later." & "); ", True)
                    End If
                End If
            Next
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub btnHoldUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHoldUpdate.Click
        Try
            Dim i As Integer = SqlTrans.UpdateHrdHold(Request("OrderIDS").Trim(), Request("txtHtlBID").Trim(), Request("txtHtlConfNo").Trim(), StatusClass.Confirm.ToString(), Session("UID").ToString(), Request("txtRemark"))
            If i > 0 Then
                Dim HoldGrdds As New DataSet()
                HoldGrdds = Session("HoldHotelInProcessDS")
                For Each strow In HoldGrdds.Tables(0).Rows
                    If strow.Item("OrderID").ToString() = Request("OrderIDS").Trim() Then
                        'Send Mail to Agent
                        Dim HtlLogObj As New HotelBAL.HotelSendMail_Log()
                        '' HtlLogObj.SendEmailForHold_Reject(Request("OrderIDS").Trim(), strow.Item("HotelName").ToString(), Request("txtHtlBID").Trim(), strow.Item("AgentID").ToString())

                        BindGrid()

                        Try
                            ''''Send Mail and SMS Start
                            Dim HtlDetailsDs As New DataSet()
                            HtlDetailsDs = SqlTrans.htlintsummary(Request("OrderIDS"), "Ticket")

                            Dim objmail As New HotelBAL.HotelSendMail_Log()
                            objmail.SendEmailForCancelAndReject(Request("OrderIDS").Trim(), Request("txtHtlBID").Trim(), HtlDetailsDs.Tables(0).Rows(0)("HotelName").ToString().Trim() + ", " + HtlDetailsDs.Tables(0).Rows(0)("CityName").ToString().Trim(), HtlDetailsDs.Tables(0).Rows(0)("LoginID").ToString(), HotelShared.HotelStatus.HOTEL_BOOKING.ToString(), HotelShared.HotelStatus.Confirm.ToString())

                            Dim objSMSAPI As New SMSAPI.SMS
                            Dim objSqlNew As New SqlTransactionNew
                            Dim smstext As String = ""
                            Dim SmsCrd As DataTable
                            Dim objDA As New SqlTransaction
                            SmsCrd = objDA.SmsCredential(SMS.HOTELBOOKING.ToString()).Tables(0)
                            Dim smsStatus As String = ""
                            Dim smsMsg As String = ""
                            If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                                smsStatus = objSMSAPI.SendHotelSms(Request("OrderIDS"), "", HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), strow.Item("HotelName").ToString(), "", "", "Book", smstext, SmsCrd)
                                objSqlNew.SmsLogDetails(Request("OrderIDS"), HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), smstext, smsStatus)
                            End If
                            ''''Send Mail and SMS End
                        Catch ex As Exception
                            HotelDAL.HotelDA.InsertHotelErrorLog(ex, " Send SMS Support Hold Accept")
                        End Try
                    End If
                Next
                Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Updated successfully'); ", True)
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim HoldHotelInProcessDS As New DataSet()
            HoldHotelInProcessDS = SqlTrans.GetHoldHotel(Session("UID").ToString(), StatusClass.InProcess.ToString(), "")
            HoldHotelAcceptGrd.DataSource = HoldHotelInProcessDS
            HoldHotelAcceptGrd.DataBind()
            Session("HoldHotelInProcessDS") = HoldHotelInProcessDS
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Function RefundAmountAndLadgerEntry(ByVal orderid As String, ByVal RefundAmount As Decimal, ByVal AgentID As String, ByVal BookingID As String, ByVal Provider As String, ByVal HotelName As String, ByVal AgencyName As String, ByVal TripType As String) As Double
        Dim ablBalance As Double = 0
        Try
            Dim SqlT As New SqlTransaction()
            ablBalance = SqlT.UpdateNew_RegsRefund(AgentID, RefundAmount)
            Dim Result As Integer = STDom.LedgerEntry_Common(orderid, 0, RefundAmount, ablBalance, Provider, HotelName, BookingID, "HTLRFND", AgentID, AgencyName, Session("UID").ToString(), _
                                     Request.UserHostAddress.ToString(), "", "", "", "Hold Hotel Reject against " + orderid, TripType.Substring(0, 1), 0)
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
        Return ablBalance
    End Function

End Class
