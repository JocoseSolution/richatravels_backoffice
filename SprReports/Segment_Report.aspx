﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="Segment_Report.aspx.vb" Inherits="SprReports_Segment_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }



        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }

        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            SearchText();
            AutoAirlineId();
        });
        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "TicketReport.aspx/GetAutoCompleteData",
                        data: "{'username':'" + $('#AirlineName').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1]
                                    }
                                }));
                            }
                            else {
                                response([{ label: 'No Records Found', val: -1 }]);
                            }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    if (ui.item.val == -1) {
                        return false;
                    }
                    $('#aircode').val(ui.item.val);
                }
            });
        }
        function AutoAirlineId() {
            debugger;
            $(".autoairlineid").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "TicketReport.aspx/GetAutoCompleteAirlineId",
                        data: "{'airid':'" + $('#AirLineNId').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1]
                                    }
                                }));
                            }
                            else {
                                response([{ label: 'No Records Found', val: -1 }]);
                            }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    if (ui.item.val == -1) {
                        return false;
                    }
                    $('#hdnairlineid').val(ui.item.val);
                }
            });
        }
    </script>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Flight Segment Report</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="fromdate">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" placeholder="Date From" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="todate">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" placeholder="Date To" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="agencyname">Agency Name</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" placeholder="Agency Name or ID" autocomplete="off" class="form-control" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" /></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="airline">Airline Name</label>
                                    <%--<asp:TextBox ID="txtSearch" runat="server" CssClass="form-control autosuggest" placeholder="Airline Name"></asp:TextBox>--%>
                                    <input id="AirlineName" placeholder="Airline Name" class="form-control autosuggest" />
                                    <input type="hidden" id="aircode" name="aircode" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="airlineid">Airline Id</label>
                                    <%--<asp:TextBox ID="txtAirLineId" runat="server" CssClass="form-control" placeholder="Airline Id"></asp:TextBox>--%>
                                    <input id="AirLineNId" placeholder="Airline Id" class="form-control autoairlineid" />
                                    <input type="hidden" id="hdnairlineid" name="hdnairlineid" value="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="airlineid">Sector</label>
                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="D" Selected="True">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <br />
                                <asp:Button ID="btn_result" runat="server" CssClass="button buttonBlue" Text="Search Result" />
                            </div>
                            <div class="col-md-3">
                                <br />
                                <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Records :</label>
                                    <asp:Label ID="lbl_Total" runat="server">0</asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="divReport" runat="server" visible="false">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff;">
                                <ContentTemplate>
                                    <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        PageSize="30">
                                        <Columns>
                                            <asp:BoundField HeaderText="AirLine" DataField="AirLine"></asp:BoundField>
                                            <asp:BoundField HeaderText="AirLine Code" DataField="vc"></asp:BoundField>
                                            <asp:BoundField HeaderText="Number Of Ticket" DataField="NumberOfTicket"></asp:BoundField>
                                            <asp:BoundField HeaderText="No Of Segment" DataField="NoOfSegment"></asp:BoundField>
                                            <asp:BoundField HeaderText="Domestic Segment" DataField="DomSegment"></asp:BoundField>
                                            <asp:BoundField HeaderText="International Segment" DataField="IntSegment"></asp:BoundField>
                                            <asp:BoundField HeaderText="Total Sale" DataField="TotalSale"></asp:BoundField>
                                            <asp:BoundField HeaderText="Net Sale" DataField="NetSale"></asp:BoundField>
                                            <asp:BoundField HeaderText="CRS" DataField=""></asp:BoundField>
                                            <asp:BoundField HeaderText="ID" DataField="TicketId"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        function closethis() {
            $("#outerdiv").hide();
        }
    </script>
</asp:Content>


