﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_AccountingSoftware_DeleteFlightDataInAS : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void btn_SampleExcel_Click(object sender, EventArgs e)
    {
        var filePath = Server.MapPath(@"~/SprReports/AccountingSoftware/Sample/flight_sample.xlsx");
        Response.ContentType = ContentType;
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
        Response.WriteFile(filePath);
        Response.End();
    }

    protected void btnDeleteFlightUpload_Click(object sender, EventArgs e)
    {
        string path = "";
        if (fluFlightOrderIds.PostedFile != null)
        {
            try
            {
                if (!string.IsNullOrEmpty(fluFlightOrderIds.FileName))
                {
                    TrancateUploadFlightByExcelTable();
                    path = string.Concat(Server.MapPath("~/SprReports/AccountingSoftware/FlightExcel/" + fluFlightOrderIds.FileName));
                    fluFlightOrderIds.SaveAs(path);

                    string excelCS = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
                    using (OleDbConnection con = new OleDbConnection(excelCS))
                    {
                        OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                        con.Open();

                        DbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            int issuccess = ExecuteDeleteFlightData(dr[0].ToString());
                        }
                        BindFailedOrderId();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Flight data deleted successfully from accounting software.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please choose excel sheet.')", true);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
                lblmsg.Text = ex.Message + path;
            }
        }
    }

    private int ExecuteDeleteFlightData(string orderid)
    {
        int isSuccess = 0;

        try
        {
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("sp_DeleteBookingDetails_AccountingSw", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("OrderId", orderid);
            con.Open();
            isSuccess = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            InsertFailedFlightData(orderid.ToString());
            ex.ToString();
        }
        return isSuccess;
    }

    private void InsertFailedFlightData(string orderid)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            string sql = "insert into T_FailedFlightDetailByExcel (orderid) values ('" + orderid + "')";
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                //lblmsg.Text = "Record Inserted Succesfully into the Database";
                //lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
            }
            con.Close();
            con.Dispose();
        }
    }

    private void TrancateUploadFlightByExcelTable()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string sql = "truncate table T_FailedFlightDetailByExcel";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
            con.Dispose();
        }
    }

    private void BindFailedOrderId()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string query = "select * from T_FailedFlightDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            DataTable dtDetails = ds.Tables[0];
            if (dtDetails != null && dtDetails.Rows.Count > 0)
            {
                //dvFailedOrderId.DataSource = dtDetails;
                //dvFailedOrderId.DataBind();
                divFailedFlightOrderIdList.Visible = true;
            }
            else
            {
                divFailedFlightOrderIdList.Visible = false;
            }

            con.Close();
        }
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlTransactionDom STDom = new SqlTransactionDom();
            con.Open();
            string query = "select * from T_FailedFlightDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            DataTable dtDetails = ds.Tables[0];
            if (dtDetails != null && dtDetails.Rows.Count > 0)
            {
                DataTable tempDT = new DataTable("Order");
                tempDT.Columns.Add("OrderId", typeof(String));

                for (int i = 0; i < dtDetails.Rows.Count; i++)
                {
                    tempDT.Rows.Add(dtDetails.Rows[i]["orderid"].ToString());
                }

                if (tempDT != null && tempDT.Rows.Count > 0)
                {
                    DataSet newds = new DataSet();
                    newds.Tables.Add(tempDT);

                    STDom.ExportData(newds);
                }
            }

            con.Close();
        }
    }
}