﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_AccountingSoftware_UploadRailDataInAS : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlTransactionDom STDom = new SqlTransactionDom();

            con.Open();
            string query = "select * from T_FailedRailDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            DataTable dtDetails = ds.Tables[0];

            List<object> UniqueOrderId = new List<object>();

            DataTable tempDT = new DataTable("Order");
            tempDT.Columns.Add("OrderId", typeof(String));
            if (dtDetails != null && dtDetails.Rows.Count > 0)
            {
                for (int i = 0; i < dtDetails.Rows.Count; i++)
                {
                    if (!UniqueOrderId.Contains(dtDetails.Rows[i]["orderid"].ToString()))
                    {
                        tempDT.Rows.Add(dtDetails.Rows[i]["orderid"].ToString());
                    }
                }
            }

            if (Session["type"].ToString() == "multiple")
            {
                DataTable multipleInsert = BindStatusZeroRailOrderId();
                if (multipleInsert != null && multipleInsert.Rows.Count > 0)
                {
                    for (int i = 0; i < multipleInsert.Rows.Count; i++)
                    {
                        if (!UniqueOrderId.Contains(multipleInsert.Rows[i]["orderid"].ToString()))
                        {
                            tempDT.Rows.Add(multipleInsert.Rows[i]["orderid"].ToString());
                        }
                    }
                }
            }

            if (tempDT != null && tempDT.Rows.Count > 0)
            {
                DataSet newds = new DataSet();
                newds.Tables.Add(tempDT);

                STDom.ExportData(newds);
            }

            con.Close();
        }

        //using (SqlConnection con = new SqlConnection(CS))
        //{
        //    SqlTransactionDom STDom = new SqlTransactionDom();
        //    con.Open();
        //    string query = "select * from T_FailedRailDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
        //    SqlCommand cmd = new SqlCommand(query, con);
        //    SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    if (ds != null && ds.Tables.Count > 0)
        //    {
        //        STDom.ExportData(ds);
        //    }

        //    con.Close();
        //}
    }

    protected void btn_SampleExcel_Click(object sender, EventArgs e)
    {
        var filePath = Server.MapPath(@"~/SprReports/AccountingSoftware/Sample/rail_sample.xlsx");
        Response.ContentType = ContentType;
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
        Response.WriteFile(filePath);
        Response.End();
    }

    protected void btnRailUpload_Click(object sender, EventArgs e)
    {
        string path = "";
        if (fluRailOrderIds.PostedFile != null)
        {
            try
            {
                if (!string.IsNullOrEmpty(fluRailOrderIds.FileName))
                {
                    Session["type"] = null;
                    TrancateFailedRailAutoTable();
                    path = string.Concat(Server.MapPath("~/SprReports/AccountingSoftware/RailExcel/" + fluRailOrderIds.FileName));
                    fluRailOrderIds.SaveAs(path);
                    string excelCS = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
                    using (OleDbConnection con = new OleDbConnection(excelCS))
                    {
                        OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                        con.Open();

                        DbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            int issuccess = ExecuteRailStoreProc(dr[0].ToString());
                        }
                        BindFailedRailOrderId("one");
                        Session["type"] = "one";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Rail data uploaded successfully into accounting software.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please choose rail order ids excel sheet.')", true);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
                lblmsg.Text = ex.Message + path;
            }
        }
    }

    private void BindFailedRailOrderId(string type)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            Session["type"] = type;
            con.Open();
            string query = "select * from T_FailedRailDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            DataTable dtDetails = ds.Tables[0];

            List<object> UniqueOrderId = new List<object>();

            DataTable tempDT = new DataTable("Order");
            tempDT.Columns.Add("OrderId", typeof(String));
            if (dtDetails != null && dtDetails.Rows.Count > 0)
            {
                for (int i = 0; i < dtDetails.Rows.Count; i++)
                {
                    if (!UniqueOrderId.Contains(dtDetails.Rows[i]["orderid"].ToString()))
                    {
                        tempDT.Rows.Add(dtDetails.Rows[i]["orderid"].ToString());
                    }
                }
            }

            if (type == "multiple")
            {
                DataTable multipleInsert = BindStatusZeroRailOrderId();
                if (multipleInsert != null && multipleInsert.Rows.Count > 0)
                {
                    for (int i = 0; i < multipleInsert.Rows.Count; i++)
                    {
                        if (!UniqueOrderId.Contains(multipleInsert.Rows[i]["orderid"].ToString()))
                        {
                            tempDT.Rows.Add(multipleInsert.Rows[i]["orderid"].ToString());
                        }
                    }
                }
            }

            if (tempDT != null && tempDT.Rows.Count > 0)
            {
                //dvFailedOrderId.DataSource = tempDT;
                //dvFailedOrderId.DataBind();
                divFailedRailOrderIdList.Visible = true;
            }
            else
            {
                divFailedRailOrderIdList.Visible = false;
            }

            con.Close();
        }
    }

    private DataTable BindStatusZeroRailOrderId()
    {
        DataTable dtDetails = new DataTable();
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string query = "select orderid from T_RailAccountOrderIdQue where status=0"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            dtDetails = ds.Tables[0];
            con.Close();
        }
        return dtDetails;
    }

    private void TrancateUploadRailAutoTable()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string sql = "truncate table T_RailAccountOrderIdQue";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
            con.Dispose();
        }
    }

    private void TrancateFailedRailAutoTable()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string sql = "truncate table T_FailedRailDetailByExcel";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
            con.Dispose();
        }
    }

    private int ExecuteRailStoreProc(string orderid)
    {
        int isSuccess = 0;

        try
        {
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("sp_InsertBookingDetails_Rail_TblSalesHead", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("OrderId", orderid);
            con.Open();
            isSuccess = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            InsertFailedRailData(orderid.ToString());
            ex.ToString();
        }
        return isSuccess;
    }

    private void InsertFailedRailData(string orderid)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            string sql = "insert into T_FailedRailDetailByExcel (orderid) values ('" + orderid + "')";
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                //lblmsg.Text = "Record Inserted Succesfully into the Database";
                //lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
            }
            con.Close();
            con.Dispose();
        }
    }

    protected void btnUploadAuto_Click(object sender, EventArgs e)
    {
        int isSuccess = 0;

        try
        {
            Session["type"] = "multiple";
            TrancateFailedRailAutoTable();
            TrancateUploadRailAutoTable();
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("sp_UploadAutoRailDataIntoAccountingSW", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            isSuccess = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        BindFailedRailOrderId("multiple");
    }
}