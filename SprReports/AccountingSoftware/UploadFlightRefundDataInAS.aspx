﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="UploadFlightRefundDataInAS.aspx.cs" Inherits="SprReports_AccountingSoftware_UploadFlightDataInAS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Accounting Software > Upload Flight Refund Data By RefundID Id</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Import Excel</h4>
                            </div>
                            <div class="col-sm-6">
                                <asp:Button ID="btn_SampleExcel" Style="float: right;" runat="server" Text="Download Excel format" OnClick="btn_SampleExcel_Click" />
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="col-sm-6" style="background: #f6f6f6; padding: 5px;">
                                <asp:FileUpload ID="fluFlightOrderIds" CssClass="custom-file-input" runat="server" />
                            </div>
                            <div class="col-sm-6">
                                <asp:Button ID="btnFlightUpload" runat="server" CssClass="btn btn-outline-primary uploadclass" Text="Upload" OnClick="btnFlightUpload_Click"  OnClientClick="return ConfirmMsg();" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <div class="input-group">
                                <h4>
                                    <asp:Label runat="server" ID="lblmsg" Style="color: red;"></asp:Label></h4>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group" id="divFailedFlightOrderIdList" runat="server" visible="false">
                            <h5 style="color: red;">There is some problem to upload data in some order id, Please do manually or download sheet and upload again
                                <asp:Button ID="btn_export" runat="server" Text="Download Failed Order Id Sheet" OnClick="btn_export_Click" />
                            </h5>

                            <br />
                            <%--  <asp:GridView ID="dvFailedOrderId" HeaderStyle-CssClass="bg-primary text-white" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" Style="width:300px;">
                                <EmptyDataTemplate>
                                    <div class="text-center">No record found</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField HeaderText="OrderId" DataField="orderid" />
                                </Columns>
                            </asp:GridView>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function ConfirmMsg() {
            var fileupllength = $('#<%=fluFlightOrderIds.ClientID%>').get(0).files.length;
            if (fileupllength > 0) {
                if (confirm("Are you sure, you want to upload all data regarding attached order id ?")) {
                    $(".uploadclass").val("Processing...");
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert("Please choose flight order ids excel sheet.");
                 return false;
            }
         }
    </script>
</asp:Content>

