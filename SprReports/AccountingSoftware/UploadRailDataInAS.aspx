﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="UploadRailDataInAS.aspx.cs" Inherits="SprReports_AccountingSoftware_UploadRailDataInAS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Accounting Software > Upload Rail Data</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Import Excel</h4>
                            </div>
                            <div class="col-sm-6">
                                <asp:Button ID="btn_SampleExcel" Style="float: right;" runat="server" Text="Download Excel format" OnClick="btn_SampleExcel_Click" />
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="col-sm-6" style="background: #f6f6f6; padding: 5px;">
                                <asp:FileUpload ID="fluRailOrderIds" CssClass="custom-file-input" runat="server" />
                            </div>
                            <div class="col-sm-6">
                                <asp:Button ID="btnRailUpload" runat="server" CssClass="btn btn-outline-primary uploadclass" Text="Upload" OnClick="btnRailUpload_Click" OnClientClick="return ConfirmMsg();" />
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="input-group">
                                <h4>
                                    <asp:Label runat="server" ID="lblmsg" Style="color: red;"></asp:Label>
                                </h4>
                            </div>
                        </div>

                        <div class="form-group" id="divFailedRailOrderIdList" runat="server" visible="false">
                            <br />
                            <br />
                            <h5 style="color: red;">There is some problem to upload data in some order id, Please do manually or download sheet and upload again
                                <asp:Button ID="btn_export" runat="server" Text="Download Failed Order Id Sheet" OnClick="btn_export_Click" />
                            </h5>
                            <br />
                            <%--<asp:GridView ID="dvFailedOrderId" HeaderStyle-CssClass="bg-primary text-white" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" Style="width: 300px;">
                                <EmptyDataTemplate>
                                    <div class="text-center">No record found</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField HeaderText="OrderId" DataField="orderid" />
                                </Columns>
                            </asp:GridView>--%>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 style="text-align: center; background: #ccc6;">OR</h3>
                                <br />
                            </div>
                            <div class="">
                                <div class="col-sm-6">
                                    <h4>Auto upload pending rail data upto date</h4>
                                </div>
                                <div class="col-sm-6">
                                    <asp:Button ID="btnUploadAuto" runat="server" CssClass="btn btn-outline-primary autoupload" Text="Click Here To Auto Upload Pending Data" OnClick="btnUploadAuto_Click" OnClientClick="return AutoUpload();" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function ConfirmMsg() {
            var fileupllength = $('#<%=fluRailOrderIds.ClientID%>').get(0).files.length;
            if (fileupllength > 0) {
                if (confirm("Are you sure, you want to upload all data regarding attached order id ?")) {
                    $(".uploadclass").val("Processing...");
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert("Please choose rail order ids excel sheet.");
                return false;
            }
        }

        function AutoUpload() {
            if (confirm("Are you sure, you want to upload all data in one time ?")) {
                $(".autoupload").val("Processing, Please wait...");
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>

