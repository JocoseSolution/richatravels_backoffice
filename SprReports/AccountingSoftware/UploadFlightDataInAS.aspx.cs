﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_AccountingSoftware_UploadFlightDataInAS : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void btnFlightUpload_Click(object sender, EventArgs e)
    {
        string path = "";
        if (fluFlightOrderIds.PostedFile != null)
        {
            try
            {
                if (!string.IsNullOrEmpty(fluFlightOrderIds.FileName))
                {
                    TrancateUploadFlightByExcelTable();
                    path = string.Concat(Server.MapPath("~/SprReports/AccountingSoftware/FlightExcel/" + fluFlightOrderIds.FileName));
                    fluFlightOrderIds.SaveAs(path);

                    string excelCS = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
                    using (OleDbConnection con = new OleDbConnection(excelCS))
                    {
                        OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                        con.Open();

                        DbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            string str = dr[0].ToString().Substring(0, 2);
                            if (str.Trim().ToLower() == "al" || str.Trim().ToLower() == "rt" || str.Trim().ToLower() == "rw")
                            {
                                int issuccess = ExecuteFlightStoreProc(dr[0].ToString(), "sp_InsertBookingDetails_TblSalesHead");
                            }
                            else
                            {
                                int issuccess = ExecuteFlightStoreProc(dr[0].ToString(), "sp_InsertBookingDetails_Offline_TblSalesHead");
                            }
                        }

                        //SqlBulkCopy bulkInsert = new SqlBulkCopy(CS);
                        //bulkInsert.DestinationTableName = "T_UploadFlightByExcel";
                        //bulkInsert.WriteToServer(dr);

                        BindFailedOrderId();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Flight data uploaded successfully into accounting software.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please choose flight order ids excel sheet.')", true);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
                lblmsg.Text = ex.Message + path;
            }
        }
    }

    private int ExecuteFlightStoreProc(string orderid, string spname)
    {
        int isSuccess = 0;

        try
        {
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand(spname, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("OrderId", orderid);
            con.Open();
            isSuccess = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            InsertFailedFlightData(orderid.ToString());
            ex.ToString();
        }
        return isSuccess;
    }

    //private string ExecuteFlightStoreProc()
    //{
    //    string msg = string.Empty;

    //    try
    //    {
    //        SqlConnection con = new SqlConnection(CS);
    //        con.Open();
    //        SqlCommand cmd = new SqlCommand("Sp_FlightExcelUploadDataInAccountingSW", con);
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        int i = cmd.ExecuteNonQuery();
    //        con.Close();
    //        if (i > 0)
    //        {
    //            msg = "Flight data uploaded successfully into accounting software.";
    //        }
    //        else
    //        {
    //            msg = "Flight data uploaded failed.";
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        msg = ex.Message;
    //    }
    //    BindFailedOrderId();
    //    return msg;
    //}

    private void BindFailedOrderId()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string query = "select * from T_FailedFlightDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            DataTable dtDetails = ds.Tables[0];
            if (dtDetails != null && dtDetails.Rows.Count > 0)
            {
                DataTable tempDT = new DataTable("Order");
                tempDT.Columns.Add("OrderId", typeof(String));
                for (int i = 0; i < dtDetails.Rows.Count; i++)
                {
                    tempDT.Rows.Add(dtDetails.Rows[i]["orderid"].ToString());
                }

                //dvFailedOrderId.DataSource = dtDetails;
                //dvFailedOrderId.DataBind();
                divFailedFlightOrderIdList.Visible = true;
            }
            else
            {
                divFailedFlightOrderIdList.Visible = false;
            }

            con.Close();
        }
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlTransactionDom STDom = new SqlTransactionDom();
            con.Open();
            string query = "select * from T_FailedFlightDetailByExcel"; //where orderid not in (select strBookingReferenceNo from [PortalIntegration_Test].[dbo].[tblSalesHead])";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dtDetails = ds.Tables[0];

                DataTable tempDT = new DataTable("Order");
                tempDT.Columns.Add("OrderId", typeof(String));
                for (int i = 0; i < dtDetails.Rows.Count; i++)
                {
                    tempDT.Rows.Add(dtDetails.Rows[i]["orderid"].ToString());
                }

                DataSet newds = new DataSet();
                newds.Tables.Add(tempDT);

                STDom.ExportData(newds);
            }

            con.Close();
        }
    }

    private void TrancateUploadFlightByExcelTable()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            con.Open();
            string sql = "truncate table T_FailedFlightDetailByExcel";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
            con.Dispose();
        }
    }

    private void InsertFailedFlightData(string orderid)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            string sql = "insert into T_FailedFlightDetailByExcel (orderid) values ('" + orderid + "')";
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                //lblmsg.Text = "Record Inserted Succesfully into the Database";
                //lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
            }
            con.Close();
            con.Dispose();
        }
    }

    protected void btn_SampleExcel_Click(object sender, EventArgs e)
    {
        var filePath = Server.MapPath(@"~/SprReports/AccountingSoftware/Sample/flight_sample.xlsx");
        Response.ContentType = ContentType;
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
        Response.WriteFile(filePath);
        Response.End();
    }
}