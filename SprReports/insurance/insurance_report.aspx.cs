﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_insurance_insurance_report : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            if (!IsPostBack)
            {
                BindInsuranceDetails();
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    private void BindInsuranceDetails()
    {
        DataSet dsDetails = FetchInsuranceDetails();
        lblTotalCount.Text = dsDetails.Tables[0].Rows.Count.ToString();
        gvInsuranceReport.DataSource = dsDetails.Tables[0];
        gvInsuranceReport.DataBind();
    }

    private DataSet FetchInsuranceDetails()
    {
        DataSet ds = new DataSet();
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            string fromdate = ""; string todate = ""; string agencyId = "";

            if (!string.IsNullOrEmpty(Request["From"]))
            {
                fromdate = String.Format(Request["From"].Split('-')[2], 4) + "-" + String.Format(Request["From"].Split('-')[1], 2) + "-" + String.Format(Request["From"].Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(Request["To"]))
            {
                todate = String.Format(Request["To"].Split('-')[2], 4) + "-" + String.Format(Request["To"].Split('-')[1], 2) + "-" + String.Format(Request["To"].Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(Request["hidtxtAgencyName"]))
            {
                agencyId = Request["hidtxtAgencyName"].ToString();
            }

            string enqid = txtEnquiryid.Text;
            string prosno = txtProposalNo.Text;
            string policyNo = txtPolicyNo.Text;
            string searchType = ddlActionType.SelectedValue;
            string policyType = ddlPolicyType.SelectedValue;

            string constr = ConfigurationManager.ConnectionStrings["myInsurance"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = new SqlCommand("sp_GetInsuanceDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromdate", fromdate);
            cmd.Parameters.AddWithValue("@todate", todate);
            cmd.Parameters.AddWithValue("@enquiryid", enqid);
            cmd.Parameters.AddWithValue("@proposalno", prosno);
            cmd.Parameters.AddWithValue("@policyno", policyNo);
            cmd.Parameters.AddWithValue("@agencyid", agencyId);
            cmd.Parameters.AddWithValue("@policytype", policyType);
            cmd.Parameters.AddWithValue("@actiontype", searchType);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandTimeout = 120;
            sda.SelectCommand = cmd;
            sda.Fill(ds);
            con.Close();
        }
        return ds;
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        SqlTransactionDom STDom = new SqlTransactionDom();
        STDom.ExportData(FetchInsuranceDetails());
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        BindInsuranceDetails();
    }
}