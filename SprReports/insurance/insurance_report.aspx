﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="insurance_report.aspx.cs" Inherits="SprReports_insurance_insurance_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>

    <script type="text/javascript">
        function fareRuleToolTip(id) {
            $.ajax({
                type: "POST",
                url: "QCTicketReport.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    debugger;
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .table {
            vertical-align: top;
            max-width: 100%;
            overflow-x: auto;
            white-space: nowrap;
            border-collapse: collapse;
            border-spacing: 0;
            width: 367px !important;
        }

        td {
            font-size: 11px !important;
        }

        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        #tooltip {
            z-index: 9999;
            position: absolute;
            top: 200px;
            float: right;
            padding: 5px;
            right: 280px;
            border: 2px solid #04034f;
            background-color: #fff;
            width: auto;
            min-width: 300px;
        }

        .table .table {
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .hovercolor {
            font-weight: bold;
            color: #004b91;
            font-size: 11px;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Insurance > Insurance Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>From</label>
                                <input type="text" name="From" id="From" placeholder="Select Date" class="form-control" readonly="readonly" />
                            </div>
                            <div class="col-md-3">
                                <label>To</label>
                                <input type="text" name="To" placeholder="Select Date" id="To" class="form-control" readonly="readonly" />
                            </div>
                            <div class="col-md-3">
                                <label>Enquiry Id</label>
                                <asp:TextBox ID="txtEnquiryid" placeholder="Enter Enquiryid" class="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Proposal No</label>
                                <asp:TextBox ID="txtProposalNo" placeholder="Enter Proposal No" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <label>Policy No</label>
                                <asp:TextBox ID="txtPolicyNo" placeholder="Enter Policy No" class="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Agency Name/ID</label>
                                <input type="text" class="form-control" id="txtAgencyName" placeholder="Enter Agency Name or ID" name="txtAgencyName" onfocus="focusObj(this);"
                                    onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                            </div>
                            <div class="col-md-3">
                                <label>Policy Type</label>
                                <asp:DropDownList class="form-control" ID="ddlPolicyType" runat="server">
                                    <asp:ListItem Value="">Select Type</asp:ListItem>
                                    <asp:ListItem Value="individual">Individual</asp:ListItem>
                                    <asp:ListItem Value="floater">Floater</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <label>Type</label>
                                <asp:DropDownList class="form-control" ID="ddlActionType" runat="server">
                                    <asp:ListItem Value="enquiry" Selected="True">Enquiry</asp:ListItem>
                                    <asp:ListItem Value="proposal">Proposal</asp:ListItem>
                                    <asp:ListItem Value="policy">Policy</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <br />
                                <label style="background: #000; color: #fff; border-radius: 4px; padding: 2px 9px;">
                                    Total Count :
                            <asp:Label ID="lblTotalCount" runat="server">0</asp:Label>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <br />
                                <asp:Button ID="btn_result" runat="server" class="btn btn-danger" Text="Search" OnClick="btn_result_Click" />
                                <asp:Button ID="btn_export" runat="server" class="btn btn-danger" Text="Export" OnClick="btn_export_Click" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvInsuranceReport" runat="server" AutoGenerateColumns="false" CssClass="table" Font-Size="12px" PageSize="30">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Enquiry Id">
                                                    <ItemTemplate>
                                                        <a href='insurance_detail.aspx?enquiryid=<%#Eval("Enquiry_Id")%> &TransID=' rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="Enquiry_Id" runat="server" Text='<%#Eval("Enquiry_Id")%>'></asp:Label></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agency Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Proposal Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProposal" runat="server" Text='<%#Eval("ProposerName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Product Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProduct" runat="server" Text='<%#Eval("ProductName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Policy Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPolicyType" runat="server" Text='<%#Eval("PolicyType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sum Insured">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSum_Insured" runat="server" Text='<%#Eval("SumInsured")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Premium">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPremium" runat="server" Text='<%#Eval("Premium")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Proposal Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProposalNumber" runat="server" Text='<%#Eval("ProposalNumber")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Policy Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPolicyNumber" runat="server" Text='<%#Eval("PolicyNumber")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Adult">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAdult" runat="server" Text='<%#Eval("AdultCount")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Child">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChild" runat="server" Text='<%#Eval("ChildCount")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pin Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPinCode" runat="server" Text='<%#Eval("Pin_Code")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

