﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="insurance_detail.aspx.cs" Inherits="SprReports_insurance_insurance_detail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <style>
        input[type="text"], input[type="password"], select, textarea {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <%=InsuranceDetail %>        
    </form>
</body>
</html>
