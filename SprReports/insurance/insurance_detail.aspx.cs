﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_insurance_insurance_detail : System.Web.UI.Page
{
    public string InsuranceDetail = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["enquiryid"] != null)
        {
            string enqid = Request.QueryString["enquiryid"].ToString();
            BindAllDetailsByEnqId(enqid);
        }
    }

    private void BindAllDetailsByEnqId(string enqid)
    {
        StringBuilder sbDetail = new StringBuilder();
        sbDetail.Append("<div align='center'>");
        sbDetail.Append("<table width='100%'>");
        sbDetail.Append("<tbody>");
        sbDetail.Append("<tr>");
        sbDetail.Append("<td align='right' style='font-weight: bold; font-size: 15px; color: #004b91;'>Enquiry Id : </td>");
        sbDetail.Append("<td id='tdRefNo' align='left' style='font-weight: bold; font-size: 15px;'>" + enqid + "</td>");
        sbDetail.Append("</tr>");
        sbDetail.Append("</tbody>");
        sbDetail.Append("</table>");


        DataSet ds = FetchInsuranceDetails(enqid);
        if (ds != null)
        {
            DataTable dtEnquiry = ds.Tables[0];
            DataTable dtProposal = ds.Tables[1];
            if (dtEnquiry != null && dtEnquiry.Rows.Count > 0)
            {
                sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                sbDetail.Append("<tbody>");
                sbDetail.Append("<tr><td align='left' style='font-weight: bold; font-size: 14px;'>Enquiry Details  </td></tr>");
                sbDetail.Append("<tr><td>");
                sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                sbDetail.Append("<tbody>");

                sbDetail.Append("<tr class='HeaderStyle'>");
                sbDetail.Append("<th scope='col'>AgencyName</th>");
                sbDetail.Append("<th scope='col'>Policy Type</th>");
                sbDetail.Append("<th scope='col'>Suminsured</th>");
                sbDetail.Append("<th scope='col'>Adult</th>");
                sbDetail.Append("<th scope='col'>Child</th>");
                sbDetail.Append("<th scope='col'>Pin Code</th>");
                sbDetail.Append("<th scope='col'>Created Date</th>");
                sbDetail.Append("</tr>");

                sbDetail.Append("<tr>");
                sbDetail.Append("<td><span>" + dtEnquiry.Rows[0]["AgencyName"].ToString() + " (" + dtEnquiry.Rows[0]["AgencyId"].ToString() + ")" + "</span></td>");
                sbDetail.Append("<td><span>" + dtEnquiry.Rows[0]["Policy_Type_Text"].ToString() + "</span></td>");
                if (dtProposal != null && dtProposal.Rows.Count > 0)
                {
                    sbDetail.Append("<td><span>" + dtProposal.Rows[0]["suminsured_amount"].ToString() + "</span></td>");
                }
                else
                {
                    sbDetail.Append("<td><span>" + dtEnquiry.Rows[0]["Sum_Insured"].ToString() + "</span></td>");
                }
                int adultcount = 0;
                if (Convert.ToBoolean(dtEnquiry.Rows[0]["Self"].ToString()) == true)
                {
                    adultcount = 1;
                }
                if (Convert.ToBoolean(dtEnquiry.Rows[0]["Spouse"].ToString()) == true)
                {
                    adultcount = 2;
                }

                int son = Convert.ToInt32(dtEnquiry.Rows[0]["Son2Age"].ToString()) > 0 ? 2 : (Convert.ToInt32(dtEnquiry.Rows[0]["Son1Age"].ToString()) > 0 ? 1 : 0);
                int daughter = Convert.ToInt32(dtEnquiry.Rows[0]["Daughter2Age"].ToString()) > 0 ? 2 : (Convert.ToInt32(dtEnquiry.Rows[0]["Daughter1Age"].ToString()) > 0 ? 1 : 0);

                sbDetail.Append("<td><span>" + adultcount + "</span></td>");
                sbDetail.Append("<td><span>" + (son + daughter) + "</span></td>");
                sbDetail.Append("<td><span>" + dtEnquiry.Rows[0]["Pin_Code"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtEnquiry.Rows[0]["CreatedDate"].ToString() + "</span></td>");
                sbDetail.Append("</tr>");

                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("</td></tr>");
                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("<div>&nbsp;</div>");
            }

            if (dtProposal != null && dtProposal.Rows.Count > 0)
            {
                sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                sbDetail.Append("<tbody>");
                sbDetail.Append("<tr><td align='left' style='font-weight: bold; font-size: 14px;'>Proposal Details  </td></tr>");
                sbDetail.Append("<tr><td>");
                sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                sbDetail.Append("<tbody>");

                sbDetail.Append("<tr class='HeaderStyle'>");
                sbDetail.Append("<th scope='col'>ProposerName</th>");
                sbDetail.Append("<th scope='col'>Mobile</th>");
                sbDetail.Append("<th scope='col'>DOB</th>");
                sbDetail.Append("<th scope='col'>Email</th>");
                sbDetail.Append("<th scope='col'>Address</th>");
                sbDetail.Append("<th scope='col'>State</th>");
                sbDetail.Append("<th scope='col'>City</th>");
                sbDetail.Append("<th scope='col'>PanNo</th>");
                sbDetail.Append("<th scope='col'>AadharNo</th>");
                sbDetail.Append("</tr>");

                sbDetail.Append("<tr>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_title"].ToString() + " " + dtProposal.Rows[0]["p_firstname"].ToString() + " " + dtProposal.Rows[0]["p_lastname"].ToString() + " (" + dtProposal.Rows[0]["p_gender"].ToString() + ")" + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_mobile"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_dob"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_email"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_address1"].ToString() + ", " + dtProposal.Rows[0]["p_address2"].ToString() + ", " + dtProposal.Rows[0]["p_landmark"].ToString() + ", " + dtProposal.Rows[0]["p_pincode"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_statename"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_cityname"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["p_pannumber"].ToString()) ? dtProposal.Rows[0]["p_pannumber"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["p_aadharno"].ToString()) ? dtProposal.Rows[0]["p_aadharno"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("</tr>");

                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("</td></tr>");
                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");

                sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                sbDetail.Append("<tbody>");
                sbDetail.Append("<tr style='display:none;'><td align='left' style='font-weight: bold; font-size: 14px;'>Proposal Details  </td></tr>");
                sbDetail.Append("<tr><td>");
                sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                sbDetail.Append("<tbody>");

                sbDetail.Append("<tr class='HeaderStyle'>");
                sbDetail.Append("<th scope='col'>Insurer</th>");
                sbDetail.Append("<th scope='col'>Product</th>");
                sbDetail.Append("<th scope='col'>Annual Income</th>");
                sbDetail.Append("<th scope='col'>Basic Premium</th>");
                sbDetail.Append("<th scope='col'>Service Tax</th>");
                sbDetail.Append("<th scope='col'>Discount(%)</th>");
                sbDetail.Append("<th scope='col'>Premium</th>");
                sbDetail.Append("</tr>");

                sbDetail.Append("<tr>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["row_insuranceCompany"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["row_policyName"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["p_annualincome"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["row_basePremium"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["row_serviceTax"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["row_discountPercent"].ToString() + "</span></td>");
                sbDetail.Append("<td><span>" + dtProposal.Rows[0]["row_totalPremium"].ToString() + "</span></td>");
                sbDetail.Append("</tr>");

                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("</td></tr>");
                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");

                sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                sbDetail.Append("<tbody>");
                sbDetail.Append("<tr style='display:none;'><td align='left' style='font-weight: bold; font-size: 14px;'>Proposal Details  </td></tr>");
                sbDetail.Append("<tr><td>");
                sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                sbDetail.Append("<tbody>");

                sbDetail.Append("<tr class='HeaderStyle'>");
                sbDetail.Append("<th scope='col'>Proposal Number</th>");
                sbDetail.Append("<th scope='col'>Policy Number</th>");
                sbDetail.Append("<th scope='col'>View Policy Document</th>");
                sbDetail.Append("<th scope='col'>Policy Start Date</th>");
                sbDetail.Append("<th scope='col'>Policy End Date</th>");
                //sbDetail.Append("<th scope='col'>Payment Link</th>");
                sbDetail.Append("</tr>");

                sbDetail.Append("<tr>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["proposalNumber"].ToString()) ? dtProposal.Rows[0]["proposalNumber"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["policyNumber"].ToString()) ? dtProposal.Rows[0]["policyNumber"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["policypdflink"].ToString()) ? "<a href=" + dtProposal.Rows[0]["policypdflink"].ToString() + " target='_blank'>View Policy</a>" : "- - -") + "</span></td>");
                //sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["paymenturl"].ToString()) ? dtProposal.Rows[0]["paymenturl"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["starton"].ToString()) ? dtProposal.Rows[0]["starton"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["endon"].ToString()) ? dtProposal.Rows[0]["endon"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("</tr>");

                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("</td></tr>");
                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("<div>&nbsp;</div>");

                DataTable dtAddon = ds.Tables[3];
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                    sbDetail.Append("<tbody>");
                    sbDetail.Append("<tr><td align='left' style='font-weight: bold; font-size: 14px;'>Addon Details  </td></tr>");
                    sbDetail.Append("<tr><td>");
                    sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                    sbDetail.Append("<tbody>");

                    sbDetail.Append("<tr class='HeaderStyle'>");
                    sbDetail.Append("<th scope='col'>AddOn Name</th>");
                    sbDetail.Append("<th scope='col'>Code</th>");
                    sbDetail.Append("<th scope='col'></th>");
                    sbDetail.Append("</tr>");

                    for (int i = 0; i < dtAddon.Rows.Count; i++)
                    {
                        sbDetail.Append("<tr>");
                        sbDetail.Append("<td><span>" + dtAddon.Rows[i]["addOns"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + dtAddon.Rows[i]["code"].ToString() + " </span></td>");
                        sbDetail.Append("<td><span style='color:green;'>Selected</span></td>");
                        sbDetail.Append("</tr>");
                    }

                    sbDetail.Append("</tbody>");
                    sbDetail.Append("</table>");
                    sbDetail.Append("</td></tr>");
                    sbDetail.Append("</tbody>");
                    sbDetail.Append("</table>");
                    sbDetail.Append("<div>&nbsp;</div>");
                }

                DataTable dtInsured = ds.Tables[2];
                if (dtInsured != null && dtInsured.Rows.Count > 0)
                {
                    sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                    sbDetail.Append("<tbody>");
                    sbDetail.Append("<tr><td align='left' style='font-weight: bold; font-size: 14px;'>Insured Details  </td></tr>");
                    sbDetail.Append("<tr><td>");
                    sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                    sbDetail.Append("<tbody>");

                    sbDetail.Append("<tr class='HeaderStyle'>");
                    sbDetail.Append("<th scope='col'>Relation</th>");
                    sbDetail.Append("<th scope='col'>Name</th>");
                    sbDetail.Append("<th scope='col'>Occupation</th>");
                    sbDetail.Append("<th scope='col'>DOB</th>");
                    sbDetail.Append("<th scope='col'>Height</th>");
                    sbDetail.Append("<th scope='col'>Weight</th>");
                    sbDetail.Append("<th scope='col'>Illness</th>");
                    sbDetail.Append("<th scope='col'>EngageManualLabour</th>");
                    sbDetail.Append("<th scope='col'>EngageWinterSport</th>");
                    sbDetail.Append("</tr>");

                    for (int i = 0; i < dtInsured.Rows.Count; i++)
                    {
                        sbDetail.Append("<tr>");
                        sbDetail.Append("<td><span>" + dtInsured.Rows[i]["relationname"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + dtInsured.Rows[i]["title"].ToString() + " " + dtInsured.Rows[i]["firstname"].ToString() + " " + dtInsured.Rows[i]["lastname"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + dtInsured.Rows[i]["occupation"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + dtInsured.Rows[i]["dob"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + dtInsured.Rows[i]["height"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + dtInsured.Rows[i]["weight"].ToString() + "</span></td>");
                        sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtInsured.Rows[i]["illnessdesc"].ToString()) ? dtInsured.Rows[i]["illnessdesc"].ToString() : "- - -") + "</span></td>");
                        sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtInsured.Rows[i]["engageManualLabourDesc"].ToString()) ? dtInsured.Rows[i]["engageManualLabourDesc"].ToString() : "- - -") + "</span></td>");
                        sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtInsured.Rows[i]["engageWinterSportDesc"].ToString()) ? dtInsured.Rows[i]["engageWinterSportDesc"].ToString() : "- - -") + "</span></td>");
                        sbDetail.Append("</tr>");
                    }

                    sbDetail.Append("</tbody>");
                    sbDetail.Append("</table>");
                    sbDetail.Append("</td></tr>");
                    sbDetail.Append("</tbody>");
                    sbDetail.Append("</table>");
                    sbDetail.Append("<div>&nbsp;</div>");
                }

                sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                sbDetail.Append("<tbody>");
                sbDetail.Append("<tr><td align='left' style='font-weight: bold; font-size: 14px;'>Nominee Details  </td></tr>");
                sbDetail.Append("<tr><td>");
                sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                sbDetail.Append("<tbody>");

                sbDetail.Append("<tr class='HeaderStyle'>");
                sbDetail.Append("<th scope='col'>Name</th>");
                sbDetail.Append("<th scope='col'>DOB</th>");
                sbDetail.Append("<th scope='col'>Age</th>");
                sbDetail.Append("<th scope='col'>Relation</th>");
                sbDetail.Append("</tr>");

                sbDetail.Append("<tr>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["n_title"].ToString()) ? (dtProposal.Rows[0]["n_title"].ToString() + " " + dtProposal.Rows[0]["n_firstname"].ToString() + " " + dtProposal.Rows[0]["n_lastname"].ToString()) : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["n_dob"].ToString()) ? dtProposal.Rows[0]["n_dob"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["n_age"].ToString()) ? dtProposal.Rows[0]["n_age"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtProposal.Rows[0]["n_relation"].ToString()) ? dtProposal.Rows[0]["n_relation"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("</tr>");

                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("</td></tr>");
                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("<div>&nbsp;</div>");
            }

            DataTable dtPayment = ds.Tables[4];
            if (dtPayment != null && dtPayment.Rows.Count > 0)
            {
                sbDetail.Append("<table width='100%' class='tbltbl' border='0' cellpadding='10' cellspacing='10'>");
                sbDetail.Append("<tbody>");
                sbDetail.Append("<tr><td align='left' style='font-weight: bold; font-size: 14px;'>Payment Details  </td></tr>");
                sbDetail.Append("<tr><td>");
                sbDetail.Append("<table class='GridViewStyle' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>");
                sbDetail.Append("<tbody>");

                sbDetail.Append("<tr class='HeaderStyle'>");
                sbDetail.Append("<th scope='col'>Transaction RefNum</th>");
                sbDetail.Append("<th scope='col'>UwDecision</th>");
                sbDetail.Append("<th scope='col'>Status</th>");
                sbDetail.Append("<th scope='col'>Payment Date</th>");
                sbDetail.Append("</tr>");

                string paymentstatus = dtPayment.Rows[0]["paymentstatus"].ToString();

                sbDetail.Append("<tr>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtPayment.Rows[0]["cc_transactionRefNum"].ToString()) ? dtPayment.Rows[0]["cc_transactionRefNum"].ToString() : "- - -") + "</span></td>");
                sbDetail.Append("<td><span>" + (!string.IsNullOrEmpty(dtPayment.Rows[0]["cc_uwDecision"].ToString()) ? dtPayment.Rows[0]["cc_uwDecision"].ToString() : "- - -") + " </span></td>");
                if (paymentstatus == "True")
                {
                    sbDetail.Append("<td><span>" + dtPayment.Rows[0]["status"].ToString() + " </span></td>");
                    sbDetail.Append("<td><span>" + dtPayment.Rows[0]["paymentdate"].ToString() + " </span></td>");
                }
                else
                {
                    sbDetail.Append("<td><span style='color:red;'>Payment Not Done Yet </span></td>");
                    sbDetail.Append("<td><span>- - -</span></td>");
                }
                
                sbDetail.Append("</tr>");

                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
                sbDetail.Append("</td></tr>");
                sbDetail.Append("</tbody>");
                sbDetail.Append("</table>");
            }
        }
        sbDetail.Append("</div>");
        InsuranceDetail = sbDetail.ToString();
    }

    private DataSet FetchInsuranceDetails(string enqid)
    {
        DataSet ds = new DataSet();

        if (!string.IsNullOrEmpty(enqid))
        {
            string constr = ConfigurationManager.ConnectionStrings["myInsurance"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = new SqlCommand("sp_GetInsuanceDetailsByEnqId");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@enquiryid", enqid);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandTimeout = 120;
            sda.SelectCommand = cmd;
            sda.Fill(ds);
            con.Close();
        }
        return ds;
    }
}