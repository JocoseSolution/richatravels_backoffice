﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_SetCreditLimit : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    SqlTransactionNew objSql = new SqlTransactionNew();
    SMSAPI.SMS objSMSAPI = new SMSAPI.SMS();
    string msgout = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        lblDistrId.Text = "";

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivDetails.Visible = false;
        BalDetails.Visible = false;
        DivAgencyDetals.Visible = false;

        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        //if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        //{
        //}
        //else
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            //string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string AgentLimit = TxtAgentCredit.Text;
            int flag = 0;
            #region Validation

            //string AgencyId = "";
            //string UserId = "";
            //if (!string.IsNullOrEmpty(TxtAgencyName))
            //{
            //    if (string.IsNullOrEmpty(HiddenAgentId))
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //        return;
            //    }                 
            //    if (TxtAgencyName.Split('-').Length > 1)
            //    {
            //        if(TxtAgencyName.Split('-')[1].Split('(').Length>1)
            //        {
            //           AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
            //           UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
            //            if(UserId !=HiddenAgentId)
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //                return;
            //            }                        
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //        return;
            //    }

            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!');", true);
            //    return;
            //}
            //if (string.IsNullOrEmpty(AgentLimit))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter credit limit amount!');", true);
            //    return;
            //}

            //if (string.IsNullOrEmpty(TxtRemark.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter remark!');", true);
            //    return;
            //}
            #endregion validation

            #region Update Limit
            if (!string.IsNullOrEmpty(hdnUserId.Value) && !string.IsNullOrEmpty(AgentLimit) && !string.IsNullOrEmpty(hdnAgencyId.Value))
            {
                string LimitExpiryDate = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.Form["ToDate"])))
                {
                    string ExpiryDate = Convert.ToString(Request.Form["ToDate"]);
                    DateTime temp = DateTime.ParseExact(ExpiryDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    LimitExpiryDate = string.Format("{0:yyyy-MM-dd}", temp);
                }

                //if(UserId ==HiddenAgentId)
                //{
                msgout = "";
                flag = UpdateRecords(hdnUserId.Value, hdnAgencyId.Value, AgentLimit, "CREDIT", TxtRemark.Text, LimitExpiryDate);
                if (flag > 1)
                {

                    try
                    {
                        string smsStatus = "";
                        string smsMsg = "";


                        DataTable dtpnr = new DataTable();
                        DataTable SmsCrd = new DataTable();
                        SqlTransaction objDA = new SqlTransaction();
                        SmsCrd = objDA.SmsCredential(SMS.AIRBOOKINGDOM.ToString()).Tables[0];

                        if (SmsCrd.Rows.Count > 0 && Convert.ToBoolean(SmsCrd.Rows[0]["Status"]) == true)
                        {
                            DataSet AgencyDs_new = objDA.GetAgencyDetails(hdnUserId.Value);
                            smsStatus = objSMSAPI.Credit_Limit_SET(TxtRemark.Text, ref smsMsg, hdnUserId.Value, AgencyDs_new.Tables[0].Rows[0]["Mobile"].ToString(), AgentLimit, SmsCrd, "CREDIT");
                            // smsStatus = objSMSAPI.Credit_Limit_SET(TxtRemark.Text, hdnUserId.Value, AgencyDs_new.Tables[0].Rows[0]["Mobile"].ToString(), AgentLimit, SmsCrd, "CREDIT");
                            //smsStatus = objSMSAPI.WalletTopup(orderID, TxtAmout.Text, Session["UID"], AgencyDs_new.Tables[0].Rows[0]["Mobile"], smsMsg, SmsCrd);
                            objSql.SmsLogDetails(hdnUserId.Value, AgencyDs_new.Tables[0].Rows[0]["Mobile"].ToString(), smsMsg, smsStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                    }


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + msgout + "');", true);
                }
                else
                {
                    if (!string.IsNullOrEmpty(msgout))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + msgout + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please try again');", true);
                    }

                }

                BindGrid(hdnUserId.Value);
                //TxtAgentCredit.Text = "";
                TxtRemark.Text = "";

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                //    return;
                //}

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id and credit amount !!');", true);
                return;
            }
            #endregion
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SetCreditLimit.aspx'; ", true);
            return;
        }

    }

    public void BindGrid(string AgentId)
    {
        try
        {
            SqlTransaction ST = new SqlTransaction();
            DataSet ds = ST.GetAgencyDetails(AgentId);
            #region Set value in text box
            double CashLimit = 0;
            double AgentCreditLimit = 0;
            double DueAmt = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                BalDetails.Visible = true;
                DivDetails.Visible = true;
                DivAgencyDetals.Visible = true;
                ds.Tables[0].Rows[0]["Title"].ToString();
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"])))
                {
                    CashLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["Crd_Limit"]);
                    //(ds.Tables[0].Rows[0]["Crd_Limit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"])))
                {
                    AgentCreditLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["AgentLimit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"])))
                {
                    DueAmt = Convert.ToDouble(ds.Tables[0].Rows[0]["DueAmount"]);
                }
                if (CashLimit > AgentCreditLimit)
                {
                    CashLimit = CashLimit - AgentCreditLimit;
                }
                else
                {
                    CashLimit = 0;
                }
                TxtAgentCredit.Text = Convert.ToString(AgentCreditLimit);
                TxtAvalBal.Text = Convert.ToString(CashLimit);
                TxtDueAmount.Text = Convert.ToString(DueAmt).Replace('-', ' ').Trim(); //Convert.ToString(DueAmt).Remove('-');
                lblAgentDetails.Text = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]) + "(" + Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]) + ")";
                hdnAgencyId.Value = Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                hdnUserId.Value = Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]);
                lblsales.Text = Convert.ToString(ds.Tables[0].Rows[0]["SalesExecID"]);

                #region Agency Table tr Bind

                tdTotalBAl.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"]);
                tdAgentId.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["user_id"]) + "/" + Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                tdMobile.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);

                tdAgentCredit.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"]);
                tdAgnetName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Title"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Fname"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Lname"]);
                tdEmail.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                tdDueAmount.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"]);
                tdAgencyName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]);
                tdAddress.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["LimitExpiryDate"]).Trim()))
                {
                    //string ExpiryDate = Convert.ToString(Request.Form["To"]);
                    //AgentCreditLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["LimitExpiryDate"]);

                    //Request.Form["To"].Value=
                    //<input type="text" name="date" value="<%= tdate %>" 

                    //    divTitle.Controls.Clear();
                    //divTitle.InnerHtml = "<input type='text' class='input_Text' id='Title'  name='Title' value='" + ds(0)("Title").ToString() + "' />";

                    spanExpiryDate.Controls.Clear();
                    //spanExpiryDate.InnerHtml = "<input type='text' name='To' id='To' readonly='readonly' class='form-control' value='" + Convert.ToString(ds.Tables[0].Rows[0]["LimitExpiryDate"]).Trim() + "' />";
                    spanExpiryDate.InnerHtml = "<input type='text' name='ToDate' id='ToDate' readonly='readonly' class='form-control' value='" + Convert.ToString(ds.Tables[0].Rows[0]["LimitExpiryDate"]).Trim() + "' />";
                }
                #endregion


                if (Convert.ToString(ds.Tables[0].Rows[0]["Distr"]).ToUpper() == "HEADOFFICE")
                {
                    BtnSubmit.Visible = true;
                    //lblDistrId.Text = "Stockist Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                }
                else
                {
                    BtnSubmit.Visible = false;
                    lblDistrId.Text = "Stockist Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Set credit limit not allowed,because agent of stockist');", true);
                    return;
                }

            }
            else
            {
                BalDetails.Visible = false;
                DivDetails.Visible = false;
                DivAgencyDetals.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record not found.');", true);
                return;
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    private int UpdateRecords(string UserId, string AgencyId, string AgentCredit, string ActionType, string Remark, string LimitExpiryDate)
    {
        int flag = 0;
        try
        {

            string RandomNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
            string InvoiceNo = "CL" + RandomNo.Substring(7, 13);
            string ipaddress;
            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipaddress == "" || ipaddress == null)
                ipaddress = Request.ServerVariables["REMOTE_ADDR"];

            //SP_INSERTUPLOADDETAILS_TRANSACTION
            SqlCommand cmd = new SqlCommand("SP_SET_AGENTCREDITLIMIT", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId);
            cmd.Parameters.AddWithValue("@AgentCredit", Convert.ToDouble(AgentCredit));
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UID"]));
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.AddWithValue("@IPAddress", ipaddress);
            cmd.Parameters.AddWithValue("@Remark", Remark);
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
            cmd.Parameters.AddWithValue("@LimitExpiryDate", LimitExpiryDate);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string AgentLimit = TxtAgentCredit.Text;
            #region Validation

            string AgencyId = "";
            string UserId = "";
            if (!string.IsNullOrEmpty(TxtAgencyName))
            {
                if (string.IsNullOrEmpty(HiddenAgentId))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
                if (TxtAgencyName.Split('-').Length > 1)
                {
                    if (TxtAgencyName.Split('-')[1].Split('(').Length > 1)
                    {
                        AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
                        UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
                        if (UserId != HiddenAgentId)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!!');", true);
                return;
            }

            #endregion validation

            if (!string.IsNullOrEmpty(HiddenAgentId))
            {
                #region Bind Grid
                //string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                BindGrid(HiddenAgentId);
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id and credit amount !!');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SetCreditLimit.aspx'; ", true);
            return;
        }
    }



}