﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ServiceCredentials.aspx.cs" Inherits="SprReports_Admin_ServiceCredentials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="../../newcss/sb-admin.css" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 35px;
            width:100%;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Search Credential</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip Type</label>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" onclick="ShowHide();" TabIndex="1">
                                        <asp:ListItem Value="D" Text="Domestic" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Provider</label>
                                    <asp:DropDownList ID="DdlProvider" runat="server" CssClass="form-control" onclick="ShowHide();">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>                                        
                                        <%--<asp:ListItem Value="1G" Text="GAL DOM"></asp:ListItem>
                                        <asp:ListItem Value="1GINT" Text="GAL INT"></asp:ListItem>--%>
                                        <asp:ListItem Value="6E" Text="Indigo"></asp:ListItem>
                                        <asp:ListItem Value="SG" Text="Spicejet"></asp:ListItem>                                       
                                        <asp:ListItem Value="G8" Text="GoAir"></asp:ListItem>                                         
                                    </asp:DropDownList>
                                </div>
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group" id="">
                                    <label for="exampleInputPassword1">Airline :</label>                                   
                                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="4" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                </div>
                            </div>
                            <%--<div class="col-md-3" id="divCorporateID" style="display:none;">--%>
                            <div class="col-md-3" id="divCorporateID">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">CorporateID :</label>
                                    <asp:TextBox ID="TxtCorporateID" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            </div>

                       

                        <div class="row">                            
                            

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">User ID:</label>
                                    <asp:TextBox ID="TxtUserID" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password :</label>
                                    <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>

                              <div class="col-md-3" id="divCarrierPCC" style="display:none;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Carrier Acc (PCC):</label>
                                    <asp:TextBox ID="TxtCarrierAcc" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>                      

                            <div class="col-md-3" id="divResultFrom" style="display:none;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Result From:</label>                                   
                                     <asp:DropDownList ID="DdlResultFrom" runat="server" CssClass="form-control">                                                                             
                                        <asp:ListItem Value="AP" Text="API" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="SP" Text="Scraping"></asp:ListItem>                                        
                                    </asp:DropDownList>                                   
                                </div>
                            </div>

                            <div class="col-md-3" id="divFareType:">                           
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Fare Type:</label>                                    
                                     <asp:DropDownList ID="DdlCrdType" runat="server" CssClass="form-control">                                        
                                        <asp:ListItem Value="NRM" Text="Normal Fare" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <asp:ListItem Value="CPN" Text="Coupon Fare"></asp:ListItem>
                                    </asp:DropDownList>                                  
                                </div>
                            </div>
                        </div>


                        <div class="row" style="display:none;" id="DivRow">
                           <%-- <div class="col-md-3" style="display:none;" id="DivLoginID">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">LoginID :</label>
                                    <asp:TextBox ID="TxtLoginID" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" style="display:none;" id="DivLoginPwd">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Login Password:</label>
                                    <asp:TextBox ID="TxtLoginPwd" CssClass="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                </div>
                            </div> --%>                           
                            <div class="col-md-3" style="display:none;" id="DivServerUrlOrIP">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Server Url Or IP:</label>
                                    <asp:TextBox ID="TxtServerUrlOrIP" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3" style="display:none;" id="DivBkgUrlOrIP">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Bkg Server Url Or IP :</label>
                                    <asp:TextBox ID="TxtBkgServerUrlOrIP" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                </div>
                            </div>



                        </div>
                        <div class="row"> 
                             <div class="col-md-3" style="display:none;" id="DivLoginID">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">LoginID :</label>
                                    <asp:TextBox ID="TxtLoginID" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" style="display:none;" id="DivLoginPwd">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Login Password:</label>
                                    <asp:TextBox ID="TxtLoginPwd" CssClass="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>                           

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>                          
                        </div>
                        
                        <div class="row">                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click" /> --%>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" OnClientClick="return Check();" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="overflow:auto;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow:auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowDeleting="OnRowDeleting" OnRowDataBound="OnRowDataBound" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                                <%--<asp:TemplateField HeaderText="Update">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                        <a href='UpdateServiceCredentials.aspx?ID=<%#Eval("Counter")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            Update
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                 <asp:TemplateField HeaderText="TripType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripTypeName") %>'></asp:Label>
                                                          </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Provider">
                                                    <ItemTemplate>
                                                         <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                        <a href='UpdateServiceCredentials.aspx?ID=<%#Eval("Counter")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                         <asp:Label ID="lblProvider" runat="server" Text='<%#Eval("Provider") %>'></asp:Label> &nbsp;(<u>Update</u>) 
                                                        </a>
                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                  <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirline" runat="server" Text='<%#Eval("AirlineName") %>'></asp:Label>
                                                          </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Corporate_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCorporateID" runat="server" Text='<%#Eval("CorporateID") %>'></asp:Label>
                                                          </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="UserID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Password">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblPassword" runat="server" Text='<%#Eval("Password") %>'></asp:Label>--%>
                                                        <asp:Label ID="lblPassword" runat="server" Text='******'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                              <%--  <asp:TemplateField HeaderText="LoginID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoginID" runat="server" Text='<%#Eval("LoginID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="LoginPwd">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoginPwd" runat="server" Text='<%#Eval("LoginPwd") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ServerUrlOrIP">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServerUrlOrIP" runat="server" Text='<%#Eval("ServerUrlOrIP") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="BkgServerUrlOrIP">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBkgServerUrlOrIP" runat="server" Text='<%#Eval("BkgServerUrlOrIP") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Port">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPort" runat="server" Text='<%#Eval("Port") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                              
                                                <asp:TemplateField HeaderText="CarrierAcc">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCarrierAcc" runat="server" Text='<%#Eval("CarrierAcc") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SearchType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSearchType" runat="server" Text='<%#Eval("SearchType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="ResultFrom">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblResultFrom" runat="server" Text='<%#Eval("ResultFrom") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fare Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCrdType" runat="server" Text='<%#Eval("CrdType") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--select Counter, CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider,  CarrierAcc,  
                       SearchType, Status, ResultFrom, CrdType FROM ServiceCredentials--%>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        </div>
    
    <%--<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>--%>
     <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        function ShowHide() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if (Provider == "1G" || Provider == "1GINT") {
                $("#txtAirline").removeAttr("disabled");
                $("#txtAirline").val("");
                $("#hidtxtAirline").val("");


                $("#divCorporateID").show();
                $("#divCarrierPCC").show();
                $("#divResultFrom").hide();

                $("#DivRow").hide();
                $("#DivLoginID").hide();
                $("#DivLoginPwd").hide();
            }
            //else if (Provider == "G8") {
            //    $("#divCorporateID").show();
            //    $("#divCarrierPCC").hide();
            //    $("#divResultFrom").show();

            //    //$("#DivRow").show();
            //    $("#DivLoginID").show();
            //    $("#DivLoginPwd").show();
                
            //}
            else {
                if (Provider == "6E") {
                    $("#txtAirline").val("Indigo(6E)");
                    $("#hidtxtAirline").val("Indigo,6E");
                }
                if (Provider == "SG") {
                    $("#txtAirline").val("Spicejet(SG)");
                    $("#hidtxtAirline").val("Spicejet,SG");
                }
                if (Provider == "G8") {
                    $("#txtAirline").val("GoAir(G8)");
                    $("#hidtxtAirline").val("GoAir,G8");
                }
                $("#txtAirline").attr("disabled", "disabled");
                $("#divCorporateID").show();
                $("#divCarrierPCC").hide();
                $("#divResultFrom").show();
                $("#DivRow").hide();
                $("#DivLoginID").hide();
                $("#DivLoginPwd").hide();

            }
        }

        function Check() {
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
                alert("Select provider.");
                $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
                return false;
            }
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if ($("#ctl00_ContentPlaceHolder1_TxtCorporateID").val() == "") {
                alert("Enter Corporate ID.");
                $("#ctl00_ContentPlaceHolder1_TxtCorporateID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtUserID").val() == "") {
                alert("Enter User ID.");
                $("#ctl00_ContentPlaceHolder1_TxtUserID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtPassword").val() == "") {
                alert("Enter Password.");
                $("#ctl00_ContentPlaceHolder1_TxtPassword").focus();
                return false;
            }
            //if (Provider == "G8") {
            //    if ($("#ctl00_ContentPlaceHolder1_TxtLoginID").val() == "") {
            //        alert("Enter Login ID.");
            //        $("#ctl00_ContentPlaceHolder1_TxtLoginID").focus();
            //        return false;
            //    }
            //    if ($("#ctl00_ContentPlaceHolder1_TxtLoginPwd").val() == "") {
            //        alert("Enter Login Password.");
            //        $("#ctl00_ContentPlaceHolder1_TxtLoginPwd").focus();
            //        return false;
            //    }
            //}

            if (Provider == "1G" || Provider == "1GINT") {
                if ($("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").val() == "") {
                    alert("Enter Carrier PCC.");
                    $("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").focus();
                    return false;
                }

                $("#ctl00_ContentPlaceHolder1_TxtLoginID").val("");
                $("#ctl00_ContentPlaceHolder1_TxtLoginPwd").val("");
                $("#ctl00_ContentPlaceHolder1_DdlResultFrom").val("AP");
            }

            if (Provider == "SG" || Provider == "6E" || Provider == "G8") {
                $("#ctl00_ContentPlaceHolder1_TxtLoginID").val("");
                $("#ctl00_ContentPlaceHolder1_TxtLoginPwd").val("");
                $("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").val("");
            }
            //if (Provider == "G8") {              
            //    $("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").val("");
            //}
            
        }

    </script>

</asp:Content>

