﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_CreditLimitHistory : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null || string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            BindGrid();
        }
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        //if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        //{
        //}
        //else
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
    }
       
    public void BindGrid()
    {
        try
        {           
            SqlTransaction ST = new SqlTransaction();            
            //grd_P_IntlDiscount.DataSource = ST.GetAgencyDetails(AgentId);
            grd_P_IntlDiscount.DataSource = GetRecord();
            grd_P_IntlDiscount.DataBind();

            //DataTable dt = GetRecord();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {
            string AgentId = null;
            if (!string.IsNullOrEmpty(Request["hidtxtAgencyName"]))
            {
                AgentId = Request["hidtxtAgencyName"].Trim();
            }
            string FromDate = null;
            string ToDate = null;
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                if (Request["From"].Split('-').Length > 2)
                {
                    FromDate = Request["From"].Split('-')[2] + "-" + Request["From"].Split('-')[1] + "-" + Request["From"].Split('-')[0];
                    FromDate = FromDate + " " + "12:00:00 AM";
                }
                else
                {
                    FromDate = "";
                }
            }            
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                if (Request["To"].Split('-').Length > 2)
                {
                    ToDate = Request["To"].Split('-')[2] + "-" + Request["To"].Split('-')[1] + "-" + Request["To"].Split('-')[0];
                    ToDate = ToDate + " " + "11:59:59 PM";
                }
                else
                {
                    ToDate = "";
                }
            }
            string ActionType = hidActionType.Value;
            if (con.State == ConnectionState.Closed)
                con.Open();
            adap = new SqlDataAdapter("GET_DistrReport", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adap.SelectCommand.Parameters.AddWithValue("@FormDate", FromDate);
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", ToDate);            
            //adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            //adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;           
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        //ActionTypeGrid = "select";        
        this.BindGrid();
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
           // string AgentLimit = TxtAgentCredit.Text;
            #region Validation

            string AgencyId = "";
            string UserId = "";
            if (!string.IsNullOrEmpty(TxtAgencyName))
            {
                if (string.IsNullOrEmpty(HiddenAgentId))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or DistrId Id.');", true);
                    return;
                }
                if (TxtAgencyName.Split('-').Length > 1)
                {
                    if (TxtAgencyName.Split('-')[1].Split('(').Length > 1)
                    {
                        AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
                        UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
                        if (UserId != HiddenAgentId)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or DistrId Id.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or DistrId Id.');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or DistrId Id.');", true);
                    return;
                }

            }
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!!');", true);
            //    return;
            //}

            #endregion validation

            //if (!string.IsNullOrEmpty(HiddenAgentId))
            //{
                #region Bind Grid
                BindGrid();
                #endregion
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid User Id or Agency Id.');", true);
            //    return;
            //}
        }
        catch (Exception ex)
        {            
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='AgentbookingHistory.aspx'; ", true);
            return;
        }
    }
    
}