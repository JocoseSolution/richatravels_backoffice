﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="SearchResultBlock.aspx.cs" Inherits="SprReports_Admin_SearchResultBlock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-12 ">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Search Result Block</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip Type</label>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="1">
                                        <asp:ListItem Value="D" Text="Domestic" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Provider</label>
                                    <asp:DropDownList ID="DdlProvider" runat="server" CssClass="form-control" onclick="ShowHide();" TabIndex="2">
                                        <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1G" Text="GAL(1G)"></asp:ListItem>
                                        <asp:ListItem Value="6E" Text="Indigo(LCC)"></asp:ListItem>
                                        <asp:ListItem Value="SG" Text="Spicejet(LCC)"></asp:ListItem>
                                        <asp:ListItem Value="G8" Text="GoAir(LCC)"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="">
                                    <label for="exampleInputPassword1">Airline :</label>
                                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="3" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Fare Type</label>
                                    <asp:DropDownList ID="DdlFareType" runat="server" CssClass="form-control" TabIndex="6">
                                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                        <asp:ListItem Value="NRM" Text="Normal"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate"></asp:ListItem>
                                        <asp:ListItem Value="CPN" Text="Coupon"></asp:ListItem>
                                        <asp:ListItem Value="PKG" Text="Package Fare"></asp:ListItem>
                                        <asp:ListItem Value="SME" Text="SME Fare"></asp:ListItem>
                                        <asp:ListItem Value="FLX" Text="Flexi Fare"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType :</label>
                                    <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" AppendDataBoundItems="true" TabIndex="4">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">City Name :</label>
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" oncopy="return false" onpaste="return false" oncut="return false" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Executive Id :</label>
                                    <asp:TextBox ID="txtExecutiveId" runat="server" CssClass="form-control" oncopy="return false" onpaste="return false" oncut="return false" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Include Agent Id  </label>
                                    <asp:TextBox ID="txtSearch" runat="server" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control" onkeydown="return checkShortcut();" TabIndex="5"></asp:TextBox>
                                    <asp:HiddenField ID="hfCustomerId" runat="server" />
                                    <%--<span onclick="ClearRec();" >Clear Value</span>--%>
                                </div>
                                <div id="content"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Exclude Agent Id  </label>
                                    <asp:TextBox ID="txtSearchExclude" runat="server" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control" onkeydown="return checkShortcut();" TabIndex="6"></asp:TextBox>
                                    <asp:HiddenField ID="hfCustomerIdExclude" runat="server" />
                                    <%--<span onclick="ClearRec();" >Clear Value</span>--%>
                                </div>
                                <div id="contentExclude"></div>
                            </div>

                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control" TabIndex="7">
                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">ActionType</label>
                                    <asp:DropDownList ID="DdlRestriction" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="2" Text="Both" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Only Exclude"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" TabIndex="8" OnClientClick="return CheckDeal();" />
                                    <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="row">

                            <div class="col-md-12">
                                <div id="DivMsg" runat="server" style="color: red;"></div>
                                <br />
                                <div id="DivMsgExclued" runat="server" style="color: red;"></div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="100" OnRowCancelingEdit="grd_P_IntlDiscount_RowCancelingEdit"
                                            OnRowEditing="grd_P_IntlDiscount_RowEditing" OnRowUpdating="grd_P_IntlDiscount_RowUpdating" OnRowDeleting="OnRowDeleting" AllowPaging="true"
                                            OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                        <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Provider">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProvider" runat="server" Text='<%#Eval("Provider") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline_Type" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirline_Type" runat="server" Text='<%#Eval("AirlineType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirlineName" runat="server" Text='<%#Eval("Airline") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Group_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroupType" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Agent_Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Exclude_Agent_Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentIdExclude" runat="server" Text='<%#Eval("AgentIdExclude") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Fare_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFare_Type" runat="server" Text='<%#Eval("FareTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_FareType" runat="server" Width="150px" DataValueField='<%#Eval("FareType")%>' SelectedValue='<%#Eval("FareType")%>'>
                                                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                                            <asp:ListItem Value="NRM" Text="Normal"></asp:ListItem>
                                                            <asp:ListItem Value="CRP" Text="Corporate"></asp:ListItem>
                                                            <asp:ListItem Value="CPN" Text="Coupon"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsActive" runat="server" Text='<%#Eval("Status") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_IsActive" runat="server" Width="150px" DataValueField='<%#Eval("Status")%>' SelectedValue='<%#Eval("Status")%>'>
                                                            <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                            <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EDIT" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <%--<asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" Font-Bold="true" />--%>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //  Autocomplete  Nationality
            var countrycode = $('.Nationality').each(function () {
                $(this).autocomplete({
                    source: function (e, t) {
                        $.ajax({
                            url: UrlBase + "CitySearch.asmx/GetCountryCd",
                            data: "{ 'country': '" + e.term + "', maxResults: 10 }",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (e) {
                                t($.map(e.d, function (e) {
                                    var t = e.CountryName + "(" + e.CountryCode + ")";
                                    var n = e.CountryCode;
                                    return {
                                        label: t,
                                        value: t,
                                        id: n
                                    }
                                }))
                            },
                            error: function (e, t, n) {
                                alert(t)
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (t, n) {
                        $(this).next().val(n.item.id)
                    }
                });
            });

        });

        function AppliedOn() {
            //var DealType = $("#ctl00_ContentPlaceHolder1_DdlDealCodeType").val();
            //if (DealType == "PF") {
            //    $("#DivAppliedOn").show();
            //    $("#DivFareType").hide();
            //}
            //else if (DealType == "PC") {
            //    $("#DivAppliedOn").show();
            //    $("#DivFareType").show();
            //}
            //else {
            //    $("#DivAppliedOn").hide();
            //    $("#DivFareType").hide();
            //}
        }

        function CheckDeal() {
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
                alert("Select provider.");
                $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "1G") {
                if ($("#txtAirline").val() == "") {
                    alert("Select airline.");
                    $("#txtAirline").focus();
                    return false;
                }
                if ($("#hidtxtAirline").val() == "") {
                    alert("Select airline.");
                    $("#txtAirline").focus();
                    return false;
                }
            }

            if ($("#ctl00_ContentPlaceHolder1_DdlFareType").val() == "") {
                alert("Select Fare Type.");
                $("#ctl00_ContentPlaceHolder1_DdlFareType").focus();
                return false;
            }
        }

        function ShowHide() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if (Provider == "1G" || Provider == "1GINT") {
                $("#txtAirline").removeAttr("disabled");
                $("#txtAirline").val("");
                $("#hidtxtAirline").val("");
            }
            else {
                if (Provider == "6E") {
                    $("#txtAirline").val("Indigo(6E)");
                    $("#hidtxtAirline").val("Indigo,6E");
                }
                if (Provider == "SG") {
                    $("#txtAirline").val("Spicejet(SG)");
                    $("#hidtxtAirline").val("Spicejet,SG");
                }
                if (Provider == "G8") {
                    $("#txtAirline").val("GoAir(G8)");
                    $("#hidtxtAirline").val("GoAir,G8");
                }
                $("#txtAirline").attr("disabled", "disabled");
            }
        }


    </script>


    <script type="text/javascript">
        function checkShortcut() {
            //if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 46) {
            //    return false;
            //}
        }
        function ClearRec() {
            $("#ctl00_ContentPlaceHolder1_txtSearch").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");
        }

        function SetAndRemove(UserId) {
            //$("#ctl00_ContentPlaceHolder1_txtSearch").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");

            var maincontent = "";
            var AgentId = "";
            var UserCount = $("#[id*=hfCustomerId]")[0].value.split(",").length;
            if (UserCount > 1) {
                var max = UserCount - 2
                for (var n = 0; n <= UserCount - 2; n++) {
                    if ($("#[id*=hfCustomerId]")[0].value.split(",")[n] != UserId) {
                        if (max == 0 || n == max) {
                            maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            AgentId = AgentId + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + ',';
                        }
                        else {
                            maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            AgentId = AgentId + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + ',';
                        }
                    }
                }
                document.getElementById("content").innerHTML = maincontent;
                $("#ctl00_ContentPlaceHolder1_hfCustomerId").val(AgentId);
            }
        }

        $(document).ready(function () {
            $("#<%=txtSearch.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        // url: ~/AgencySearch.asmx/GetCustomers
                        //data: "{ 'prefix': '" + request.term + "'}",
                        url: '<%=ResolveUrl("~/AgencySearch.asmx/FetchAgencyList") %>',
                        data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.Agency_Name + "(" + item.User_Id + ")",
                                    val: item.User_Id
                                    //label: item.split('-')[0],
                                    //val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            // alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    var text = this.value.split(/,\s*/);
                    //debugger
                    //text.pop();
                    //text.push(i.item.value);
                    //text.push("");
                    //this.value = text.join(", ");
                    this.value = "";
                    var value = $("[id*=hfCustomerId]").val().split(/,\s*/);

                    var maincontent = "";
                    value.pop();
                    value.push(i.item.val);
                    value.push("");
                    $("#[id*=hfCustomerId]")[0].value = value.join(",");
                    var UserCount = $("#[id*=hfCustomerId]")[0].value.split(",").length;
                    if (UserCount > 1) {
                        var max = UserCount - 2;
                        for (var n = 0; n <= UserCount - 2; n++) {
                            if (max == 0 || n == max) {
                                maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            }
                            else {
                                maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            }


                        }
                        document.getElementById("content").innerHTML = maincontent;
                    }
                    return false;
                },
                minLength: 1
            });

        });
    </script>


    <script type="text/javascript">
        function checkShortcutExclude() {
            //if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 46) {
            //    return false;
            //}
        }
        function ClearRecExclude() {
            $("#ctl00_ContentPlaceHolder1_txtSearchExclude").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");
        }

        function SetAndRemoveExclude(UserId) {
            //$("#ctl00_ContentPlaceHolder1_txtSearch").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");

            var maincontentExclude = "";
            var AgentIdExclude = "";
            var UserCount = $("#[id*=hfCustomerIdExclude]")[0].value.split(",").length;
            if (UserCount > 1) {
                var max = UserCount - 2
                for (var n = 0; n <= UserCount - 2; n++) {
                    if ($("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] != UserId) {
                        if (max == 0 || n == max) {
                            maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            AgentIdExclude = AgentIdExclude + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + ',';
                        }
                        else {
                            maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            AgentIdExclude = AgentIdExclude + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + ',';
                        }
                    }
                }
                document.getElementById("contentExclude").innerHTML = maincontentExclude;
                $("#ctl00_ContentPlaceHolder1_hfCustomerIdExclude").val(AgentIdExclude);
            }
        }

        $(document).ready(function () {
            $("#<%=txtSearchExclude.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        // url: ~/AgencySearch.asmx/GetCustomers
                        //data: "{ 'prefix': '" + request.term + "'}",
                        url: '<%=ResolveUrl("~/AgencySearch.asmx/FetchAgencyList") %>',
                        data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.Agency_Name + "(" + item.User_Id + ")",
                                    val: item.User_Id
                                    //label: item.split('-')[0],
                                    //val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            // alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    var text = this.value.split(/,\s*/);
                    //debugger
                    //text.pop();
                    //text.push(i.item.value);
                    //text.push("");
                    //this.value = text.join(", ");
                    this.value = "";
                    var value = $("[id*=hfCustomerIdExclude]").val().split(/,\s*/);

                    var maincontentExclude = "";
                    value.pop();
                    value.push(i.item.val);
                    value.push("");
                    $("#[id*=hfCustomerIdExclude]")[0].value = value.join(",");
                    var UserCount = $("#[id*=hfCustomerIdExclude]")[0].value.split(",").length;
                    if (UserCount > 1) {
                        var max = UserCount - 2;
                        for (var n = 0; n <= UserCount - 2; n++) {
                            if (max == 0 || n == max) {
                                maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            }
                            else {
                                maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            }


                        }
                        document.getElementById("contentExclude").innerHTML = maincontentExclude;
                    }
                    return false;
                },
                minLength: 1
            });

            $("#<%=txtCity.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({                       
                        url: '<%=ResolveUrl("~/AgencySearch.asmx/GetCityFromAgentRegisterList") %>',
                        data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.CityName,
                                    val: item.CityName
                                }
                            }))
                        },
                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            // alert(response.responseText);
                        }
                    });
                },                
                minLength: 1
            });

              $("#<%=txtExecutiveId.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({                       
                        url: '<%=ResolveUrl("~/AgencySearch.asmx/GetExecutiveDetailList") %>',
                        data: "{ 'exectid': '" + request.term + "', maxResults: 10 }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.SalesExecID,
                                    val: item.SalesExecID
                                }
                            }))
                        },
                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            // alert(response.responseText);
                        }
                    });
                },                
                minLength: 1
            });
        });
    </script>




</asp:Content>

