<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Update_Agent.aspx.vb" Inherits="Update_Agent" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agent Grade</title>

    <link href="<%= ResolveUrl("~/css/style.css")%>" rel="stylesheet" type="text/css" />

    <%--  <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ResolveUrl("~/CSS/bootstrap.min.css")%>" rel="stylesheet" />
    <script src="<%= ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= btn_update.ClientID%>').click(function () {
                <%--var old = $('#<%= txtOldPassword.ClientID%>').val();--%>
                var newPass = $('#<%= txtNewPassword.ClientID%>').val();
                var confirmPass = $('#<%= txtConfirmPassword.ClientID%>').val();

                if (newPass == '') {
                    if (confirm("Are you sure!")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (confirmPass == '') {
                        alert("Enter confirm password.");
                        return false;
                    }

                    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
                    if (!regex.test(newPass)) {
                        alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                        document.newPass.focus();
                        return false;
                    }



                    if (confirmPass != newPass) {
                        alert("Password not confirm.");
                        $('#txtConfirmPassword').focus();
                        return false;
                    }
                    if (confirm("Are you sure!")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            })
        });
    </script>

    <script language="javascript">



        function validatetype() {
            if (document.getElementById("ddl_type").value == "--Select Type--") {
                alert('Please Select Agent Type');
                document.getElementById("ddl_type").focus();
                return false;
            }
            //if (confirm("Are You Sure!!"))
            //    return true;
            //return false;
        }
        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function MyFunc() {
            alert("Agency record updated successfully.")
            window.open('Agent_Details.aspx', '_parent');
            window.close();
        }


        function showBox() {
            $("#td_displayBox").show();
            $("#showHeadingBox").hide();
        }
        function hideBox() {
            $("#td_displayBox").hide();
            $("#showHeadingBox").show();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>
    <style type="text/css">
        .tablesss {
            margin-bottom: 10px;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 12px 24px;
            margin: .3em 0 1em 0;
            width: 100%;
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            background: transparent;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }

        .buttonBlue {
            background: #d9534f;
        }

            .buttonBlue:hover {
                background: #af3e3a;
            }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class=" col-lg-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary">

                <div class="panel-heading">
                    <h4>Agency Information</h4>
                </div>

                <div class="panel-body">

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                User ID:
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7" id="td_AgentID" runat="server">
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Avilable Balance
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7" id="td_CrLimit" runat="server">
                            </div>
                        </div>
                    </div>
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Credit Limit
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:Label ID="lblAgentLimit" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Due Amount :
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:Label ID="lblDueAmount" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss" style="display: none;">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Tds:
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7" id="td_tds" runat="server">
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Last Transaction Date
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7" id="td_LTDate" runat="server">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <h3>Agency Address</h3>
                        </div>
                    </div>
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Address
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_Address" runat="server" TextMode="MultiLine" Enabled="False" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Agency Name :
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_AgencyName" runat="server" Enabled="False" CssClass="form-control" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz0123456789');" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                City
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_City" runat="server" Enabled="False" CssClass="form-control" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                State
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">

                                <asp:TextBox ID="txt_State" runat="server" Enabled="False" CssClass="form-control" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                            </div>

                        </div>
                    </div>


                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Zip Code
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_zip" runat="server" Enabled="False" MaxLength="8" CssClass="form-control" onkeypress="return checkitt(event)"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regexpcontactZipCode" runat="server" ControlToValidate="txt_zip"
                                    ValidationGroup="contactValidation" ForeColor="Red" ErrorMessage="Enter Only Digit"
                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Country
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_Country" runat="server" Enabled="False" CssClass="form-control" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <h3>Agent Details</h3>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-4 col-sm-4 col-xs-12">
                            <div class="col-lg-6 col-sm-6 col-xs-5">
                                Name
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-7">
                                <asp:TextBox ID="txt_title" runat="server" Enabled="False" Width="100px" CssClass="form-control"></asp:TextBox>

                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-3 col-xs-12">
                            <asp:TextBox ID="txt_Fname" runat="server" Enabled="False" Width="90%" CssClass="form-control"></asp:TextBox>


                        </div>
                        <div class="col-lg-4 col-sm-3 col-xs-12">
                            <asp:TextBox ID="txt_Lname" runat="server" Enabled="False" Width="90%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Mobile
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_Mobile" runat="server" MaxLength="10" Enabled="False" onkeypress="return checkitt(event)" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Email
                            
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_Email" runat="server" Enabled="False" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_Email" ValidationGroup="group1" ErrorMessage=" enter valid email " ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>



                    <div class="row tablesss">

                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                PAN Name
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="TxtNameOnPancard" runat="server" Enabled="False" CssClass="form-control" MaxLength="50" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Pan No
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_Pan" runat="server" Enabled="False" MaxLength="15" CssClass="form-control" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz1234567890');"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Fax No
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_Fax" runat="server" Enabled="False" CssClass="form-control" onKeyPress="return keyRestrict(event,'1234567890');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                OTP Login
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:CheckBox ID="OPTLOGIN" runat="server" />
                            </div>
                        </div>

                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Exp Password Login 
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:CheckBox ID="EXPPASS" runat="server" />
                            </div>
                        </div>
                    </div>


                    <div class="row tablesss" style="display: none;">
                        <div class="col-lg-6 col-sm-6 col-xs-12" id="td_pwd" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Password
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txt_pwd" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <h3>GST  Details</h3>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div3" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                    GST No
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="txt_gstno" runat="server"  ReadOnly="false"  CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-lg-6 col-sm-6 col-xs-12" id="Div10" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                    GST Company Name
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="txt_gstcompany" runat="server"  ReadOnly="false"  CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div4" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                    GST Company Add
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="txt_GST_Company" runat="server"  ReadOnly="false"  CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div5" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                    GST_PhoneNo
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="txt_gstPhone" runat="server"  ReadOnly="false"  CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                             
                        </div>
                    </div>
                    <div class="row form-group  ">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div6" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                   GST_Email
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="txt_gstemail" runat="server" ReadOnly="false" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div7" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                    GST_Pincode
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="txt_gstpin" runat="server" ReadOnly="false" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                             
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div8" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                   GST_State
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="ddlGSTState" runat="server" class="form-control" onchange="javascript:ChangeStateEvent();">
                                </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12" id="Div9" runat="server">
                                <div class="col-lg-4 col-sm-4 col-xs-5">
                                    GST_City
                                </div>
                                <div class="col-lg-8 col-sm-8 col-xs-7">
                                  <asp:DropDownList ID="ddlGSTCity" runat="server" class="form-control" onchange="javascript:ChangeCityEvent();">
                                </asp:DropDownList>
                                     <asp:HiddenField ID="hdnSelectedCity" runat="server" />
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <h3>Update Details</h3>
                        </div>
                    </div>
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Type
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Sales Ref
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <%--<asp:TextBox ID="txt_saleref" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                <asp:DropDownList ID="Sales_DDL" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Activation
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="ddl_activation" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                    <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Ticketing Activation
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="ddl_TicketingActiv" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                    <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12" id="Div1" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Agent Credit
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="TxtAgentCredit" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12" id="Div2" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                IRCTC ID
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtirctcid" runat="server" CssClass="form-control" ReadOnly="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row tablesss">
                        <div class="col-lg-4 col-sm-4 col-xs-12" id="Div121" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                IRCTC Type
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="ddlIrctDeduct" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlIrctDeduct_SelectedIndexChanged" AutoPostBack="true">
                                     <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                    <asp:ListItem Value="HalfYearly">Half Yearly</asp:ListItem>
                                    <asp:ListItem Value="Yearly">Yearly</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-xs-12" id="Div131" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                IRCTC Amount
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtIrctDedAmt" runat="server" CssClass="form-control" Text="0" onkeypress="return checkitt(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-xs-12" id="Div20" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                IRCTC Next Payment Date
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtIrctcPayDate" runat="server" CssClass="form-control" ReadOnly="true" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                      <div class="row tablesss">
                        <div class="col-lg-12 col-sm-12 col-xs-12" id="Div11" runat="server">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Change Distributor
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                 <asp:DropDownList ID="ddl_distributor" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>                       
                    </div>
                </div>

                <%--Update Password box--%>
                <div id="showHeadingBox">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="btnHyper" Text="Click here to update password" runat="server" onclick="showBox();"></asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div id="td_displayBox" style="display: none; width: 95%; margin: auto; padding-bottom: 10px; border: 1px solid #94aee5; border-radius: 5px;">
                    <br />
                    <h4 style="font-size: 16px; font-weight: bold;">Update Agency Password</h4>
                    <br />

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                <asp:Label ID="lbl_currentPassMsg" runat="server" Text="Current Password:" Visible="false" Font-Size="12px"></asp:Label>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:Label ID="lbl_currentPassword" runat="server" Text="" Visible="false" Font-Size="12px" Font-Bold="true"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                New Password
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" MaxLength="16" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Confirm Password
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" MaxLength="16" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="large-2 medium-2 small-12">
                        <a onclick="hideBox();" href="#">
                            <img src="<%= ResolveUrl("~/Images/close.png")%>" align="right" alt="Close" title="Close this box" /></a>
                    </div>
                    <br />
                </div>


                <%--End password updation--%>

                <div class="row tablesss">
                    <div class="col-lg-2 col-sm-2 col-xs-6 col-sm-push-6 col-lg-push-6 col-xs-push-6">
                        <asp:Button ID="btn_update" runat="server" ValidationGroup="group1" Text="Update" CssClass="button buttonBlue" OnClientClick="return validatetype()" />
                    </div>
                </div>
                <%--<tr>
<td style="padding-right: 17px">
                    <fieldset style="border: thin solid #999999;   padding-left: 10px">
                        <legend style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #004b91;">Agent Details</legend>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                        <td class="style1" height="25">
                        Name
                        </td>
                        <td id="td_Name" runat="server" width="100"  >
                        
                        </td>
                         <td width="120">
                        Mobile
                        </td>
                        <td id="td2" runat="server" width="100"  >
                      
                        </td>
                         <td  width="100">
                        Email
                        </td>
                        <td id="td4" runat="server"  >
                      
                        </td>
                        </tr>
                        <tr>
                        <td height="25px" width="100">
                        Alternate Email
                        </td>
                        <td id="td_AEmail" runat="server" width="100" >
                        
                        </td>
                         <td class="fltdtls" style="font-weight: bold; color: #000000;" width="120">
                        
                             Fax</td>
                        <td id="td8" runat="server" width="100"  >
                      
                        </td>
                        <td class="fltdtls" style="font-weight: bold; color: #000000;">
                      
                            User ID</td>
                        <td id="td9" runat="server" width="100"  >
                      
                        </td>
                        </tr>
                        </table>
                    </fieldset>
                    
                </td>
</tr>--%>
            </div>
        </div>
        <script type="text/javascript">    
            CheckGStIsCheckOrNot();
            ChangeStateEvent();

            function ChangeCityEvent() {
                var ddlGSTCityval = document.getElementById("ddlGSTCity");
                document.getElementById("hdnSelectedCity").value = ddlGSTCityval.options[ddlGSTCityval.selectedIndex].text;
            }
            function ChangeStateEvent() {
                var stateid = document.getElementById("ddlGSTState").value;
                if (stateid != null) {
                    var cityid = document.getElementById("ddlGSTCity")
                    BindStateDetails(stateid, cityid);
                }
            }        
            function BindStateDetails(stateid, cityid) {
                var UrlBase = '<%=ResolveUrl("~/") %>';
                $.ajax({
                    url: UrlBase + "SateCityService.asmx/FetchGSTStateList",
                    data: JSON.stringify({ cityCode: stateid }),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var target = cityid;
                        target.innerHTML = "";

                        $(document.createElement('option'))

                            .attr('value', 'select')
                            .text('--Select City--')
                            .appendTo(target);


                        $(data.d).each(function () {
                            $(document.createElement('option'))

                                .attr('value', this.CityName)
                                .text(this.CityName)
                                .appendTo(target);
                        });

                    }
                });
            }
    </script>
    </form>
</body>
</html>
