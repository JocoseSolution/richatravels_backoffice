﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_b2bnotification : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("sp_GetHomeNotification", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.Fill(dt);

            GridView1.DataSource = dt;
                GridView1.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void btnSummerNote_Click(object sender, EventArgs e)
    {
        string record = txtNotTextArea.Text;

        try
        {
            if (!string.IsNullOrEmpty(record) && record != "")
            {
                SqlCommand cmd = new SqlCommand("sp_InsertHomeNotification", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@content", record);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int k = cmd.ExecuteNonQuery();
                if (k != 0)
                {
                    BindGrid();
                    Response.Write("<script>alert('Notification inserted succesfully.')</script>");
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }
	
	protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string counterValue = ((HiddenField)GridView1.Rows[e.RowIndex].FindControl("hdnId")).Value;
            SqlCommand cmd = new SqlCommand("delete from HomeNotification where id=" + counterValue, con);
            cmd.CommandType = CommandType.Text;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            int isSuccess = cmd.ExecuteNonQuery();
            if (isSuccess > 0)
            {
                Response.Write("<script>alert('Notification deleted succesfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        BindGrid();
    }
}