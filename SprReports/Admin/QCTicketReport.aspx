﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="QCTicketReport.aspx.vb" Inherits="SprReports_Admin_QCTicketReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>

    <script type="text/javascript">
        function fareRuleToolTip(id) {
            $.ajax({
                type: "POST",
                url: "QCTicketReport.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    debugger;
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
        .table {    
    vertical-align: top;
    max-width: 100%;
    overflow-x: auto;
    white-space: nowrap;
    border-collapse: collapse;
    border-spacing: 0;
    width: 367px !important;
}
        td {
    font-size: 11px !important;
}
        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        #tooltip {
            z-index: 9999;
            position: absolute;
            top: 200px;
            float: right;
            padding: 5px;
            right: 280px;
            border: 2px solid #04034f;
            background-color: #fff;
            width: auto;
            min-width: 300px;
        }

        .table .table {
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .hovercolor {
            font-weight: bold;
            color: #004b91;
            font-size: 11px;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Flight Search Booking Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4" id="divFromDate" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>                            
                            <div class="col-md-4" id="divToDate" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PNR</label>
                                    <asp:TextBox ID="txt_PNR" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Pax Name</label>

                                    <asp:TextBox ID="txt_PaxName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Ticket No</label>

                                    <asp:TextBox ID="txt_TktNo" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="td_Agency" runat="server">
                                    <label for="exampleInputPassword1">
                                        Agency Name
                                    </label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                    <input type="hidden" id="hidtxtAgencyName" class="form-control" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        OrderId
                                    </label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">TransactionId</label>
                                    <asp:TextBox ID="txt_TransactionId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                    <asp:DropDownList CssClass="form-control" ID="dd_status" runat="server">
                                        <asp:ListItem Text="ALL" Value="ALL">ALL</asp:ListItem>
                                        <asp:ListItem Text="Request" Value="Request"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                        <asp:ListItem Text="Hold" Value="Confirm"></asp:ListItem>
                                        <asp:ListItem Text="HoldByAgent" Value="ConfirmByAgent"></asp:ListItem>
                                        <asp:ListItem Text="PreHoldByAgent" Value="PreConfirmByAgent"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>
                                        <asp:ListItem Text="Ticketed" Value="Ticketed"></asp:ListItem>
                                        <asp:ListItem Text="Failed" Value="Failed"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>





                            <div class="col-md-4" id="divPaymentMode" runat="server">
                                <label for="exampleInputEmail1">PaymentMode :</label>
                                <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                    <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="row">
                               <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Airline
                                    </label>
                                    <asp:TextBox ID="txt_airlinecode" runat="server" CssClass="form-control" placeholder="search by air code"></asp:TextBox>

                                </div>
                            </div>
                             <div class="col-md-4" id="SalesDropDiv" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1" id="tr_ddlSalesPerson" runat="server">Search Sales Person :</label>
                                    <asp:DropDownList ID="DropDownListSalesPerson" runat="server" AppendDataBoundItems="true"
                                        CssClass="form-control">
                                        <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" id="divPartnerName" runat="server">
                                <div class="form-group" id="Partnernameid" runat="server">
                                    <label for="exampleInputEmail1">PartnerName :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPartnerName" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <br />
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <br />
                                <div class="form-group">
                                    <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Ticket Sale :
                                    </label>
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Amount Business : <span style="color:red">₹ <%=TotalAmtofBussines %></span>
                                    </label>
                                    <asp:Label ID="Label4" runat="server"></asp:Label>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                      Net Amount Business : <span style="color:red">₹ <%=NetAmtofBussines %></span>
                                    </label>
                                    <asp:Label ID="Label3" runat="server"></asp:Label>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Commision : <span  style="color:red">₹ <%=TotalCommission %></span>
                                    </label>
                                    <asp:Label ID="Label2" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Records : <span  style="color:red"><%=TotalRecordCount %></span>
                                    </label>
                                    <%--<asp:Label ID="lbl_counttkt" runat="server"></asp:Label>--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">
                                    * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                            AutoGenerateColumns="False" CssClass="table"
                                            GridLines="None" PageSize="30">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Booking Details">
                                                    <ItemTemplate>

                                                        <a href='Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%> &TransID='
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Booking&nbsp;Details
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--  <asp:TemplateField HeaderText="Booking Date/Time">
                                                    <ItemTemplate>

                                                             <a href='BookingTime.aspx?OrderId=<%#Eval("OrderId")%> '
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91"><b> <asp:Label ID="lblCreateDate_" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label></b>
                                                        </a>

                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <%--   <asp:TemplateField HeaderText="Pax&nbsp;Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                               <%-- <asp:TemplateField HeaderText="Pax&nbsp;ID">
                                                    <ItemTemplate>
                                                        <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=<%#Eval("PaxId")%>'
                                                            rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="TID" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>(TktDetail)
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="OrderId">
                                                    <ItemTemplate>
                                                        <div class="tag">
                                                            <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe" class="gridViewToolTip"
                                                                rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                            <div id="tooltip" style="display: none;">
                                                                <div style="float: left;">
                                                                    <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Refund_Request:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("SubmitDate")%>  
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Accepted:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("AcceptDate")%>  
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Refunded:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("UpdateDate")%>  
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <a href='BookingTime.aspx?OrderId=<%#Eval("OrderId")%> '
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91"><b>
                                                                <asp:Label ID="lblCreateDate_" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label></b>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="JourneyType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="JourneyType" runat="server" Text='<%#Eval("JourneyType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OB_TX ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ReferenceNo" runat="server" Text='<%#Eval("ReferenceNo")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="AgencyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgencyName" runat="server" class="gridViewToolTip hovercolor" Text='<%#Eval("AgencyName")%>'>&nbsp;<span class="fa fa-info-circle"></span></asp:Label>
                                                        <div id="tooltip" style="display: none;">
                                                            <div>
                                                                <table width="100%" cellpadding="11" cellspacing="11" border="0" class="table table-border" style="border: 1px solid #ccc; margin-top: 6px;">
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Master Code:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("Agent_Type")%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Agency ID:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("AgencyId")%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Contact No:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("AgencyMobile")%>  
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Email:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("AgencyEmail")%>  
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Address:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("AgencyAddress")%>  
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Carrier" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="TicketID" DataField="TicketId">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                <%--  <asp:TemplateField HeaderText="AgencyID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgencyID" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="PNR Details">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GdsPNR" runat="server" class="gridViewToolTip hovercolor" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                        <div id="tooltip" style="display: none;">
                                                            <div>
                                                                <table width="100%" cellpadding="11" cellspacing="11" border="0" class="table table-border" style="border: 1px solid #ccc; margin-top: 6px;">
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Airline PNR:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("AirlinePnr")%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">GDS PNR:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("GdsPnr")%>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Ticket No:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("TicketNumber")%>  
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField HeaderText="FareType" DataField="RESULTTYPE">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Gross Cost">
                                                    <ItemTemplate>
                                                        <asp:Label ID="TotalBookingCost" runat="server" class="gridViewToolTip hovercolor" Text='<%#Eval("TotalBookingCost")%>'></asp:Label>
                                                        <div id="tooltip" style="display: none;">
                                                            <div>
                                                                <table width="100%" cellpadding="11" cellspacing="11" border="0" class="table table-border" style="border: 1px solid #ccc; margin-top: 6px;">
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Gross:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("TotalBookingCost")%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Admin Markup:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("AdminMrk")%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Total:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Convert.ToDecimal(Eval("TotalBookingCost")) + Convert.ToDecimal(Eval("AdminMrk")) %>  
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--  <asp:BoundField HeaderText="Gross Cost" DataField="TotalBookingCost">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>--%>
                                                <asp:BoundField HeaderText="Net Cost" DataField="TotalAfterDis">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                 <asp:TemplateField HeaderText="Comm.">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Total_Commission"   runat="server" Text='<%#Eval("Total_Commsion")%>'>    </asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField HeaderText="Commission Cost" DataField="TotalAfterDis">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="JourneyDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="JourneyDate" runat="server" Text='<%#Eval("JourneyDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Exce ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Sales Person" DataField="ExecuName">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                <%--   <asp:TemplateField HeaderText="AirlinePnr">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AirlinePnr" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                 
                                                 <asp:BoundField HeaderText="TicketNumber" DataField="TicketNumber"></asp:BoundField>--%>
                                                <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>

                                                <asp:TemplateField HeaderText="Ticket Date Time">
                                                    <ItemTemplate>
                                                                                                                <%--<asp:Label ID="Label5" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label>--%>
                                                        <asp:Label ID="lblCreateDate_" runat="server" Text='<%#Eval("FLTUpdatedDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DUE/Balance">
                                                    <ItemTemplate>
                                                        <asp:Label ID="DueAmount" runat="server" Text='<%#Eval("DueAmount").ToString().Replace(".0000", ".00")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment Mode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agent Remark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentRemark" runat="server" Text='<%#Eval("GSTRemark")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Staff Remark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStaffRemark" runat="server" Text='<%#Eval("StaffRemarks")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pax Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="FN" runat="server" Text='<%#Eval("Fname")%>'></asp:Label>
                                                        <asp:Label ID="LN" runat="server" Text='<%#Eval("Lname")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>




                                                <%-- <asp:BoundField HeaderText="Sector" DataField="Trip">
                                                     <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                 </asp:BoundField>
                                                 <asp:BoundField HeaderText="Trip Type" DataField="TripType">
                                                     <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                 </asp:BoundField>
                                                
                                                                                           
                                                 <asp:BoundField HeaderText="Search ID" DataField="SearchId">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                 <asp:BoundField HeaderText="Booking ID" DataField="PNRId">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                
                                                 
                                                    <asp:TemplateField HeaderText="PG TRAX ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPgTransaId" runat="server" Text='<%#Eval("PGTransactionid")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                  <asp:TemplateField HeaderText="Branch">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Branch" runat="server" Text='<%#Eval("Branch")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="FareType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFareType" runat="server" Text='<%#Eval("RESULTTYPE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="FareRule">
                                                    <ItemTemplate>
                                                        <span class="fareRuleToolTip">
                                                            <img src='<%#ResolveClientUrl("~/Images/air(ex).png")%>' class="cursorpointer" alt="Click to View Full Details" title="Click to View Full Details" style="height: 20px; cursor: pointer;" onclick="fareRuleToolTip('<%#Eval("PaxId") %>')" />
                                                        </span>
                                                        </div>             
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>




                                                <%--    <asp:TemplateField HeaderText="AgentType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAgentType" runat="server" Text='<%#Eval("Agent_Type")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                                <%-- <asp:TemplateField HeaderText="MasterAgent">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMasterAgent" runat="server" Text='<%#Eval("MasterAgent")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User&nbsp;ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LB_CahngeStatus" runat="server" CommandName="ChangeStatus" CommandArgument='<%#Eval("OrderId")%>' Visible='<%# IsVisible(Eval("Status"))%>'
                                                            Font-Bold="True" Font-Underline="False" ForeColor="Red" OnClientClick="javascript:return validate();">Change Status to Hold</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  --%>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="text-decoration-line: initial; display: none" id="outerdiv" class="vew321">
        <div onclick="closethis();" title="click to close" style="background: url(../../Images/closebox.png); cursor: pointer; float: right; z-index: 9999; min-height: 30px; min-width: 30px;"></div>
        <div id="ddd"></div>
    </div>

    <script type="text/javascript">
        function validate() {
            if (confirm("Are you sure you want to change status to hold!")) {
                return true;
            } else {
                return false;
            }
        }
        function closethis() {
            $("#outerdiv").hide();
        }

        $(function () {
            InitializeToolTip();
        });
    </script>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

