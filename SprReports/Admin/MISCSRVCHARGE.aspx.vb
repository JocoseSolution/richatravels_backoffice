﻿Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient

Partial Class SprReports_Admin_MISCSRVCHARGE
    Inherits System.Web.UI.Page
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private adap As SqlDataAdapter
    Private ds As New DataSet()
   
    Private dt As New DataTable()

 
    Public Function GroupTypeMGMT(ByVal type As String, ByVal desc As String, ByVal cmdType As String, ByRef msg As String) As DataTable
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim dt As New DataTable()
        Try

            con.Open()

            Dim cmd As New SqlCommand()

            cmd.CommandText = "usp_agentTypeMGMT"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 200).Value = type
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 500).Value = desc
            cmd.Parameters.Add("@cmdType", SqlDbType.VarChar, 50).Value = cmdType
            cmd.Parameters.Add("@msg", SqlDbType.VarChar, 500)
            cmd.Parameters("@msg").Direction = ParameterDirection.Output

            cmd.Connection = con
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            msg = cmd.Parameters("@msg").Value.ToString().Trim()



            con.Close()


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()

        End Try
        Return dt
    End Function
    Public Sub reset()
        ddl_airline.SelectedIndex = 0
        ddl_TripType.SelectedIndex = 0
        'TXTAgentId.Text = ""
        ddl_GroupType.SelectedIndex = 0
        TXTOrg.Text = ""
        TXTAmount.Text = "0"
        TXTDest.Text = ""
        drpMarkupType.SelectedValue = "0"
        txt_basic.Text = "0"
        txt_CYQ.Text = "0"
        txt_CYB.Text = "0"
    End Sub

    Protected Sub SAVE_Click(Sender As [Object], e As EventArgs)
        Try
            If (drpMarkupType.SelectedValue = "0") Then
                ShowAlertMessage("Please select markup type. ")
            Else
                '    ShowAlertMessage("Unable to insert .Please try again ")
                'End If

                If (drpMarkupType.SelectedValue = "P") Then
                    TXTAmount.Text = "0"
                End If
                If (drpMarkupType.SelectedValue = "F") Then
                    txt_basic.Text = "0"
                    txt_CYQ.Text = "0"
                    txt_CYB.Text = "0"
                End If

                Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "ALL", "ALL", Request("hidtxtAgencyName"))

                'Checking for entry
                Dim cmd1 As New SqlCommand("SP_CHECK_MISCDETAILS", con)
                con.Open()
                cmd1.CommandType = CommandType.StoredProcedure
                cmd1.Parameters.AddWithValue("@Airline", ddl_airline.SelectedValue)
                cmd1.Parameters.AddWithValue("@Trip", ddl_TripType.SelectedValue)
                cmd1.Parameters.AddWithValue("@AgentId", AgentID)
                cmd1.Parameters.AddWithValue("@GroupType", ddl_GroupType.SelectedValue)
                cmd1.Parameters.AddWithValue("@ORIGIN", TXTOrg.Text.Trim())
                cmd1.Parameters.AddWithValue("@DEST", TXTDest.Text)
                cmd1.Parameters.AddWithValue("@FareType", DdlFareType.SelectedValue)
                cmd1.Parameters.AddWithValue("@MarkupType", drpMarkupType.SelectedValue)
                Dim st As Boolean = cmd1.ExecuteScalar()
                con.Close()
                'End Checking for entry
                If (st = False) Then
                    Dim cmd As New SqlCommand("sp_INSERTMISCSRV", con)
                    con.Open()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Airline", ddl_airline.SelectedValue)
                    cmd.Parameters.AddWithValue("@Trip", ddl_TripType.SelectedValue)
                    cmd.Parameters.AddWithValue("@AgentId", AgentID)
                    cmd.Parameters.AddWithValue("@GroupType", ddl_GroupType.SelectedValue)
                    cmd.Parameters.AddWithValue("@Org", TXTOrg.Text)
                    cmd.Parameters.AddWithValue("@Amount", Convert.ToDecimal(TXTAmount.Text))
                    cmd.Parameters.AddWithValue("@Dest", TXTDest.Text)
                    
                    cmd.Parameters.AddWithValue("@FareType", DdlFareType.SelectedValue)
                    cmd.Parameters.AddWithValue("@MarkupType", drpMarkupType.SelectedValue)
                    cmd.Parameters.AddWithValue("@CommisionOnBasic", Convert.ToDecimal(txt_basic.Text))
                    cmd.Parameters.AddWithValue("@CommissionOnYq", Convert.ToDecimal(txt_CYQ.Text))
                    cmd.Parameters.AddWithValue("@CommisionOnBasicYq", Convert.ToDecimal(txt_CYB.Text))
                    cmd.Parameters.AddWithValue("@CreatedBy", Session("UID"))
                    Dim i As Integer = cmd.ExecuteNonQuery()
                    con.Close()
                    If (i > 0) Then
                        ShowAlertMessage("MISC charges submitted successfully")
                    Else
                        ShowAlertMessage("Unable to insert .Please try again ")
                    End If
                    getgv()
                    reset()
                Else
                    ShowAlertMessage("MISC charges already applied . Please edit.")
                End If
                drpMarkupType.SelectedValue = "0"
                'ShowAlertMessage("Unable to insert .Please try again ")
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub btnreset_Click(sender As Object, e As EventArgs)
        reset()
    End Sub
    Private Sub getgv()
        Try
            Dim da As New SqlDataAdapter("sp_MISCSRV", con)
            da.Fill(ds)
            grdemp.DataSource = ds
            grdemp.DataBind()
        Catch ex As Exception
        End Try

    End Sub
    Protected Sub grdemp_RowEditing(sender As Object, e As GridViewEditEventArgs)
        Try
            grdemp.EditIndex = e.NewEditIndex


            getgv()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub grdemp_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Try
            Dim lbtnCounter As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnCounter"), Label), Label)
            Dim txtAmount As TextBox = TryCast(grdemp.Rows(e.RowIndex).FindControl("txtAmount"), TextBox)

            Dim lblMarkupType As Label = TryCast(grdemp.Rows(e.RowIndex).FindControl("lbMarkupType"), Label)
            Dim txtCommisionOnBasic As TextBox = TryCast(grdemp.Rows(e.RowIndex).FindControl("txtCommisionOnBasic"), TextBox)
            Dim txtCommissionOnYq As TextBox = TryCast(grdemp.Rows(e.RowIndex).FindControl("txtCommissionOnYq"), TextBox)
            Dim txtCommisionOnBasicYq As TextBox = TryCast(grdemp.Rows(e.RowIndex).FindControl("txtCommisionOnBasicYq"), TextBox)
            If (lblMarkupType.Text = "P") Then
                txtAmount.Text = "0"
            End If
            If (lblMarkupType.Text = "F") Then
                txtCommisionOnBasic.Text = "0"
                txtCommissionOnYq.Text = "0"
                txtCommisionOnBasicYq.Text = "0"
            End If

            'If Not String.IsNullOrEmpty(pgResponse) AndAlso Not String.IsNullOrEmpty(Request.Form("txnid")) AndAlso String.IsNullOrEmpty(Convert.ToString(Session("UID"))) Then
            '    PGResponsParse(Request.Form("txnid"))
            'End If
            If String.IsNullOrEmpty(Convert.ToString(txtAmount.Text)) Then
                txtAmount.Text = "0"
            End If
            If String.IsNullOrEmpty(Convert.ToString(txtCommisionOnBasic.Text)) Then
                txtCommisionOnBasic.Text = "0"
            End If
            If String.IsNullOrEmpty(Convert.ToString(txtCommissionOnYq.Text)) Then
                txtCommissionOnYq.Text = "0"
            End If
            If String.IsNullOrEmpty(Convert.ToString(txtCommisionOnBasicYq.Text)) Then
                txtCommisionOnBasicYq.Text = "0"
            End If

            If Not String.IsNullOrEmpty(Convert.ToString(lbtnCounter.Text)) Then
                con.Open()
                Dim cmd As New SqlCommand("sp_GetMISCUPDATE", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Counter", lbtnCounter.Text)
                cmd.Parameters.AddWithValue("@Amount", txtAmount.Text)

                cmd.Parameters.AddWithValue("@FareType", DdlFareType.SelectedValue)
                cmd.Parameters.AddWithValue("@MarkupType", drpMarkupType.SelectedValue)
                cmd.Parameters.AddWithValue("@CommisionOnBasic", Convert.ToDecimal(txtCommisionOnBasic.Text))
                cmd.Parameters.AddWithValue("@CommissionOnYq", Convert.ToDecimal(txtCommissionOnYq.Text))
                cmd.Parameters.AddWithValue("@CommisionOnBasicYq", Convert.ToDecimal(txtCommisionOnBasicYq.Text))
                cmd.Parameters.AddWithValue("@UserID", Session("UID"))
                cmd.Parameters.AddWithValue("@IPAddress", Request.UserHostAddress)
                cmd.ExecuteNonQuery()
                con.Close()
            End If
            grdemp.EditIndex = -1
            getgv()
        Catch ex As Exception
            con.Close()
        End Try


    End Sub
    Protected Sub grdemp_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs)


        grdemp.EditIndex = -1
        getgv()
    End Sub
    'Protected Sub grdemp_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
    '    Dim lbtnCounter As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnCounter"), Label), Label)

    '    con.Open()
    '    Dim cmd As New SqlCommand("sp_DELETEMISCSRV", con)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@Counter", lbtnCounter.Text)

    '    cmd.ExecuteNonQuery()
    '    con.Close()
    '    grdemp.EditIndex = -1
    '    getgv()
    'End Sub


    Protected Sub grdemp_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Dim lbtnCounter As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnCounter"), Label), Label)
        Dim lbtnAirline As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnAirline"), Label), Label)
        Dim lbtnTrip As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnTrip"), Label), Label)
        Dim lbtnAgentId As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnAgentId"), Label), Label)
        Dim lbtnGroupType As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnGroupType"), Label), Label)
        Dim lbtnOrg As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnOrg"), Label), Label)
        Dim lbtdest As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtdest"), Label), Label)
        Dim lbtnAmount As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnAmount"), Label), Label)
        con.Open()
        Dim IPAddress As String = Request.ServerVariables("REMOTE_ADDR")
        Dim UserID As String = Session("UID").ToString()
        Dim cmd As New SqlCommand("sp_DELETEMISCSRV", con)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Counter", lbtnCounter.Text)
        cmd.Parameters.AddWithValue("@Airline", lbtnAirline.Text)
        cmd.Parameters.AddWithValue("@Trip", lbtnTrip.Text)
        cmd.Parameters.AddWithValue("@AgentId", lbtnAgentId.Text)
        cmd.Parameters.AddWithValue("@GroupType", lbtnGroupType.Text)
        cmd.Parameters.AddWithValue("@Origin", lbtnOrg.Text)
        cmd.Parameters.AddWithValue("@Destination", lbtdest.Text)
        cmd.Parameters.AddWithValue("@Amount", lbtnAmount.Text)
        cmd.Parameters.AddWithValue("@UserID", UserID)
        cmd.Parameters.AddWithValue("@IPAddress", IPAddress)
        cmd.ExecuteNonQuery()
        con.Close()
        grdemp.EditIndex = -1
        getgv()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (Session("UID") = "" Or Session("UID") Is Nothing) Or Session("User_Type") <> "ADMIN" Then
            Response.Redirect("~/Login.aspx")
        End If

        If Not Page.IsPostBack Then
            'grdemp.DataSource = BindGridView();


            Dim msg As String = ""
            ddl_GroupType.AppendDataBoundItems = True

            ddl_GroupType.DataSource = GroupTypeMGMT("", "", "MultipleSelect", msg)
            ddl_GroupType.DataTextField = "GroupType"
            ddl_GroupType.DataValueField = "GroupType"
            ddl_GroupType.DataBind()

            grdemp.DataBind()

            getgv()


            ddl_airline.AppendDataBoundItems = True
            ddl_airline.Items.Clear()
            '<asp:ListItem Value="ALL">--ALL--</asp:ListItem>
            ddl_airline.Items.Insert(0, New ListItem("-- ALL Airline --", "ALL"))
            ddl_airline.DataSource = GetAirline()
            ddl_airline.DataTextField = "AL_Name"
            ddl_airline.DataValueField = "AL_Code"
            ddl_airline.DataBind()

            DdlFareType.AppendDataBoundItems = True
            DdlFareType.Items.Clear()
            '<asp:ListItem Value="ALL">--ALL--</asp:ListItem>
            DdlFareType.Items.Insert(0, New ListItem("-- Select FareType --", "ALL"))
            DdlFareType.DataSource = SP_GetFareType()
            DdlFareType.DataTextField = "DisplayName"
            DdlFareType.DataValueField = "FareType"
            DdlFareType.DataBind()


        End If
    End Sub
    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Function GetAirline() As DataTable
        Dim dt As DataTable = New DataTable()
        Try
            adap = New SqlDataAdapter("SP_GetAirlinenames", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.Fill(dt)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        Finally
            adap.Dispose()
        End Try

        Return dt
    End Function

    Public Function SP_GetFareType() As DataTable
        Dim dt As DataTable = New DataTable()
        Try
            adap = New SqlDataAdapter("SP_GetFareTypeDllValue", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.Fill(dt)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        Finally
            adap.Dispose()
        End Try

        Return dt
    End Function
End Class
