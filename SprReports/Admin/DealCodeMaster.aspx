﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="DealCodeMaster.aspx.cs" Inherits="SprReports_Admin_DealCodeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
<div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Deal Code Master</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip Type</label>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="1">
                                        <asp:ListItem Value="D" Text="Domestic" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType :</label>                                    
                                    <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" AppendDataBoundItems="true" TabIndex="2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Id :</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                        class="form-control" tabindex="3" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />                                   
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline :</label>                                   
                                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="4" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Flight No:</label>
                                    <asp:TextBox ID="TxtFlightNo" CssClass="form-control" runat="server"  placeholder="Enter Flight No" TabIndex="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Code Type</label>
                                    <asp:DropDownList ID="DdlDealCodeType" runat="server" CssClass="form-control" TabIndex="6" onclick="AppliedOn();">
                                        <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="TC" Text="Tour Code(TC)"></asp:ListItem>
                                        <asp:ListItem Value="DC" Text="Deal Code(DC)"></asp:ListItem>
                                        <%--<asp:ListItem Value="TR" Text="Tracking Code(TR)"></asp:ListItem>--%>
                                        <asp:ListItem Value="PC" Text="Promotion Code(PC)"></asp:ListItem>
                                        <asp:ListItem Value="PF" Text="Private Fare(PF)"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Deal/Tour Code :</label>
                                    <asp:TextBox ID="TxtDTCode" CssClass="form-control" runat="server" placeholder="Enter Deal/Tour Code" MaxLength="50" TabIndex="7"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Orgin:</label> 
                                    <asp:TextBox ID="TxtOrgin" CssClass="form-control" runat="server" placeholder="Enter Orgin City" TabIndex="8"></asp:TextBox>                                   
                                    <%--<input type="text" name="txtDepCity1" class="form-control" placeholder="Enter Orgin City" id="txtDepCity1" tabindex="8" />
                                    <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />--%>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Destination</label> 
                                    <asp:TextBox ID="TxtDestination" CssClass="form-control" runat="server" placeholder="Enter Destination City"  TabIndex="9"></asp:TextBox>                                  
                                   <%-- <input type="text" name="txtArrCity1" id="txtArrCity1" class="form-control" placeholder="Enter Destination City" tabindex="9" />
                                    <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />--%>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Orgin Country :</label>                                   
                                    <asp:TextBox ID="TxtOrginCountry" runat="server" CssClass="form-control" placeholder="Enter Orgin Country" TabIndex="10"></asp:TextBox>
                                    <%--<input type="hidden" id="HdnOrginCountry" name="HdnOrginCountry" runat="server" value="" />--%>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Destination Country :</label>                                    
                                    <asp:TextBox ID="TxtDestCountry" runat="server" CssClass="form-control" placeholder="Enter Destination Country" TabIndex="11"></asp:TextBox>
                                    <%--<input type="hidden" id="HdnDestCountry" name="HdnDestCountry" runat="server" value="" />--%>
                                </div>
                            </div>
                            <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control" TabIndex="13">
                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" id="DivAppliedOn" style="display:none;">
                                    <label for="exampleInputPassword1">Applied On</label>
                                    <asp:DropDownList ID="DdlAppliedOn" runat="server" CssClass="form-control" TabIndex="12">
                                        <asp:ListItem Value="BOTH" Text="Both"></asp:ListItem>                                        
                                        <asp:ListItem Value="BOOK" Text="Booking"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                            </div>

                             <div class="col-md-3" id="DivFareType" style="display:none;">
                                <div class="form-group">
                                     <label for="exampleInputPassword1">Fare Type</label>
                                    <asp:DropDownList ID="DdlFareType" runat="server" CssClass="form-control">                                        
                                        <asp:ListItem Value="NRM" Text="Normal Type"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
					<asp:ListItem Value="CPN" Text="Coupon"></asp:ListItem>
                                        <asp:ListItem Value="PKG" Text="Package"></asp:ListItem> 
					<asp:ListItem Value="SME" Text="SME Fare"></asp:ListItem>
                                        <asp:ListItem Value="SMF" Text="SME Fare-SMF"></asp:ListItem> 
					<asp:ListItem Value="FLX" Text="Flexi"></asp:ListItem> 
					<asp:ListItem Value="CRG" Text="Special-CRG"></asp:ListItem> 						                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="button buttonBlue" OnClick="BtnExport_Click" />--%>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click" />--%>
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" TabIndex="14" OnClientClick="return CheckDeal();" />
                                </div>
                            </div>                           
                        </div>
                        <%--<div class="row">
                             <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" TabIndex="14" OnClientClick="return CheckDeal();" />
                                </div>
                            </div>
                            </div>--%>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow:auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="grd_P_IntlDiscount_RowCancelingEdit"
                                            OnRowEditing="grd_P_IntlDiscount_RowEditing" OnRowUpdating="grd_P_IntlDiscount_RowUpdating" OnRowDeleting="OnRowDeleting"  AllowPaging="true"
                                            OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                              <%--  <asp:TemplateField HeaderText="Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label>
                                                        <a href='FilterCommissionUpdate.aspx?ID=<%#Eval("Id")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="lbl_group" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>                                            

                                                <asp:TemplateField HeaderText="Trip_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblTripTypeName" runat="server" Text='<%#Eval("TripTypeName") %>'></asp:Label>                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="Group_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroupType" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>                                                        
                                                    </ItemTemplate>                                                   
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Agent_Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>                                                        
                                                    </ItemTemplate>                                                                                             
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirlineName" runat="server" Text='<%#Eval("AirlineName") %>'></asp:Label>
                                                    </ItemTemplate>                                               
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Flight_No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFltNo" runat="server" Text='<%#Eval("FltNo") %>'></asp:Label>                                                                                                             
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdFltNo" runat="server" Text='<%#Eval("FltNo") %>' Width="100px" BackColor="#ffff66"></asp:TextBox>                                                       
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Code_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCodeType" runat="server" Text='<%#Eval("CodeType") %>'></asp:Label>
                                                    </ItemTemplate>
                                               <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_CodeType" runat="server" Width="150px" DataValueField='<%#Eval("CodeType")%>' SelectedValue='<%#Eval("CodeType")%>'>
                                                            <asp:ListItem Value="TC" Text="Tour Code(TC)"></asp:ListItem>
                                                            <asp:ListItem Value="DC" Text="Deal Code(DC)"></asp:ListItem>                                                            
                                                            <asp:ListItem Value="PC" Text="Promotion Code(PC)"></asp:ListItem>
                                                             <asp:ListItem Value="PF" Text="Private Fare(PF)"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Deal/Tour_Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblD_T_Code" runat="server" Text='<%#Eval("D_T_Code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_D_T_Code" runat="server" Text='<%#Eval("D_T_Code") %>' Width="100px" BackColor="#ffff66"></asp:TextBox>                                                       
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Applied_On">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAppliedOn" runat="server" Text='<%#Eval("AppliedOn") %>'></asp:Label>
                                                    </ItemTemplate>
                                                   <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_AppliedOn" runat="server" Width="150px" DataValueField='<%#Eval("AppliedOn")%>' SelectedValue='<%#Eval("AppliedOn")%>'>
                                                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                                            <asp:ListItem Value="BOTH" Text="Both"></asp:ListItem>                                                            
                                                            <asp:ListItem Value="BOOK" Text="Booking"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Fare_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFare_Type" runat="server" Text='<%#Eval("IdType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                   <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_FareType" runat="server" Width="150px" DataValueField='<%#Eval("IdType")%>' SelectedValue='<%#Eval("IdType")%>'>
                                                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                                            <asp:ListItem Value="NRM" Text="Normal Type"></asp:ListItem>
                                                            <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
							    <asp:ListItem Value="CPN" Text="Coupon"></asp:ListItem>
                                        		    <asp:ListItem Value="PKG" Text="Package"></asp:ListItem> 
					                    <asp:ListItem Value="SME" Text="SME Fare"></asp:ListItem>
                                                           <asp:ListItem Value="SMF" Text="SME Fare-SMF"></asp:ListItem> 
					                   <asp:ListItem Value="FLX" Text="Flexi"></asp:ListItem> 
					                   <asp:ListItem Value="CRG" Text="Special-CRG"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsActive" runat="server" Text='<%#Eval("IsActive") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_IsActive" runat="server" Width="150px" DataValueField='<%#Eval("IsActive")%>' SelectedValue='<%#Eval("IsActive")%>'>
                                                             <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                             <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem> 
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="Orgin">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrginAirport" runat="server" Text='<%#Eval("OrginAirport") %>'></asp:Label>
                                                    </ItemTemplate>
                                                      <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdOrginAirport" runat="server" Text='<%#Eval("OrginAirport") %>' Width="100px" BackColor="#ffff66"></asp:TextBox>                                                       
                                                    </EditItemTemplate>                                                
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Orgin_Country">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrginCountry" runat="server" Text='<%#Eval("OrginCountry") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdOrginCountry" runat="server" Text='<%#Eval("OrginCountry") %>' Width="100px" BackColor="#ffff66"></asp:TextBox>                                                       
                                                    </EditItemTemplate> 
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Destination">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDestination" runat="server" Text='<%#Eval("DestAirport") %>'></asp:Label>
                                                    </ItemTemplate> 
                                                     <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdDestAirport" runat="server" Text='<%#Eval("DestAirport") %>' Width="100px" BackColor="#ffff66"></asp:TextBox>                                                       
                                                    </EditItemTemplate>                                                   
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Dest_Country">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDestCountry" runat="server" Text='<%#Eval("DestCountry") %>'></asp:Label>
                                                    </ItemTemplate>
                                                      <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdDestCountry" runat="server" Text='<%#Eval("DestCountry") %>' Width="100px" BackColor="#ffff66"></asp:TextBox>                                                       
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                               
                                                <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                    </ItemTemplate>                                                  
                                                </asp:TemplateField>                                                                                                                                        
                                                <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>                                                       
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>                                                       
                                                       <%--<asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" Font-Bold="true" />--%>
                                                       <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>                                                   
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //  Autocomplete  Nationality
            var countrycode = $('.Nationality').each(function () {
                $(this).autocomplete({
                    source: function (e, t) {
                        $.ajax({
                            url: UrlBase + "CitySearch.asmx/GetCountryCd",
                            data: "{ 'country': '" + e.term + "', maxResults: 10 }",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (e) {
                                t($.map(e.d, function (e) {
                                    var t = e.CountryName + "(" + e.CountryCode + ")";
                                    var n = e.CountryCode;
                                    return {
                                        label: t,
                                        value: t,
                                        id: n
                                    }
                                }))
                            },
                            error: function (e, t, n) {
                                alert(t)
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (t, n) {
                        $(this).next().val(n.item.id)
                    }
                });
            });
                      
        });

        function AppliedOn() {
            var DealType = $("#ctl00_ContentPlaceHolder1_DdlDealCodeType").val();
           // if ( DealType == "PF") {                
              //  $("#DivAppliedOn").show();
                //$("#DivFareType").hide();
           // }
           // else 
            if(DealType == "PC" || DealType == "PF")
            {
                $("#DivAppliedOn").show();
                $("#DivFareType").show();
            }
            else {
                $("#DivAppliedOn").hide();
                $("#DivFareType").hide();
            }
        }

        function CheckDeal() {           
            if ($("#ctl00_ContentPlaceHolder1_DdlDealCodeType").val() == "0") {
                alert("Select Deal/Tour Code Type.");
                $("#ctl00_ContentPlaceHolder1_DdlDealCodeType").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtDTCode").val() == "") {
                alert("Enter Deal/Tour Code :.");
                $("#ctl00_ContentPlaceHolder1_TxtDTCode").focus();
                return false;
            }
        }

    </script>

</asp:Content>
