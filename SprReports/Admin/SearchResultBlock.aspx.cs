﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_SearchResultBlock : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        DivMsg.InnerHtml = "";
        DivMsgExclued.InnerHtml = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_ptype.AppendDataBoundItems = true;
                    ddl_ptype.Items.Clear();
                    //Dim item As New ListItem("All Type", "0")
                    //ddl_ptype.Items.Insert(0, item)
                    ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                    ddl_ptype.DataTextField = "GroupType";
                    ddl_ptype.DataValueField = "GroupType";
                    ddl_ptype.DataBind();
                    ddl_ptype.Items.Insert(0, new ListItem("-- Select Type --", "ALL"));

                    //ddl_Pairline.AppendDataBoundItems = true;
                    //ddl_Pairline.Items.Clear();                   
                    //ddl_Pairline.Items.Insert(0, new ListItem("-- Select Airline --", "ALL"));
                    //ddl_Pairline.DataSource = GetAirline();
                    //ddl_Pairline.DataTextField = "AL_Name";
                    //ddl_Pairline.DataValueField = "AL_Code";
                    //ddl_Pairline.DataBind();


                    //ddl_Pairline.Items.Add(new ListItem("ALL_TYPE", "All Type"));
                   
                    BindGrid();
                }


            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            #region Insert

            int Counter = 0;
            string Trip = DdlTripType.SelectedValue;
            string Provider = DdlProvider.SelectedValue;
            string GroupType = ddl_ptype.SelectedValue;
            string FareType = DdlFareType.SelectedValue;
            string FareTypeName = Convert.ToString(DdlFareType.SelectedItem.Text);
            string Airline = Convert.ToString(Request["hidtxtAirline"]);
            string FlightName = Convert.ToString(Request["txtAirline"]);
            string AirCode = "";
            string AirlineName = "";

            string cityName = txtCity.Text.Trim();
            string executiveId = txtExecutiveId.Text.Trim();

            string ExcludeMsg = "";

            if (!string.IsNullOrEmpty(FlightName))
            {
                if (!string.IsNullOrEmpty(Airline))
                {
                    AirlineName = Airline.Split(',')[0];
                    if (Airline.Split(',').Length > 1)
                    {
                        AirCode = Airline.Split(',')[1];
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                        return;
                    }
                }
            }
            //else
            //{
            //    AirlineName = "";
            //    AirCode = "";
            //}

            if (Provider == "6E")
            {
                AirlineName = "Indigo";
                AirCode = Provider;
            }
            else if (Provider == "SG")
            {
                AirlineName = "Spicejet";
                AirCode = Provider;
            }
            else if (Provider == "G8")
            {
                AirlineName = "GoAir";
                AirCode = Provider;
            }
                    
            string RTF = "true";//Convert.ToString(TxtFlightNo.Text);
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);            
            string Status = Convert.ToString(DdlStatus.SelectedValue);
            string ActionType="insert";
            string AirlineType = "";

            string AgentId = "";// Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string SelectedUserId = hfCustomerId.Value;
            //AirlineType-LCC,GDS
            //Provider-LCC,1G
            if (Provider == "1G")
            {
                Provider = "1G";
                AirlineType = "GDS"; 
            }
            else
            {
                Provider = "LCC";
                AirlineType = "LCC";

            }
            if(DdlRestriction.SelectedValue=="2")
            {
                #region Result Block Insert
                string AddMsg = "";
                msgout = "";
                //lblMsg.Text = "";
                DivMsg.InnerHtml = "";
                if (!string.IsNullOrEmpty(SelectedUserId))
                {
                    //string AgentIdMsg = "";
                    int flag = 0;
                    int len = SelectedUserId.Split(',').Length;//> 1
                    for (int i = 0; i < len - 1; i++)
                    {
                        msgout = "";
                        GroupType = "";
                        AgentId = SelectedUserId.Split(',')[i];
                        flag = Insert(Counter, AirCode, Provider, RTF, Trip, AirlineType, Status, GroupType, AgentId, FareType, FareTypeName, ActionType, cityName, executiveId);
                        AddMsg = AddMsg + AgentId + "-" + msgout + ".<br />";
                    }

                    if (!string.IsNullOrEmpty(hfCustomerIdExclude.Value))
                    {
                        try
                        {
                            ExcludeMsg = ExcludeAgentInsert(AirCode, Provider, Trip, AirlineType, Status, FareType, FareTypeName, cityName, executiveId);
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    DdlProvider.SelectedValue = "0";
                    DdlFareType.SelectedValue = "";

                    hfCustomerId.Value = "";
                    if (!string.IsNullOrEmpty(AddMsg))
                    {
                        //lblMsg.Text = AddMsg;
                        DivMsg.InnerHtml = AddMsg;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert(" + AddMsg + ");", true);
                        hidActionType.Value = "select"; 
                        BindGrid();
                    }
                    else
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Already exists,Please update..');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                        hidActionType.Value = "select"; 
                        BindGrid();
                    }
                }
                else
                {
                    //lblMsg.Text = "";
                    DivMsg.InnerHtml = AddMsg;
                    AgentId = "";
                    msgout = "";
                    int flag = Insert(Counter, AirCode, Provider, RTF, Trip, AirlineType, Status, GroupType, AgentId, FareType, FareTypeName, ActionType, cityName, executiveId);
                    AddMsg = msgout;
                    try
                    {
                        ExcludeMsg = ExcludeAgentInsert(AirCode, Provider, Trip, AirlineType, Status, FareType, FareTypeName, cityName, executiveId);
                    }
                    catch (Exception ex)
                    {

                    }
                    hfCustomerId.Value = "";
                    //lblMsg.Text = AddMsg;
                    DivMsg.InnerHtml = AddMsg;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert(" + AddMsg + ");", true);
                    DdlProvider.SelectedValue = "0";
                    DdlFareType.SelectedValue = "";
                    hidActionType.Value = "select";
                    txtCity.Text = string.Empty;
                    txtExecutiveId.Text = string.Empty;
                    BindGrid();

                }
                #endregion Result Block Insert
            }
            else
            {
                if (!string.IsNullOrEmpty(hfCustomerIdExclude.Value))
                {
                    try
                    {
                        ExcludeMsg = ExcludeAgentInsert(AirCode, Provider, Trip, AirlineType, Status, FareType, FareTypeName, cityName, executiveId);
                        hidActionType.Value = "select";
                        BindGrid();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Select Exclude AgentId');", true);
                }
                //Only show result Agentwise
            }

            



            #endregion



        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SearchResultBlock.aspx'; ", true);
            return;
        }

    }

    private int Insert(int Counter, string Airline, string Provider, string RTF, string Trip, string AirlineType, string Status, string GroupType, string AgentId, string FareType, string FareTypeName, string ActionType, string cityName, string executiveId)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        try
        {
            SqlCommand cmd = new SqlCommand("SpSearchResultBlock_Modified", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@Airline", Airline);
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@RTF", Convert.ToBoolean(RTF));
            cmd.Parameters.AddWithValue("@Trip", Trip);
            cmd.Parameters.AddWithValue("@AirlineType", AirlineType);
            cmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(Status));
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@FareType", FareType);
            cmd.Parameters.AddWithValue("@FareTypeName", FareTypeName);

            cmd.Parameters.AddWithValue("@CityName", cityName);
            cmd.Parameters.AddWithValue("@ExecId", executiveId);
            // cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }
    public void BindGrid()
    {
        try
        {
            string Trip="",Provider="", Airline="", FareType="",GroupType="",AgentId="";
            string AirCode = "";
            string AirlineName = "";
            #region
            
            if (hidActionType.Value == "search")
            {
                #region search value set
               
                 Trip = DdlTripType.SelectedValue;
                Provider = DdlProvider.SelectedValue;
                GroupType = ddl_ptype.SelectedValue;
                 FareType = DdlFareType.SelectedValue;
                string FareTypeName = Convert.ToString(DdlFareType.SelectedItem.Text);
                 Airline = Convert.ToString(Request["hidtxtAirline"]);
                string FlightName = Convert.ToString(Request["txtAirline"]);
                if (!string.IsNullOrEmpty(FlightName))
                {
                    if (!string.IsNullOrEmpty(Airline))
                    {
                        AirlineName = Airline.Split(',')[0];
                        if (Airline.Split(',').Length > 1)
                        {
                            AirCode = Airline.Split(',')[1];
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid airline!!');", true);
                            return;
                        }
                    }
                }
                if (Provider == "6E")
                {
                    AirlineName = "Indigo";
                    AirCode = Provider;
                }
                else if (Provider == "SG")
                {
                    AirlineName = "Spicejet";
                    AirCode = Provider;
                }
                else if (Provider == "G8")
                {
                    AirlineName = "GoAir";
                    AirCode = Provider;
                }
                string TripType = Convert.ToString(DdlTripType.SelectedValue);
                string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
                string Status = Convert.ToString(DdlStatus.SelectedValue);

                string SelectedUserId = hfCustomerId.Value;
                if (Provider == "1G")
                {
                    Provider = "1G";
                }
                else if (Provider == "6E" || Provider == "SG" || Provider == "G8")
                {
                    Provider = "LCC";
                }
                else
                {
                    Provider = "0";
                }
                DivMsg.InnerHtml = "";

                if (!string.IsNullOrEmpty(SelectedUserId))
                {
                    string JoinAgentId = "";
                    int len = SelectedUserId.Split(',').Length;//> 1
                    for (int i = 0; i < len - 1; i++)
                    {
                        msgout = "";
                        GroupType = "";
                        //AgentId = SelectedUserId.Split(',')[i];
                        JoinAgentId = JoinAgentId + "'" + SelectedUserId.Split(',')[i] + "',";

                    }
                    JoinAgentId = JoinAgentId.TrimEnd(',');
                    AgentId = JoinAgentId; //"(" + JoinAgentId + ")";
                                           // hfCustomerId.Value = "";                   
                }
                else
                {
                    hfCustomerId.Value = "";
                }

                #endregion
            }
            #endregion
            grd_P_IntlDiscount.DataSource = GetRecord(Trip, Provider, AirCode, FareType, GroupType, AgentId);
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord(string Trip,string Provider,string Airline,string FareType,string GroupType,string AgentId )
    {
        DataTable dt = new DataTable();
        try
        {
           string ActionType = hidActionType.Value;
            if (con.State == ConnectionState.Closed)
                con.Open();
            adap = new SqlDataAdapter("SpSearchResultBlock", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Airline", Airline);
            adap.SelectCommand.Parameters.AddWithValue("@Provider", Provider);
            adap.SelectCommand.Parameters.AddWithValue("@Trip", Trip);
            adap.SelectCommand.Parameters.AddWithValue("@GroupType", ddl_ptype.SelectedValue);
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adap.SelectCommand.Parameters.AddWithValue("@FareType", FareType);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", ActionType);
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;           
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }


    protected void grd_P_IntlDiscount_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = e.NewEditIndex;
            //ActionTypeGrid = "select";
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void grd_P_IntlDiscount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = -1;
            //ActionTypeGrid = "select";
            BindGrid();
           
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void grd_P_IntlDiscount_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());

            //DropDownList ddl_CType = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_CodeType");
            //string CodeType = ddl_CType.SelectedValue;

            //TextBox txt_DT_Code = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txt_D_T_Code"));
            //string D_T_Code = Convert.ToString(txt_DT_Code.Text);
            //if (string.IsNullOrEmpty(D_T_Code))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter Deal/Tour Code!');", true);
            //    return;
            //}
            //DropDownList ddl_Applied_On = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_AppliedOn");
            //string AppliedOn = ddl_Applied_On.SelectedValue;

            //if ((CodeType == "PC" || CodeType == "PF") && AppliedOn == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select  Applied On!');", true);
            //    return;
            //}

            //if (CodeType == "PC" || CodeType == "PF")
            //{
            //    AppliedOn = ddl_Applied_On.SelectedValue;
            //}
            //else
            //{
            //    AppliedOn = "";
            //}
            //DropDownList ddl_FareType = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_FareType");
            //string IdType = ddl_FareType.SelectedValue;

            //if (CodeType == "PC" && IdType == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select  fare type!');", true);
            //    return;
            //}

            //if (CodeType == "PC")
            //{
            //    IdType = ddl_FareType.SelectedValue;
            //}
            //else
            //{
            //    IdType = "";
            //}


            //DropDownList ddl_Active = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_IsActive");           
            //string IsActive = ddl_Active.SelectedValue;
          
            //TextBox Txt_FltNo = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdFltNo"));
            //string FltNo = Convert.ToString(Txt_FltNo.Text);

            //TextBox TxtGrdOrginCity = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdOrginAirport"));
            //string OrginAirport = Convert.ToString(TxtGrdOrginCity.Text);

            //TextBox TxtGrdDestAirport = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdDestAirport"));
            //string DestAirport = Convert.ToString(TxtGrdDestAirport.Text);

            //TextBox TxtGrdOrginCountry = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdOrginCountry"));
            //string OrginCountry = Convert.ToString(TxtGrdOrginCountry.Text);

            //TextBox TxtGrdDestCountry = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdDestCountry"));
            //string DestCountry = Convert.ToString(TxtGrdDestCountry.Text);


            //result = UpdateRecords(Id, CodeType, D_T_Code, AppliedOn, IsActive, FltNo, OrginAirport, DestAirport, OrginCountry, DestCountry, IdType);
            grd_P_IntlDiscount.EditIndex = -1;
           // ActionTypeGrid = "select";
            BindGrid();

            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("SpSearchResultBlock", con);
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.AddWithValue("@Counter", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }
            //ActionTypeGrid = "select";
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        //ActionTypeGrid = "select";        
        this.BindGrid();
    }

    private int UpdateRecords(int Id, string CodeType, string D_T_Code, string AppliedOn, string IsActive, string FltNo, string OrginAirport, string DestAirport, string OrginCountry, string DestCountry, string IdType)
    {
        int flag = 0;
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightDealMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", Id);
            cmd.Parameters.AddWithValue("@CodeType", CodeType);
            cmd.Parameters.AddWithValue("@D_T_Code", D_T_Code);
            cmd.Parameters.AddWithValue("@AppliedOn", AppliedOn);
            cmd.Parameters.AddWithValue("@FltNo", FltNo);
            cmd.Parameters.AddWithValue("@OrginAirport", OrginAirport);
            cmd.Parameters.AddWithValue("@DestAirport", DestAirport);
            cmd.Parameters.AddWithValue("@OrginCountry", OrginCountry);
            cmd.Parameters.AddWithValue("@DestCountry", DestCountry);
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UID"]));
            cmd.Parameters.AddWithValue("@IdType", IdType);
            cmd.Parameters.AddWithValue("@ActionType", "GRIDUPDATE");
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hidActionType.Value = "search";           
            BindGrid();


        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SearchResultBlock.aspx'; ", true);
            return;
        }

    }

    private string ExcludeAgentInsert(string AirCode, string Provider, string Trip, string AirlineType, string Status, string FareType, string FareTypeName, string cityName, string executiveId)
    {
        string ExcludeMsg = "";
        string CreatedBy = Convert.ToString(Session["UID"]);
        try
        {
            #region Insert
            string AgentIdExclude = "";
            string SelectedUserIdExclude = hfCustomerIdExclude.Value;           
            
            string AddMsg = "";
            msgout = "";            
            DivMsgExclued.InnerHtml = "";
            if (!string.IsNullOrEmpty(SelectedUserIdExclude))
            {                
                string ActionType = "Exclude";
                int flag = 0;
                int len = SelectedUserIdExclude.Split(',').Length;//> 1
                for (int i = 0; i < len - 1; i++)
                {
                    msgout = "";
                    string GroupType = ddl_ptype.SelectedValue;

                    AgentIdExclude = SelectedUserIdExclude.Split(',')[i];
                    flag = Insert(0, AirCode, Provider, "True", Trip, AirlineType, Status, GroupType, AgentIdExclude, FareType, FareTypeName, ActionType, cityName, executiveId);
                    AddMsg = AddMsg + AgentIdExclude + "-" + msgout + ".<br />";
                }
                DdlProvider.SelectedValue = "0";
                DdlFareType.SelectedValue = "";
                hfCustomerIdExclude.Value = "";
                if (!string.IsNullOrEmpty(AddMsg))
                {
                    DivMsgExclued.InnerHtml = AddMsg;
                    //BindGrid();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                    //BindGrid();
                }
            }
            #endregion  
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return ExcludeMsg;

    }

}