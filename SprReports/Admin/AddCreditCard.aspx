﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="AddCreditCard.aspx.cs" Inherits="SprReports_Admin_AddCreditCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="../../newcss/sb-admin.css" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
        .HotelCards{
            display:none;
        }
        .ALLCards{
            display:none;
        }
        /*.selectiontab
        {
            position: relative;
    display: inline-block;
    padding: 10px 24px;
    margin: .3em 0 1em 0;
     width: 100%; 
    vertical-align: middle;
    color: #ffffff !important;
    font-size: 16px;
    line-height: 20px;
    border-radius: 2px;
    -webkit-font-smoothing: antialiased;
    text-align: center;
    letter-spacing: 1px;
    background: #337ab7;
    border: 1px;
    cursor: pointer;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
    
        }
        .unselectedtab
        {
            position: relative;
    display: inline-block;
    padding: 10px 24px;
    margin: .3em 0 1em 0;
     width: 100%; 
    vertical-align: middle;
    color: #fff !important;
    font-size: 16px;
    line-height: 20px;
    border-radius: 2px;
    -webkit-font-smoothing: antialiased;
    text-align: center;
    letter-spacing: 1px;
    background: #ccc;
    border: 1px;
    cursor: pointer;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
    
        }*/
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Add Credit Card</h3>
                    </div>
                    <div class="panel-body">
                      <%--  <div class="row">
                            <div class="col-md-3">
                        <asp:RadioButton runat="server" ID="rbtFlightCreditcard"  GroupName="cardselection" Checked="true" Text="Flight Credit Card" class="FlightCardsTAB selectiontab"/>
                                </div>
                            <div class="col-md-3">
                             <asp:RadioButton runat="server" ID="rbtHotelCreditcard"  GroupName="cardselection" Text="Hotel Credit Card" class="HotelCardsTAB unselectedtab"/>
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Product Type</label>
                                    <asp:DropDownList ID="ddlProduct" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="Hotel" Text="Hotel"></asp:ListItem>
                                        <asp:ListItem Value="Flight" Text="Flight"></asp:ListItem>  
                                        <asp:ListItem Value="Bus" Text="Bus"></asp:ListItem>                                  
                                    </asp:DropDownList>
                                </div>
                            </div>
                               <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip</label>
                                    <asp:DropDownList ID="ddltrip" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="D" Text="Domestic"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>                                    
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PCC :</label>
                                    <asp:TextBox ID="txtpcc" CssClass="form-control" runat="server" onkeypress="return isCharNumberKey(event)" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 ALLCards">
                                <div class="form-group FlightCards">
                                    <label for="exampleInputPassword1">Airline:</label>
                                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="4" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                </div>
                                 <div class="form-group HotelCards">
                                    <label for="exampleInputPassword1">Hotel Supplier</label>
                                    <asp:DropDownList ID="ddlHotelSupplier" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="ZUMATA" Text="ZUMATA"></asp:ListItem>
                                        <asp:ListItem Value="EXPEDIA" Text="EXPEDIA"></asp:ListItem> 
                                        <asp:ListItem Value="SuperShopper" Text="SuperShopper"></asp:ListItem>
                                        <asp:ListItem Value="GAL" Text="GAL"></asp:ListItem>                                    
                                    </asp:DropDownList>
                                </div>
                                 <div class="form-group BusCards">
                                    <label for="exampleInputPassword1">Bus Supplier</label>
                                    <asp:DropDownList ID="ddlBusSupplier" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="REDBUS" Text="REDBUS"></asp:ListItem>
                                        <asp:ListItem Value="GUJARATSERVICE" Text="GUJARATSERVICE"></asp:ListItem>                                    
                                    </asp:DropDownList>
                                </div>
                            </div>
                         
                           
                        </div>
                         <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Card Type :</label>
                                   <asp:DropDownList ID="ddlCardType" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="AX" Text="Amex"></asp:ListItem>
                                        <asp:ListItem Value="VI" Text="Visa"></asp:ListItem>
					<asp:ListItem Value="CA" Text="Master"></asp:ListItem>
					<asp:ListItem Value="DC" Text="Diner"></asp:ListItem>                                
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Card Holder Name :</label>
                                    <asp:TextBox ID="txtname" CssClass="form-control" runat="server" onkeypress="return isCharKey(event)" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Card Number:</label>
                                    <asp:TextBox ID="txtcardnumber" CssClass="form-control" runat="server" onkeypress="return isNumberKey(event)" MaxLength="16"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Confirm Card Number:</label>
                                    <asp:TextBox ID="txtcardnumber1" CssClass="form-control" onkeypress="return isNumberKey(event)" runat="server" MaxLength="16"></asp:TextBox>
                                </div>
                            </div>
                          
                        </div>
                        <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Card Expiry Date</label>
                                    <asp:TextBox ID="txtexpiry" CssClass="form-control" runat="server" MaxLength="7" onkeypress="return isExpCCDate(event)" placeholder="date formate like mm/yyyy"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                   <label for="exampleInputPassword1">Card CVV Number(optional)</label>
                                    <asp:TextBox ID="txtCVV" CssClass="form-control" runat="server" onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                   <asp:DropDownList ID="ddlstatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>                                    
                                    </asp:DropDownList>
                                </div>
                            </div>
                             
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Card Number:</label>--%>
                                     <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" OnClientClick="return ValidateCreditCard();" />
                                </div>
                            </div>
                           
                          
                        </div>
                         <div class="row">
                            <div class="col-md-12" style="width: 1200px; overflow-y: scroll;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_CCDetails" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-striped" GridLines="None" Width="100%" PageSize="30" OnRowDeleting="OnRowDeleting" OnRowDataBound="OnRowDataBound" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                                  <asp:TemplateField HeaderText="Update">
                                                    <ItemTemplate>
                                                         <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                        <a href='UpdateCreditCard.aspx?ID=<%#Eval("Counter")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                          <u>Update</u> 
                                                        </a>
                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:BoundField DataField="Product" HeaderText="Product" />
                                                 <asp:TemplateField HeaderText="PCC">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPCC" runat="server" Text='<%#Eval("PCC") %>'></asp:Label>
                                                          </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirline" runat="server" Text='<%#Eval("Airline") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                             
                                                <asp:TemplateField HeaderText="CardType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCardType" runat="server" Text='<%#Eval("CardType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CardHolderName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCardHolderName" runat="server" Text='<%#Eval("CardHolderName") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Card Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCardNumber" runat="server" Text='<%#Eval("CardNumber") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="CardExpDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCardExpDate" runat="server" Text='<%#Eval("CardExpDate") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Isactive") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               
                      
                       <asp:BoundField DataField="SupplierName" HeaderText="Supplier Name" />

                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <%--<div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>--%>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        
        function ValidateCreditCard()
        {
            if ($("#ctl00_ContentPlaceHolder1_ddlProduct").val() == "") {
                alert("Plese select product type");
                $("#ctl00_ContentPlaceHolder1_ddlProduct").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_ddltrip").val() == "0") {
                alert("Plese select trip");
                $("#ctl00_ContentPlaceHolder1_ddltrip").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_txtpcc").val() == "")
            {
                alert("Plese enter pcc");
                $("#ctl00_ContentPlaceHolder1_txtpcc").focus();
                return false;
            } 

            if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Flight") {
                if ($("#txtAirline").val() == "") {
                    alert("Plese select airline");
                    $("#txtAirline").focus();
                    return false;
                }
                if ($("#hidtxtAirline").val() == "") {
                    alert("Plese select valid airline");
                    $("#txtAirline").focus();
                    return false;
                }
           }
           else if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Hotel") {
                if ($("#ctl00_ContentPlaceHolder1_ddlHotelSupplier").val() == "") {
                    alert("Plese select Hotel Supplier");
                    $("#ctl00_ContentPlaceHolder1_ddlHotelSupplier").focus();
                    return false;
                }
           }
           else if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Bus") {
               if ($("#ctl00_ContentPlaceHolder1_ddlBusSupplier").val() == "") {
                   alert("Plese select Bus Supplier");
                   $("#ctl00_ContentPlaceHolder1_ddlBusSupplier").focus();
                   return false;
               }
           }
            if ($("#ctl00_ContentPlaceHolder1_ddlCardType").val() == "0") {
                alert("Plese select card type");
                $("#ctl00_ContentPlaceHolder1_ddlCardType").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_txtcardnumber").val() == "") {
                alert("Plese enter card number");
                $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                return false;
            }
            
            if ($.trim($("#ctl00_ContentPlaceHolder1_ddlCardType").val()).toUpperCase() == "AX") {
                if ($.trim($("#ctl00_ContentPlaceHolder1_txtcardnumber").val()).length != 15) {
                    alert('Credit Card no seems to be incorrect.Amex card length must be 15 digits long');
                    $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                    return false;
                }
                if ($.trim($("#ctl00_ContentPlaceHolder1_txtcardnumber").val()[0]) != "3") {
                    alert('Amex credit card no. must start with 3');
                    $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                    return false;
                }
            }

            if ($.trim($("#ctl00_ContentPlaceHolder1_ddlCardType").val()).toUpperCase() == "VI") {
                if ($.trim($("#ctl00_ContentPlaceHolder1_txtcardnumber").val()).length != 16) {
                    alert('Credit Card no seems to be incorrect.Visa card length must be 16 digits long');
                    $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                    return false;
                }
                if ($.trim($("#ctl00_ContentPlaceHolder1_txtcardnumber").val()[0]) != "4") {
                    alert('Visa credit card no. must start with 4');
                    $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                    return false;
                }
            }
            if ($.trim($("#ctl00_ContentPlaceHolder1_ddlCardType").val()).toUpperCase() == "CA") {
                if ($.trim($("#ctl00_ContentPlaceHolder1_txtcardnumber").val()).length != 16) {
                    alert('Credit Card no seems to be incorrect.Master card length must be 16 digits long');
                    $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                    return false;
                }
                if ($.trim($("#ctl00_ContentPlaceHolder1_txtcardnumber").val()[0]) != "5") {
                    alert('Master credit card no. must start with 5');
                    $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                    return false;
                }
            }
          
            if ($("#ctl00_ContentPlaceHolder1_txtcardnumber1").val() == "") {
                alert("Plese enter confirm card number");
                $("#ctl00_ContentPlaceHolder1_txtcardnumber1").focus();
                return false;
            }

        
            if ($("#ctl00_ContentPlaceHolder1_txtcardnumber1").val() != $("#ctl00_ContentPlaceHolder1_txtcardnumber").val())
            {
                alert("Check card number and confirm card number");
                $("#ctl00_ContentPlaceHolder1_txtcardnumber").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_txtexpiry").val() == "") {
                alert("Please enter expiry date");
                $("#ctl00_ContentPlaceHolder1_txtexpiry").focus();
                return false;
            }
            var status = ValidateCCExpDate();
            if (status == false)
                return false;
            if ($("#ctl00_ContentPlaceHolder1_ddlstatus").val() == "0") {
                alert("Please select status");
                $("#ctl00_ContentPlaceHolder1_ddlstatus").focus();
                return false;
            }
            if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Hotel" && $("#ctl00_ContentPlaceHolder1_ddlHotelSupplier").val() == "ZUMATA") {
                if ($("#ctl00_ContentPlaceHolder1_txtCVV").val() == "") {
                    alert("card verification number is mandetory for Zumata API");
                    $("#ctl00_ContentPlaceHolder1_txtCVV").focus();
                    return false;
                }
            }
            
            //return false;
        }

        function isExpCCDate(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 47) {
                return true;
            }
            else {

                return false;
            }
        }
        function isNumberKey(evt) {
            //debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {

                return false;
            }
        }
        function isCharKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                //alert("please enter Alphabets  only");
                return false;
            }
        }
        function isCharNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122 || charCode >= 48 && charCode <= 57 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                //alert("please enter Alphabets  only");
                return false;
            }
        }
        function ValidateCCExpDate() {
            
            var ExpCCdate = $("#ctl00_ContentPlaceHolder1_txtexpiry").val();
            if (ExpCCdate.length > 0) {
                if (ExpCCdate.length == 7) {
                    ExpCCdate = ExpCCdate.substr(2, 1);
                    if (ExpCCdate != '/') {
                        alert("Please write correct date formate like mm/yyyy ")
                        $("#ctl00_ContentPlaceHolder1_txtexpiry").focus();
                        return false;
                    }
                }
                else {
                    alert("Please write correct date formate like mm/yyyy ")
                    $("#ctl00_ContentPlaceHolder1_txtexpiry").focus();
                    return false;
                }
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            var today = yyyy + '/' + mm + '/' + dd;
            var CCExp = $.trim($("#ctl00_ContentPlaceHolder1_txtexpiry").val());
            CCExp = CCExp.split('/')[1] + '/' + CCExp.split('/')[0] + '/' + dd;

            var CCExpDate = Date.parse(CCExp); //new Date(CCExp);
            var todayDate = Date.parse(today); //new Date(today);
            if (CCExpDate < todayDate) {
                alert('Your credit card date is expired. Please update credit card information.');
                return false;
            }
            return true;
        }
        //var conceptName = $('#aioConceptName').find(":selected").text();  $( "#myselect" ).val();
        $(document).ready(function () {
           
            $("#ctl00_ContentPlaceHolder1_ddlProduct").change(function () {
                $(".ALLCards").show();
                if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Hotel") {
                    $("#ctl00_ContentPlaceHolder1_txtpcc").val("richatravels");
                    $(".HotelCards").show();
                    $(".FlightCards").hide();
                    $(".BusCards").hide();
                }
                else if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Flight") {
                    $("#ctl00_ContentPlaceHolder1_txtpcc").val("");
                    $(".FlightCards").show();
                    $(".HotelCards").hide();
                    $(".BusCards").hide();
                }
                else if ($('#ctl00_ContentPlaceHolder1_ddlProduct').find(":selected").text() == "Bus") {
                    $("#ctl00_ContentPlaceHolder1_txtpcc").val("");
                    $(".FlightCards").hide();
                    $(".HotelCards").hide();
                    $(".BusCards").show();
                }
                else 
                    $(".ALLCards").hide();
            });
            //$(".FlightCardsTAB").click(function () { 
            //    $(".FlightCards").show();
            //    $(".HotelCards").hide();
            //    $("#ctl00_ContentPlaceHolder1_txtpcc").val("");
            //    $(".HotelCardsTAB").removeClass("unselectedtab");
            //    $(".FlightCardsTAB").removeClass("unselectedtab");
            //    $('.FlightCardsTAB').addClass("selectiontab");
            //    $('.HotelCardsTAB').addClass("unselectedtab");
            //});
            //$(".HotelCardsTAB").click(function () {
            //    $(".HotelCards").show();
            //    $(".FlightCards").hide();
            //    $("#ctl00_ContentPlaceHolder1_txtpcc").val("richatravels");
            //    $(".HotelCardsTAB").removeClass("unselectedtab");
            //    $(".FlightCardsTAB").removeClass("unselectedtab");
            //    $('.FlightCardsTAB').addClass("unselectedtab");
            //    $('.HotelCardsTAB').addClass("selectiontab");
            //});
        });
    </script>

</asp:Content>

