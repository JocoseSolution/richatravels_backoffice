﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_UpdateFlightHoldBookingSetting : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    public string msgout = "";
    public string TxtAgentId = "";
    public string HdnAgentId = "";
    public string TxtAirline = "";
    public string HdnAirline = "";


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                string Id = Request.QueryString["id"];
                
                if (!string.IsNullOrEmpty(Convert.ToString(Id)))
                {
                    if (!IsPostBack)
                    {
                        HdnId.Value = Id;
                        //BindComminssion(Id);
                        ddl_ptype.AppendDataBoundItems = true;
                        ddl_ptype.Items.Clear();

                        ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                        ddl_ptype.DataTextField = "GroupType";
                        ddl_ptype.DataValueField = "GroupType";
                        ddl_ptype.DataBind();
                        ddl_ptype.Items.Insert(0, new ListItem("-- Select Type --", "ALL"));
                        BindData(Id);
                    }
                }
                else
                {
                    Response.Redirect("HoldBookingSetting.aspx", false);
                }
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    public DataTable GetAirline()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SP_GetAirlinenames", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Convert.ToString(HdnId.Value)) && (HdnHoldId.Value == Convert.ToString(HdnId.Value)) && !string.IsNullOrEmpty(Convert.ToString(TxtCharges.Text)))
            {
                #region Insert
                string GroupType = ddl_ptype.SelectedValue;
                string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                if (!string.IsNullOrEmpty(AgentId))
                {
                    GroupType = "";
                }
                string Airline = Convert.ToString(Request["hidtxtAirline"]);
                string FlightName = Convert.ToString(Request["txtAirline"]);
                string AirCode = "";
                string AirlineName = "";

                if (!string.IsNullOrEmpty(FlightName))
                {
                    if (!string.IsNullOrEmpty(Airline))
                    {
                        AirlineName = Airline.Split(',')[0];
                        if (Airline.Split(',').Length > 1)
                        {
                            AirCode = Airline.Split(',')[1];
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                            return;
                        }
                    }
                }
                else
                {
                    AirlineName = "ALL";
                    AirCode = "ALL";
                }

                string TripType = Convert.ToString(DdlTripType.SelectedValue);
                string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
                string IsActive = Convert.ToString(DdlStatus.SelectedValue);
                int flag = 0;
                double Charges = Convert.ToDouble(TxtCharges.Text);
                msgout = "";
                if (HdnHoldId.Value == Convert.ToString(HdnId.Value))
                {
                    flag = UpdateRecords(Convert.ToInt32(HdnId.Value), GroupType, AgentId, AirCode, AirlineName, TripType, TripTypeName, IsActive, Charges);
                }                
                if (flag > 0)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc()", true);
                }
                else
                {
                    if (msgout == "EXISTS")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('already exists.');", true);                       
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                    }                    
                }
                #endregion
            }
            else
            {
                if (string.IsNullOrEmpty(Convert.ToString(TxtCharges.Text))) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter charges!!');", true);
                    return;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "TryAgain()", true);
                }
                
            }

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again..');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='HoldBookingSetting.aspx'; ", true);
            return;
        }

    }
    private int UpdateRecords(int Id, string GroupType, string AgentId, string AirCode, string AirlineName, string TripType, string TripTypeName, string IsActive, double Charges)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "UPDATE";            
        try
        {
            //SqlCommand cmd = new SqlCommand("SpFlightHoldBookingSetting", con);
            SqlCommand cmd = new SqlCommand("SpFlightHoldBookingCharges", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", Id);
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@AirCode", AirCode);
            cmd.Parameters.AddWithValue("@AirlineName", AirlineName);
            cmd.Parameters.AddWithValue("@TripType", TripType);
            cmd.Parameters.AddWithValue("@TripTypeName", TripTypeName);
            cmd.Parameters.AddWithValue("@Charges", Charges);
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }
    
    public DataTable GetRecord( int HoldId)
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SpFlightHoldBookingCharges", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@ID", HoldId);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETBYID");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }
        
    public void BindData(string HoldId)
    {

        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(HoldId))
        {
            dt = GetRecord(Convert.ToInt32(HoldId));
            if (dt.Rows.Count > 0)
            {
                //ID, TripType, TripTypeName, GroupType, AgentId, AirCode, AirlineName, IsActive, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy
                int ID = Convert.ToInt32(dt.Rows[0]["ID"]);
                HdnHoldId.Value = Convert.ToString(ID);
                string TripType = Convert.ToString(dt.Rows[0]["TripType"]);                
                string TripTypeName = Convert.ToString(dt.Rows[0]["TripTypeName"]);
                DdlTripType.SelectedValue = TripType;
                string GroupType = Convert.ToString(dt.Rows[0]["GroupType"]);
                ddl_ptype.SelectedValue = GroupType;
                string IsActive = Convert.ToString(dt.Rows[0]["IsActive"]);
                DdlStatus.SelectedValue = IsActive;
                string AgentId = Convert.ToString(dt.Rows[0]["AgentId"]);
                TxtCharges.Text = Convert.ToString(dt.Rows[0]["Charges"]);
                if (!string.IsNullOrEmpty(Convert.ToString(AgentId)))
                {
                    TxtAgentId = AgentId;
                    HdnAgentId = AgentId;
                }
                else
                {
                    TxtAgentId = "";
                    HdnAgentId = "";
                }
                string AirCode = Convert.ToString(dt.Rows[0]["AirCode"]);
                string AirlineName = Convert.ToString(dt.Rows[0]["AirlineName"]);
                if (!string.IsNullOrEmpty(Convert.ToString(AirCode)) && !string.IsNullOrEmpty(Convert.ToString(AirlineName)))
                {
                    TxtAirline = AirlineName + "(" + AirCode + ")";
                    HdnAirline = AirlineName + "," + AirCode;
                    //Air Canada(AC)
                    //hidtxtAirline
                    //Air Canada,AC
                }
                else
                {                  
                    TxtAirline = "";
                    HdnAirline = "";
                }             

            }
            else
            {
                BtnSubmit.Visible = false;
            }
        }

    }
}