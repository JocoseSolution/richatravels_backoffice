﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ConvertToDistributor.aspx.cs" Inherits="SprReports_Admin_ConvertToDistributor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css?V=0")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js?V=2")%>"></script>
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <script language="javascript" type="text/javascript">   
      
        function Validate() {
            if ($("#txtAgencyName").val() == "") {
                alert("Please select agent id");
                $("#txtAgencyName").focus();
                return false;
            }

        }
    </script>


    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Profile > Convert To Distributor</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">User Id/Agency Id :</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                        class="form-control" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <%--<asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click" />--%>

                                    <%--<asp:Button ID="BtnDebit" runat="server" Text="DEBIT" OnClick="BtnDebit_Click" CssClass="button buttonBlue"  OnClientClick="return Check();" />--%>
                                    <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClientClick="return Validate();" OnClick="BtnSearch_Click" />


                                </div>
                            </div>


                        </div>
                        <div class="clear"></div>
                        <div class="row" id="DivAgencyDetals" runat="server" visible="false">

                            <div class="col-md-12 mtable">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Total Balance</td>
                                        <td id="tdTotalBAl" runat="server"></td>
                                        <td>User Id/Agency Id</td>
                                        <td id="tdAgentId" runat="server"></td>
                                        <td>Mobile</td>
                                        <td id="tdMobile" runat="server"></td>
                                    </tr>
                                    <tr>
                                        <td>Total Credit Limit</td>
                                        <td id="tdAgentCredit" runat="server"></td>
                                        <td>Name</td>
                                        <td id="tdAgnetName" runat="server"></td>
                                        <td>Email</td>
                                        <td id="tdEmail" runat="server"></td>

                                    </tr>

                                    <tr>
                                        <td>Due Amount</td>
                                        <td id="tdDueAmount" runat="server"></td>
                                        <td>Agency Name</td>
                                        <td id="tdAgencyName" runat="server"></td>
                                        <td>Address</td>
                                        <td id="tdAddress" runat="server"></td>
                                    </tr>


                                </table>
                            </div>

                        <div class="form-group" id="UploadSubmitButton" runat="server" visible="false">
                            <br />
                            <asp:Button ID="BtnSubmit" runat="server" Text="Convert To Distributor" OnClick="BtnSubmit_Click" CssClass="button buttonBlue pull-right"  />
                            &nbsp;&nbsp;
                        </div>
                        </div>

                    </div>
                        
                    </div>
                </div>
            </div>      
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>





</asp:Content>

