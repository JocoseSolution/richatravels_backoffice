﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="Agent_Details.aspx.vb" Inherits="Reports_Admin_Agent_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%-- <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%-- <style type="text/css">
        input[type="text"], input[type="password"], select, radio, legend, fieldset
        {
            border: 1px solid #004b91;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>--%>
    <%--<link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />--%>
    <%--<script src="../../JS/lytebox.js" type="text/javascript"></script>--%>
    <%--  <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%--<style type="text/css">
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
       <div class="col-md-12"  >
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                Profile > Agency Details
                           </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"  id="divFromDate" runat="server">
                                    <label for="exampleInputPassword1">Registration From :</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"  id="divToDate" runat="server">
                                    <label for="exampleInputPassword1">Registration To :</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="td_SBS" runat="server">
                                    <label for="exampleInputPassword1" id="td_ddlSBS" runat="server">Search By Stockist :</label>
                                    <asp:DropDownList ID="ddl_stock" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        <asp:ListItem Text="All Stockist" Value="ALL"></asp:ListItem>
                                        <asp:ListItem Text="Stockist Agent" Value="STAG"></asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Search Agency :</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                        class="form-control" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="tr_AgentType" runat="server">
                                    <label for="exampleInputPassword1" id="tr_GroupType" runat="server">Agent Type :</label>
                                    <asp:DropDownList ID="DropDownListType" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group" id="tr_SalesPerson" runat="server">
                                    <label for="exampleInputPassword1" id="tr_ddlSalesPerson" runat="server">Search Sales Person :</label>
                                    <asp:DropDownList ID="DropDownListSalesPerson" runat="server" AppendDataBoundItems="true"
                                        CssClass="form-control">
                                        <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                               <div class="col-md-4">
                                <div class="form-group" id="Div1" runat="server">
                                    <label for="exampleInputPassword1" id="Label2" runat="server">OutStanding Bal :</label>
                                    <asp:DropDownList ID="DDLDUE" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        <asp:ListItem Text="DueAmount" Value="DueAmount"></asp:ListItem>
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group" id="Div2" runat="server">
                                    <label for="exampleInputPassword1" id="Label3" runat="server">Email ID</label>
                                     <asp:TextBox ID="TxtEmail" CssClass="form-control" runat="server" plceholder="enter eamil.."></asp:TextBox>
                                </div>
                            </div>
                              <div class="col-md-4">
                                <div class="form-group" id="Div3" runat="server">
                                    <label for="exampleInputPassword1" id="Label4" runat="server">Mobile No</label>
                                     <asp:TextBox ID="Textmobile" CssClass="form-control" runat="server" plceholder="enter mobile no.."></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_Search" runat="server" Text="Search" CssClass="button buttonBlue" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="export" runat="server" Text="Export" CssClass="button buttonBlue" />

                                </div>
                            </div>
                        </div>



                        <div class="row" style="background-color: #fff; overflow: auto;" runat="server">

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        AllowSorting="True" PageSize="25" CssClass="table" GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Debit/Credit" SortExpression="user_id" Visible="false">
                                                <ItemTemplate>
                                                    <a target="_blank" href="../Distr/UploadAmount.aspx?AgentID=<%#Eval("user_id")%>"
                                                        rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Debit/Credit </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Agency Name" SortExpression="Agency_Name" ItemStyle-ForeColor="Black">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User_Id" SortExpression="user_id" ItemStyle-ForeColor="Black">
                                                <ItemTemplate>
                                                    <a href='Update_Agent.aspx?AgentID=<%#Eval("user_id")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("user_id")%>'></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AgencyId" HeaderText="Agency ID" SortExpression="AgencyId" />
                                            <asp:BoundField DataField="Agent_Type" HeaderText="Agent_Type" SortExpression="Agent_Type" />
                                            <%-- <asp:BoundField DataField="Crd_Limit" HeaderText="Credit Limit" SortExpression="Crd_Limit" />--%>
                                            <asp:BoundField DataField="timestamp_create" HeaderText="Registration_Date" SortExpression="timestamp_create" />
                                            <asp:BoundField DataField="Crd_Trns_Date" HeaderText="Transaction_Date" SortExpression="timestamp_create" />
                                            <asp:BoundField DataField="Mobile" HeaderText="Mobile" SortExpression="Mobile" />
                                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                            <asp:BoundField DataField="SalesExecID" HeaderText="Sales Ref." SortExpression="SalesExecID" />
                                            <asp:BoundField DataField="Agent_status" HeaderText="Status" SortExpression="Agent_status" />
                                            <asp:BoundField DataField="Online_Tkt" HeaderText="Online_Tkt" SortExpression="Online_Tkt" />
					<asp:BoundField DataField="Distr" HeaderText="Dist" SortExpression="Balance" />
                                            <asp:BoundField DataField="Balance" HeaderText="Balance" SortExpression="Balance" />
                                            <asp:BoundField DataField="DueAmount" HeaderText="DueAmount" SortExpression="DueAmount" />
                                            <asp:BoundField DataField="AgentLimit" HeaderText="AgentLimit" SortExpression="AgentLimit" />
                                            <asp:BoundField DataField="NamePanCard" HeaderText="NamePanCard" SortExpression="NamePanCard" />
                                            <asp:BoundField DataField="PanNo" HeaderText="PanNo" SortExpression="AgentLimit" />
											 <asp:BoundField DataField="IrctcId" HeaderText="IrctcId" SortExpression="IrctcId" />
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" Font-Bold="False" Font-Strikeout="False" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>


                        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>


</asp:Content>
