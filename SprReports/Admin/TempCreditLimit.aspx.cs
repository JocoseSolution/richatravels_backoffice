﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_TempCreditLimit : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    SqlTransactionNew objSql = new SqlTransactionNew();
    SMSAPI.SMS objSMSAPI = new SMSAPI.SMS();
    private SqlTransaction ST = new SqlTransaction();
    string msgout = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        lblDistrId.Text = "";

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivDetails.Visible = false;
        BalDetails.Visible = false;
        DivAgencyDetals.Visible = false;
        DivUpdateButton.Visible = false;
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        //if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        //{
        //}
        //else
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            //string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string AgentLimit = TxtAgentCredit.Text;
            int flag = 0;
            #region Validation

            //string AgencyId = "";
            //string UserId = "";
            //if (!string.IsNullOrEmpty(TxtAgencyName))
            //{
            //    if (string.IsNullOrEmpty(HiddenAgentId))
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //        return;
            //    }                 
            //    if (TxtAgencyName.Split('-').Length > 1)
            //    {
            //        if(TxtAgencyName.Split('-')[1].Split('(').Length>1)
            //        {
            //           AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
            //           UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
            //            if(UserId !=HiddenAgentId)
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //                return;
            //            }                        
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
            //        return;
            //    }

            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!');", true);
            //    return;
            //}
            //if (string.IsNullOrEmpty(AgentLimit))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter credit limit amount!');", true);
            //    return;
            //}

            //if (string.IsNullOrEmpty(TxtRemark.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter remark!');", true);
            //    return;
            //}

            #endregion validation

            #region Update Limit
            if (!string.IsNullOrEmpty(hdnUserId.Value) && !string.IsNullOrEmpty(AgentLimit) && !string.IsNullOrEmpty(hdnAgencyId.Value))
            {
                string LimitExpiryDate = null;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.Form["ToDate"])))
                {
                    string ExpiryDate = Convert.ToString(Request.Form["ToDate"]);
                    DateTime temp = DateTime.ParseExact(ExpiryDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    LimitExpiryDate = string.Format("{0:yyyy-MM-dd}", temp);
                }

                //if(UserId ==HiddenAgentId)
                //{
                msgout = "";
                flag = UpdateRecords(hdnUserId.Value, hdnAgencyId.Value, AgentLimit, "CREDIT", TxtRemark.Text, LimitExpiryDate);
                if (flag > 1)
                {

                    try
                    {
                        string smsStatus = "";
                        string smsMsg = "";


                        DataTable dtpnr = new DataTable();
                        DataTable SmsCrd = new DataTable();
                        SqlTransaction objDA = new SqlTransaction();
                        SmsCrd = objDA.SmsCredential(SMS.AIRBOOKINGDOM.ToString()).Tables[0];

                        if (SmsCrd.Rows.Count > 0 && Convert.ToBoolean(SmsCrd.Rows[0]["Status"]) == true)
                        {
                            DataSet AgencyDs_new = objDA.GetAgencyDetails(hdnUserId.Value);
                            //smsStatus = objSMSAPI.Credit_Limit_SET(TxtRemark.Text, ref smsMsg, hdnUserId.Value, AgencyDs_new.Tables[0].Rows[0]["Mobile"].ToString(), AgentLimit, SmsCrd, "CREDIT");
                            // smsStatus = objSMSAPI.Credit_Limit_SET(TxtRemark.Text, hdnUserId.Value, AgencyDs_new.Tables[0].Rows[0]["Mobile"].ToString(), AgentLimit, SmsCrd, "CREDIT");
                            //smsStatus = objSMSAPI.WalletTopup(orderID, TxtAmout.Text, Session["UID"], AgencyDs_new.Tables[0].Rows[0]["Mobile"], smsMsg, SmsCrd);
                            objSql.SmsLogDetails(hdnUserId.Value, AgencyDs_new.Tables[0].Rows[0]["Mobile"].ToString(), smsMsg, smsStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                    }


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + msgout + "');", true);

                    OTPSendID.Visible = true;
                    RequestOTP.Visible = false;
                    RequestOTPButton.Visible = false;
                    UploadSubmitButton.Visible = false;
                    btnOtpSend.Text = "Send OTP";
                }
                else
                {
                    if (!string.IsNullOrEmpty(msgout))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + msgout + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please try again');", true);
                    }

                }

                BindGrid(hdnUserId.Value);
                //TxtAgentCredit.Text = "";
                TxtRemark.Text = "";

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                //    return;
                //}

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id and credit amount !!');", true);
                return;
            }
            #endregion
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='TempCreditLimit.aspx'; ", true);
            return;
        }
    }

    public void BindGrid(string AgentId)
    {
        try
        {
            SqlTransaction ST = new SqlTransaction();
            DataSet ds = ST.GetAgencyDetails(AgentId);
            #region Set value in text box
            double CashLimit = 0;
            double AgentCreditLimit = 0;
            double DueAmt = 0;
            double FixedLimit = 0;
            double TempLimit = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DivUpdateButton.Visible = true;
                BalDetails.Visible = true;
                DivDetails.Visible = true;
                DivAgencyDetals.Visible = true;
                ds.Tables[0].Rows[0]["Title"].ToString();
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"])))
                {
                    CashLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["Crd_Limit"]);
                    //(ds.Tables[0].Rows[0]["Crd_Limit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"])))
                {
                    AgentCreditLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["AgentLimit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"])))
                {
                    DueAmt = Convert.ToDouble(ds.Tables[0].Rows[0]["DueAmount"]);
                }
                if (CashLimit > AgentCreditLimit)
                {
                    CashLimit = CashLimit - AgentCreditLimit;
                }
                else
                {
                    CashLimit = 0;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["FixedLimit"])))
                {
                    FixedLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["FixedLimit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["TempLimit"])))
                {
                    TempLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["TempLimit"]);
                }
                //FixedLimit, FixedLimitTrnsDate, TempLimit, TempLimitTrnsDate, TempLimitFromDate,TempLimitToDate

                txtTotalCreditLimit.Text = Convert.ToString(AgentCreditLimit);
                txtFixedCreditLimit.Text = Convert.ToString(FixedLimit);
                TxtAgentCredit.Text = Convert.ToString(TempLimit);
                TxtAvalBal.Text = Convert.ToString(CashLimit);
                TxtDueAmount.Text = Convert.ToString(DueAmt).Replace('-', ' ').Trim(); //Convert.ToString(DueAmt).Remove('-');
                lblAgentDetails.Text = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]) + "(" + Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]) + ")";
                hdnAgencyId.Value = Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                hdnUserId.Value = Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]);


                #region Agency Table tr Bind

                tdTotalBAl.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"]);
                tdAgentId.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["user_id"]) + "/" + Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                tdMobile.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);

                tdAgentCredit.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"]);
                tdAgnetName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Title"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Fname"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Lname"]);
                tdEmail.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                tdDueAmount.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"]);
                tdAgencyName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]);
                tdAddress.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["TempLimitToDate"]).Trim()))
                {
                    spanExpiryDate.Controls.Clear();
                    //spanExpiryDate.InnerHtml = "<input type='text' name='To' id='To' readonly='readonly' class='form-control' value='" + Convert.ToString(ds.Tables[0].Rows[0]["LimitExpiryDate"]).Trim() + "' />";
                    spanExpiryDate.InnerHtml = "<input type='text' name='ToDate' id='ToDate' readonly='readonly' class='form-control' value='" + Convert.ToString(ds.Tables[0].Rows[0]["TempLimitToDate"]).Trim() + "' />";
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["TempLimitToDate"]).Trim()))
                    {
                        btnClearDateField.Visible = true;
                    }
                    else
                    {
                        btnClearDateField.Visible = false;
                    }
                }
                #endregion


                if (Convert.ToString(ds.Tables[0].Rows[0]["Distr"]).ToUpper() == "HEADOFFICE")
                {
                    BtnSubmit.Visible = true;
                    //lblDistrId.Text = "Stockist Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                }
                else
                {
                    BtnSubmit.Visible = false;
                    lblDistrId.Text = "Stockist Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Set credit limit not allowed,because agent of stockist');", true);
                    return;
                }

            }
            else
            {
                DivUpdateButton.Visible = false;
                BalDetails.Visible = false;
                DivDetails.Visible = false;
                DivAgencyDetals.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record not found.');", true);
                return;
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    private int UpdateRecords(string UserId, string AgencyId, string AgentCredit, string ActionType, string Remark, string LimitExpiryDate)
    {
        int flag = 0;
        try
        {

            string RandomNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
            string InvoiceNo = "CL" + RandomNo.Substring(7, 13);
            string ipaddress;
            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipaddress == "" || ipaddress == null)
                ipaddress = Request.ServerVariables["REMOTE_ADDR"];

            //SP_INSERTUPLOADDETAILS_TRANSACTION
            SqlCommand cmd = new SqlCommand("SP_SET_AGENTCREDITLIMIT_TEMP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId);
            cmd.Parameters.AddWithValue("@AgentCredit", Convert.ToDouble(AgentCredit));
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UID"]));
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.AddWithValue("@IPAddress", ipaddress);
            cmd.Parameters.AddWithValue("@Remark", Remark);
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
            cmd.Parameters.AddWithValue("@LimitExpiryDate", LimitExpiryDate);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Session["TempCreditLimit"] = null;
            Session["TempDate"] = null;
            Session["TempRemark"] = null;
            Session["TxtAgencyName"] = null;
            Session["HiddenAgentId"] = null;
            Session["GenratedOTP"] = null;

            string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string AgentLimit = TxtAgentCredit.Text;
            #region Validation

            string AgencyId = "";
            string UserId = "";
            if (!string.IsNullOrEmpty(TxtAgencyName))
            {
                if (string.IsNullOrEmpty(HiddenAgentId))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
                if (TxtAgencyName.Split('-').Length > 1)
                {
                    if (TxtAgencyName.Split('-')[1].Split('(').Length > 1)
                    {
                        AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
                        UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
                        if (UserId != HiddenAgentId)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!!');", true);
                return;
            }

            #endregion validation

            if (!string.IsNullOrEmpty(HiddenAgentId))
            {
                #region Bind Grid
                //string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                BindGrid(HiddenAgentId);
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id and credit amount !!');", true);
                return;
            }

            Session["TxtAgencyName"] = TxtAgencyName;
            Session["HiddenAgentId"] = HiddenAgentId;

            OTPSendID.Visible = true;
            RequestOTP.Visible = false;
            RequestOTPButton.Visible = false;
            UploadSubmitButton.Visible = false;
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='TempCreditLimit.aspx'; ", true);
            return;
        }
    }




    protected void OtpSend_Click(object sender, EventArgs e)
    {
        btnOtpSend.Text = "Please Wait...";
        string tempCreditLimit = TxtAgentCredit.Text;
        if (!string.IsNullOrEmpty(tempCreditLimit))
        {
            Session["TempCreditLimit"] = tempCreditLimit;
            string tempDate = !string.IsNullOrEmpty(Request["ToDate"].ToString()) ? Request["ToDate"].ToString().Trim() : string.Empty;
            if (!string.IsNullOrEmpty(tempDate))
            {
                Session["TempDate"] = tempDate;
                string tempRemark = TxtRemark.Text;
                if (!string.IsNullOrEmpty(tempRemark))
                {
                    Session["TempRemark"] = tempRemark;
                    string TxtAgencyName = Session["TxtAgencyName"] != null ? Session["TxtAgencyName"].ToString() : string.Empty;
                    string HiddenAgentId = Session["HiddenAgentId"] != null ? Session["HiddenAgentId"].ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(TxtAgencyName) && !string.IsNullOrEmpty(HiddenAgentId))
                    {
                        DataTable SmsCrd = new DataTable();
                        SqlTransaction objDA = new SqlTransaction();

                        SmsCrd = objDA.SmsCredential(SMS.AIRBOOKINGDOM.ToString()).Tables[0];

                        if (SmsCrd.Rows.Count > 0 && Convert.ToBoolean(SmsCrd.Rows[0]["Status"]) == true)
                        {
                            DataSet AgencyDs_new = objDA.GetAgencyDetails(hdnUserId.Value);

                            string genrateotp = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 6).ToUpper();
                            Session["GenratedOTP"] = genrateotp;
                            string smstext = string.Empty;
                            string subject = "Your one time password (OTP) is " + genrateotp + " against upload temporary credit limit Rs. " + tempCreditLimit + " for Agency ID login : " + HiddenAgentId + ", valid for next 5 minutes";
                            string smsStatus = objSMSAPI.SendCreditLimitOTP(HiddenAgentId, "9825326799", tempCreditLimit, ref smstext, subject, genrateotp, SmsCrd);
                            int iotpsentid = objSql.SmsLogDetails(hdnUserId.Value, "9825326799", "", "");
                            int OTPSave = ST.InsurtSMSTransactionOTP(genrateotp, HiddenAgentId, AgencyDs_new.Tables[0].Rows[0]["Agency_Name"].ToString(), tempCreditLimit, "9508278990", subject);
                        }


                        BindGrid(HiddenAgentId);

                        OTPSendID.Visible = false;
                        RequestOTP.Visible = true;
                        RequestOTPButton.Visible = true;

                        TxtAgentCredit.Text = Session["TempCreditLimit"].ToString();
                        TxtRemark.Text = Session["TempRemark"].ToString();
                        spanExpiryDate.Controls.Clear();                        
                        spanExpiryDate.InnerHtml = "<input type='text' name='ToDate' id='ToDate' readonly='readonly' class='form-control' value='" + Session["TempDate"].ToString().Trim() + "' />";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter remark.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter expiry  date.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter temporary credit limit.');", true);
            return;
        }



    }

    protected void VerifyOtp_Click(object sender, EventArgs e)
    {
        btnVerifyOtp.Text = "Please Wait...";
        string TxtAgencyName = Session["TxtAgencyName"] != null ? Session["TxtAgencyName"].ToString() : string.Empty;
        string HiddenAgentId = Session["HiddenAgentId"] != null ? Session["HiddenAgentId"].ToString() : string.Empty;

        string OTP = Session["GenratedOTP"] != null ? Session["GenratedOTP"].ToString() : string.Empty;

        if (!string.IsNullOrEmpty(TxtAgencyName) && !string.IsNullOrEmpty(HiddenAgentId) && !string.IsNullOrEmpty(OTP))
        {
            BindGrid(HiddenAgentId);
            TxtRemark.Text = Session["TempRemark"] != null ? Session["TempRemark"].ToString() : string.Empty;
            TxtAgentCredit.Text = Session["TempCreditLimit"] != null ? Session["TempCreditLimit"].ToString() : string.Empty;
            spanExpiryDate.Controls.Clear();
            spanExpiryDate.InnerHtml = "<input type='text' name='ToDate' id='ToDate' readonly='readonly' class='form-control' value='" + Session["TempDate"].ToString().Trim() + "' />";

            string enteredOTP = txtOTP.Text;
            if (OTP == enteredOTP)
            {
                UploadSubmitButton.Visible = true;
                OTPSendID.Visible = false;
                RequestOTPButton.Visible = false;
                RequestOTP.Visible = false;               
            }
            else
            {
                btnVerifyOtp.Text = "Verify OTP";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Invalid otp !!');", true);
                return;
            }
        }
    }    
}