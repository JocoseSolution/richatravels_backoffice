﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_ConvertToDistributor : System.Web.UI.Page
{

    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    SqlTransactionNew objSql = new SqlTransactionNew();
    SMSAPI.SMS objSMSAPI = new SMSAPI.SMS();
    private SqlTransaction ST = new SqlTransaction();
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivAgencyDetals.Visible = false;

        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        //if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        //{
        //}
        //else
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Session["TxtAgencyName"] = null;
            Session["HiddenAgentId"] = null;


            string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();

            #region Validation

            string AgencyId = "";
            string UserId = "";
            if (!string.IsNullOrEmpty(TxtAgencyName))
            {
                if (string.IsNullOrEmpty(HiddenAgentId))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
                if (TxtAgencyName.Split('-').Length > 1)
                {
                    if (TxtAgencyName.Split('-')[1].Split('(').Length > 1)
                    {
                        AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
                        UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
                        if (UserId != HiddenAgentId)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!!');", true);
                return;
            }

            #endregion validation

            if (!string.IsNullOrEmpty(HiddenAgentId))
            {
                #region Bind Grid
                //string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                BindGrid(HiddenAgentId);
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id and credit amount !!');", true);
                return;
            }

            Session["TxtAgencyName"] = TxtAgencyName;
            Session["HiddenAgentId"] = HiddenAgentId;




            UploadSubmitButton.Visible = true;
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='ConvertToDistributor.aspx'; ", true);
            return;
        }
    }

    public void BindGrid(string AgentId)
    {
        try
        {
            SqlTransaction ST = new SqlTransaction();
            DataSet ds = ST.GetAgencyDetailsConverttoDistributor(AgentId);
            #region Set value in text box
            double CashLimit = 0;
            double AgentCreditLimit = 0;
            double DueAmt = 0;
            double FixedLimit = 0;
            double TempLimit = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                DivAgencyDetals.Visible = true;
                ds.Tables[0].Rows[0]["Title"].ToString();
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"])))
                {
                    CashLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["Crd_Limit"]);
                    //(ds.Tables[0].Rows[0]["Crd_Limit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"])))
                {
                    AgentCreditLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["AgentLimit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"])))
                {
                    DueAmt = Convert.ToDouble(ds.Tables[0].Rows[0]["DueAmount"]);
                }
                if (CashLimit > AgentCreditLimit)
                {
                    CashLimit = CashLimit - AgentCreditLimit;
                }
                else
                {
                    CashLimit = 0;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["FixedLimit"])))
                {
                    FixedLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["FixedLimit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["TempLimit"])))
                {
                    TempLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["TempLimit"]);
                }
                //FixedLimit, FixedLimitTrnsDate, TempLimit, TempLimitTrnsDate, TempLimitFromDate,TempLimitToDate
                #region Agency Table tr Bind

                tdTotalBAl.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"]);
                tdAgentId.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["user_id"]) + "/" + Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                tdMobile.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);

                tdAgentCredit.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"]);
                tdAgnetName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Title"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Fname"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Lname"]);
                tdEmail.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                tdDueAmount.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"]);
                tdAgencyName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]);
                tdAddress.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                #endregion
            }
            else
            {
                DivAgencyDetals.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record not found.');", true);
                return;
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string UserID = Session["HiddenAgentId"].ToString();
            if (!string.IsNullOrEmpty(UserID))
            {
                int i = ST.UpdateAgencyORUseridmapping(UserID);
                if (i > 1)
                {
                    UploadSubmitButton.Visible = false;
                    Session["TxtAgencyName"] = null;
                    Session["HiddenAgentId"] = null;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('converted successfull');window.location='ConvertToDistributor.aspx'; ", true);                  
                }
                else
                {
                    UploadSubmitButton.Visible = false;
                    Session["TxtAgencyName"] = null;
                    Session["HiddenAgentId"] = null;
                    DivAgencyDetals.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Error');window.location='ConvertToDistributor.aspx'; ", true);                  
                    return;
                }
            }
            else
            {
                UploadSubmitButton.Visible = false;
                Session["TxtAgencyName"] = null;
                Session["HiddenAgentId"] = null;
                DivAgencyDetals.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Search again');window.location='ConvertToDistributor.aspx'; ", true);               
                return;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Search again');window.location='ConvertToDistributor.aspx'; ", true);
        }

    }
}