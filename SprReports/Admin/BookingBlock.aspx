﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="BookingBlock.aspx.cs" Inherits="SprReports_Admin_BookingBlock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 10px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Booking Block</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip Type</label>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="1">
                                        <asp:ListItem Value="D" Text="Domestic" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3" id="divFareType:">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Fare Type:</label>
                                    <asp:DropDownList ID="DdlCrdType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="NRM" Text="Normal Fare" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <asp:ListItem Value="CPN" Text="coupon fare"></asp:ListItem>
										<asp:ListItem Value="CPN" Text="SME fare"></asp:ListItem>
										<asp:ListItem Value="CPN" Text="FLX fare"></asp:ListItem>
										<asp:ListItem Value="CPN" Text="PKG fare"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline :</label>
                                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="3" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control" TabIndex="12">
                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%-- <div class="col-md-3" style="display:none;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Provider</label>
                                    <asp:DropDownList ID="DdlProvider" runat="server" CssClass="form-control" TabIndex="2">
                                        <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>                                        
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                        </div>

                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                </div>
                            </div>



                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" TabIndex="14" OnClientClick="return Check();" />
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-12" style="overflow: auto;">
                                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;">
                                    <ContentTemplate>--%>
                                <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="false"
                                    CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="Grid1_RowCancelingEdit"
                                    OnRowEditing="Grid1_RowEditing" OnRowUpdating="Grid1_RowUpdating" OnRowDeleting="OnRowDeleting" AllowPaging="true"
                                    OnPageIndexChanging="OnPageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Trip_Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                <asp:Label ID="lblTripTypeName" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FareType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFareType" runat="server" Text='<%#Eval("FareType") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Airline">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirline" runat="server" Text='<%#Eval("AirlineCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' Style="text-wrap: inherit;"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlGrdStatus" runat="server" Width="150px" DataValueField='<%#Eval("Status")%>' SelectedValue='<%#Eval("Status")%>'>
                                                    <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                    <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedBy">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Eval("CreatedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CreatedDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EDIT">
                                            <ItemTemplate>
                                                <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                    CssClass="newbutton_2" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                    CssClass="newbutton_2" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                    <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                </asp:GridView>
                                <%-- </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">


        function ShowHide() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlTicketThrough").val();
            if (Provider == "1G" || Provider == "0") {
                $("#DivApi").hide();
                $("#DivApi1").hide();
                // $("#ctl00_ContentPlaceHolder1_TxtTicketThrough").val("");
                $("#ctl00_ContentPlaceHolder1_TxtQueuePCC").val("");
                $("#ctl00_ContentPlaceHolder1_TxtQNOForAPI").val("");
            }
            else {
                $("#DivApi").show();
                $("#DivApi1").show();
            }
        }

        function Check() {
            //var Provider = $("#ctl00_ContentPlaceHolder1_DdlTicketThrough").val();
            //if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
            //    alert("Select provider.");
            //    $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
            //    return false;
            //}
            //if ($("#ctl00_ContentPlaceHolder1_TxtCorporateID").val() == "") {
            //    alert("Enter Corporate Id .");
            //    $("#ctl00_ContentPlaceHolder1_TxtCorporateID").focus();
            //    return false;
            //}

            //if ($("#ctl00_ContentPlaceHolder1_TxtUserID").val() == "") {
            //    alert("Enter User Id .");
            //    $("#ctl00_ContentPlaceHolder1_TxtUserID").focus();
            //    return false;
            //}
            //if ($("#ctl00_ContentPlaceHolder1_TxtPassword").val() == "") {
            //    alert("Enter Password .");
            //    $("#ctl00_ContentPlaceHolder1_TxtPassword").focus();
            //    return false;
            //}
            //if ($("#ctl00_ContentPlaceHolder1_TxtProviderPcc").val() == "") {
            //    alert("Enter Provider PCC .");
            //    $("#ctl00_ContentPlaceHolder1_TxtProviderPcc").focus();
            //    return false;
            //}
            //if ($("#ctl00_ContentPlaceHolder1_TxtPnrQNo").val() == "") {
            //    alert("Enter Pnr Queue No.");
            //    $("#ctl00_ContentPlaceHolder1_TxtPnrQNo").focus();
            //    return false;
            //}
            //if ($("#ctl00_ContentPlaceHolder1_DdlTicketThrough").val() == "0") {
            //    alert("Select Ticketing API .");
            //    $("#ctl00_ContentPlaceHolder1_DdlTicketThrough").focus();
            //    return false;
            //}

            //if (Provider != "1G") {
            //    if ($("#ctl00_ContentPlaceHolder1_TxtQueuePCC").val() == "") {
            //        alert("Enter Queue PCC.");
            //        $("#ctl00_ContentPlaceHolder1_TxtQueuePCC").focus();
            //        return false;
            //    }
            //    if ($("#ctl00_ContentPlaceHolder1_TxtQNOForAPI").val() == "") {
            //        alert("Enter Queue NO For API .");
            //        $("#ctl00_ContentPlaceHolder1_TxtQNOForAPI").focus();
            //        return false;
            //    }
            //}

        }

    </script>

</asp:Content>

