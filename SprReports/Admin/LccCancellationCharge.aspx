﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="LccCancellationCharge.aspx.vb" Inherits="SprReports_Admin_LccCancellationCharge" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting >LCC Cancellation Charge</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                           
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline</label>
                                    <asp:DropDownList ID="ddl_airline" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0">Select Airline</asp:ListItem>
                                        <asp:ListItem Value="6E">Indigo</asp:ListItem>
                                        <asp:ListItem Value="G8">Goair</asp:ListItem>
                                        <asp:ListItem Value="SG">Spicejet</asp:ListItem>
                                    </asp:DropDownList>

                                    <%--<asp:DropDownList ID="ddl_airline" CssClass="form-control" runat="server" data-placeholder="Choose a Airline...">
                                    </asp:DropDownList>--%>
                                </div>
                            </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType</label>
                                    <asp:DropDownList ID="ddl_GroupType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">--Select Group Type--</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                                                       
                        </div>

                        <div class="row">
                            
                          
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Id</label>
                                    <input type="text" id="txtAgencyName" class="form-control" name="txtAgencyName" onfocus="focusObjag(this);"
                                        onblur="blurObjag(this);" defvalue="ALL" autocomplete="off" value="ALL" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                           
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Cancellation Charge:</label>
                                    <asp:TextBox ID="txt_CanCharge" CssClass="form-control" runat="server" onKeyPress="return isNumberKey(event)" MaxLength="5"
                                      Text="0"></asp:TextBox>
                                </div>
                            </div>
                                                        
                        </div>

                       

              
                        <div class="row">

                          

                        <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Service Charge(GST)%:</label>
                                    <asp:TextBox ID="txt_Gst" runat="server" CssClass="form-control" onKeyPress="return isNumberKey(event)" MaxLength="5"
                                        Text="0"></asp:TextBox>
                                </div>
                            </div>

                              <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip</label>
                                    <asp:DropDownList ID="DDLTRIP" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0">Select Trip</asp:ListItem>
                                        <asp:ListItem Value="D">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>

                                    <%--<asp:DropDownList ID="ddl_airline" CssClass="form-control" runat="server" data-placeholder="Choose a Airline...">
                                    </asp:DropDownList>--%>
                                </div>
                            </div>
                          
                        </div>

                        <div class="row">
                              <div class="col-md-4">
                                <br />
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <asp:Button ID="save" runat="server" Text="Save" CssClass="button buttonBlue" OnClientClick=" return validate()"
                                        OnClick="SAVE_Click" />
                                    <asp:Button ID="btnreset" runat="server" OnClick="btnreset_Click" Text="RESET" Visible="false" />
                                </div>
                            </div>
                         </div>


                        <div class="row" style="background-color: #fff; overflow: auto;">
                            <div class="large-10 medium-12 small-12 large-push-1">

                                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>--%>
                                        <asp:GridView ID="grdemp" runat="server" AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                            OnRowEditing="grdemp_RowEditing" OnRowUpdating="grdemp_RowUpdating"
                                            OnRowCancelingEdit="grdemp_RowCancelingEdit" OnRowDeleting="grdemp_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnCounter" runat="server" Text='<%#Bind("ID")%>' CssClass="hide"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GroupType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbGroupType" runat="server" Text='<%#Bind("AgentType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agent ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnAgentId" runat="server" Text='<%#Bind("AgentID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                             
                                                <asp:TemplateField HeaderText="AirlineCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbAirlineCode" runat="server" Text='<%#Bind("AirlineCode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbTrip" runat="server" Text='<%#Bind("Trip")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                              
                                               
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnAmount" runat="server" Text='<%#Bind("Amount")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAmount" onKeyPress="return isNumberKey(event)" runat="server"
                                                            Text='<%#Eval("Amount") %>' Width="90px" MaxLength="7"></asp:TextBox>

                                                         <asp:RequiredFieldValidator ID="rfvtxtAmount" ControlToValidate="txtAmount" runat="server"
                                                         ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gst(%)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbGst" runat="server" Text='<%#Bind("Gst")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGst" onKeyPress="return isNumberKey(event)" runat="server"
                                                            Text='<%#Eval("Gst")%>' Width="90px" MaxLength="5"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtGst" ControlToValidate="txtGst" runat="server"
                                                         ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                                                         
                                                <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtncreatedDate" runat="server" Text='<%#Bind("createdDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="UpadtedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUpadtedDate" runat="server" Text='<%#Bind("UpdateDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="button_edit" Text="Edit" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="btnUpdate" runat="server" CommandName="Update" CssClass="button_update" Text="Update" />&nbsp;&nbsp
                                            <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="button_cancel" Text="Cancel" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="button_delete" Text="Delete"
                                                            OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                   <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function isNumberKey(event) {

            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {
                alert("please enter 0 to 9 only ");
                return false;
            }
        }
        function isCharKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 122 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                alert("please enter char type ");
                return false;
            }
        }
        function validate() {
            debugger;


            
            if ($('#txtAgencyName').val() == "" || $('#txtAgencyName').val() == "ALL") {
               
                if ($('#ctl00_ContentPlaceHolder1_ddl_GroupType').val() == "0") {
                    alert('Select Group Type Or Agend ID Atleast One is Required')
                    return false;
                }

               
            }
            if ($('#ctl00_ContentPlaceHolder1_ddl_airline').val() == "0") {
                alert("Select Airline");
                $('#ctl00_ContentPlaceHolder1_ddl_airline').focus()
                return false;
            }

            if ($('#ctl00_ContentPlaceHolder1_DDLTRIP').val() == "0") {
                alert("Select Trip");
                $('#ctl00_ContentPlaceHolder1_DDLTRIP').focus()
                return false;
            }


            
            if ($('#ctl00_ContentPlaceHolder1_txt_CanCharge').val() == "") {
                alert("Enter Amount value")
                return false;
            }

            if ($('#ctl00_ContentPlaceHolder1_txt_Gst').val() = "") {
                alert("Enter Gst Value")
                return false;
            }


        
           
            if (confirm("Are you sure??"))
                return true;
            return false;
        }
    </script>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        function focusObjag(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjag(obj) { if (obj.value == "") obj.value = "ALL"; }

        function focusObjorigin(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjorigin(obj) { if (obj.value == "") obj.value = "ALL"; }

        function focusObjdest(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjdest(obj) { if (obj.value == "") obj.value = "ALL"; }

    </script>


</asp:Content>
