﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_DueAmountReport : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null && (Session["User_Type"] == null))
        {
            Response.Redirect("~/Login.aspx");
        }
        if (Session["User_Type"].ToString() == "SALES")
        {
            divFromDate.Style.Add("display", "none");
            divToDate.Style.Add("display", "none");
            tr_Salestperson.Style.Add("display", "none");

           
        }
        if (!this.IsPostBack)
        {
            ResetFilters();
            BindSalesPerson();
            BindDueAmountReport();
        }
    }

    protected void BindDueAmountReport()
    {
        string salesname = "";
         if (Session["User_Type"].ToString() == "SALES")
        {
            salesname = Session["username"].ToString();
        }

        DataTable dtRecord = GetDueReportList(null, null, null, salesname, null, null);

        if (dtRecord != null && dtRecord.Rows.Count > 0)
        {
            lstDueAmount.DataSource = dtRecord;
            lstDueAmount.DataBind();
        }
    }

    private DataTable GetDueReportList(string agencyid = null, string fromDate = null, string toDate = null, string salesPersion = null, string fixedLimit = null, string tempLimit = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetDueAmountReport", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@AgencyId", !string.IsNullOrEmpty(agencyid) ? agencyid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            adap.SelectCommand.Parameters.AddWithValue("@SalesPersion", !string.IsNullOrEmpty(salesPersion) ? salesPersion.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FixedLimit", !string.IsNullOrEmpty(fixedLimit) ? fixedLimit.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@TempLimit", !string.IsNullOrEmpty(tempLimit) ? tempLimit.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }

    protected void ResetFilters()
    {

    }

    protected void BindSalesPerson()
    {
        try
        {
            DataSet dtSelsPerson = STDom.GetSalesRef();

            if (dtSelsPerson != null)
            {
                ddlSalesPerson.DataSource = dtSelsPerson.Tables[0];
                ddlSalesPerson.DataTextField = "Name";
                ddlSalesPerson.DataValueField = "Name";
                ddlSalesPerson.DataBind();
                ddlSalesPerson.Items.Insert(0, new ListItem("Select", string.Empty));
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        string salesper = ddlSalesPerson.SelectedValue;

        if (Session["User_Type"].ToString() == "SALES")
        {
            salesper = Session["username"].ToString();
        }

            DataTable dtRecord = GetDueReportList(Request["hidtxtAgencyName"].ToString(), Request["From"].ToString(), Request["To"].ToString(), salesper, txtFixedCreditLimit.Text, txtTempCreditLimit.Text);

        lstDueAmount.DataSource = dtRecord;
        lstDueAmount.DataBind();
    }
    protected void lstDueAmount_PagePropertiesChanged(object sender, EventArgs e)
    {
        string salesper = ddlSalesPerson.SelectedValue;

        if (Session["User_Type"].ToString() == "SALES")
        {
            salesper = Session["username"].ToString();
        }
        DataTable dtRecord = GetDueReportList(Request["hidtxtAgencyName"].ToString(), Request["From"].ToString(), Request["To"].ToString(), salesper, txtFixedCreditLimit.Text, txtTempCreditLimit.Text);

        lstDueAmount.DataSource = dtRecord;
        lstDueAmount.DataBind();
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        string salesper = ddlSalesPerson.SelectedValue;

        DataTable dtRecord = GetDueReportList(Request["hidtxtAgencyName"].ToString(), Request["From"].ToString(), Request["To"].ToString(), salesper, txtFixedCreditLimit.Text, txtTempCreditLimit.Text);

        DataSet dsExport = new DataSet();
        dsExport.Tables.Add(dtRecord);
        STDom.ExportData(dsExport, ("Due_Amount_Report_" + DateTime.Now));
    }
}