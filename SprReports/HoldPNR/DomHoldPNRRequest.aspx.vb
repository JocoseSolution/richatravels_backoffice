﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

'Imports PG

Partial Class Reports_HoldPNR_DomHoldPNRRequest
    Inherits System.Web.UI.Page

    Private ID As New IntlDetails
    Private ST As New SqlTransaction()
    Private STDom As New SqlTransactionDom()
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim adap As SqlDataAdapter
    'Private objPG As New PaymentGateway

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        Try
            Response.AppendHeader("Refresh", "60")
            BindGrid()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub BindGrid()

        'Dim OrderId As String = Request.QueryString("ProxyID")
        Try


            GridHoldPNRAccept.DataSource = IntlConfirmHoldPNR("Confirm", "D")
            GridHoldPNRAccept.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    Protected Sub GridHoldPNRAccept_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridHoldPNRAccept.RowCommand

        Try


            If e.CommandName = "Accept" Then
                Dim OrderId As String = e.CommandArgument.ToString()
                Dim ds As New DataSet()
                adap = New SqlDataAdapter("Select ExecutiveId,Status from FltHeader where OrderId='" & OrderId & "'", con)
                adap.Fill(ds)
                Dim dt As New DataTable()
                dt = ds.Tables(0)
                Dim ExecutiveId As String = dt.Rows(0)("ExecutiveId").ToString()
                Dim Status As String = dt.Rows(0)("Status").ToString()
                If ExecutiveId = "" AndAlso Status = "Confirm" Then

                    Dim ds1 As New DataSet()

                    ' ds = ID.UpdateIntlFltHeaderExecutiveID(OrderId, Session("UID").ToString, "InProcess")
                    ds = ID.UpdateIntlFltHeaderExecutiveID(OrderId, Session("UID").ToString(), "InProcess")


                    BindGrid()
                Else

                    Response.Write("<script>alert('Already Allocated')</script>")
                    BindGrid()
                End If



            End If

            If e.CommandName = "Reject" Then

                Dim Orderid As String = e.CommandArgument.ToString()
                Dim ds As New DataSet()
                adap = New SqlDataAdapter("Select ExecutiveId,Status from FltHeader where OrderId='" & Orderid & "'", con)
                adap.Fill(ds)
                Dim dt As New DataTable()
                dt = ds.Tables(0)
                Dim ExecutiveId As String = dt.Rows(0)("ExecutiveId").ToString()
                Dim Status As String = dt.Rows(0)("Status").ToString()
                If ExecutiveId = "" AndAlso Status = "Confirm" Then


                    Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
                    Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
                    gvr.BackColor = System.Drawing.Color.Yellow

                    td_Reject.Visible = True
                    Dim ID As String = e.CommandArgument.ToString()
                    ViewState("ID") = ID
                End If
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        Try
            td_Reject.Visible = False
            BindGrid()
            txt_Reject.Text = ""

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    'Protected Sub btn_Comment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Comment.Click
    '    Try
    '        Dim OrderID As String = ""
    '        Dim PaymentMode As String = ""
    '        Dim AgentId As String = ""
    '        Dim Trip As String = ""
    '        OrderID = ViewState("ID").ToString()



    '        If txt_Reject.Text IsNot Nothing AndAlso txt_Reject.Text <> "" Then

    '            Dim ds As New DataSet()
    '            ds = ST.GetFltHeaderDetail(OrderID)
    '            PaymentMode = ds.Tables(0).Rows(0)("PaymentMode").ToString()
    '            AgentId = ds.Tables(0).Rows(0)("AgentId").ToString()
    '            Trip = ds.Tables(0).Rows(0)("Trip").ToString()
    '            Dim dtID As New DataTable()
    '            dtID = ds.Tables(0)
    '            Dim ChecksSatus As String = dtID.Rows(0)("Status").ToString()

    '            Dim objItzT As New Itz_Trans_Dal
    '            Dim inst As Boolean = False
    '            Dim objIzT As New ITZ_Trans
    '            Dim ablBalance As Double = 0
    '            Dim Aval_Bal As Double = 0
    '            If (ChecksSatus = "Confirm" And PaymentMode = "wallet") Then
    '                Aval_Bal = ST.AddCrdLimit(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("TotalAfterDis").ToString())

    '                'Ledger

    '                'Adding Refund Amount in Agent balance
    '                'objParamCrd._DECODE = IIf(Session("_DCODE") <> Nothing, Session("_DCODE").ToString().Trim(), " ")


    '                GetMerchantKey(OrderID)


    '                Dim rndnum As New RandomKeyGenerator()

    '                Dim numRand As String = rndnum.Generate()
    '                Try
    '                    objParamCrd._MERCHANT_KEY = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                    objParamCrd._AMOUNT = IIf(dtID.Rows(0)("TotalAfterDis").ToString() <> Nothing, dtID.Rows(0)("TotalAfterDis").ToString(), 0)
    '                    objParamCrd._ORDERID = IIf(numRand <> Nothing AndAlso numRand <> "", numRand.Trim(), " ")
    '                    objParamCrd._REFUNDORDERID = IIf(OrderID <> Nothing AndAlso OrderID <> "", OrderID.Trim(), " ")
    '                    objParamCrd._MODE = IIf(Session("ModeTypeITZ") <> Nothing, Session("ModeTypeITZ").ToString().Trim(), " ") ''IIf(Not ConfigurationManager.AppSettings("ITZMode") Is Nothing, ConfigurationManager.AppSettings("ITZMode").Trim(), " ")
    '                    objParamCrd._REFUNDTYPE = "F"
    '                    ''objParamCrd._CHECKSUM = " "
    '                    Dim stringtoenc As String = "MERCHANTKEY=" & objParamCrd._MERCHANT_KEY & "&ORDERID=" & objParamCrd._ORDERID & "&REFUNDTYPE=" & objParamCrd._REFUNDTYPE
    '                    objParamCrd._CHECKSUM = VGCheckSum.calculateEASYChecksum(stringtoenc)
    '                    'objParamCrd._SERVICE_TYPE = IIf(Not ConfigurationManager.AppSettings("ITZSvcType") Is Nothing, ConfigurationManager.AppSettings("ITZSvcType").Trim(), " ")
    '                    objParamCrd._DESCRIPTION = "refund to agent -" & dtID.Rows(0)("AgentId").ToString() & " against PNR-" & dtID.Rows(0)("GdsPnr").ToString()
    '                    objRefnResp = objCrd.ITZRefund(objParamCrd)
    '                    If objRefnResp.MESSAGE.Trim().ToLower().Contains("successfully execute") Then
    '                        ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
    '                        STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", IIf(objRefnResp.EASY_ORDER_ID IsNot Nothing, objRefnResp.EASY_ORDER_ID, " "), "", Session("UID").ToString(), Request.UserHostAddress, 0, dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. Rejection", "Dom. PNR Rejected Against  OrderID=" & OrderID, 0, 0)

    '                        objItzT = New Itz_Trans_Dal()
    '                        Try

    '                            objIzT.AMT_TO_DED = "0"
    '                            objIzT.AMT_TO_CRE = IIf(dtID.Rows(0)("TotalAfterDis") <> Nothing, dtID.Rows(0)("TotalAfterDis").ToString(), 0)
    '                            objIzT.B2C_MBLNO_ITZ = " "
    '                            objIzT.COMMI_ITZ = " "
    '                            objIzT.CONVFEE_ITZ = " "
    '                            objIzT.DECODE_ITZ = IIf(dtID.Rows(0)("AgentId").ToString() <> Nothing, dtID.Rows(0)("AgentId").ToString().Trim(), " ")
    '                            objIzT.EASY_ORDID_ITZ = IIf(objRefnResp.EASY_ORDER_ID IsNot Nothing, objRefnResp.EASY_ORDER_ID, " ")
    '                            objIzT.EASY_TRANCODE_ITZ = IIf(objRefnResp.EASY_TRAN_CODE IsNot Nothing, objRefnResp.EASY_TRAN_CODE, " ")
    '                            objIzT.ERROR_CODE = IIf(objRefnResp.ERROR_CODE IsNot Nothing, objRefnResp.ERROR_CODE, " ")
    '                            objIzT.MERCHANT_KEY_ITZ = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                            objIzT.MESSAGE_ITZ = IIf(objRefnResp.MESSAGE IsNot Nothing, objRefnResp.MESSAGE, " ")
    '                            objIzT.ORDERID = IIf(OrderID <> Nothing AndAlso OrderID <> "", OrderID.Trim(), " ")
    '                            objIzT.RATE_GROUP_ITZ = " "
    '                            objIzT.REFUND_TYPE_ITZ = IIf(objRefnResp.REFUND_TYPE IsNot Nothing AndAlso objRefnResp.REFUND_TYPE <> "" AndAlso objRefnResp.REFUND_TYPE <> " ", objRefnResp.REFUND_TYPE, " ")
    '                            objIzT.SERIAL_NO_FROM = " "
    '                            objIzT.SERIAL_NO_TO = " "
    '                            objIzT.SVC_TAX_ITZ = " "
    '                            objIzT.TDS_ITZ = " "
    '                            objIzT.TOTAL_AMT_DED_ITZ = " "
    '                            objIzT.TRANS_TYPE = "REFUND"
    '                            objIzT.USER_NAME_ITZ = IIf(dtID.Rows(0)("AgentId").ToString() <> Nothing, dtID.Rows(0)("AgentId").ToString().Trim(), " ")
    '                            Try
    '                                objBalResp = New GetBalanceResponse()
    '                                objParamBal._DCODE = IIf(dtID.Rows(0)("AgentId").ToString() <> Nothing, dtID.Rows(0)("AgentId").ToString().Trim(), " ")
    '                                objParamBal._MERCHANT_KEY = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                                objParamBal._PASSWORD = IIf(Session("_PASSWORD") <> Nothing, Session("_PASSWORD").ToString().Trim(), " ")
    '                                objParamBal._USERNAME = IIf(Session("_USERNAME") <> Nothing, Session("_USERNAME").ToString().Trim(), " ")
    '                                objBalResp = objItzBal.GetBalanceCustomer(objParamBal)
    '                                objIzT.ACCTYPE_NAME_ITZ = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_TYPE_NAME IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_TYPE_NAME, " ")
    '                                objIzT.AVAIL_BAL_ITZ = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE, " ")
    '                                Session("CL") = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE, " ")
    '                                ablBalance = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE, " ")
    '                            Catch ex As Exception
    '                            End Try
    '                            inst = objItzT.InsertItzTrans(objIzT)
    '                        Catch ex As Exception
    '                        End Try
    '                        Response.Write("<script>alert('PNR Rejected Successfully.')</script>")
    '                        BindGrid()
    '                        txt_Reject.Text = ""

    '                    Else
    '                        Response.Write("<script>alert('Unable to  Refund, please try again')</script>")
    '                        BindGrid()
    '                        txt_Reject.Text = ""

    '                    End If
    '                Catch ex As Exception
    '                End Try

    '            ElseIf ChecksSatus = "Confirm" And PaymentMode = "PG" Then
    '                Dim msg As String = ""
    '                Dim PGds As DataSet
    '                PGds = PgAmount(OrderID)
    '                If (PGds.Tables(0).Rows.Count > 0) Then
    '                    msg = objPG.PgRefundAmount(OrderID, "Flight", Trip, Convert.ToDouble(PGds.Tables(0).Rows(0)("Amount").ToString), AgentId, txt_Reject.Text, Session("UID").ToString(), "PNRReject", 0, "", 0, "")
    '                    If (msg = "Refunded") Then
    '                        ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", PGds.Tables(0).Rows(0)("Amount").ToString(), Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
    '                        STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", IIf(objRefnResp.EASY_ORDER_ID IsNot Nothing, objRefnResp.EASY_ORDER_ID, " "), "", Session("UID").ToString(), Request.UserHostAddress, 0, PGds.Tables(0).Rows(0)("Amount").ToString, Aval_Bal, "Dom. Rejection", " PG Dom. PNR Rejected Against  OrderID=" & OrderID, 0, 0)
    '                        Response.Write("<script>alert('" + msg + "')</script>")
    '                    ElseIf (msg <> "Already refunded" Or msg <> "Refunded") Then
    '                        ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "RefundRequested", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "RefundRequested", PGds.Tables(0).Rows(0)("Amount").ToString, Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
    '                        Response.Write("<script>alert('" + msg + "')</script>")
    '                    ElseIf (msg = "Already refunded") Then
    '                        Response.Write("<script>alert('" + msg + "')</script>")

    '                    Else
    '                        Response.Write("<script>alert('" + msg + "')</script>")
    '                    End If
    '                Else
    '                    Response.Write("<script>alert('PG Transaction Not Found!!')</script>")
    '                End If


    '                BindGrid()
    '                txt_Reject.Text = ""
    '            Else
    '                Response.Write("<script>alert('PNR already Rejected')</script>")
    '                BindGrid()
    '                txt_Reject.Text = ""
    '            End If

    '            td_Reject.Visible = False

    '        End If
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)

    '    End Try
    'End Sub


    Protected Sub btn_Comment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Comment.Click
        Try
            Dim OrderID As String = ""
            OrderID = ViewState("ID").ToString()



            If txt_Reject.Text IsNot Nothing AndAlso txt_Reject.Text <> "" Then

                Dim ds As New DataSet()
                ds = ST.GetFltHeaderDetail(OrderID)
                Dim dtID As New DataTable()
                dtID = ds.Tables(0)
                Dim ChecksSatus As String = dtID.Rows(0)("Status").ToString()

                Dim objItzT As New Itz_Trans_Dal
                Dim inst As Boolean = False
                Dim objIzT As New ITZ_Trans
                ' Dim ablBalance As Double = 0

                If (ChecksSatus = "Confirm") Then
                    'Dim Aval_Bal As Double = ST.AddCrdLimit(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("TotalAfterDis").ToString())
                    'Ledger
                    'Adding Refund Amount in Agent balance
                    'objParamCrd._DECODE = IIf(Session("_DCODE") <> Nothing, Session("_DCODE").ToString().Trim(), " ")

                    'ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
                    'STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", "", "", Session("UID").ToString(), Request.UserHostAddress, 0, dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. Rejection", "Dom. PNR Rejected Against  OrderID=" & OrderID, 0)


                    Dim flag As Integer = ST.RejectHoldPNRStatusAndAmount(OrderID, Convert.ToDouble(dtID.Rows(0)("TotalAfterDis")), dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), Session("UID").ToString(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", txt_Reject.Text.Trim(), "Dom. PNR Rejected Against  OrderID=" & OrderID, "Dom. Rejection", Request.UserHostAddress)

                    If (flag > 0) Then
                        Response.Write("<script>alert('PNR Rejected Sucessfully')</script>")
                    Else
                        Response.Write("<script>alert('Please Check ledger,then refund')</script>")
                    End If

                    BindGrid()
                    txt_Reject.Text = ""

                Else
                    Response.Write("<script>alert('PNR Already Rejected')</script>")
                    BindGrid()
                    txt_Reject.Text = ""
                End If

                td_Reject.Visible = False

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub


    Protected Sub GridHoldPNRAccept_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridHoldPNRAccept.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim l As LinkButton = DirectCast(e.Row.FindControl("LB_Reject"), LinkButton)
        '    l.Attributes.Add("onclick", "javascript:return " & "confirm('Are you sure you want to Reject Proxy ID " & DataBinder.Eval(e.Row.DataItem, "ProxyID") & "')")
        'End If
        Try
            If Session("User_Type") = "ADMIN" Then
                If (e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate) AndAlso (e.Row.RowType = DataControlRowType.DataRow OrElse e.Row.RowType = DataControlRowType.Header) Then

                    e.Row.Cells(15).Visible = False
                End If
            End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    Protected Sub GridHoldPNRAccept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridHoldPNRAccept.PageIndexChanging
        Try
            GridHoldPNRAccept.PageIndex = e.NewPageIndex
            BindGrid()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Public Function GetMerchantKey(ByVal orderID As String) As String


        Dim mrchntKey As String = ConfigurationManager.AppSettings("MerchantKey").ToString()

        Try


            Dim provider12 As String = ""
            Dim sqldom As New SqlTransactionDom()

            Dim dsp As New DataSet()

            dsp = sqldom.GetTicketingProvider(orderID)




            If (dsp.Tables.Count > 0) Then
                If (dsp.Tables(0).Rows.Count > 0) Then

                    provider12 = dsp.Tables(0).Rows(0)(0)

                End If


            End If


            If provider12.ToLower().Trim() = "yatra" Then

                mrchntKey = ConfigurationManager.AppSettings("YatraITZMerchantKey").ToString()
            End If

        Catch ex As Exception
            mrchntKey = ConfigurationManager.AppSettings("MerchantKey").ToString()
        End Try
        Session("MchntKeyITZ") = mrchntKey
        Return mrchntKey

    End Function

    Public Function PgAmount(ByVal orderID As String) As DataSet
        Dim adap As New SqlDataAdapter("USP_RefundPGAmount", con)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@orderid", orderID)
        Dim ds As New DataSet()
        adap.Fill(ds)
        Return ds
    End Function
    Public Function IntlConfirmHoldPNR(ByVal Status As String, ByVal Trip As String, Optional ByVal Multistatus As String = "") As DataSet
        Dim ds As New DataSet()
        If (Multistatus = "") Then

            ''adap = New SqlDataAdapter("Select CASE  WHEN FltHeader.PgCharges is null THEN  '0.00'  ELSE  FltHeader.PgCharges END as  PGCharges,st.Track_id, case  when  isnull(IsBagFare,0) = 0 then 'NO' ELSE 'YES' END  as IsBagFare,* from FltHeader left outer join (SELECT  * FROM    (  SELECT  IsBagFare, track_id, ROW_NUMBER() OVER (PARTITION BY Track_id ORDER BY Counter asc) as rn FROM    SelectedFlightDetails_Gal ) as tbl WHERE   rn = 1) as st on FltHeader.OrderId=st.track_id where Status='" & Status & "' and Trip='" & Trip & "' order by CreateDate desc", con)

            '' adap = New SqlDataAdapter("Select  CASE  WHEN PaymentMode =  'PG' THEN  'PG'  ELSE  'Wallet'  END as PaymentMode,CASE  WHEN FltHeader.PgCharges is null THEN  '0.00'  ELSE  FltHeader.PgCharges END as  PGCharges,st.Track_id, case  when  isnull(IsBagFare,0) = 0 then 'NO' ELSE 'YES' END  as IsBagFare,FltHeader.*, isnull(agent_register.Branch,'DELHI') AS Branch,agent_register.SalesExecID,convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())/3600)+':'+convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())%3600/60)+':'+convert(varchar(5),(DateDiff(s,FltHeader.CreateDate, GETDATE())%60)) as TimeSincePending from FltHeader inner join agent_register on FltHeader.AgentId=agent_register.User_Id  left outer join (SELECT  * FROM    (  SELECT  IsBagFare, track_id, ROW_NUMBER() OVER (PARTITION BY Track_id ORDER BY Counter asc) as rn FROM    SelectedFlightDetails_Gal ) as tbl WHERE   rn = 1) as st on FltHeader.OrderId=st.track_id where FltHeader.Status='" & Status & "' and Trip='" & Trip & "' order by FltHeader.CreateDate desc", con)
            adap = New SqlDataAdapter("Select  CASE  WHEN PaymentMode =  'PG' THEN  'PG'  ELSE  'Wallet'  END as PaymentMode,CASE  WHEN FltHeader.PgCharges is null THEN  '0.00'  ELSE  FltHeader.PgCharges END as  PGCharges,st.Track_id, case  when  isnull(IsBagFare,0) = 0 then 'NO' ELSE 'YES' END  as IsBagFare,FltHeader.*, isnull(agent_register.Branch,'DELHI') AS Branch,agent_register.SalesExecID,agent_register.AgencyId,convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())/3600)+':'+convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())%3600/60)+':'+convert(varchar(5),(DateDiff(s,FltHeader.CreateDate, GETDATE())%60)) as TimeSincePending ,FltHeader.GSTRemark as AgentRemark,(select ProxyTicket.Remark from ProxyTicket where ProxyTicket.OrderIdOneWay = FltHeader.OrderId ) as StaffRemarks,case when agent_register.Agent_Type = 'NEWB2C' THEN 'B2C' ELSE case  when  agent_register.Distr = 'HEADOFFICE' then 'Direct' ELSE 'DIST AGT' END END as Agent_Type,case  when  agent_register.Distr = 'HEADOFFICE' then 'richatravels' ELSE (select top 1 Agency_Name from agent_register agg where  agg.user_id = agent_register.Distr) END  as MasterAgent from FltHeader inner join agent_register on FltHeader.AgentId=agent_register.User_Id left outer join (SELECT  * FROM    (  SELECT  IsBagFare, track_id, ROW_NUMBER() OVER (PARTITION BY Track_id ORDER BY Counter asc) as rn FROM SelectedFlightDetails_Gal ) as tbl WHERE   rn = 1) as st on FltHeader.OrderId=st.track_id where FltHeader.Status='" & Status & "' and Trip='" & Trip & "' AND  GdsPnr NOT LIKE '%-FQ%' order by FltHeader.CreateDate desc", con)


            adap.Fill(ds)

        Else

            adap = New SqlDataAdapter("Select CASE  WHEN PaymentMode =  'PG' THEN  'PG'  ELSE  'Wallet'  END as PaymentMode,CASE  WHEN FltHeader.PgCharges is null THEN  '0.00'  ELSE  FltHeader.PgCharges END as  PGCharges,st.Track_id, case  when  isnull(IsBagFare,0) = 0 then 'NO' ELSE 'YES' END  as IsBagFare,FltHeader.*, isnull(agent_register.Branch,'DELHI') AS Branch,agent_register.SalesExecID,agent_register.AgencyId,convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())/3600)+':'+convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())%3600/60)+':'+convert(varchar(5),(DateDiff(s,FltHeader.CreateDate, GETDATE())%60)) as TimeSincePending ,FltHeader.GSTRemark as AgentRemark,(select ProxyTicket.Remark from ProxyTicket where ProxyTicket.OrderIdOneWay = FltHeader.OrderId ) as StaffRemarks,case when agent_register.Agent_Type = 'NEWB2C' THEN 'B2C' ELSE case  when  agent_register.Distr = 'HEADOFFICE' then 'Direct' ELSE 'DIST AGT' END END as Agent_Type,case  when  agent_register.Distr = 'HEADOFFICE' then 'richatravels' ELSE (select top 1 Agency_Name from agent_register agg where  agg.user_id = agent_register.Distr) END  as MasterAgent from FltHeader inner join agent_register on FltHeader.AgentId=agent_register.User_Id left outer join (SELECT  * FROM    (  SELECT  IsBagFare, track_id, ROW_NUMBER() OVER (PARTITION BY Track_id ORDER BY Counter asc) as rn FROM SelectedFlightDetails_Gal ) as tbl WHERE   rn = 1) as st on FltHeader.OrderId=st.track_id where (FltHeader.Status=('" & Status & "') or  FltHeader.Status=('" & Multistatus & "')) and Trip='" & Trip & "' AND  GdsPnr NOT LIKE '%-FQ%'  order by FltHeader.CreateDate desc", con)
            adap.Fill(ds)

        End If

        Return ds
    End Function
End Class

