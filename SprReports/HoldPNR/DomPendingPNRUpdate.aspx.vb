﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
'Imports PG

Partial Class Reports_HoldPNR_DomPendingPNRUpdate
    Inherits System.Web.UI.Page
    Private ID As New IntlDetails
    Private ST As New SqlTransaction()
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim adap As SqlDataAdapter
    Private STDom As New SqlTransactionDom()
    'Private objPG As New PaymentGateway
#Region "Pawan"
    'Dim objRefnResp As New RefundResponse
    'Dim objParamCrd As New _CrOrDb
    'Dim objCrd As New ITZcrdb
    'Dim objItzBal As New ITZGetbalance
    'Dim objParamBal As New _GetBalance
    'Dim objBalResp As New GetBalanceResponse
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        Try
            BindGrid()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub BindGrid()
        Try
            'Dim OrderId As String = Request.QueryString("ProxyID")
            GridHoldPNRAccept.DataSource = IntlUpdateHoldPNR("InProcess", "D", Session("UID"))
            GridHoldPNRAccept.DataBind()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub GridHoldPNRAccept_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridHoldPNRAccept.RowCommand
        Try
            If e.CommandName = "Reject" Then

                Dim Orderid As String = e.CommandArgument.ToString()
                'Dim ds As New DataSet()
                'adap = New SqlDataAdapter("Select ExecutiveId,Status from FltHeader where OrderId='" & Orderid & "'", con)
                'adap.Fill(ds)
                'Dim dt As New DataTable()
                'dt = ds.Tables(0)
                'Dim ExecutiveId As String = dt.Rows(0)("ExecutiveId").ToString()
                'Dim Status As String = dt.Rows(0)("Status").ToString()
                'If ExecutiveId = "" AndAlso Status = "I" Then


                Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
                Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
                gvr.BackColor = System.Drawing.Color.Yellow

                td_Reject.Visible = True
                Dim ID As String = e.CommandArgument.ToString()
                ViewState("ID") = ID
                'End If


            End If




        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub ITZ_Accept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim OrderId = CType(sender, LinkButton).CommandArgument.ToString
            'Response.Write("<script>javascript:window.open('UpdateHoldPNRIntl.aspx?OrderId=" & OrderId & "','Print','scrollbars=yes,width=800,height=500,top=20,left=150')</script>")
            ' ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "javascript:window.open('UpdateHoldPNRIntl.aspx?OrderId=" & OrderId & "','Print','scrollbars=yes,width=800,height=500,top=20,left=150')", True)
            Dim popupScript As String = "window.open('UpdateHoldPNRIntl.aspx?OrderId=" & OrderId & "','Print','scrollbars=yes,width=800,height=500,top=20,left=150');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "popup", popupScript, True)
            BindGrid()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub


    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        td_Reject.Visible = False

    End Sub


    Protected Sub btn_Comment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Comment.Click
        Try
            Dim OrderID As String = ""
            OrderID = ViewState("ID").ToString()
            If txt_Reject.Text IsNot Nothing AndAlso txt_Reject.Text <> "" Then
                Dim ds As New DataSet()
                ds = ST.GetFltHeaderDetail(OrderID)
                Dim dtID As New DataTable()
                dtID = ds.Tables(0)
                'adap = New SqlDataAdapter("Select GdsPnr,AgencyName,AgentId,TotalAfterDis From FltHeader where OrderId='" & OrderID & "'", con)
                'adap.Fill(dtID)
                Dim ChecksSatus As String = dtID.Rows(0)("Status").ToString()
                'Dim Aval_Bal As Double = ST.AddCrdLimit(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("TotalAfterDis").ToString())
                'ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
                'STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", 0, "", Session("UID").ToString(), Request.UserHostAddress, 0, dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. Rejection", "Dom. PNR Rejected Against  OrderID=" & OrderID, 0)

                Dim flag As Integer = ST.RejectHoldPNRStatusAndAmount(OrderID, Convert.ToDouble(dtID.Rows(0)("TotalAfterDis")), dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), Session("UID").ToString(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", txt_Reject.Text.Trim(), "Dom. PNR Rejected Against  OrderID=" & OrderID, "Dom. Rejection", Request.UserHostAddress)
                If (flag > 0) Then
                    Response.Write("<script>alert('PNR Refunded Sucessfully')</script>")
                Else
                    Response.Write("<script>alert('Please Check ledger,then refund')</script>")
                End If
                'Response.Write("<script>alert('PNR Refunded Sucessfully')</script>")
                BindGrid()
                td_Reject.Visible = False

            Else
                Response.Write("<script>alert('Unable to  refund, please try again')</script>")

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub


    'Protected Sub btn_Comment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Comment.Click
    '    Try
    '        Dim OrderID As String = ""
    '        Dim PaymentMode As String = ""
    '        Dim AgentId As String = ""
    '        Dim Aval_Bal As Double = 0
    '        Dim Trip As String = ""
    '        OrderID = ViewState("ID").ToString()
    '        Dim objItzT As New Itz_Trans_Dal
    '        Dim inst As Boolean = False
    '        Dim objIzT As New ITZ_Trans
    '        Dim ablBalance As Double = 0

    '        If txt_Reject.Text IsNot Nothing AndAlso txt_Reject.Text <> "" Then
    '            Dim ds As New DataSet()
    '            ds = ST.GetFltHeaderDetail(OrderID)
    '            PaymentMode = ds.Tables(0).Rows(0)("PaymentMode").ToString()
    '            AgentId = ds.Tables(0).Rows(0)("AgentId").ToString()
    '            Trip = ds.Tables(0).Rows(0)("Trip").ToString()
    '            Dim dtID As New DataTable()
    '            dtID = ds.Tables(0)
    '            If PaymentMode = "wallet" Then
    '                Aval_Bal = ST.AddCrdLimit(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("TotalAfterDis").ToString())
    '                GetMerchantKey(OrderID)
    '                Dim rndnum As New RandomKeyGenerator()

    '                Dim numRand As String = rndnum.Generate()
    '                Try
    '                    objParamCrd._MERCHANT_KEY = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                    objParamCrd._AMOUNT = IIf(dtID.Rows(0)("TotalAfterDis").ToString() <> Nothing, dtID.Rows(0)("TotalAfterDis").ToString(), 0)
    '                    objParamCrd._ORDERID = IIf(numRand <> Nothing AndAlso numRand <> "", numRand.Trim(), " ")
    '                    objParamCrd._REFUNDORDERID = IIf(OrderID <> Nothing AndAlso OrderID <> "", OrderID.Trim(), " ")
    '                    objParamCrd._MODE = IIf(Session("ModeTypeITZ") <> Nothing, Session("ModeTypeITZ").ToString().Trim(), " ") ''IIf(Not ConfigurationManager.AppSettings("ITZMode") Is Nothing, ConfigurationManager.AppSettings("ITZMode").Trim(), " ")
    '                    objParamCrd._REFUNDTYPE = "F"
    '                    ''objParamCrd._CHECKSUM = " "
    '                    Dim stringtoenc As String = "MERCHANTKEY=" & objParamCrd._MERCHANT_KEY & "&ORDERID=" & objParamCrd._ORDERID & "&REFUNDTYPE=" & objParamCrd._REFUNDTYPE
    '                    objParamCrd._CHECKSUM = VGCheckSum.calculateEASYChecksum(stringtoenc)
    '                    'objParamCrd._SERVICE_TYPE = IIf(Not ConfigurationManager.AppSettings("ITZSvcType") Is Nothing, ConfigurationManager.AppSettings("ITZSvcType").Trim(), " ")
    '                    objParamCrd._DESCRIPTION = "refund to agent -" & dtID.Rows(0)("AgentId").ToString() & " against PNR-" & dtID.Rows(0)("GdsPnr").ToString()
    '                    objRefnResp = objCrd.ITZRefund(objParamCrd)

    '                    If objRefnResp.MESSAGE.Trim().ToLower().Contains("successfully execute") Then
    '                        ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Intl PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
    '                        STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", "", "", Session("UID").ToString(), Request.UserHostAddress, 0, dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Intl Rejection", "Intl PNR Rejected Against  OrderID=" & OrderID, 0, 0)
    '                        'Ledger


    '                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('PNR Refund Successfully.');", True)

    '                        BindGrid()
    '                        td_Reject.Visible = False


    '                    Else
    '                        Response.Write("<script>alert('Unable to  Refund, Please try again')</script>")
    '                        BindGrid()
    '                        td_Reject.Visible = False

    '                    End If
    '                Catch ex As Exception
    '                End Try
    '            ElseIf PaymentMode = "PG" Then
    '                Dim PGds As DataSet
    '                PGds = PgAmount(OrderID)
    '                If (PGds.Tables(0).Rows.Count > 0) Then
    '                    Dim msg As String = ""
    '                    msg = objPG.PgRefundAmount(OrderID, "Flight", Trip, Convert.ToDouble(PGds.Tables(0).Rows(0)("Amount").ToString), AgentId, txt_Reject.Text, Session("UID").ToString(), "PNRReject", 0, "", 0, "")
    '                    If (msg = "Refunded") Then
    '                        ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", PGds.Tables(0).Rows(0)("Amount").ToString, Aval_Bal, "Intl PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
    '                        STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", "", "", Session("UID").ToString(), Request.UserHostAddress, 0, PGds.Tables(0).Rows(0)("Amount").ToString, Aval_Bal, "Intl Rejection", "PG Intl PNR Rejected Against  OrderID=" & OrderID, 0, 0)
    '                        Response.Write("<script>alert('" + msg + "')</script>")
    '                    ElseIf (msg <> "Already refunded" Or msg <> "Refunded") Then
    '                        ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "RefundRequested", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "RefundRequested", PGds.Tables(0).Rows(0)("Amount").ToString, Aval_Bal, "Intl PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
    '                        Response.Write("<script>alert('" + msg + "')</script>")
    '                    ElseIf (msg = "Already refunded") Then
    '                        Response.Write("<script>alert('" + msg + "')</script>")
    '                    Else
    '                        Response.Write("<script>alert('something went wrong please try after some time!!')</script>")
    '                    End If
    '                Else
    '                    Response.Write("<script>alert('PG Transaction Not Found!!')</script>")
    '                End If

    '                BindGrid()
    '                td_Reject.Visible = False

    '            End If


    '        End If
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)

    '    End Try
    'End Sub

    Protected Sub GridHoldPNRAccept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridHoldPNRAccept.PageIndexChanging
        Try
            GridHoldPNRAccept.PageIndex = e.NewPageIndex
            BindGrid()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Public Function GetMerchantKey(ByVal orderID As String) As String


        Dim mrchntKey As String = ConfigurationManager.AppSettings("MerchantKey").ToString()

        Try


            Dim provider12 As String = ""
            Dim sqldom As New SqlTransactionDom()

            Dim dsp As New DataSet()

            dsp = sqldom.GetTicketingProvider(orderID)




            If (dsp.Tables.Count > 0) Then
                If (dsp.Tables(0).Rows.Count > 0) Then

                    provider12 = dsp.Tables(0).Rows(0)(0)

                End If


            End If


            If provider12.ToLower().Trim() = "yatra" Then

                mrchntKey = ConfigurationManager.AppSettings("YatraITZMerchantKey").ToString()
            End If

        Catch ex As Exception
            mrchntKey = ConfigurationManager.AppSettings("MerchantKey").ToString()
        End Try
        Session("MchntKeyITZ") = mrchntKey
        Return mrchntKey

    End Function
    Public Function PgAmount(ByVal orderID As String) As DataSet
        Dim adap As New SqlDataAdapter("USP_RefundPGAmount", con)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@orderid", orderID)
        Dim ds As New DataSet()
        adap.Fill(ds)
        Return ds
    End Function
    Public Function IntlUpdateHoldPNR(ByVal Status As String, ByVal Trip As String, ByVal ExecID As String) As DataSet
        Dim ds As New DataSet()

        adap = New SqlDataAdapter("Select CASE  WHEN FltHeader.PgCharges is null THEN  '0.00'  ELSE  FltHeader.PgCharges END as  PGCharges,FltHeader.*, isnull(agent_register.Branch,'DELHI') AS Branch,agent_register.SalesExecID,convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())/3600)+':'+convert(varchar(5),DateDiff(s, FltHeader.CreateDate,GETDATE())%3600/60)+':'+convert(varchar(5),(DateDiff(s,FltHeader.CreateDate, GETDATE())%60)) as TimeSincePending from FltHeader inner join agent_register on FltHeader.AgentId=agent_register.User_Id  where FltHeader.Status='" & Status & "' and Trip='" & Trip & "' and FltHeader.ExecutiveId='" & ExecID & "' AND  GdsPnr LIKE '%-FQ%'  order by CreateDate desc", con)
        ''adap = New SqlDataAdapter("Select CASE  WHEN FltHeader.PgCharges is null THEN  '0.00'  ELSE  FltHeader.PgCharges END as  PGCharges,* from FltHeader where Status='" & Status & "' and Trip='" & Trip & "' and ExecutiveId='" & ExecID & "' order by CreateDate desc", con)
        adap.Fill(ds)
        Return ds

    End Function
End Class
