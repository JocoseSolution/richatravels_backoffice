﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports YatraBilling
Imports System.IO

Imports HtmlAgilityPack
Imports System.Linq
'Imports iTextSharp.text
'Imports iTextSharp.text.pdf
'Imports iTextSharp.tool.xml
'Imports iTextSharp.text.html.simpleparser

Partial Class Reports_HoldPNR_UpdateHoldPNRIntl
    Inherits System.Web.UI.Page

    Dim ID As New IntlDetails()
    Dim STDom As New SqlTransactionDom
    Dim objSql As New SqlTransactionNew

    Dim STYTR As New SqlTransactionYatra

    ''changes my javed
    Dim objTktCopy As New IntlDetails()
    Dim baseUrl As String
    Dim FltHeaderList As New DataTable()
    Dim fltTerminalDetails As New DataTable()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                BindPartner()
                TicketDetail()


            Catch ex As Exception
                clsErrorLog.LogInfo(ex)

            End Try
        End If

    End Sub
    Private Sub TicketDetail()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim OrderId As String = "1"
            Dim dtagentid As New DataTable()
            dtagentid = ID.SelectAgent(OrderId)


            Dim dtagency As New DataTable()
            dtagency = ID.SelectAgencyDetail(dtagentid.Rows(0)("AgentId").ToString())


            Dim dtpnr As New DataTable()
            dtpnr = STDom.SelectHeaderDetail_HOLD(OrderId).Tables(0)
            Dim PRT As String = ""
            If (dtpnr.Rows(0)("PRT").ToString().ToUpper() = "PART OF ROUNDTRIP") Then
                PRT = " : Part of normal round trip(" & dtpnr.Rows(0)("FareType").ToString() & ") Reference OrderId : " & dtpnr.Rows(0)("PartOfOrderId").ToString() & ""
            End If

            td_agencyinfo.InnerText = "Agency Informaton" & PRT
            td_AgenctName.InnerText = dtagency.Rows(0)("Agency_Name").ToString()
            td_Add.InnerText = dtagency.Rows(0)("Address").ToString()
            td_City.InnerText = dtagency.Rows(0)("Address1").ToString()
            td_Mobile.InnerText = dtagency.Rows(0)("Mobile").ToString()
            td_Email.InnerText = dtagency.Rows(0)("Email").ToString()
            txt_GDSPNR.Text = dtpnr.Rows(0)("GdsPnr").ToString()
            txt_AirlinePNR.Text = dtpnr.Rows(0)("AirlinePnr").ToString()
            td_BookingDate.InnerText = dtpnr.Rows(0)("CreateDate").ToString()
            td_Status.InnerText = dtpnr.Rows(0)("Status").ToString()
            Repeater_Traveller.DataSource = ID.TravellerInfo(OrderId)
            Repeater_Traveller.DataBind()
            Dim dtflight As New DataTable()
            dtflight = ID.SelectFlightDetail(OrderId)
            Dim MgtFeeVisibleStatus As Boolean = False

            If Not IsDBNull(dtagency.Rows(0)("IsCorp")) Then
                If Convert.ToBoolean(dtagency.Rows(0)("IsCorp")) Then
                    MgtFeeVisibleStatus = True

                End If

            End If


            Dim my_table As String = ""
            my_table += "<table width='100%'>"
            my_table += "<tr>"
            my_table += "<td >"
            my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >"

            my_table += "<tr>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>From - To</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px; padding-left:10px'></td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Depart Date</td>"


            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>DepTime</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>ArrTime</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>Aircraft</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>AdtRbd</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>ChdRbd</td>"

            my_table += "</tr>"
            For Each dr1 As DataRow In dtflight.Rows
                my_table += "<tr>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px'> " & dr1("DepAirName").ToString() & " (" & dr1("DFrom").ToString() & " ) - " & dr1("ArrAirName").ToString() & " (" & (dr1("ATo").ToString()) & ")</td>"

                my_table += "<td>"
                my_table += "<table>"
                my_table += "<tr>"
                my_table += "<td> <img src='../../AirLogo/sm" & dr1("AirlineCode").ToString() & ".gif' /></td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'> " & dr1("AirlineName").ToString() & "(" & dr1("AirlineCode").ToString() & "-" & dr1("FltNumber").ToString() & ")</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"

                Dim depDate As String = ""
                depDate = Left(dr1("DepDate").ToString, 2) & " " & datecon(Mid(dr1("DepDate").ToString, 3, 2)) & ", " & Right(dr1("DepDate").ToString, 2)
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & depDate & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'> " & dr1("DepTime").ToString() & "Hrs</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'> " & dr1("ArrTime").ToString() & "Hrs</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr1("AirCraft").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr1("AdtRbd").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr1("ChdRbd").ToString() & "</td>"


                my_table += "</tr>"
            Next
            my_table += "</table>"
            my_table += "</td>"
            my_table += "</tr>"
            my_table += "</table>"
            lbl_FlightInfo.Text = my_table


            'Fare Details
            Dim my_table1 As String = ""
            Dim TransTD As String = ""

            Dim dtfare As New DataTable()
            dtfare = ID.SelectFareDetail(OrderId, TransTD)
            ViewState("TFADIS") = dtfare

            Dim dtadtcount As New DataTable()
            dtadtcount = ID.CountADT(OrderId)

            Dim dtchdcount As New DataTable()
            dtchdcount = ID.CountCHD(OrderId)

            Dim dtinfcount As New DataTable()
            dtinfcount = ID.CountINF(OrderId)

            If TransTD = "" OrElse TransTD Is Nothing Then
                Dim GrandTotal As Double = 0
                td_fareinfo.InnerText = "Fare Information " & "(" & dtfare.Rows(0)("FareType") & ")"
                my_table1 += "<table width='100%'>"

                my_table1 += "<tr>"
                my_table1 += "<td>"
                my_table1 += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >"
                my_table1 += "<tr>"
                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:100px'>Pax Detail</td>"
                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:100px'>Base Fare</td>"
                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:100px'>Fuel Surcharge</td>"
                my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:60px'>Tax</td>"
                my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:60px'>STax</td>"
                If MgtFeeVisibleStatus = True Then
                    my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px'>Mgt Fee</td>"

                Else
                    my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px'>Trans Fee</td>"
                    my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px'>Trans Charge</td>"

                End If


                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px''>TOTAL</td>"
                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px''></td>"
                my_table1 += "</tr>"
                For Each dr2 As DataRow In dtfare.Rows
                    If dr2("PaxType").ToString() = "ADT" Then
                        Dim BaseFare As Double = Convert.ToDouble(dr2("BaseFare").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim Fuel As Double = Convert.ToDouble(dr2("Fuel").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim Tax As Double = Convert.ToDouble(dr2("Tax").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim ServiceTax As Double = Convert.ToDouble(dr2("ServiceTax").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim TFee As Double = Convert.ToDouble(dr2("TFee").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim TCharge As Double = Convert.ToDouble(dr2("TCharge").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim mgtfee1 As Double = Convert.ToDouble(dr2("MgtFee").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())

                        Dim Total As Double = (If(MgtFeeVisibleStatus = True, (Convert.ToDouble(dr2("Total").ToString()) - Convert.ToDouble(dr2("TFee").ToString()) - Convert.ToDouble(dr2("TCharge").ToString())), Convert.ToDouble(dr2("Total").ToString()))) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString()) + mgtfee1
                        my_table1 += "<tr>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr2("PaxType").ToString() & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & BaseFare & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & Fuel & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & Tax & "</td>"
                        my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & ServiceTax & "</td>"
                        If MgtFeeVisibleStatus = True Then
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & mgtfee1.ToString() & "</td>"

                        Else
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & TFee & "</td>"
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & TCharge & "</td>"

                        End If

                        my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; height: 20px;font-weight:Bold'>" & Total & "</td>"
                        GrandTotal += (If(MgtFeeVisibleStatus = True, (Convert.ToDouble(dr2("Total").ToString()) - Convert.ToDouble(dr2("TFee").ToString()) - Convert.ToDouble(dr2("TCharge").ToString())), Convert.ToDouble(dr2("Total").ToString()))) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString()) + mgtfee1
                        my_table1 += "</tr>"
                    End If
                    If dr2("PaxType").ToString() = "CHD" Then
                        Dim BaseFare As Double = Convert.ToDouble(dr2("BaseFare").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        Dim Fuel As Double = Convert.ToDouble(dr2("Fuel").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        Dim Tax As Double = Convert.ToDouble(dr2("Tax").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        Dim ServiceTax As Double = Convert.ToDouble(dr2("ServiceTax").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        Dim TFee As Double = Convert.ToDouble(dr2("TFee").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        Dim TCharge As Double = Convert.ToDouble(dr2("TCharge").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        Dim mgtfee1 As Double = Convert.ToDouble(dr2("MgtFee").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim Total As Double = (If(MgtFeeVisibleStatus = True, (Convert.ToDouble(dr2("Total").ToString()) - Convert.ToDouble(dr2("TFee").ToString()) - Convert.ToDouble(dr2("TCharge").ToString())), Convert.ToDouble(dr2("Total").ToString()))) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString()) + mgtfee1
                        my_table1 += "<tr>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr2("PaxType").ToString() & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & BaseFare & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & Fuel & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & Tax & "</td>"
                        my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & ServiceTax & "</td>"


                        If MgtFeeVisibleStatus = True Then
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & mgtfee1.ToString() & "</td>"

                        Else
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & TFee & "</td>"
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & TCharge & "</td>"

                        End If
                        my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; height: 20px;font-weight:Bold'>" & Total & "</td>"
                        GrandTotal += Convert.ToDouble(dr2("Total").ToString()) * Convert.ToDouble(dtchdcount.Rows(0)(0).ToString())
                        my_table1 += "</tr>"
                    End If
                    If dr2("PaxType").ToString() = "INF" Then
                        Dim BaseFare As Double = Convert.ToDouble(dr2("BaseFare").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
                        Dim Fuel As Double = Convert.ToDouble(dr2("Fuel").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
                        Dim Tax As Double = Convert.ToDouble(dr2("Tax").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
                        Dim ServiceTax As Double = Convert.ToDouble(dr2("ServiceTax").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
                        Dim TFee As Double = Convert.ToDouble(dr2("TFee").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
                        Dim TCharge As Double = Convert.ToDouble(dr2("TCharge").ToString()) * Convert.ToDouble(dtinfcount.Rows(0)(0).ToString())
                        Dim mgtfee1 As Double = Convert.ToDouble(dr2("MgtFee").ToString()) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString())
                        Dim Total As Double = (If(MgtFeeVisibleStatus = True, (Convert.ToDouble(dr2("Total").ToString()) - Convert.ToDouble(dr2("TFee").ToString()) - Convert.ToDouble(dr2("TCharge").ToString())), Convert.ToDouble(dr2("Total").ToString()))) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString()) + mgtfee1
                        my_table1 += "<tr>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr2("PaxType").ToString() & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & BaseFare & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & Fuel & "</td>"
                        my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & Tax & "</td>"
                        my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & ServiceTax & "</td>"


                        If MgtFeeVisibleStatus = True Then
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & mgtfee1.ToString() & "</td>"

                        Else
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & TFee & "</td>"
                            my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & TCharge & "</td>"

                        End If
                        my_table1 += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; height: 20px;font-weight:Bold'>" & Total & "</td>"
                        GrandTotal += (If(MgtFeeVisibleStatus = True, (Convert.ToDouble(dr2("Total").ToString()) - Convert.ToDouble(dr2("TFee").ToString()) - Convert.ToDouble(dr2("TCharge").ToString())), Convert.ToDouble(dr2("Total").ToString()))) * Convert.ToDouble(dtadtcount.Rows(0)(0).ToString()) + mgtfee1
                        my_table1 += "</tr>"

                    End If
                Next
                Dim FinalTotal As Double
                'Dim clstktcopy As New clsTicketCopy
                'my_table1 += "<tr>"
                'my_table1 += "<td colspan='6'>"
                'If (dtpnr.Rows(0)("TripType") = "R") Then
                '    my_table1 += clstktcopy.Meal_BagDetails(OrderId, TransTD, dtflight.Rows(0)("AirlineCode"), "O", FinalTotal, "OutBound")
                '    my_table1 += clstktcopy.Meal_BagDetails(OrderId, TransTD, dtflight.Rows(0)("AirlineCode"), "R", FinalTotal, "InBound")
                'Else
                '    my_table1 += clstktcopy.Meal_BagDetails(OrderId, TransTD, dtflight.Rows(0)("AirlineCode"), "O", FinalTotal, "OutBound")
                'End If
                'my_table1 += "</td>"
                'my_table1 += "</tr>"

                'BAGGAGE

                Dim dtfare1 As DataSet = objSql.Get_PAX_MB_Details(OrderId, TransTD, dtflight.Rows(0)("AirlineCode"), dtpnr.Rows(0)("TripType"))
                Dim DtPxMB As DataTable = dtfare1.Tables(0)
                If DtPxMB.Rows.Count > 0 Then
                    my_table1 += "<tr >"
                    my_table1 += "<td colspan='9' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 25px;padding-left:10px;width:250px;background-color: #CCCCCC;'>Meals Baggage Fare Information</td>"
                    my_table1 += "</td>"
                    my_table1 += "</tr>"

                    my_table1 += "<tr >"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px;width:250px'>Pax Name</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Type</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Meal Detail</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Meal Price</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Bag Detail</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Bag Price</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Ticket No</td>"
                    my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>TOTAL</td>"
                    my_table += "</tr>"

                    For Each dr As DataRow In DtPxMB.Rows
                        If (Convert.ToDouble(dr("MPRICE").ToString()) > 0 OrElse Convert.ToDouble(dr("BPRICE").ToString()) > 0) Then


                            my_table1 += "<tr>"
                            my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>" & dr("Name").ToString() & "</td>"
                            my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;width:250px' >" & dr("PaxType").ToString() & "</td>"
                            If (Convert.ToDouble(dr("MPRICE").ToString()) > 0) Then
                                my_table1 += "<td  style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>" & dr("MDisc").ToString() & "</td>"
                                my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>" & dr("MPRICE").ToString() & "</td>"
                            Else
                                my_table1 += "<td  style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>N/A</td>"
                                my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>N/A</td>"

                            End If
                            If (Convert.ToDouble(dr("BPRICE").ToString()) > 0) Then
                                my_table1 += "<td align='left' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>" & dr("BDisc").ToString() & "</td>"
                                my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>" & dr("BPRICE").ToString() & "</td>"
                            Else
                                my_table1 += "<td align='left' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>N/A</td>"
                                my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;width:250px'>N/A</td>"
                            End If

                            my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;width:250px'>" & dr("TicketNumber").ToString() & "</td>"
                            my_table1 += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;color: #004b91;font-weight:Bold'>" & Convert.ToDouble(Convert.ToDouble(dr("MPRICE").ToString()) + Convert.ToDouble(dr("BPRICE").ToString())) & "</td>"
                            my_table1 += "</tr>"
                            FinalTotal += Convert.ToDouble(Convert.ToDouble(dr("MPRICE").ToString()) + Convert.ToDouble(dr("BPRICE").ToString()))
                        End If
                    Next

                End If
                'END


                my_table1 += "<tr style='background-color: #FFF;'>"
                If MgtFeeVisibleStatus = True Then
                    my_table1 += "<td colspan='3' align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'></td>"
                    my_table1 += "<td colspan='3'  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;color: #004b91; height: 20px'>GRAND TOTAL</td>"

                Else
                    my_table1 += "<td colspan='5' align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'></td>"
                    my_table1 += "<td colspan='2'  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;color: #004b91; height: 20px'>GRAND TOTAL</td>"

                End If

                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; color: #000000; height: 20px;'>" & GrandTotal + FinalTotal & "</td>"
                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'></td>"
                my_table1 += "</tr>"
                my_table1 += "<tr style='background-color: #FFF;'>"
                If MgtFeeVisibleStatus = True Then
                    my_table1 += "<td colspan='3' align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'></td>"
                    my_table1 += "<td colspan='3' align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;color: #004b91; height: 20px'>NET FARE</td>"

                Else
                    my_table1 += "<td colspan='5' align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'></td>"
                    my_table1 += "<td colspan='2' align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;color: #004b91; height: 20px'>NET FARE</td>"

                End If

                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; color: #000000; height: 20px;'>" & dtpnr.Rows(0)("TotalAfterDis").ToString() & "</td>"
                my_table1 += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'></td>"
                my_table1 += "</tr>"
                my_table1 += "</table>"
                my_table1 += "</td>"
                my_table1 += "</tr>"

                my_table1 += "</table>"
                lbl_FareInfo.Text = my_table1
            End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_update.Click
        Try
            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Try
                Dim cmd As New SqlCommand("USP_updatePnrSp_PP")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Order_id", OrderId)
                cmd.Parameters.AddWithValue("@AirLinePnr", txt_AirlinePNR.Text.Trim())
                cmd.Parameters.AddWithValue("@GdsPnr", txt_GDSPNR.Text.Trim())
                cmd.Parameters.Add("@PartnerName", SqlDbType.VarChar).Value = Partnername
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery().ToString()
                con.Close()
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try

            Dim dt As New DataTable
            'dt = ViewState("TFADIS")
            dt = ID.SelectHeaderDetail(OrderId)

            Dim i As Integer = 0
            For Each rw As RepeaterItem In Repeater_Traveller.Items
                i += 1

                Dim TktNo As TextBox = DirectCast(rw.FindControl("txt_TktNo"), TextBox)
                Dim TID As Label = DirectCast(rw.FindControl("lbl_TransID"), Label)
                Dim PaxType As Label = DirectCast(rw.FindControl("lbl_paxtype"), Label)
                Dim Fare As Label = DirectCast(rw.FindControl("lbl_Fare"), Label)
                If dt.Rows(0)("VC").ToString.Trim.ToUpper <> "6E" And dt.Rows(0)("VC").ToString.Trim.ToUpper <> "SG" And dt.Rows(0)("VC").ToString.Trim.ToUpper <> "G8" Then
                    ID.UpdateTicketIntl(OrderId, TID.Text, TktNo.Text.Trim())
                    STDom.UpdateTktDomIntlOnLedger(OrderId, Convert.ToInt32(TID.Text.Trim), TktNo.Text.Trim())
                Else
                    ID.UpdateTicketIntl(OrderId, TID.Text, TktNo.Text.Trim() & i.ToString)
                    STDom.UpdateTktDomIntlOnLedger(OrderId, Convert.ToInt32(TID.Text.Trim), TktNo.Text.Trim() & i.ToString)
                End If
            Next
            'NAV METHOD  CALL START
            Try
                UpdateSearchNBookID(OrderId, Txt_TICKETID.Text.Trim)
            Catch ex As Exception

            End Try

            Try

                'Dim objNav As New AirService.clsConnection(OrderId, "0", "0")
                'objNav.airBookingNav(OrderId, "", 0)

            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
            'Nav METHOD END'
            Try

                'STYTR.InsertYatra_MIRHEADER(OrderId, txt_GDSPNR.Text.Trim())
                'STYTR.InsertYatra_PAX(OrderId, txt_GDSPNR.Text.Trim())
                'STYTR.InsertYatra_SEGMENT(OrderId, txt_GDSPNR.Text.Trim())
                'STYTR.InsertYatra_FARE(OrderId, txt_GDSPNR.Text.Trim())
                'STYTR.InsertYatra_DIFTLINES(OrderId, txt_GDSPNR.Text.Trim())

                'Dim AirObj As New AIR_YATRA
                'AirObj.ProcessYatra_Air(OrderId, txt_GDSPNR.Text.Trim(), "B")
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
            Try
                'Dim agent_id As String
                'Dim email_id As String
                'Dim dtaid As New DataTable()
                'dtaid = ID.EmailID(OrderId)
                'agent_id = dtaid.Rows(0)("AgentId").ToString()
                'Dim dtemail As New DataTable()
                'dtemail = ID.AgentIDInfo(agent_id)
                'email_id = dtemail.Rows(0)("Email").ToString()
                'Dim s As String = ""
                'Dim objTktCopy As New clsTicketCopy
                's = objTktCopy.TicketDetail(OrderId, "")
                'Dim MailDt As New DataTable
                'MailDt = STDom.GetMailingDetails(MAILING.AIR_PNRSUMMARY.ToString(), Session("UID").ToString()).Tables(0)

                'Dim email As String = Request("txt_email")
                Dim dtflight As New DataTable()
                dtflight = ID.SelectFlightDetail(OrderId)
                mailTktCopy(dt.Rows(0)("VC").ToString.Trim, dtflight.Rows(0)("FltNumber").ToString.Trim, dt.Rows(0)("Sector").ToString.Trim, dtflight.Rows(0)("DepDate").ToString.Trim, dt.Rows(0)("FareType").ToString.Trim, dt.Rows(0)("AirlinePnr").ToString.Trim, dt.Rows(0)("GdsPnr").ToString.Trim, dt.Rows(0)("Status").ToString.Trim, OrderId, dt.Rows(0)("PgEmail").ToString.Trim)
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
            Try
                Dim smsStatus As String = ""
                Dim smsMsg As String = ""
                Dim objSMSAPI As New SMSAPI.SMS
                Dim dtpnr As New DataTable()
                dtpnr = ID.SelectHeaderDetail(OrderId)
                Dim dtflight As New DataTable()
                dtflight = ID.SelectFlightDetail(OrderId)
                Dim SmsCrd As DataTable
                Dim objDA As New SqlTransaction
                SmsCrd = objDA.SmsCredential(SMS.AIRHOLDCNF.ToString()).Tables(0)
                If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                    smsStatus = objSMSAPI.sendSms(OrderId, dtpnr.Rows(0)("PgMobile").ToString.Trim, dtpnr.Rows(0)("sector").ToString.Trim, dtpnr.Rows(0)("VC").ToString.Trim, dtflight.Rows(0)("FltNumber").ToString.Trim, dtflight.Rows(0)("DepDate"), txt_GDSPNR.Text, smsMsg, SmsCrd)
                    objSql.SmsLogDetails(OrderId, dtpnr.Rows(0)("PgMobile").ToString.Trim, smsMsg, smsStatus)
                End If

            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
            'Response.Write("<script language='javascript'>alert('Ticket Updated Successfully.');window.location.reload();</script>")
            'Response.Write("<script language='javascript'>alert('Ticket Updated Sucessfully')</script>")
            ' Response.Write("<script language=javascript>window.opener.location.reload();self.close();</script>")
            'Response.Write("<script language='javascript'>self.close();</script>")
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Updated Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", True)

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function datecon(ByVal MM As String) As String
        Dim mm_str As String = ""
        Select Case MM
            Case "01"
                mm_str = "JAN"
            Case "02"
                mm_str = "FEB"
            Case "03"
                mm_str = "MAR"
            Case "04"
                mm_str = "APR"
            Case "05"
                mm_str = "MAY"
            Case "06"
                mm_str = "JUN"
            Case "07"
                mm_str = "JUL"
            Case "08"
                mm_str = "AUG"
            Case "09"
                mm_str = "SEP"
            Case "10"
                mm_str = "OCT"
            Case "11"
                mm_str = "NOV"
            Case "12"
                mm_str = "DEC"
            Case Else

        End Select

        Return mm_str

    End Function
    Public Function Meal_BagDetails(ByVal OrderId As String, ByVal TransTD As String, ByVal VC As String, ByVal TT As String, ByRef FinalTotal As Double, ByVal HD As String) As String
        Dim my_table As String = ""
        Dim dtfare1 As DataSet = objSql.Get_PAX_MB_Details(OrderId, TransTD, VC, TT)
        Dim DtPxMB As DataTable = dtfare1.Tables(0)

        If DtPxMB.Rows.Count > 0 Then
            my_table += "<tr>"
            my_table += "<td style='border: thin solid #999999'>"
            my_table += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>"
            my_table += "<tr>"
            my_table += "<td colspan='3'  style='background-color: #135f06; color: #ffffff; font-family: arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; height: 25px;padding-left:10px'>Meals Bagagae Fare Information" + HD + "</td>"
            my_table += "</tr>"
            my_table += "<tr >"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Passenger Name</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Type</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>MEAL_DETAIL</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>MEAL_PRICE</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>BAG_DETAIL</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>BAG_PRICE</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>TOTAL</td>"
            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Ticket No</td>"
            my_table += "</tr>"
            'If TransTD = "" OrElse TransTD Is Nothing Then
            For Each dr As DataRow In DtPxMB.Rows
                my_table += "<tr>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" & dr("Name").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px' >" & dr("PaxType").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" & dr("MDisc").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" & dr("MPRICE").ToString() & "</td>"
                my_table += "<td  style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" & dr("BDisc").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" & dr("BPRICE").ToString() & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" & Convert.ToDouble(Convert.ToDouble(dr("MPRICE").ToString()) + Convert.ToDouble(dr("BPRICE").ToString())) & "</td>"
                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" & dr("TicketNumber").ToString() & "</td>"
                my_table += "</tr>"
                FinalTotal += Convert.ToDouble(Convert.ToDouble(dr("MPRICE").ToString()) + Convert.ToDouble(dr("BPRICE").ToString()))
            Next
        End If
        my_table += "<tr style='background-color: gray;'>"
        my_table += "<td   align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'></td>"
        my_table += "<td   colspan='7' align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;color: #FFFF99; height: 20px' >GRAND TOTAL </td>"
        my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 14px; color: #FFFF99; height: 20px; ' id='td_grandtot'    >" & FinalTotal & "</td>"
        my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'></td>"
        my_table += "</tr>"
        my_table += "</table>"
        my_table += "</td>"
        my_table += "</tr>"
        Return my_table

    End Function
    Public Function SaveTextToFile(ByVal strData As String, ByVal FullPath As String, Optional ByVal ErrInfo As String = "") As Boolean
        'Dim Contents As String
        Dim Saved As Boolean = False
        Dim objReader As StreamWriter
        Try
            objReader = New StreamWriter(FullPath)
            objReader.Write(strData)
            objReader.Close()
            Saved = True
        Catch Ex As Exception
            ErrInfo = Ex.Message
        End Try
        Return Saved
    End Function

    Public Sub BindPartner()
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("BindPartnerNameSP_PP")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        con.Open()
        txtPartnerName.DataSource = cmd.ExecuteReader()
        txtPartnerName.DataTextField = "PartnerName"
        txtPartnerName.DataValueField = "PartnerName"
        txtPartnerName.DataBind()
        con.Close()
        txtPartnerName.Items.Insert(0, New ListItem("--Select PartnerName--", ""))
    End Sub
    Public Function UpdateSearchNBookID(ByVal OrderID As String, ByVal TicketID As String) As Integer
        Dim objDataAcess As New DataAccess(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim paramHashtable As New Hashtable
        paramHashtable.Clear()
        paramHashtable.Add("@OrderID", OrderID)
        paramHashtable.Add("@TicketID", TicketID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateSearchNBookID", 1)
    End Function

    ''changes by javed

    Private Sub mailTktCopy(ByVal VC As String, ByVal FltNo As String, ByVal Sector As String, ByVal DepDate As String, ByVal FT As String, ByVal AirlinePnr As String, ByVal GdsPnr As String, ByVal BkgStatus As String, ByVal OrderId As String, ByVal EmailId As String) 'As String
        'Try
        '    Dim objTktCopy As New clsTicketCopy
        '    Dim strTktCopy As String = "", strHTML As String = "", strFileName As String = "", strMailMsg As String = ""

        '    Dim rightHTML As Boolean = False
        '    strFileName = Convert.ToString(ConfigurationManager.AppSettings("TicketCopy")) & GdsPnr & "-" & FT & " Flight details-" & DateAndTime.Now.ToString.Replace(":", "").Trim & ".html"


        '    strFileName = strFileName.Replace("/", "-")
        '    'strTktCopy = objTktCopy.TicketDetail(OrderId, "")
        '    strTktCopy = TicketCopyExportPDF(OrderId, "")
        '    strHTML = "<html><head><title>Booking Details</title><style type='text/css'> .maindiv{border: #20313f 1px solid; margin: 10px auto 10px auto; width: 650px; font-size:12px; font-family:tahoma,Arial;}	 .text1{color:#333333; font-weight:bold;}	 .pnrdtls{font-size:12px; color:#333333; text-align:left;font-weight:bold;}	 .pnrdtls1{font-size:12px; color:#333333; text-align:left;}	 .bookdate{font-size:11px; color:#CC6600; text-align:left}	 .flthdr{font-size:11px; color:#CC6600; text-align:left; font-weight:bold}	 .fltdtls{font-size:11px; color:#333333; text-align:left;}	.text3{font-size:11px; padding:5px;color:#333333; text-align:right}	 .hdrtext{padding-left:5px; font-size:14px; font-weight:bold; color:#FFFFFF;}	 .hdrtd{background-color:#333333;}	  .lnk{color:#333333;text-decoration:underline;}	  .lnk:hover{color:#333333;text-decoration:none;}	  .contdtls{font-size:12px; padding-top:8px; padding-bottom:3px; color:#333333; font-weight:bold}	  .hrcss{color:#CC6600; height:1px; text-align:left; width:450px;}	 </style></head><body>" & strTktCopy & "</body></html>"
        '    rightHTML = SaveTextToFile(strHTML, strFileName)
        '    strMailMsg = "<p style='font-family:verdana; font-size:12px'>Dear Customer<br /><br />"
        '    strMailMsg = strMailMsg & "Greetings of the day !!!!<br /><br />"
        '    strMailMsg = strMailMsg & "Please find an attachment for your E-ticket, kindly carry the print out of the same for hassle-free travel. Your onward booking for " & Sector & " is confirmed on " & VC & "-" & FltNo & " for " & DepDate & ". Your airline  booking reference no is " & AirlinePnr & ". <br /><br />"
        '    strMailMsg = strMailMsg & "Have a nice &amp; wonderful trip.<br /><br />"

        '    If BkgStatus = "Ticketed" Then


        '        Dim MailDt As New DataTable
        '        MailDt = STDom.GetMailingDetails(MAILING.AIR_BOOKING.ToString(), Session("UID").ToString()).Tables(0)
        '        Try
        '            If rightHTML Then
        '                STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), strFileName)
        '            Else
        '                STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), "")
        '            End If
        '        Catch ex As Exception

        '        End Try
        '    End If

        'Catch ex As Exception

        'End Try
        ' Return strTktCopy

        Try
            Dim Body As String = ""
            Dim strFileNmPdf As String = ""
            Dim TicketFormate As String
            Dim strMailMsg As String
            Dim writePDF As Boolean = False
            Dim status1 As Integer = 0
            Try
                TicketFormate = TicketCopyExportPDF(OrderId, "").Trim.ToString
                'strFileNmPdf = ConfigurationManager.AppSettings("TicketCopy").ToString().Trim() + Request.QueryString("OrderId").ToString + "-" + DateTime.Now.ToString().Replace(":", "").Replace("/", "-").Replace(" ", "-").Trim() + ".pdf"
                'Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4)
                'Dim writer As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, New FileStream(strFileNmPdf, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
                'pdfDoc.Open()
                'Dim sr As New StringReader(TicketFormate)
                'iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr)
                'pdfDoc.Close()
                'writer.Dispose()
                'sr.Dispose()
                'pdfDoc.Dispose()
                'writePDF = True
            Catch ex As Exception
            End Try

            'strMailMsg = "<p style='font-family:verdana; font-size:12px'>Dear Customer<br /><br />"
            'strMailMsg = strMailMsg & "Greetings of the day !!!!<br /><br />"
            'strMailMsg = strMailMsg & "Please find an attachment for your E-ticket, kindly carry the print out of the same for hassle-free travel. Your onward booking for " & Sector & " is confirmed on " & VC & "-" & FltNo & " for " & DepDate & ". Your airline  booking reference no is " & AirlinePnr & ". <br /><br />"
            'strMailMsg = strMailMsg & "Have a nice &amp; wonderful trip.<br /><br />"

            strMailMsg = TicketFormate

            Dim MailDt As New DataTable
            MailDt = STDom.GetMailingDetails(MAILING.AIR_BOOKING.ToString(), Session("UID").ToString()).Tables(0)

            If (MailDt.Rows.Count > 0) Then
                Dim Status As Boolean = False
                Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())
                Try
                    If Status = True And writePDF = True Then
                        STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), strFileNmPdf)
                    Else
                        STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), "")
                    End If
                Catch ex As Exception

                End Try

            End If

        Catch ex As Exception

        End Try

    End Sub



    Public Function TicketCopyExportPDF(OrderId As String, TransID As String) As String

        Dim strFileNmPdf As String = ""
        Dim writePDF As Boolean = False
        Dim TktCopy As String = ""
        Dim Gtotal As Integer = 0
        Dim initialAdt As Integer = 0
        Dim initalChld As Integer = 0
        Dim initialift As Integer = 0
        Dim MealBagTotalPrice As Decimal = 0
        Dim AdtTtlFare As Decimal = 0
        Dim ChdTtlFare As Decimal = 0
        Dim INFTtlFare As Decimal = 0
        Dim fare As Decimal = 0

        'Dim OrderId As String = "1c2019deXCP9cVSU"
        'Dim TransID As String = ""


        Dim objTranDom As New SqlTransactionDom()
        Dim SqlTrasaction As New SqlTransaction()
        Dim objSql As New SqlTransactionNew()
        Dim FltPaxList As New DataTable()

        Dim FltDetailsList As New DataTable()
        Dim FltProvider As New DataTable()
        Dim FltBaggage As New DataTable()
        Dim dtagentid As New DataTable()
        Dim FltagentDetail As New DataTable()
        Dim fltTerminal As New DataTable()
        Dim fltFare As New DataTable()
        Dim fltMealAndBag As New DataTable()
        Dim fltMealAndBag1 As New DataTable()
        Dim fltAirportDetails As New DataSet()
        Dim SelectedFltDS As New DataSet()
        FltPaxList = SelectPaxDetail(OrderId, TransID)
        FltHeaderList = objTktCopy.SelectHeaderDetail(OrderId)
        FltDetailsList = objTktCopy.SelectFlightDetail(OrderId)
        FltProvider = (objTranDom.GetTicketingProvider(OrderId)).Tables(0)
        dtagentid = objTktCopy.SelectAgent(OrderId)
        SelectedFltDS = SqlTrasaction.GetFltDtls(OrderId, dtagentid.Rows(0)("AgentID").ToString())
        Dim Bag As Boolean = False
        If Not String.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))) Then
            Bag = Convert.ToBoolean(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))
        End If
        FltBaggage = (objTranDom.GetBaggageInformation(Convert.ToString(FltHeaderList.Rows(0)("Trip")), Convert.ToString(FltHeaderList.Rows(0)("VC")), Bag)).Tables(0)

        FltagentDetail = objTktCopy.SelectAgencyDetail(dtagentid.Rows(0)("AgentID").ToString())

        fltFare = objTktCopy.SelectFareDetail(OrderId, TransID)
        Dim dt As DateTime = Convert.ToDateTime(Convert.ToString(FltHeaderList.Rows(0)("CreateDate")))
        Dim [date] As String = dt.ToString("dd/MMM/yyyy").Replace("-", "/")

        Dim Createddate As String = [date].Split("/")(0) + " " + [date].Split("/")(1) + " " + [date].Split("/")(2)

        Dim fltmealbag As DataRow() = objSql.Get_MEAL_BAG_FareDetails(OrderId, TransID).Tables(0).Select("MealPrice>0 or BaggagePrice>0 ")
        fltMealAndBag1 = objSql.Get_MEAL_BAG_FareDetails(OrderId, TransID).Tables(0) '.Select("MealPrice>0 or BaggagePrice>0 ").CopyToDataTable()
        If fltmealbag.Length > 0 Then

            fltMealAndBag = fltMealAndBag1.Select("MealPrice>0 or BaggagePrice>0 ").CopyToDataTable()
        End If
        Try
            'Dim strAirline As String = "SG6EG8"

            Dim TicketFormate As String = ""


            If ((Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm" OrElse Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirmbyagent") And Session("UserType") = "TA") Then

                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 15px; width: 15%; text-align: left; padding: 5px;'>"
                TicketFormate += "<b>Booking Reference No. " & OrderId & "</b>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 14px; width: 15%; text-align: left; padding: 5px;'>"
                TicketFormate += "The PNR-<b>" & FltHeaderList.Rows(0)("GdsPnr") & " </b>is on <b>HOLD</b> and contact customer care for issuance."
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                'TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>"
                ' TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                'TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                ' TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                ' TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>"
                ' TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                ' TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                ' TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                'TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>"
                'TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                'TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                'TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"

                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>FareType</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next

                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"

                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    'Response.Write(strDepdt)

                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"

                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"

                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</table>"
                'Div_Main.Visible = False


            ElseIf (Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "rejected" And Session("UserType") = "TA") Then

                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left;font-size:15px;'>"
                TicketFormate += "<b>Booking Reference No. " & OrderId & "</b>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left;font-size:14px;'>"
                TicketFormate += "Please re-try the booking.Your booking has been rejected due to some technical issue at airline end."
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger & Ticket Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"
                TicketFormate += "</tr>"


                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>FareType</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next

                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    ''Response.Write(strDepdt)

                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"
                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</table>"

            ElseIf (Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "inprocess" And Session("UserType") = "TA") Then

                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:center;font-size:15px;'>"
                TicketFormate += "<b>Booking Reference No. " & OrderId & "</b>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left;font-size:14px;'>"
                TicketFormate += "We are updating the details, Please wait for some time."
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger & Ticket Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>FareType</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"


                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"

                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"
                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</table>"

            Else

                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='width:50%;text-align:left;'>"


                If Not IsDBNull(FltagentDetail.Rows(0)("ag_logo")) Then
                    If (FltagentDetail.Rows(0)("ag_logo") <> "") Then
                        Dim dd As String = ResolveClientUrl("~/AgentLogo/" + FltagentDetail.Rows(0)("ag_logo"))
                        TicketFormate += "<img src='" + dd + "' alt='Logo' style='height:70px; width:200px' />"
                    Else
                        TicketFormate += "<img src='http://richatravels.com/images/logo.png' alt='Logo' style='height:70px; width:200px' />"
                    End If
                Else
                    TicketFormate += "<img src='http://richatravels.com/images/logo.png' alt='Logo' style='height:70px;width:200px'/>"
                    'TicketFormate += "<img src='" + ResolveUrl("~/images/logo.png") + "' alt='Logo' style='height:70px; width:200px' />"
                End If


                TicketFormate += "</td>"
                TicketFormate += "<td style='width: 50%;text-align:right;display:none;'>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='width:50%;text-align:left;'>"
                TicketFormate += ""
                TicketFormate += "</td>"
                TicketFormate += "<td style='width: 50%;text-align:right;'>"
                TicketFormate += ""
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='width:100%;height:10px;'></td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='2' style='vertical-align:bottom;color:#f58220;text-align:right;width:100%;font-size:16px;font-weight:bold;'>"
                TicketFormate += "Electronic Ticket"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='2' style='height: 2px; width: 100%; border: 1px solid #0b2759'></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"


                TicketFormate += "<table style='width: 100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='width: 100%; text-align: justify; color: #0b2759; font-size: 11px; padding: 10px;'>"
                TicketFormate += "This is travel itinerary and E-ticket receipt. You may need to show this receipt to enter the airport and/or to show return or onward travel to "
                TicketFormate += "customs and immigration officials."
                TicketFormate += "<br />"

                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"

                TicketFormate += "<table style='border: 1px solid #0b2759; font-family: Verdana, Geneva, sans-serif; font-size: 12px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger & Ticket Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 5px; width: 100%'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"

                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm" OrElse Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirmbyagent", "Hold", FltHeaderList.Rows(0)("Status"))
                'TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>FareType</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px; font-weight: bold;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next

                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #0b2759; color: #fff; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #f58220; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #0b2759;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #fff; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1


                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"


                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += "<img alt='' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    'TicketFormate += "<img alt='' src='" + ResolveUrl("~/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode")) + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    TicketFormate += "<b>" + FltDetailsList.Rows(0)("AirlineName") + "</b>"

                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    'strDepdt = strDepdt.Substring(0, 2) + "-" + strDepdt.Substring(2, 2) + "-" + strDepdt.Substring(4, 2)
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    ''Response.Write(strDepdt)


                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    Try
                        If strdeptime.Length > 4 Then
                            strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(3, 2)
                        Else
                            strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                        End If
                    Catch ex As Exception
                        clsErrorLog.LogInfo(ex)
                    End Try

                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    'strArvdt = strArvdt.Substring(0, 2) + "-" + strArvdt.Substring(2, 2) + "-" + strArvdt.Substring(4, 2)
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    Try
                        If strArrtime.Length > 4 Then
                            strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(3, 2)
                        Else
                            strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                        End If
                    Catch ex As Exception
                        clsErrorLog.LogInfo(ex)
                    End Try


                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"

                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    'if (!String.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[0]["DepartureTerminal"])))
                    '    TicketFormate += "Terminal:" + fltTerminal.Rows[0]["DepartureTerminal"];
                    'else
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; font-weight: bold;'>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    'if (!String.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[f]["ArrivalTerminal"])))
                    '    TicketFormate += "Terminal:" + fltTerminal.Rows[f]["ArrivalTerminal"];
                    'else
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://richatravels.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:11px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"

                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='background-color: #0b2759; color: #f58220;font-size:11px;font-weight:bold; padding: 5px;'>"
                TicketFormate += "Fare Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                If TransID = "" OrElse TransID Is Nothing Then
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='8' style= 'background-color: #0b2759;width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Type</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Count</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Base fare</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Fuel Surcharge</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Tax</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>STax</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Trans Fee</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trans Charge</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"


                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='8' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    For fd As Integer = 0 To fltFare.Rows.Count - 1

                        If fltFare.Rows(fd)("PaxType").ToString() = "ADT" AndAlso initialAdt = 0 Then
                            Dim numberOfADT As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "ADT").ToList().Count
                            TicketFormate += "<tr>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += fltFare.Rows(fd)("PaxType")
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_adtcnt'>" & numberOfADT & "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("BaseFare")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Fuel")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'id='td_taxadt'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Tax")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ServiceTax")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TFee")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_tcadt'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TCharge")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;' id='td_adttot'>"
                            AdtTtlFare = (Convert.ToDecimal(fltFare.Rows(fd)("Total")) * numberOfADT).ToString
                            TicketFormate += AdtTtlFare.ToString
                            TicketFormate += "</td>"

                            TicketFormate += "</tr>"

                            initialAdt += 1
                        End If

                        If fltFare.Rows(fd)("PaxType").ToString() = "CHD" AndAlso initalChld = 0 Then
                            Dim numberOfCHD As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "CHD").ToList().Count
                            TicketFormate += "<tr>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += fltFare.Rows(fd)("PaxType")
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_chdcnt'>" & numberOfCHD & "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("BaseFare")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Fuel")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'id='td_taxchd'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Tax")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ServiceTax")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TFee")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_tcchd'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TCharge")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_chdtot'>"
                            ChdTtlFare = (Convert.ToDecimal(fltFare.Rows(fd)("Total")) * numberOfCHD).ToString
                            TicketFormate += ChdTtlFare.ToString
                            TicketFormate += "</td>"

                            TicketFormate += "</tr>"

                            initalChld += 1
                        End If
                        If fltFare.Rows(fd)("PaxType").ToString() = "INF" AndAlso initialift = 0 Then
                            Dim numberOfINF As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "INF").ToList().Count
                            TicketFormate += "<tr>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += fltFare.Rows(fd)("PaxType")
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_infcnt'>" & numberOfINF & "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("BaseFare")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Fuel")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Tax")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ServiceTax")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TFee")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TCharge")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'id='td_Inftot'>"
                            INFTtlFare = (Convert.ToDecimal(fltFare.Rows(fd)("Total")) * numberOfINF).ToString
                            TicketFormate += INFTtlFare.ToString
                            TicketFormate += "</td>"
                            TicketFormate += "</tr>"
                            initialift += 1

                        End If
                    Next
                    fare = AdtTtlFare + ChdTtlFare + INFTtlFare
                Else
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='2' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"


                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top; display:none;'id='td_perpaxtype'>" + FltPaxList.Rows(0)("PaxType") + "</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Base Fare</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("BaseFare").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Fuel Surcharge</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("Fuel").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Tax</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;' id='td_perpaxtax'>"
                    TicketFormate += fltFare.Rows(0)("Tax").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>STax</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("ServiceTax").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Trans Fee</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("TFee").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Trans Charge</td>"
                    TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'id='td_perpaxtc'>"
                    TicketFormate += fltFare.Rows(0)("TCharge").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"

                    Dim ResuCharge As Decimal = 0
                    Dim ResuServiseCharge As Decimal = 0
                    Dim ResuFareDiff As Decimal = 0
                    If Convert.ToString(FltHeaderList.Rows(0)("ResuCharge")) IsNot Nothing AndAlso Convert.ToString(FltHeaderList.Rows(0)("ResuCharge")) <> "" Then
                        TicketFormate += "<tr>"
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Reissue Charge</td>"
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                        TicketFormate += FltHeaderList.Rows(0)("ResuCharge").ToString
                        ResuCharge = (Convert.ToDecimal(FltHeaderList.Rows(0)("ResuCharge"))).ToString
                        TicketFormate += "</td>"
                        TicketFormate += "</tr>"
                    End If
                    If Convert.ToString(FltHeaderList.Rows(0)("ResuServiseCharge")) IsNot Nothing AndAlso Convert.ToString(FltHeaderList.Rows(0)("ResuServiseCharge")) <> "" Then
                        TicketFormate += "<tr>"
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Reissue Srv. Charge</td>"
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                        TicketFormate += FltHeaderList.Rows(0)("ResuServiseCharge").ToString
                        ResuServiseCharge = (Convert.ToDecimal(FltHeaderList.Rows(0)("ResuServiseCharge"))).ToString
                        TicketFormate += "</td>"
                        TicketFormate += "</tr>"
                    End If
                    If Convert.ToString(FltHeaderList.Rows(0)("ResuFareDiff")) IsNot Nothing AndAlso Convert.ToString(FltHeaderList.Rows(0)("ResuFareDiff")) <> "" Then
                        TicketFormate += "<tr>"
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Reissue Fare Diff</td>"
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>"
                        TicketFormate += FltHeaderList.Rows(0)("ResuFareDiff").ToString
                        ResuFareDiff = (Convert.ToDecimal(FltHeaderList.Rows(0)("ResuFareDiff"))).ToString
                        TicketFormate += "</td>"
                        TicketFormate += "</tr>"
                    End If
                    TicketFormate += "<td style='font-size: 11px; width: 50%; text-align: left; vertical-align: top;'>TOTAL</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 50%; text-align: left; vertical-align: top;' id='td_totalfare'>"
                    fare = (Convert.ToDecimal(fltFare.Rows(0)("BaseFare")) + Convert.ToDecimal(fltFare.Rows(0)("Fuel")) + Convert.ToDecimal(fltFare.Rows(0)("Tax")) + Convert.ToDecimal(fltFare.Rows(0)("ServiceTax")) + Convert.ToDecimal(fltFare.Rows(0)("TCharge")) + Convert.ToDecimal(fltFare.Rows(0)("TFee")) + ResuCharge + ResuServiseCharge + ResuFareDiff).ToString
                    TicketFormate += fare.ToString
                    TicketFormate += "</td>"

                    'fare = Convert.ToDecimal(fltFare.Rows[0]["Total"]) + ResuCharge + ResuServiseCharge + ResuFareDiff;
                    TicketFormate += "</tr>"
                End If
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                If fltMealAndBag.Rows.Count > 0 Then
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='7' style= 'background-color: #0b2759;width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Pax Name</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trip Type</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Meal Code</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Meal Price</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Baggage Code</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Baggage Price</td>"
                    TicketFormate += "<td style='font-size: 10px; color: #fff; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='7' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"


                    For i As Integer = 0 To fltMealAndBag.Rows.Count - 1
                        'If Convert.ToString(fltMealAndBag.Rows(i)("MealPrice")) <> "0.00" AndAlso Convert.ToString(fltMealAndBag.Rows(i)("BaggagePrice")) <> "0.00" Then
                        TicketFormate += "<tr>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("Name"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("TripType"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("MealCode"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("MealPrice"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("BaggageCode"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("BaggagePrice"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                        MealBagTotalPrice += Convert.ToDecimal(fltMealAndBag.Rows(i)("TotalPrice"))
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("TotalPrice"))
                        TicketFormate += "</td>"

                        TicketFormate += "</tr>"
                        'End If
                    Next
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                End If



                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='background-color: #0b2759; color:#fdc42c;font-size:11px;font-weight:bold; padding: 5px;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10px; width: 20%; text-align: left; vertical-align: top;'></td>"
                TicketFormate += "<td style='font-size: 10px; width: 20%; text-align: left; vertical-align: top;'></td>"
                TicketFormate += "<td style='font-size: 10px; width: 20%; text-align: left; vertical-align: top;'></td>"
                TicketFormate += "<td style='font-size: 10px; width: 15%; text-align: left; vertical-align: top;'></td>"
                TicketFormate += "<td style='color: #fff; font-size: 10px; width: 15%; text-align: left; vertical-align: top;'>GRAND TOTAL</td>"
                TicketFormate += "<td style='color: #fff; font-size: 10px; width: 10%; text-align: left; vertical-align: top;'id='td_grandtot'>"
                TicketFormate += (fare + MealBagTotalPrice).ToString
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<br/><br/>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4'>"
                TicketFormate += "<ul style='list-style-image: url(http://richatravels.com/Images/bullet.png);'>"
                TicketFormate += "<li style='font-size:10.5px;'>Kindly confirm the status of your PNR within 24 hrs of booking, as at times the same may fail on account of payment failure, internet connectivity, booking engine or due to any other reason beyond our control."
                TicketFormate += "For Customers who book their flights well in advance of the scheduled departure date it is necessary that you re-confirm the departure time of your flight between 72 and 24 hours before the Scheduled Departure Time.</li>"
                TicketFormate += "</ul>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;'>TERMS AND CONDITIONS :</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4'>"
                TicketFormate += "<ul style='list-style-image: url(http://richatravels.com/Images/bullet.png);'>"
                TicketFormate += "<li style='font-size:10.5px;'>Guests are requested to carry their valid photo identification for all guests, including children.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>We recommend check-in at least 2 hours prior to departure.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Boarding gates close 45 minutes prior to the scheduled time of departure. Please report at your departure gate at the indicated boarding time. Any passenger failing to report in time may be refused boarding privileges.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Cancellations and Changes permitted more than two (2) hours prior to departure with payment of change fee and difference in fare if applicable only in working hours (10:00 am to 06:00 pm) except Sundays and Holidays.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>"
                TicketFormate += "Flight schedules are subject to change and approval by authorities."
                TicketFormate += "<br />"
                TicketFormate += "</li>"
                TicketFormate += "<li style='font-size:10.5px;'>"
                TicketFormate += "Name Changes on a confirmed booking are strictly prohibited. Please ensure that the name given at the time of booking matches as mentioned on the traveling Guests valid photo ID Proof."
                TicketFormate += "<br />"
                TicketFormate += " Travel Agent does not provide compensation for travel on other airlines, meals, lodging or ground transportation."
                TicketFormate += "</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Bookings made under the Armed Forces quota are non cancelable and non- changeable.</li>"

                TicketFormate += "<li style='font-size:10.5px;'>Guests are advised to check their all flight details (including their Name, Flight numbers, Date of Departure, Sectors) before leaving the Agent Counter.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Cancellation amount will be charged as per airline rule.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Guests requiring wheelchair assistance, stretcher, Guests traveling with infants and unaccompanied minors need to be booked in advance since the inventory for these special service requests are limited per flight.</li>"
                TicketFormate += "</ul>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "<table style='width: 100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='background-color: #0b2759; color: #f58220; font-size: 11px; font-weight: bold; padding: 5px;'>BAGGAGE INFORMATION :"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                If Not String.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))) Then
                    Bag = Convert.ToBoolean(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))
                End If

                Dim dtbaggage As New DataTable
                dtbaggage = objTranDom.GetBaggageInformation("D", FltHeaderList.Rows(0)("VC"), Bag).Tables(0)
                Dim bginfo As String = GetBagInfo(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("Provider")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AirlineRemark")))

                If bginfo = "" Then

                    For Each drbg In dtbaggage.Rows

                        TicketFormate += "<tr>"
                        TicketFormate += "<td colspan='2'>" & drbg("BaggageName") & "</td>"
                        TicketFormate += "<td colspan='2'>" & drbg("Weight") & "</td>"
                        TicketFormate += "</tr>"
                    Next


                Else
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='2'></td>"
                    TicketFormate += "<td colspan='2'>" & bginfo & "</td>"
                    TicketFormate += "</tr>"

                End If




                TicketFormate += "</table>"
            End If

            'TicketFormate += "<table style='width: 100%;'>"
            'TicketFormate += "<tr>"
            'TicketFormate += "<td style='width: 100%; text-align: justify; color: #0b2759; font-size: 11px; padding: 10px; font-size:10.5px;'>"
            ''TicketFormate += "For any assistance contact: ATPI International Pvt. Ltd. | Tel: 00-91-2240095555 | Fax: 00-91-2240095556 | "

            'TicketFormate += "</td>"
            'TicketFormate += "</tr>"
            'TicketFormate += "</table>"
            '#End Region
            'Dim Body As String = ""

            'Dim status As Integer = 0
            'Try

            '    strFileNmPdf = ConfigurationManager.AppSettings("HTMLtoPDF").ToString().Trim() + FltHeaderList.Rows(0)("GdsPnr") + "-" + DateTime.Now.ToString().Replace(":", "").Replace("/", "-").Replace(" ", "-").Trim() + ".pdf"
            '    Dim pdfDoc As New iTextSharp.text.Document(PageSize.A4)
            '    Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(strFileNmPdf, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
            '    pdfDoc.Open()
            '    Dim sr As New StringReader(TicketFormate)
            '    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr)
            '    pdfDoc.Close()
            '    writer.Dispose()
            '    sr.Dispose()
            '    writePDF = True
            '    Return TicketFormate
            'Catch ex As Exception
            'End Try
            Return TicketFormate
        Catch ex As Exception
            ' Response.Write(ex.Message & ex.StackTrace.ToString())
            clsErrorLog.LogInfo(ex)



        End Try
    End Function

    Public Function SelectPaxDetail(ByVal OrderId As String, ByVal TID As String) As DataTable
        Dim adap As New SqlDataAdapter()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        If String.IsNullOrEmpty(TID) Then
            Dim dt As New DataTable()

            adap = New SqlDataAdapter("SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' ", con)
            'adap = New SqlDataAdapter("SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' ", con)
            adap.Fill(dt)

            Return dt
        Else
            Dim dt As New DataTable()
            adap = New SqlDataAdapter("SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' and PaxId= '" & TID & "' ", con)
            'adap = New SqlDataAdapter("SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' and PaxId= '" & TID & "' ", con)
            adap.Fill(dt)
            Return dt
        End If
    End Function


    Public Function TerminalDetails(ByVal OrderID As String, ByVal DepCity As String, ByVal ArrvlCity As String) As DataTable
        Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim adap As New SqlDataAdapter("USP_TERMINAL_INFO", con1)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@DEPARTURECITY", DepCity)
        adap.SelectCommand.Parameters.AddWithValue("@ARRIVALCITY", ArrvlCity)
        adap.SelectCommand.Parameters.AddWithValue("@ORDERID", OrderID)
        Dim dt1 As New DataTable()
        con1.Open()
        adap.Fill(dt1)
        con1.Close()
        Return dt1
    End Function

    Public Function GetBagInfo(ByVal Provider As String, ByVal Remark As String) As String

        Dim baginfo As String = ""
        If Provider = "TB" Then

            If Remark.Contains("Hand") Then
                baginfo = Remark

            End If
        ElseIf Provider = "YA" Then

            If Remark.Contains("Hand") Then
                baginfo = Remark

            ElseIf Not String.IsNullOrEmpty(Remark) Then

                baginfo = Remark & " Baggage allowance"

            End If


        ElseIf Provider = "1G" Then

            If Remark.Contains("PC") Then

                baginfo = Remark.Replace("PC", " Piece(s) Baggage allowance")
            ElseIf Remark.Contains("K") Then

                baginfo = Remark.Replace("K", " Kg Baggage allowance")

            End If

        End If
        Return baginfo

    End Function
  
End Class

