﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="Proxy.aspx.vb" Inherits="Report_Proxy_Proxy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../JS/JScript.js" type="text/javascript"></script>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />


    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>


    <%--<link href="../../html/css/container-style.css?v=1" rel="stylesheet" />--%>

    <style type="text/css">
        .container-box {
            background-color: #1fc8db;
            background-image: linear-gradient(141deg, #58a3bf 0%, #5b8fa2 51%, #b4e2f3 75%);
            width: 100%;
            height: 300px;
        }

        table {
            background: none !important;
        }
    </style>
    <script type="text/javascript">

        function Show(obj) {

            if (obj.checked) {
                //document.getElementById("txtRetDate").style.display = "block";
                document.getElementById("td_ret").style.display = "block";
                document.getElementById("td_time").style.display = "block";
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_ReturnAnytime").style.display = "block";
                document.getElementById("rtn").style.display = "block";
            }
        }
        function Hide(obj) {
            if (obj.checked) {
                //document.getElementById("txtRetDate").style.display = "none";
                document.getElementById("td_ret").style.display = "none";
                document.getElementById("td_time").style.display = "none";
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_ReturnAnytime").style.display = "none";
                document.getElementById("rtn").style.display = "none";
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd").click(function () {
                $("#From").focus();
            }
            );
            $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd1").click(function () {
                $("#To").focus();
            }
            );
        });
    </script>
    <%--<div>--%>


    <%--            <div class="page-title-container">
                <div class="container">
                    <div class="page-title">
                        <h2 class="entry-title" style="text-align: left">Domestic/International Proxy Request</h2>
                    </div>

                </div>
            </div>--%>






    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Domestic/International Proxy Request</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <br />


                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="form-group" id="td_Agency" runat="server">
                                            <label for="exampleInputPassword1">
                                                Agency Name
                                            </label>
                                            <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                            <input type="hidden" id="hidtxtAgencyName" class="form-control" name="hidtxtAgencyName" value="" />
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="font-size: 14px; color: #000;">
                                        Trip Type :
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="radio-inline">
                                        <asp:RadioButton ID="RB_OneWay" runat="server" Checked="true" GroupName="Trip" onclick="Hide(this)"
                                            Style="color: #000" Text="One Way" /></label>
                                    <label class="radio-inline">
                                        <asp:RadioButton ID="RB_RoundTrip" runat="server" GroupName="Trip" onclick="Show(this)"
                                            Text="Round Trip" Style="color: #000" /></label>
                                </div>
                            </div>
                                    </div>
                                    <div class="col-md-6 " style="font-size: 14px; color: #000;">
                                        Booking :
                              <div class="row">
                                  <div class="col-md-6">
                                      <label class="radio-inline">
                                          <asp:RadioButtonList ID="RBL_Booking" runat="server" CssClass="form-group" RepeatDirection="vertical" RepeatColumns="3" Style="color: #000">
                                              <asp:ListItem Selected="True">Proxy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>


                                              <asp:ListItem>LTC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>

                                              <asp:ListItem>Group</asp:ListItem>
                                          </asp:RadioButtonList></label>
                                  </div>
                              </div>

                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Leaving Form</label>
                                            <input id="txtDepCity1" name="txtDepCity1" type="text" class="form-control" />
                                            <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" />

                                        </div>
                                    </div>


                                    <div class="col-md-3  col-xs-6">
                                        <div class="form-group">
                                            <label>Leaving To</label>
                                            <input id="txtArrCity1" name="txtArrCity1" type="text" class="form-control" />
                                            <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" class="form-control" />
                                        </div>
                                    </div>


                                    <div class="col-md-3  col-xs-6">
                                        <div class="form-group">
                                            <label>Departure Date</label>

                                            <input type="text" name="txtDepDate" id="From" class="form-control" value=""
                                                readonly="readonly" />
                                            <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />
                                        </div>
                                    </div>




                                    <div class="col-md-3  col-xs-6">
                                        <div class="form-group">
                                            <label>Time </label>

                                            <asp:DropDownList ID="ddl_DepartAnytime" runat="server" class=" form-control">
                                                <asp:ListItem>Anytime</asp:ListItem>
                                                <asp:ListItem>Morning(0.00-12:00)</asp:ListItem>
                                                <asp:ListItem>Midday(10:00-14:00)</asp:ListItem>
                                                <asp:ListItem>Afternoon(12:00-17:00)</asp:ListItem>
                                                <asp:ListItem>Evening(17:00-20:00)</asp:ListItem>
                                                <asp:ListItem>Night(20:00-23:59)</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>



                                <div class="col-md-3 col-xs-6" id="td_ret" style="display: none;">

                                    <div class="form-group">
                                        <label style="color: black">Return Date</label>

                                        <input type="text" name="txtRetDate" id="To" class=" form-control" value=""
                                            readonly="readonly" />

                                        <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />
                                    </div>
                                </div>



                                <div class="col-md-3  col-xs-6" id="rtn" style="display: none;">
                                    <div class="form-group">
                                        <label>Time</label>
                                        <asp:DropDownList ID="ddl_ReturnAnytime" runat="server" CssClass=" form-control">
                                            <asp:ListItem>Anytime</asp:ListItem>
                                            <asp:ListItem>Morning(0.00-12:00)</asp:ListItem>
                                            <asp:ListItem>Midday(10:00-14:00)</asp:ListItem>
                                            <asp:ListItem>Afternoon(12:00-17:00)</asp:ListItem>
                                            <asp:ListItem>Evening(17:00-20:00)</asp:ListItem>
                                            <asp:ListItem>Night(20:00-23:59)</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>





                                <div class="row">


                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Adult(>12 yrs)</label>
                                            <asp:DropDownList ID="ddl_Adult" runat="server" CssClass="form-control">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3  col-xs-6">
                                        <div class="form-group">
                                            <label>Child(2 - 11 yrs)</label>
                                            <asp:DropDownList ID="ddl_Child" runat="server" CssClass="form-control">
                                                <asp:ListItem>0</asp:ListItem>
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Infant(Under 2 yrs)</label>
                                            <asp:DropDownList ID="ddl_Infrant" runat="server" CssClass="form-control">
                                                <asp:ListItem>0</asp:ListItem>
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>



                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Class</label>
                                            <asp:DropDownList ID="ddl_Class" runat="server" CssClass="form-control">
                                                <asp:ListItem>Economy</asp:ListItem>
                                                <asp:ListItem>Non Refundable Economy</asp:ListItem>
                                                <asp:ListItem>Refundable Economy</asp:ListItem>
                                                <asp:ListItem>Full Fare Economy</asp:ListItem>
                                                <asp:ListItem>Business</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Prefered Airlines</label>
                                            <input type="text" name="txtAirline" value="" id="txtAirline" class="form-control" />
                                            <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                        </div>
                                    </div>


                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Classes</label>
                                            <asp:DropDownList ID="ddl_Classes" runat="server" CssClass="form-control">
                                                <asp:ListItem>All</asp:ListItem>
                                                <asp:ListItem>A</asp:ListItem>
                                                <asp:ListItem>B</asp:ListItem>
                                                <asp:ListItem>C</asp:ListItem>
                                                <asp:ListItem>D</asp:ListItem>
                                                <asp:ListItem>E</asp:ListItem>
                                                <asp:ListItem>F</asp:ListItem>
                                                <asp:ListItem>G</asp:ListItem>
                                                <asp:ListItem>H</asp:ListItem>
                                                <asp:ListItem>I</asp:ListItem>
                                                <asp:ListItem>J</asp:ListItem>
                                                <asp:ListItem>K</asp:ListItem>
                                                <asp:ListItem>L</asp:ListItem>
                                                <asp:ListItem>M</asp:ListItem>
                                                <asp:ListItem>N</asp:ListItem>
                                                <asp:ListItem>O</asp:ListItem>
                                                <asp:ListItem>P</asp:ListItem>
                                                <asp:ListItem>Q</asp:ListItem>
                                                <asp:ListItem>R</asp:ListItem>
                                                <asp:ListItem>S</asp:ListItem>
                                                <asp:ListItem>T</asp:ListItem>
                                                <asp:ListItem>U</asp:ListItem>
                                                <asp:ListItem>V</asp:ListItem>
                                                <asp:ListItem>W</asp:ListItem>
                                                <asp:ListItem>X</asp:ListItem>
                                                <asp:ListItem>Y</asp:ListItem>
                                                <asp:ListItem>Z</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Payment Limit</label>
                                            <asp:DropDownList ID="ddl_PaymentMode" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="CL">Cash Limit</asp:ListItem>
                                                <asp:ListItem Value="CL">Card Limit</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>









                                <div class="row">
                                    <div id="TBL_Projects" runat="server" class="col-md-9 col-xs-12">

                                        <div>
                                            <div class="text1">
                                                Project Id :
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="DropDownListProject" runat="server" CssClass="drpBox">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="text1">
                                                Booked By :
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="DropDownListBookedBy" runat="server" CssClass="drpBox">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div>
                                            <div height="30px" class="text1">
                                                Remark :
                                            </div>
                                            <%--<div style="padding-left: 15px; padding-right: 10px;">--%>
                                            <asp:TextBox ID="txt_Remark" runat="server" class="form control input-text full-width" TextMode="MultiLine"></asp:TextBox>
                                            <%-- </div>--%>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <asp:Button ID="btn_Submit" runat="server" Text="Submit" OnClientClick="return Proxy();" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </div>




                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--</div>--%>

    <script type="text/javascript">

      <%--  var myDate = new Date();
        var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
        document.getElementById("txtDepDate").value = currDate;
        document.getElementById("hidtxtDepDate").value = currDate;
        document.getElementById("txtRetDate").value = currDate;
        document.getElementById("hidtxtRetDate").value = currDate;
        var UrlBase = '<%=ResolveUrl("~/") %>';
        document.getElementById("txtRetDate").style.display = "none";
        document.getElementById("td_ret").style.display = "none";
        document.getElementById("td_time").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_ddl_ReturnAnytime").style.display = "none";
        document.getElementById("rtn").style.display = "none";--%>


</script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Common.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Search3_off.js")%>"></script>
</asp:Content>
