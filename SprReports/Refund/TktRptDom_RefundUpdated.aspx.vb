﻿Imports System.Data
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports YatraBilling

'Imports PG
Imports System.Data.SqlClient


Partial Class Reports_Refund_TktRptDom_RefundUpdated
    Inherits System.Web.UI.Page

    Private ST As New SqlTransaction()
    Dim GridDS As New DataSet()
    Private Refundfare As Double
    Private STDom As New SqlTransactionDom
    Dim objSMSAPI As New SMSAPI.SMS
    'Private objPG As New PaymentGateway
    Dim objSql As New SqlTransactionNew

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        If Not IsPostBack Then
            BindGrid()
            lbluserid.Text = Session("UID")
            Try
                If GridDS.Tables(0).Rows.Count <> 0 Then
                    'Bind Agency DDL
                    Dim AgencyDS As New DataSet()
                    AgencyDS = ST.GetAgencyDetails(GridDS.Tables(0).Rows(0).Item("UserID").ToString.Trim)
                    Dim dt1 As New DataTable()
                    dt1 = AgencyDS.Tables(0)
                    Dim addr As String = dt1.Rows(0)("city").ToString() & ", " & dt1.Rows(0)("State").ToString() & ", " & dt1.Rows(0)("country").ToString() & ", " & dt1.Rows(0)("zipcode").ToString()
                    td_AgentID.InnerText = GridDS.Tables(0).Rows(0).Item("UserID").ToString.Trim
                    td_AgentName.InnerText = dt1.Rows(0)("Name").ToString()
                    td_AgentAddress.InnerText = dt1.Rows(0)("Address").ToString()
                    td_Street.InnerText = addr
                    td_AgentMobNo.InnerText = dt1.Rows(0)("Mobile").ToString()
                    td_Email.InnerText = dt1.Rows(0)("Email").ToString()
                    td_AgencyName.InnerText = dt1.Rows(0)("Agency_Name").ToString()
                    td_CardLimit.InnerText = dt1.Rows(0)("Crd_Limit").ToString()
                    hdnRefundid.Value = GridDS.Tables(0).Rows(0)("RefundID").ToString()
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)

            End Try
        End If
    End Sub
    Private Sub BindGrid()
        Try
            GridDS = ST.GetReIssueCancelIntl(Convert.ToInt32(Request.QueryString("counter")), "C", StatusClass.InProcess)             'GetCancelletionInt(Request.QueryString("counter").ToString())
            grd_Pax.DataSource = GridDS
            grd_Pax.DataBind()
            ViewState("GridDS") = GridDS
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    'Protected Sub btn_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Update.Click
    '    Dim result As String = "Ticket Refund Failed"

    '    Dim objItzT As New Itz_Trans_Dal
    '    Dim inst As Boolean = False
    '    Dim objIzT As New ITZ_Trans
    '    Dim ablBalance As Double = 0
    '    Dim PaymentMode As String = ""
    '    Dim Trip As String = ""
    '    Try
    '        For Each row As GridViewRow In grd_Pax.Rows
    '            Dim lblTktNo As String = DirectCast(row.FindControl("lbltktno"), Label).Text
    '            Dim lblfare As String = DirectCast(row.FindControl("lbltotalfareafterdiscount"), Label).Text
    '            Dim lbluser As String = DirectCast(row.FindControl("lbluserid"), Label).Text
    '            Dim lblSector As String = DirectCast(row.FindControl("lblSector"), Label).Text
    '            Dim lblpnr As String = DirectCast(row.FindControl("lblpnr"), Label).Text
    '            Dim lblpaxfname As String = DirectCast(row.FindControl("lblpaxfname"), Label).Text
    '            Dim lblagencyname As String = DirectCast(row.FindControl("lblagencyname"), Label).Text
    '            Dim lblorderId As String = DirectCast(row.FindControl("lblorderId"), Label).Text
    '            Dim TDS As String = DirectCast(row.FindControl("lblTDS"), Label).Text
    '            Dim BookingDate As String = DirectCast(row.FindControl("lblBookingDate"), Label).Text
    '            Dim lblPaxID As Integer = Convert.ToInt32(DirectCast(row.FindControl("lblPaxID"), Label).Text)
    '            Dim AgentId As String = td_AgentID.InnerText.Trim()
    '            Dim PaxType As String = DirectCast(row.FindControl("lblpaxtype"), Label).Text
    '            Dim mgtFee As String = DirectCast(row.FindControl("HiddenMgtFee"), HiddenField).Value
    '            Dim mgtFeeVal As Double = If(String.IsNullOrEmpty(mgtFee), 0, Convert.ToDouble(mgtFee))
    '            Dim ServiceTax As Double = 0
    '            If (mgtFeeVal > 0) Then
    '                Dim SrvTax As String = DirectCast(row.FindControl("HiddenSrvTax"), HiddenField).Value
    '                ServiceTax = If(String.IsNullOrEmpty(SrvTax), 0, Convert.ToDouble(SrvTax))
    '            End If
    '            Dim FareDiff As Double = 0
    '            Dim TotlCharge As Double = 0
    '            Dim HdrDs As New DataSet()
    '            HdrDs = ST.GetHdrDetails(lblorderId)
    '            PaymentMode = HdrDs.Tables(0).Rows(0)("PMode").ToString()
    '            Trip = HdrDs.Tables(0).Rows(0)("Trip").ToString()


    '            Dim PgCharges As String

    '            Dim PaxCount As Double
    '            Dim Pgcharges_pax_Wise As Double
    '            Dim Current_refund_Amount As Double

    '            If (txt_charge.Text.Trim = "") Then
    '                txt_charge.Text = "0"

    '            End If
    '            If (txt_Service.Text.Trim = "") Then
    '                txt_Service.Text = "0"
    '            End If

    '            PgCharges = HdrDs.Tables(0).Rows(0)("PgCharges").ToString()
    '            If (PgCharges = "" Or PaxType = "INF") Then
    '                PgCharges = "0"
    '            End If


    '            PaxCount = Convert.ToDouble(HdrDs.Tables(0).Rows(0)("Adult").ToString()) + Convert.ToDouble(HdrDs.Tables(0).Rows(0)("Child").ToString())
    '            ' AgentId = HdrDs.Tables(0).Rows(0)("AgentId").ToString()

    '            Dim Status As Boolean = ST.CheckRefundReissueUpdate(lblPaxID, StatusClass.Cancelled.ToString(), "REFUND")

    '            If (Status = 0) Then
    '                GetMerchantKey(lblorderId)
    '                If HdrDs.Tables(0).Rows.Count <> 0 Then
    '                    'Checking Its Allready ReIssued or not
    '                    'If dtpnr.Rows(0)("ResuID").ToString() <> "" AndAlso dtpnr.Rows(0)("ResuID").ToString() IsNot Nothing Then

    '                    If HdrDs.Tables(0).Rows(0)("ResuID").ToString() <> "" AndAlso HdrDs.Tables(0).Rows(0)("ResuID").ToString() IsNot Nothing Then
    '                        FareDiff = Convert.ToDouble(HdrDs.Tables(0).Rows(0)("ResuFareDiff"))
    '                    End If
    '                End If

    '                'Checking Booking Month and Today Month is not Same then TDS charge will be deducted 
    '                Dim m1 As Integer = Convert.ToInt32(DatePart(DateInterval.Month, Date.Now))
    '                Dim m() As String = BookingDate.Replace("-", "/").Split("/")


    '                Dim m2 As Integer = Convert.ToInt32(m(1))
    '                'Dim m2 As String = BookingDate.Substring(3, 2).ToString()

    '                If m1 = m2 Then
    '                    TotlCharge = Convert.ToDouble(txt_charge.Text.Trim) + Convert.ToDouble(txt_Service.Text.Trim) + mgtFeeVal + ServiceTax - FareDiff
    '                    Refundfare = Convert.ToDouble(lblfare) - TotlCharge
    '                    TDS = Convert.ToDouble("0.00")
    '                Else
    '                    TotlCharge = Convert.ToDouble(txt_charge.Text.Trim) + Convert.ToDouble(txt_Service.Text.Trim) + Convert.ToDouble(TDS) + mgtFeeVal + ServiceTax - FareDiff
    '                    Refundfare = Convert.ToDouble(lblfare) - TotlCharge
    '                End If
    '                If (PaymentMode = "wallet") Then
    '                    'Adding Refund Amount in Agent balance
    '                    Dim CheckBoolean As String = ST.CheckStatusForWalletRefund(lblorderId, Refundfare)

    '                    If (CheckBoolean = "yes") Then

    '                        objParamCrd._DECODE = IIf(AgentId <> Nothing, AgentId.ToString().Trim(), " ")
    '                        Try
    '                            objParamCrd._MERCHANT_KEY = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                            objParamCrd._AMOUNT = IIf(Refundfare <> Nothing, Refundfare, 0)
    '                            objParamCrd._ORDERID = IIf(hdnRefundid.Value <> Nothing AndAlso hdnRefundid.Value <> "", hdnRefundid.Value.Trim(), " ")
    '                            objParamCrd._REFUNDORDERID = IIf(lblorderId <> Nothing AndAlso lblorderId <> "", lblorderId.Trim(), " ")
    '                            objParamCrd._MODE = IIf(Session("ModeTypeITZ") <> Nothing, Session("ModeTypeITZ").ToString().Trim(), " ") ''IIf(Not ConfigurationManager.AppSettings("ITZMode") Is Nothing, ConfigurationManager.AppSettings("ITZMode").Trim(), " ")
    '                            objParamCrd._REFUNDTYPE = "P"
    '                            ''objParamCrd._CHECKSUM = " "
    '                            Dim stringtoenc As String = "MERCHANTKEY=" & objParamCrd._MERCHANT_KEY & "&ORDERID=" & objParamCrd._ORDERID & "&REFUNDTYPE=" & objParamCrd._REFUNDTYPE
    '                            objParamCrd._CHECKSUM = VGCheckSum.calculateEASYChecksum(stringtoenc)
    '                            'objParamCrd._SERVICE_TYPE = IIf(Not ConfigurationManager.AppSettings("ITZSvcType") Is Nothing, ConfigurationManager.AppSettings("ITZSvcType").Trim(), " ")
    '                            objParamCrd._DESCRIPTION = "refund to agent -" & AgentId & " against PNR-" & lblpnr
    '                            objRefnResp = objCrd.ITZRefund(objParamCrd)
    '                        Catch ex As Exception
    '                            clsErrorLog.LogInfo(ex)

    '                        End Try

    '                        Try
    '                            objItzT = New Itz_Trans_Dal()
    '                            objIzT.AMT_TO_DED = "0"
    '                            objIzT.AMT_TO_CRE = IIf(Refundfare <> Nothing, Refundfare, 0)
    '                            objIzT.B2C_MBLNO_ITZ = " "
    '                            objIzT.COMMI_ITZ = " "
    '                            objIzT.CONVFEE_ITZ = " "
    '                            objIzT.DECODE_ITZ = IIf(AgentId <> Nothing, AgentId.ToString().Trim(), " ")
    '                            objIzT.EASY_ORDID_ITZ = IIf(objRefnResp.EASY_ORDER_ID IsNot Nothing, objRefnResp.EASY_ORDER_ID, " ")
    '                            objIzT.EASY_TRANCODE_ITZ = IIf(objRefnResp.EASY_TRAN_CODE IsNot Nothing, objRefnResp.EASY_TRAN_CODE, " ")
    '                            objIzT.ERROR_CODE = IIf(objRefnResp.ERROR_CODE IsNot Nothing, objRefnResp.ERROR_CODE, " ")
    '                            objIzT.MERCHANT_KEY_ITZ = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                            objIzT.MESSAGE_ITZ = IIf(objRefnResp.MESSAGE IsNot Nothing, objRefnResp.MESSAGE, " ")
    '                            objIzT.ORDERID = IIf(lblorderId <> Nothing AndAlso lblorderId <> "", lblorderId.Trim(), " ")
    '                            objIzT.RATE_GROUP_ITZ = " "
    '                            objIzT.REFUND_TYPE_ITZ = IIf(objRefnResp.REFUND_TYPE IsNot Nothing AndAlso objRefnResp.REFUND_TYPE <> "" AndAlso objRefnResp.REFUND_TYPE <> " ", objRefnResp.REFUND_TYPE, " ")
    '                            objIzT.SERIAL_NO_FROM = " "
    '                            objIzT.SERIAL_NO_TO = " "
    '                            objIzT.SVC_TAX_ITZ = " "
    '                            objIzT.TDS_ITZ = " "
    '                            objIzT.TOTAL_AMT_DED_ITZ = " "
    '                            objIzT.TRANS_TYPE = "REFUND"
    '                            objIzT.USER_NAME_ITZ = IIf(Session("_USERNAME") <> Nothing, Session("_USERNAME").ToString().Trim(), " ")
    '                            Try
    '                                objBalResp = New GetBalanceResponse()
    '                                objParamBal._DCODE = IIf(AgentId <> Nothing, AgentId.ToString().Trim(), " ")
    '                                objParamBal._MERCHANT_KEY = IIf(Session("MchntKeyITZ") <> Nothing, Session("MchntKeyITZ").ToString().Trim(), " ") ''IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                                objParamBal._PASSWORD = IIf(Session("_PASSWORD") <> Nothing, Session("_PASSWORD").ToString().Trim(), " ")
    '                                objParamBal._USERNAME = IIf(Session("_USERNAME") <> Nothing, Session("_USERNAME").ToString().Trim(), " ")
    '                                objBalResp = objItzBal.GetBalanceCustomer(objParamBal)
    '                                objIzT.ACCTYPE_NAME_ITZ = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_TYPE_NAME IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_TYPE_NAME, " ")
    '                                objIzT.AVAIL_BAL_ITZ = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE, " ")
    '                                Session("CL") = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE, " ")
    '                                ablBalance = IIf(objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE IsNot Nothing, objBalResp.VAL_ACCOUNT_TYPE_DETAIL(0).VAL_ACCOUNT_BALANCE, " ")

    '                            Catch ex As Exception
    '                                clsErrorLog.LogInfo(ex)
    '                            End Try
    '                            inst = objItzT.InsertItzTrans(objIzT)
    '                        Catch ex As Exception
    '                            clsErrorLog.LogInfo(ex)
    '                        End Try
    '                        ablBalance = ST.UpdateNew_RegsRefund(lbluser, Refundfare)

    '                        ''If objRefnResp IsNot Nothing Then
    '                        ''    If objRefnResp.MESSAGE.Trim().Contains("successfully execute") Then
    '                        ''objParamBal._DCODE = IIf(Session("_DCODE") <> Nothing, Session("_DCODE").ToString().Trim(), " ")
    '                        ''objParamBal._MERCHANT_KEY = IIf(ConfigurationManager.AppSettings("MerchantKey") <> Nothing, ConfigurationManager.AppSettings("MerchantKey").Trim(), " ")
    '                        ''objParamBal._PASSWORD = IIf(Session("_PASSWORD") <> Nothing, Session("_PASSWORD").ToString().Trim(), " ")
    '                        ''objParamBal._USERNAME = IIf(Session("_USERNAME") <> Nothing, Session("_USERNAME").ToString().Trim(), " ")
    '                        ''objBalResp = objItzBal.GetBalanceCustomer(objParamBal)
    '                        'Session("CL") = ablBalance
    '                        If objRefnResp.MESSAGE.Trim().ToLower().Contains("successfully execute") Then

    '                            Dim rm As String = "ReFund Charge Against Ticket No " & lblTktNo

    '                            'Insert data in Transaction Report
    '                            ST.InsertTransactionRepot(lblpnr, lbluser, Refundfare.ToString, ablBalance.ToString, TotlCharge.ToString, lblSector, lblpaxfname, "", lblagencyname, rm, StatusClass.Cancelled)

    '                            ' Insert Data in LedgerDetails Table
    '                            STDom.insertLedgerDetails(lbluser, lblagencyname, lblorderId, lblpnr, lblTktNo, "", objIzT.EASY_ORDID_ITZ, "", Session("UID").ToString, Request.UserHostAddress, 0, _
    '                                                Convert.ToDecimal(Refundfare), Convert.ToDecimal(ablBalance), "Ticket Refund", rm, 0, TDS)

    '                            ' Update Cancellationintl table after Refund 
    '                            ST.UpdateCancelIntlTbl(Convert.ToInt32(Request.QueryString("counter")), Convert.ToInt32(txt_charge.Text), Convert.ToInt32(txt_Service.Text), Convert.ToInt32(Refundfare), txtRemark.Text, StatusClass.Cancelled, 0)

    '                            'Update Status =Cancelled in FltHeader and FltPaxDetails 
    '                            ST.UpdateStatus_Pax_Header(lblorderId, lblpnr, lblPaxID, StatusClass.Cancelled)
    '                            result = "Ticket Refund Successful."

    '                        Else

    '                            result = "Ticket Refund was not Successful."
    '                        End If

    '                        ''    Else
    '                        ''        result = objRefnResp.MESSAGE.Trim()
    '                        ''    End If
    '                        ''Else
    '                        ''    result = "Could not refund the ticket, please try after some time"
    '                        ''End If

    '                        'NAV METHOD  CALL START
    '                        Try

    '                            'Dim objNav As New AirService.clsConnection(lblorderId, "2", lblPaxID)
    '                            'objNav.airBookingNav(lblorderId, "", 2)

    '                        Catch ex As Exception
    '                            clsErrorLog.LogInfo(ex)


    '                        End Try
    '                        'Nav METHOD END'

    '                        'Online Yatra
    '                        Try
    '                            'Dim AirObj As New AIR_YATRA
    '                            'AirObj.ProcessYatra_Air_CN(lblorderId, lblpnr, Convert.ToInt32(lblPaxID))
    '                        Catch ex As Exception
    '                            clsErrorLog.LogInfo(ex)


    '                        End Try
    '                        'Online Yatra End


    '                        BindGrid()
    '                        'ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Refund Sucessfull');javascript:window.close();window.location='ProcessImportPnr.aspx';", True)
    '                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('" + result + "');javascript:window.close();window.opener.location.href = window.opener.location.href;", True)
    '                    Else

    '                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Cannot Refund');javascript:window.close();window.opener.location.href = window.opener.location.href;", True)
    '                    End If

    '                ElseIf (PaymentMode = "PG") Then
    '                    Dim Remark As String = txtRemark.Text
    '                    Dim msg As String = ""
    '                    Dim rm As String = "ReFund Charge Against Ticket No " & lblTktNo
    '                    Dim con As New SqlConnection
    '                    Dim DS As New DataSet()
    '                    Dim strSpmsg As String = ""
    '                    con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
    '                    con.Open()
    '                    If (CheckBox1.Checked = True And PgCharges <> "0") Then
    '                        Remark = TextBox3.Text
    '                        Pgcharges_pax_Wise = Math.Round((Convert.ToDouble(PgCharges) / PaxCount), 2)
    '                        Current_refund_Amount = Refundfare + Pgcharges_pax_Wise
    '                    Else
    '                        Current_refund_Amount = Refundfare
    '                    End If

    '                    Dim cmd As New SqlCommand()
    '                    Dim da As New SqlDataAdapter(cmd)
    '                    cmd.CommandText = "USP_PGIsRefund"
    '                    cmd.CommandType = CommandType.StoredProcedure
    '                    cmd.Parameters.Add("@orderid ", SqlDbType.VarChar).Value = lblorderId
    '                    cmd.Parameters.Add("@CurrentTobeRefundFare ", SqlDbType.VarChar).Value = Current_refund_Amount
    '                    cmd.Connection = con
    '                    da.Fill(DS)
    '                    If (DS.Tables(0).Rows.Count > 0) Then
    '                        Dim SpRefundamount As Double
    '                        strSpmsg = DS.Tables(0).Rows(0)("msg").ToString()
    '                        SpRefundamount = DS.Tables(0).Rows(0)("AmountTobeRefunded").ToString()
    '                        Current_refund_Amount = SpRefundamount
    '                    End If

    '                    If (strSpmsg = "1") Then
    '                        msg = objPG.PgRefundAmount(lblorderId, "Flight", Trip, Convert.ToDouble(Current_refund_Amount.ToString), AgentId, Remark, Session("UID").ToString(), "PNRReject", Pgcharges_pax_Wise, lblPaxID, Convert.ToDouble(TDS), lblagencyname)
    '                        If (msg = "Refunded") Then
    '                            ST.InsertTransactionRepot(lblpnr, lbluser, Current_refund_Amount.ToString, ablBalance.ToString, TotlCharge.ToString, lblSector, lblpaxfname, "", lblagencyname, rm, StatusClass.Cancelled)
    '                            STDom.insertLedgerDetails(lbluser, lblagencyname, lblorderId, lblpnr, lblTktNo, "", "", "", Session("UID").ToString, Request.UserHostAddress, 0, _
    '                                                Convert.ToDecimal(Current_refund_Amount), Convert.ToDecimal(ablBalance), "Ticket Refund via PG", rm, 0, Convert.ToDouble(TDS))

    '                            ST.UpdateCancelIntlTbl(Convert.ToInt32(Request.QueryString("counter")), Convert.ToInt32(txt_charge.Text), Convert.ToInt32(txt_Service.Text), Convert.ToInt32(Current_refund_Amount), Remark, StatusClass.Cancelled, Pgcharges_pax_Wise)
    '                            ST.UpdateStatus_Pax_Header(lblorderId, lblpnr, lblPaxID, StatusClass.Cancelled)
    '                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('" + msg + "');javascript:window.close();window.opener.location.href=window.opener.location.href;", True)
    '                        ElseIf (msg <> "Already refunded" Or msg <> "Refunded") Then
    '                            ST.UpdateCancelIntlTbl(Convert.ToInt32(Request.QueryString("counter")), Convert.ToInt32(txt_charge.Text), Convert.ToInt32(txt_Service.Text), Convert.ToInt32(Current_refund_Amount), Remark, StatusClass.Confirm, Pgcharges_pax_Wise)
    '                            ST.UpdateStatus_Pax_Header(lblorderId, lblpnr, lblPaxID, StatusClass.Confirm)
    '                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('" + msg + "');javascript:window.close();window.opener.location.href=window.opener.location.href;", True)
    '                        ElseIf (msg = "Already refunded") Then
    '                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('" + msg + "');javascript:window.close();window.opener.location.href=window.opener.location.href;", True)
    '                        Else
    '                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Something went wrong,please try after some time');javascript:window.close();window.opener.location.href=window.opener.location.href;", True)
    '                        End If
    '                    Else
    '                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('" + strSpmsg + "');javascript:window.close();window.opener.location.href=window.opener.location.href;", True)
    '                    End If

    '                    BindGrid()
    '                End If
    '            Else
    '                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Already Refunded');javascript:window.close();window.opener.location.href = window.opener.location.href;", True)
    '            End If
    '        Next
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '        lblmessage.Text = ex.Message
    '    End Try
    'End Sub


    Protected Sub btn_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Update.Click
        Dim result As String = "Ticket Refund Failed"
        '  Dim ablBalance As Double = 0
        Dim PaymentMode As String = ""
        Dim Trip As String = ""
        Dim flag As Integer = 0
        Try
            For Each row As GridViewRow In grd_Pax.Rows
                Dim lblTktNo As String = DirectCast(row.FindControl("lbltktno"), Label).Text
                Dim lblfare As String = DirectCast(row.FindControl("lbltotalfareafterdiscount"), Label).Text
                Dim lbluser As String = DirectCast(row.FindControl("lbluserid"), Label).Text
                Dim lblSector As String = DirectCast(row.FindControl("lblSector"), Label).Text
                Dim lblpnr As String = DirectCast(row.FindControl("lblpnr"), Label).Text
                Dim lblpaxfname As String = DirectCast(row.FindControl("lblpaxfname"), Label).Text
                Dim lblagencyname As String = DirectCast(row.FindControl("lblagencyname"), Label).Text
                Dim lblorderId As String = DirectCast(row.FindControl("lblorderId"), Label).Text
                Dim TDS As String = DirectCast(row.FindControl("lblTDS"), Label).Text
                Dim BookingDate As String = DirectCast(row.FindControl("lblBookingDate"), Label).Text
                Dim lblPaxID As Integer = Convert.ToInt32(DirectCast(row.FindControl("lblPaxID"), Label).Text)
                Dim AgentId As String = td_AgentID.InnerText.Trim()
                Dim PaxType As String = DirectCast(row.FindControl("lblpaxtype"), Label).Text
                Dim mgtFee As String = DirectCast(row.FindControl("HiddenMgtFee"), HiddenField).Value
                Dim mgtFeeVal As Double = If(String.IsNullOrEmpty(mgtFee), 0, Convert.ToDouble(mgtFee))
                Dim ServiceTax As Double = 0
                If (mgtFeeVal > 0) Then
                    Dim SrvTax As String = DirectCast(row.FindControl("HiddenSrvTax"), HiddenField).Value
                    ServiceTax = If(String.IsNullOrEmpty(SrvTax), 0, Convert.ToDouble(SrvTax))
                End If
                Dim FareDiff As Double = 0
                Dim TotlCharge As Double = 0
                Dim HdrDs As New DataSet()
                HdrDs = ST.GetHdrDetails(lblorderId)
                PaymentMode = HdrDs.Tables(0).Rows(0)("PMode").ToString()
                Trip = HdrDs.Tables(0).Rows(0)("Trip").ToString()


                Dim PgCharges As String

                Dim PaxCount As Double
                Dim Pgcharges_pax_Wise As Double
                Dim Current_refund_Amount As Double

                If (txt_charge.Text.Trim = "") Then
                    txt_charge.Text = "0"

                End If
                If (txt_Service.Text.Trim = "") Then
                    txt_Service.Text = "0"
                End If

                PgCharges = HdrDs.Tables(0).Rows(0)("PgCharges").ToString()
                If (PgCharges = "" Or PaxType = "INF") Then
                    PgCharges = "0"
                End If


                PaxCount = Convert.ToDouble(HdrDs.Tables(0).Rows(0)("Adult").ToString()) + Convert.ToDouble(HdrDs.Tables(0).Rows(0)("Child").ToString())
                ' AgentId = HdrDs.Tables(0).Rows(0)("AgentId").ToString()

                Dim Status As Boolean = ST.CheckRefundReissueUpdate(lblPaxID, StatusClass.Cancelled.ToString(), "REFUND")
                If (Status = 0) Then
                    '' GetMerchantKey(lblorderId)
                    'Dim HdrDs As New DataSet()
                    'HdrDs = ST.GetHdrDetails(lblorderId)
                    If HdrDs.Tables(0).Rows.Count <> 0 Then
                        'Checking Its Allready ReIssued or not
                        If HdrDs.Tables(0).Rows(0)("ResuID").ToString() <> "" AndAlso HdrDs.Tables(0).Rows(0)("ResuID").ToString() IsNot Nothing Then
                            FareDiff = Convert.ToDouble(HdrDs.Tables(0).Rows(0)("ResuFareDiff"))
                        End If
                    End If

                    'Checking Booking Month and Today Month is not Same then TDS charge will be deducted 
                    'Dim m1 As String = DatePart(DateInterval.Month, Date.Now)
                    'Dim m() As String = BookingDate.Split("/")
                    'Dim m2 As String = m(0)


                    Dim m1 As Integer = Convert.ToInt32(DatePart(DateInterval.Month, Date.Now))
                    Dim m() As String = BookingDate.Replace("-", "/").Split("/")


                    Dim m2 As Integer = Convert.ToInt32(m(1))
                    'Dim m2 As String = BookingDate.Substring(3, 2).ToString()

                    If m1 = m2 Then
                        TotlCharge = Convert.ToDouble(txt_charge.Text.Trim) + Convert.ToDouble(txt_Service.Text.Trim) + mgtFeeVal + ServiceTax - FareDiff
                        Refundfare = Convert.ToDouble(lblfare) - TotlCharge
                        TDS = Convert.ToDouble("0.00")
                    Else
                        TotlCharge = Convert.ToDouble(txt_charge.Text.Trim) + Convert.ToDouble(txt_Service.Text.Trim) + Convert.ToDouble(TDS) + mgtFeeVal + ServiceTax - FareDiff
                        Refundfare = Convert.ToDouble(lblfare) - TotlCharge
                    End If

                    'Adding Refund Amount in Agent balance
                    Dim ablBalance As Double = ST.UpdateNew_RegsRefund(lbluser, Refundfare)
                    Session("CL") = ablBalance
                    Dim rm As String = "ReFund Charge Against Ticket No " & lblTktNo

                    'Insert data in Transaction Report
                    ST.InsertTransactionRepot(lblpnr, lbluser, Refundfare.ToString, ablBalance.ToString, TotlCharge.ToString, lblSector, lblpaxfname, "", lblagencyname, rm, StatusClass.Cancelled)

                    ' Insert Data in LedgerDetails Table
                    flag = STDom.insertLedgerDetails(lbluser, lblagencyname, lblorderId, lblpnr, lblTktNo, "", "", "", Session("UID").ToString, Request.UserHostAddress, 0, _
                                        Convert.ToDecimal(Refundfare), Convert.ToDecimal(ablBalance), "Ticket Refund", rm, 0, TDS)
                    ' Update Cancellationintl table after Refund 
                    ST.UpdateCancelIntlTbl(Convert.ToInt32(Request.QueryString("counter")), Convert.ToInt32(txt_charge.Text), Convert.ToInt32(txt_Service.Text), Convert.ToInt32(Refundfare), txtRemark.Text, StatusClass.Cancelled, 0)
                    'Update Status =Cancelled in FltHeader and FltPaxDetails 
                    ST.UpdateStatus_Pax_Header(lblorderId, lblpnr, lblPaxID, StatusClass.Cancelled)


                    Try
                        Dim rt As String = "Deducted Cashback Amount, OrderId-" & lblorderId
                        STDom.insertLedgerDetailsCashBack(lbluser, lblagencyname, lblorderId, lblpnr, lblTktNo, "", "", "", Session("UID").ToString, Request.UserHostAddress, 0,
                                            Convert.ToDecimal(Refundfare), Convert.ToDecimal(ablBalance), "CashBack Amount Deducted", rt, 0, TDS)

                    Catch ex As Exception

                    End Try


                    If flag > 0 Then
                        Dim GridDST As New DataSet()
                        GridDST = ST.GetReIssueCancelIntl(Convert.ToInt32(Request.QueryString("counter")), "C", StatusClass.Cancelled)
                        Dim dtCanc As New DataTable()
                        dtCanc = GridDST.Tables(0)
                        ST.InsertRefundDetailsAccountPortal(dtCanc.Rows(0)("RefundID").ToString())
                        'SendSMS(lbluser, Convert.ToString(Refundfare), lblpnr, lblorderId, "FLTREFUND")
                        SendSMS(lbluser, Convert.ToString(Refundfare), lblpnr, lblorderId, SMS.AIRCANCELREFUND)
                        SendEmail(lblagencyname, lblpnr, lblorderId, Refundfare, lblpaxfname, lblTktNo, lbluser)

                        'Else
                    End If
                    result = "Ticket Refund Sucessfull"
                    BindGrid()
                    'ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Refund Sucessfull');javascript:window.close();window.location='ProcessImportPnr.aspx';", True)
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('" + result + "');javascript:window.close();window.location='ProcessImportPnr.aspx';", True)

                    'Response.Write("<script>javascript:window.close();</script>")
                Else
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Already Refunded');javascript:window.close();window.location='ProcessImportPnr.aspx';", True)

                End If
            Next
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            lblmessage.Text = ex.Message

        End Try

    End Sub

    Public Function GetMerchantKey(ByVal orderID As String) As String


        Dim mrchntKey As String = ConfigurationManager.AppSettings("MerchantKey").ToString()

        Try


            Dim provider12 As String = ""
            Dim sqldom As New SqlTransactionDom()

            Dim dsp As New DataSet()

            dsp = sqldom.GetTicketingProvider(orderID)

            If (dsp.Tables.Count > 0) Then
                If (dsp.Tables(0).Rows.Count > 0) Then

                    provider12 = dsp.Tables(0).Rows(0)(0)

                End If


            End If


            If provider12.ToLower().Trim() = "yatra" Then

                mrchntKey = ConfigurationManager.AppSettings("YatraITZMerchantKey").ToString()
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

            mrchntKey = ConfigurationManager.AppSettings("MerchantKey").ToString()
        End Try
        Session("MchntKeyITZ") = mrchntKey
        Return mrchntKey

    End Function
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Write("<script>javascript:window.close();</script>")
    End Sub

    Private Sub SendEmail(ByVal AgencyName As String, ByVal Pnr As String, ByVal OrderID As String, ByVal Refundfare As String, ByVal PaxName As String, ByVal TktNo As String, ByVal Uesrid As String)
        Try
            'Email = "devesh.mailme@gmail.com"
            Dim AgentMobileNo As String = ""
            Dim AgentEmail As String = ""
            Dim AgentName As String = ""
            Dim AvalBal As String = ""
            Dim MailDt As New DataTable
            Dim AgencyDs As DataSet
            AgencyDs = ST.GetAgencyDetails(Uesrid)
            AgentEmail = AgencyDs.Tables(0).Rows(0)("Email")
            MailDt = STDom.GetMailingDetails(MAILING.AIR_BOOKING.ToString().Trim(), "").Tables(0)
            Dim strBody As String = ""
            strBody = "<html><head><title></title><meta http-equiv=Content-Type content=text/html; charset=iso-8859-1></head><body>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Dear " & AgencyName & "</strong></font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Your Ticket No: " & TktNo & " , Pnr No: " & Pnr & " and Pax Name: " & PaxName & "  </strong> has been canceled.</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Refunded Amount :" & Refundfare & " and OrderID=" & OrderID & ".</font>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">If you need immediate assistance or have any questions, concerns or suggestions, please do not hesitate to email us at <a href=""mailto:jakkin@richaworldtravls.com" > "jakkin@richaworldtravls.com </a>.</font></p>"
            'strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">We value you As a customer And would Like To thank  you For your interest.</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Sincerely,</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">" & MailDt.Rows(0)("REGARDS").ToString() & "<br />"
            strBody = strBody + "</body></html>"


            Try
                If (MailDt.Rows.Count > 0) Then
                    Dim Status As Boolean = False
                    Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())
                    If Status = True Then
                        Dim i As Integer = SendMail(AgentEmail, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), "jakkin@richaworldtravls.com", MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strBody, "Air cancelation OrderID:" + OrderID, "")

                    End If
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Public Function SendMail(ByVal toEMail As String, ByVal from As String, ByVal bcc As String, ByVal cc As String, ByVal smtpClient As String, ByVal userID As String, ByVal pass As String, ByVal body As String, ByVal subject As String, ByVal AttachmentFile As String) As Integer
        Dim objMail As New System.Net.Mail.SmtpClient
        Dim msgMail As New System.Net.Mail.MailMessage
        msgMail.To.Clear()
        msgMail.To.Add(New System.Net.Mail.MailAddress(toEMail))
        msgMail.From = New System.Net.Mail.MailAddress(from)
        If bcc <> "" Then
            msgMail.Bcc.Add(New System.Net.Mail.MailAddress(bcc))
        End If
        If cc <> "" Then
            msgMail.CC.Add(New System.Net.Mail.MailAddress(cc))
        End If
        If AttachmentFile <> "" Then
            msgMail.Attachments.Add(New System.Net.Mail.Attachment(AttachmentFile))
        End If

        msgMail.Subject = subject
        msgMail.IsBodyHtml = True
        msgMail.Body = body

        Try
            objMail.Credentials = New System.Net.NetworkCredential(userID, pass)
            objMail.Host = smtpClient
            objMail.EnableSsl = True
            objMail.Send(msgMail)
            Return 1

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            Return 0

        End Try
    End Function
    Private Sub SendSMS(ByVal UserId As String, ByVal Amount As String, ByVal PNR As String, ByVal OrderId As String, ByVal ServiceType As String)
        Try
            Dim smsStatus As String = ""
            Dim smsMsg As String = ""
            Dim AgentMobileNo As String = ""
            Dim AgentEmail As String = ""
            Dim AgentName As String = ""
            Dim AvalBal As String = ""
            'Dim DueAmount As String = ""
            'Dim AgentCreditLimit As String = ""
            Try
                'Mobile = "9871186224"
                Dim objDA As New SqlTransaction
                Dim AgencyDs As DataSet
                Dim AdMobileDs As DataSet
                AgencyDs = ST.GetAgencyDetails(UserId)
                AdMobileDs = ST.GetMobileAndEmailForMsg(SMS.AIRCANCELREFUND)
                AgentMobileNo = AgencyDs.Tables(0).Rows(0)("Mobile")
                AgentEmail = AgencyDs.Tables(0).Rows(0)("Email")
                AgentName = AgencyDs.Tables(0).Rows(0)("Name")
                AvalBal = AgencyDs.Tables(0).Rows(0)("Crd_Limit")
                ' Dear AgentName,OrderId-2133621375(PNR-2133621375) ticket cancelled, Amt 750  refunded in your a/c.
                'CancelRefundAmountMsg(Name,UserId,mobno, smstext, DtCrd,MsgSendTo ,OrderId,Pnr,RefundAmount) 

                Dim FullName As String = AgentName 'FirstName + " " + LastName
                Dim SmsCrd As DataTable
                SmsCrd = ST.SmsCredential(SMS.AIRCANCELREFUND).Tables(0)
                If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                    smsStatus = objSMSAPI.CancelRefundAmountMsg(AgentName, UserId, AgentMobileNo, smsMsg, SmsCrd, "MsgSendTo", OrderId, PNR, Amount)
                    objSql.SmsLogDetails(OrderId, AgentMobileNo, smsMsg, smsStatus)

                    If AdMobileDs IsNot Nothing Then
                        For i As Integer = 0 To AdMobileDs.Tables(0).Rows.Count - 1
                            If AdMobileDs.Tables(0).Rows.Count > 0 Then
                                AvalBal = AgencyDs.Tables(0).Rows(0)("Mobile")
                                smsStatus = objSMSAPI.CancelRefundAmountMsg(AgentName, UserId, AgencyDs.Tables(0).Rows(i)("Mobile"), smsMsg, SmsCrd, "admin", OrderId, PNR, Amount)
                                objSql.SmsLogDetails(OrderId, AgencyDs.Tables(0).Rows(i)("Mobile"), smsMsg, smsStatus)
                            End If
                        Next

                    End If
                End If



               


            Catch ex As Exception
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
End Class


''Imports System.Data
''Imports System.Collections.Generic
''Imports System.Web
''Imports System.Web.UI
''Imports System.Web.UI.WebControls
''Imports YatraBilling

''Partial Class Reports_Refund_TktRptDom_RefundUpdated
''    Inherits System.Web.UI.Page

''    Private ST As New SqlTransaction()
''    Dim GridDS As New DataSet()
''    Private Refundfare As Double
''    Private STDom As New SqlTransactionDom
''    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
''        Response.Cache.SetCacheability(HttpCacheability.NoCache)

''        If Session("UID") = "" Or Session("UID") Is Nothing Then
''            Response.Redirect("~/Login.aspx")
''        End If
''        If Not IsPostBack Then
''            BindGrid()
''            lbluserid.Text = Session("UID")
''            Try
''                If GridDS.Tables(0).Rows.Count <> 0 Then
''                    'Bind Agency DDL
''                    Dim AgencyDS As New DataSet()
''                    AgencyDS = ST.GetAgencyDetails(GridDS.Tables(0).Rows(0).Item("UserID").ToString.Trim)
''                    Dim dt1 As New DataTable()
''                    dt1 = AgencyDS.Tables(0)
''                    Dim addr As String = dt1.Rows(0)("city").ToString() & ", " & dt1.Rows(0)("State").ToString() & ", " & dt1.Rows(0)("country").ToString() & ", " & dt1.Rows(0)("zipcode").ToString()
''                    td_AgentID.InnerText = GridDS.Tables(0).Rows(0).Item("UserID").ToString.Trim
''                    td_AgentName.InnerText = dt1.Rows(0)("Name").ToString()
''                    td_AgentAddress.InnerText = dt1.Rows(0)("Address").ToString()
''                    td_Street.InnerText = addr
''                    td_AgentMobNo.InnerText = dt1.Rows(0)("Mobile").ToString()
''                    td_Email.InnerText = dt1.Rows(0)("Email").ToString()
''                    td_AgencyName.InnerText = dt1.Rows(0)("Agency_Name").ToString()
''                    td_CardLimit.InnerText = dt1.Rows(0)("Crd_Limit").ToString()
''                End If
''            Catch ex As Exception
''                clsErrorLog.LogInfo(ex)

''            End Try
''        End If
''    End Sub
''    Private Sub BindGrid()
''        Try
''            GridDS = ST.GetReIssueCancelIntl(Convert.ToInt32(Request.QueryString("counter")), "C", StatusClass.InProcess)             'GetCancelletionInt(Request.QueryString("counter").ToString())
''            grd_Pax.DataSource = GridDS
''            grd_Pax.DataBind()
''            ViewState("GridDS") = GridDS
''        Catch ex As Exception
''            clsErrorLog.LogInfo(ex)

''        End Try
''    End Sub

''    Protected Sub btn_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Update.Click
''        Try
''            For Each row As GridViewRow In grd_Pax.Rows
''                Dim lblTktNo As String = DirectCast(row.FindControl("lbltktno"), Label).Text
''                Dim lblfare As String = DirectCast(row.FindControl("lbltotalfareafterdiscount"), Label).Text
''                Dim lbluser As String = DirectCast(row.FindControl("lbluserid"), Label).Text
''                Dim lblSector As String = DirectCast(row.FindControl("lblSector"), Label).Text
''                Dim lblpnr As String = DirectCast(row.FindControl("lblpnr"), Label).Text
''                Dim lblpaxfname As String = DirectCast(row.FindControl("lblpaxfname"), Label).Text
''                Dim lblagencyname As String = DirectCast(row.FindControl("lblagencyname"), Label).Text
''                Dim lblorderId As String = DirectCast(row.FindControl("lblorderId"), Label).Text
''                Dim TDS As String = DirectCast(row.FindControl("lblTDS"), Label).Text
''                Dim BookingDate As String = DirectCast(row.FindControl("lblBookingDate"), Label).Text
''                Dim lblPaxID As Integer = Convert.ToInt32(DirectCast(row.FindControl("lblPaxID"), Label).Text)

''                Dim mgtFee As String = DirectCast(row.FindControl("HiddenMgtFee"), HiddenField).Value
''                Dim mgtFeeVal As Double = If(String.IsNullOrEmpty(mgtFee), 0, Convert.ToDouble(mgtFee))

''                Dim ServiceTax As Double = 0
''                If (mgtFeeVal > 0) Then
''                    Dim SrvTax As String = DirectCast(row.FindControl("HiddenSrvTax"), HiddenField).Value
''                    ServiceTax = If(String.IsNullOrEmpty(SrvTax), 0, Convert.ToDouble(SrvTax))
''                End If
''                Dim FareDiff As Double = 0
''                Dim TotlCharge As Double = 0


''                Dim Status As Boolean = ST.CheckRefundReissueUpdate(lblPaxID, StatusClass.Cancelled.ToString(), "REFUND")
''                If (Status = 0) Then

''                    Dim HdrDs As New DataSet()
''                    HdrDs = ST.GetHdrDetails(lblorderId)
''                    If HdrDs.Tables(0).Rows.Count <> 0 Then
''                        'Checking Its Allready ReIssued or not
''                        'If dtpnr.Rows(0)("ResuID").ToString() <> "" AndAlso dtpnr.Rows(0)("ResuID").ToString() IsNot Nothing Then

''                        If HdrDs.Tables(0).Rows(0)("ResuID").ToString() <> "" AndAlso HdrDs.Tables(0).Rows(0)("ResuID").ToString() IsNot Nothing Then
''                            FareDiff = Convert.ToDouble(HdrDs.Tables(0).Rows(0)("ResuFareDiff"))
''                        End If
''                    End If

''                    'Checking Booking Month and Today Month is not Same then TDS charge will be deducted 
''                    Dim m1 As String = DatePart(DateInterval.Month, Date.Now)
''                    Dim m() As String = BookingDate.Split("/")
''                    Dim m2 As String = m(0)
''                    If m1 = m2 Then
''                        TotlCharge = Convert.ToDouble(txt_charge.Text.Trim) + Convert.ToDouble(txt_Service.Text.Trim) + mgtFeeVal + ServiceTax - FareDiff
''                        Refundfare = Convert.ToDouble(lblfare) - TotlCharge
''                    Else
''                        TotlCharge = Convert.ToDouble(txt_charge.Text.Trim) + Convert.ToDouble(txt_Service.Text.Trim) + Convert.ToDouble(TDS) + mgtFeeVal + ServiceTax - FareDiff
''                        Refundfare = Convert.ToDouble(lblfare) - TotlCharge
''                    End If

''                    'Adding Refund Amount in Agent balance
''                    Dim ablBalance As Double = ST.UpdateNew_RegsRefund(lbluser, Refundfare)
''                    Session("CL") = ablBalance
''                    Dim rm As String = "ReFund Charge Against Ticket No " & lblTktNo

''                    'Insert data in Transaction Report
''                    ST.InsertTransactionRepot(lblpnr, lbluser, Refundfare.ToString, ablBalance.ToString, TotlCharge.ToString, lblSector, lblpaxfname, "", lblagencyname, rm, StatusClass.Cancelled)

''                    ' Insert Data in LedgerDetails Table
''                    STDom.insertLedgerDetails(lbluser, lblagencyname, lblorderId, lblpnr, lblTktNo, "", "", "", Session("UID").ToString, Request.UserHostAddress, 0, _
''                                        Convert.ToDecimal(Refundfare), Convert.ToDecimal(ablBalance), "Ticket Refund", rm, 0)

''                    ' Update Cancellationintl table after Refund 
''                    ST.UpdateCancelIntlTbl(Convert.ToInt32(Request.QueryString("counter")), Convert.ToInt32(txt_charge.Text), Convert.ToInt32(txt_Service.Text), Convert.ToInt32(Refundfare), txtRemark.Text, StatusClass.Cancelled)

''                    'Update Status =Cancelled in FltHeader and FltPaxDetails 
''                    ST.UpdateStatus_Pax_Header(lblorderId, lblpnr, lblPaxID, StatusClass.Cancelled)

''                    'NAV METHOD  CALL START
''                    Try

''                        'Dim objNav As New AirService.clsConnection(lblorderId, "2", lblPaxID)
''                        'objNav.airBookingNav(lblorderId, "", 2)

''                    Catch ex As Exception

''                    End Try
''                    'Nav METHOD END'

''                    'Online Yatra
''                    Try
''                        'Dim AirObj As New AIR_YATRA
''                        'AirObj.ProcessYatra_Air_CN(lblorderId, lblpnr, Convert.ToInt32(lblPaxID))
''                    Catch ex As Exception

''                    End Try
''                    'Online Yatra End


''                    BindGrid()
''                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Refund Sucessfull');javascript:window.close();window.location='ProcessImportPnr.aspx';", True)

''                    'Response.Write("<script>javascript:window.close();</script>")
''                Else
''                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Already Refunded');javascript:window.close();window.location='ProcessImportPnr.aspx';", True)

''                End If


''            Next




''        Catch ex As Exception
''            clsErrorLog.LogInfo(ex)
''            lblmessage.Text = ex.Message
''            Throw
''        End Try

''    End Sub

''    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
''        Response.Write("<script>javascript:window.close();</script>")
''    End Sub
''End Class

