﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Accounts_ChangeType : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         if(!IsPostBack)
         {
             DropDownListType.AppendDataBoundItems = true;
             
                DropDownListType.DataSource = GroupTypeMGMT("", "", "MultipleSelect");
                DropDownListType.DataTextField = "GroupType";
                DropDownListType.DataValueField = "GroupType";
                DropDownListType.DataBind();
                msg.Visible = false;
         }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (DropDownListType.SelectedValue != "Select")
        {
            if (FileUpload1.HasFile)
            {

                string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);

                string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);

                string FolderPath = ConfigurationManager.AppSettings["FolderPath"];



                string FilePath = Server.MapPath(FolderPath + FileName);

                FileUpload1.SaveAs(FilePath);

                Import_To_Grid(FilePath, Extension, "NO");

            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Select Upload file Format .xls or .xlsx');", true);
            }
        }
        else
        { 
         ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Select Agent Type Value');", true);
        }

    }
    private void Import_To_Grid(string FilePath, string Extension, string isHDR)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

        string conStr = "";

        switch (Extension)
        {

            case ".xls": //Excel 97-03

                conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"]

                         .ConnectionString;

                break;

            case ".xlsx": //Excel 07

                conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"]

                          .ConnectionString;

                break;

        }

        conStr = String.Format(conStr, FilePath, isHDR);

        OleDbConnection connExcel = new OleDbConnection(conStr);

        OleDbCommand cmdExcel = new OleDbCommand();

        OleDbDataAdapter oda = new OleDbDataAdapter();

        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;



        //Get the name of First Sheet

        connExcel.Open();

        DataTable dtExcelSchema;

        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();

        connExcel.Close();



        //Read Data from First Sheet

        connExcel.Open();

        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";

        oda.SelectCommand = cmdExcel;

        oda.Fill(dt);

        connExcel.Close();

        try
        {
            string updateUserID = "";
            string notupdateUSERID = "";
            if (dt.Rows.Count > 0)
            {
               
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int a = 0;
                    string UserID = dt.Rows[i][0].ToString();
                    if (UserID != "")
                    {

                        con.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "Sp_ChangeAgentTypeLog";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserID", UserID);
                        cmd.Parameters.AddWithValue("@AgentType", DropDownListType.SelectedValue.Trim());
                        cmd.Parameters.AddWithValue("@UpdateBy", Session["UID"].ToString());
                        cmd.Connection = con;
                        a = cmd.ExecuteNonQuery();
                        con.Close();
                        if (a > 0)
                        {
                            updateUserID += UserID + ",";
                        }
                        else
                        {
                            notupdateUSERID += UserID + ",";
                        }

                    }

                }
                msg.Visible = true;
                msg.InnerText= "UpdateAgent:-"+updateUserID + " NotUpdate:-" +notupdateUSERID;
                if (updateUserID.Replace(",", "") == "" && notupdateUSERID.Replace(",", "") == "")
                {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('No Record Update');",true);
                }
                if (updateUserID.Replace(",", "") != "" && notupdateUSERID.Replace(",", "") == "")
                {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('All Records are update successfully');",true);
                }
                if (updateUserID.Replace(",", "") != "" && notupdateUSERID.Replace(",", "") != "")
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Some Records are not Updated');", true);
                }
                

            }
        }
        catch (Exception EX)
        { }

        finally
        {
            con.Close();
        }
        //Bind Data to GridView

  

    }

    public DataTable GroupTypeMGMT(string type, string desc, string cmdType)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        DataTable dt = new DataTable();
        try
        {
            con.Open();

            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "usp_agentTypeMGMT";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 200).Value = type;
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 500).Value = desc;
            cmd.Parameters.Add("@cmdType", SqlDbType.VarChar, 50).Value = cmdType;
            cmd.Parameters.Add("@msg", SqlDbType.VarChar, 500);
            cmd.Parameters["@msg"].Direction = ParameterDirection.Output;

            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();



        }

        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        finally
        {
            con.Close();
        }
        return dt;
    }

}