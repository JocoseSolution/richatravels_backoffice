﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesSummaryReportByExecID.aspx.vb" Inherits="SprReports_Accounts_SalesSummaryReportByExecID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="<%= ResolveUrl("~/newcss/bootstrap.min.css")%>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/newcss/font-awesome/css/font-awesome.min.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }

        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .viewbutton {
            border: 1px solid #af3e3a;
            padding: 5px;
            border-radius: 5px;
            background: #af3e3a;
            color: #fff;
            font-weight: bold;
            /* text-decoration: none; */
            text-decoration: none;
        }

            .viewbutton:hover {
                text-decoration: none !important;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:GridView ID="execsummaryrept_grdview" runat="server" AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%">
            <Columns>
                <asp:TemplateField HeaderText="Executive ID">
                    <ItemTemplate>
                        <asp:Label ID="ExecutiveID" runat="server" Text='<%#Eval("SalesExecID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Executive Name">
                    <ItemTemplate>
                        <asp:Label ID="ExecutiveName" runat="server" Text='<%#Eval("ExecutiveName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="User ID">
                    <ItemTemplate>
                        <asp:Label ID="UserID" runat="server" Text='<%#Eval("user_id")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Agency Name">
                    <ItemTemplate>
                        <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Booking Count">
                    <ItemTemplate>
                        <asp:Label ID="BookingCount" runat="server" Text='<%#Eval("BookingCount")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Sale Amount">
                    <ItemTemplate>
                        <asp:Label ID="totalsale" runat="server" Text='<%#Eval("totalsale")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Due Amount">
                    <ItemTemplate>
                        <asp:Label ID="totaldue" runat="server" Text='<%#Eval("totaldue")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>            
        </asp:GridView>
    </form>
</body>
</html>
