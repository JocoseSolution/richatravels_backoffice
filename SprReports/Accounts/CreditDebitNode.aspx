﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CreditDebitNode.aspx.vb" Inherits="SprReports_Accounts_CreditDebitNode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <script src="../../JS/JScript.js" type="text/javascript"></script>
    <link href="../../CSS/itz.css" rel="stylesheet" />

    <link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function ConfirmMsg() {
            var Amount = document.getElementById("txtAmount").value;
            var remarks = document.getElementById("txtRemarks").value;

            if (Amount == '') {
                alert("Amount is required.");
                return false;
            }

            if (remarks == '') {
                alert("Enter remarks.");
                return false;
            }

            var x = confirm("Are you sure to proceed?");
            if (x)
                return true;
            else
                return false;
        }
        function validateNumbersOnly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if ((unicode == 8) || (unicode == 9) || (unicode > 47 && unicode < 58) || (unicode == 46)) {
                return true;
            }
            else {
                window.alert("Amount should be in decimail format.");
                return false;
            }
        }


    </script>
    <style>
        .gridview {
            height: auto;
            width: 96%;
            margin: auto;
            background-color: #f3f3f3;
            text-align: center;
        }

            .gridview th {
                background-color: #004b91;
                color: #fff;
                font-weight: bold;
                height: 30px;
            }

            .gridview tr td {
                text-align: center;
                padding-bottom: 5px;
                padding-top: 5px;
                
            }

        .auto-style1 {
            width: 325px;
        }

        .tableClass {
            width: 80%;
            margin:auto;
        }

            .tableClass tr td {
                padding: 2px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UP" runat="server">
                <ContentTemplate>
                    <fieldset style="border: thin solid #004b91; padding: 0; width: 90%; margin: auto;">
                        <legend style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #004b91;">Credit/Debit Note</legend>
                        <br />
                        <table id="adminrict"  runat="server" visible="false">
                            <tr>
                                <td> Credit / Debit Note is Not For Admin</td>
                            </tr>
                        </table>
                        <table class="tableClass" id="userOther" runat="server" visible="true">


                            <tr>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left" colspan="4">
                                    <asp:Label ID="lblAgencyInfo" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr><td></td><td></td><td></td><td></td></tr>
                            <tr>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">User Id:</td>
                                <td>
                                    <asp:Label ID="lbl_agentId" runat="server" Text=""></asp:Label></td>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">Available Bal:</td>
                                <td>
                                    <asp:Label ID="lblCurrentBal" runat="server" Text=""></asp:Label></td>
                            </tr>

                            <tr>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">Order Id:</td>
                                <td>
                                    <asp:Label ID="lbl_OrderId" runat="server" Text=""></asp:Label></td>
                           
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">Total Credit :</td>
                                <td>
                                    <asp:Label ID="lblTotalCredit" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td></td><td></td>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">Total Debit :</td>
                                <td>
                                    <asp:Label ID="lblTotalDebit" runat="server" Text=""></asp:Label></td>
                            </tr>

                            <tr>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">Enter Amount:</td>
                                <td>
                                    <asp:TextBox ID="txtAmount" runat="server" onKeyPress="return validateNumbersOnly(event);" Width="200px" Height="22px"></asp:TextBox></td>
                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;" align="left">Remarks</td>
                                <td>
                                    <asp:TextBox ID="txtRemarks" runat="server" Width="200px" Height="22px"></asp:TextBox></td>
                            </tr>
                            
                            <tr>
                                <td></td><td></td>
                                <td class="auto-style1" colspan="2">
                                    <asp:Button ID="btnAddCredit" runat="server" Text="Credit" CssClass="buttonfltbks" Width="130px" OnClientClick="return ConfirmMsg();" />
                                    &nbsp;<asp:Button ID="btnAddDebit" runat="server" Text="Debit" CssClass="buttonfltbks" Width="130px" OnClientClick="return ConfirmMsg();" /></td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <div>
                            <h4 style="color: #004b91; text-align: left; margin-left: 2%;">Previous Transactions</h4>
                            <hr />
                            <asp:GridView ID="gridCreditDebitDetails" runat="server"
                                EmptyDataText="No record found.!" CssClass="gridview" AllowPaging="true"
                                AutoGenerateColumns="False" PageSize="30">

                                <Columns>
                                    <asp:TemplateField HeaderText="Order_Id">

                                        <ItemTemplate>
                                            <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PNR">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("PNR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User_Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_AgentId" runat="server" Text='<%#Eval("AgentId")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_agencyId" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Distributer">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DistrId" runat="server" Text='<%#Eval("DistributerId")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DEbitAmt" runat="server" Text='<%#Eval("Debit")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CRDAMT" runat="server" Text='<%#Eval("Credit")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Remark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service_Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ServiceType" runat="server" Text='<%#Eval("ServiceType")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="IP Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_IP" runat="server" Text='<%#Eval("IPAddress")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Created_Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Date" runat="server" Text='<%#Eval("CreatedDate")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created_By">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CRBy" runat="server" Text='<%#Eval("CreatedBy")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reference_Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Ref" runat="server" Text='<%#Eval("ReferenceId")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>
                            <br />
                            <br />
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
