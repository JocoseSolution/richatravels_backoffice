﻿Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Imports System.Diagnostics
Partial Class Reports_Accounts_Upload
    Inherits System.Web.UI.Page

    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            'If Session("User_Type").ToString() <> "ACC" Then
            '    Response.Redirect("~/Login.aspx")
            'End If
            tblUploadType.Visible = False
            If Not IsPostBack Then
                Dim DTUT As New DataTable
                DTUT = STDom.GetUploadType().Tables(0)
                RBL_Type.DataSource = DTUT
                RBL_Type.DataTextField = "UploadCategoryText"
                RBL_Type.DataValueField = "UploadCategory"
                RBL_Type.DataBind()
                RBL_Type.SelectedIndex = 0
                Dim DTUC As New DataTable
                DTUC = STDom.GetCategory("CA").Tables(0)
                If DTUC.Rows.Count > 0 Then
                    ddl_Category.AppendDataBoundItems = True
                    ddl_Category.Items.Clear()
                    ddl_Category.Items.Insert(0, "--Select Category--")
                    ddl_Category.DataSource = DTUC
                    ddl_Category.DataTextField = "SubCategory"
                    ddl_Category.DataValueField = "GroupType"
                    ddl_Category.DataBind()
                End If
                BindGrid()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub


    Protected Sub ddl_Category_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Category.SelectedIndexChanged
        BindGrid()

    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging

        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Private Sub BindGrid()
        Try
            Dim dtgrid As New DataTable
            ''dtgrid = STDom.GetAgencyByType(ddl_Category.SelectedValue).Tables(0)
            'dtgrid = STDom.GetAllAgency("all").Tables(0)
            dtgrid = STDom.GetAllAgency("HEADOFFICE").Tables(0)
            If dtgrid.Rows.Count = 0 Then
                tr_search.Visible = False
            Else
                tr_search.Visible = True

            End If
            GridView1.DataSource = dtgrid
            GridView1.DataBind()
            'ViewState("DtGrid") = dtgrid
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub RBL_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBL_Type.SelectedIndexChanged
        Try
            Dim DTUC As New DataTable
            DTUC = STDom.GetCategory(RBL_Type.SelectedValue).Tables(0)
            If DTUC.Rows.Count > 0 Then
                ddl_Category.AppendDataBoundItems = True
                ddl_Category.Items.Clear()
                ddl_Category.Items.Insert(0, "--Select Category--")
                ddl_Category.DataSource = DTUC
                ddl_Category.DataTextField = "SubCategory"
                ddl_Category.DataValueField = "GroupType"
                ddl_Category.DataBind()
                BindGrid()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub



    'Private Sub SearchText()
    '    Try
    '        Dim dt As DataTable = STDom.GetAgencyByType(ddl_Category.SelectedValue).Tables(0)
    '        Dim dv As New DataView(dt)


    '        Dim SearchExpression As String = Nothing
    '        If Not [String].IsNullOrEmpty(txtSearch.Text) Then

    '            SearchExpression = String.Format("{0} '%{1}%'", GridView1.SortExpression, txtSearch.Text)
    '        End If
    '        dv.RowFilter = "Agency_Name like" & SearchExpression
    '        GridView1.DataSource = dv
    '        GridView1.DataBind()
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try
    'End Sub
    'Public Function Highlight(ByVal InputTxt As String) As String

    '    Dim Search_Str As String = txtSearch.Text.ToString()
    '    ' Setup the regular expression and add the Or operator.
    '    Dim RegExp As New Regex(Search_Str.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase)

    '    ' Highlight keywords by calling the 
    '    'delegate each time a keyword is found.
    '    Return RegExp.Replace(InputTxt, New MatchEvaluator(AddressOf ReplaceKeyWords))

    '    ' Set the RegExp to null.
    '    RegExp = Nothing

    'End Function

    'Public Function ReplaceKeyWords(ByVal m As Match) As String

    '    Return "<span class='highlight'>" & Convert.ToString(m.Value) & "</span>"

    'End Function

    'Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
    '    SearchText()
    'End Sub

    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click
        Try
            Dim dtag As New DataTable
            dtag = STDom.GetAgencyByType(Request("hidtxtAgencyName")).Tables(0) 'ST.GetAgencyDetails(Request("hidtxtAgencyName")).Tables(0)
            GridView1.DataSource = dtag
            GridView1.DataBind()
        Catch ex As Exception

        End Try

    End Sub
End Class

