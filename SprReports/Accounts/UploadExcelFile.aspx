﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="UploadExcelFile.aspx.cs" Inherits="SprReports_Accounts_UploadExcelFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Accounts > Import Excel File</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:FileUpload ID="FileUpload1" class="form-control" runat="server" />
                                    <label class="custom-file-label"></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnUpload" runat="server" class="btn btn-primary" Text="Upload Excel File" OnClick="btnUpload_Click" />

                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Import Excel File</h4>
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Choose excel file</label>
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                        </div>
                                                        <label id="filename"></label>
                                                        <div class="input-group-append">
                                                        </div>
                                                    </div>
                                                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                            <asp:GridView ID="GridView1" HeaderStyle-CssClass="bg-primary text-white"  runat="server" AutoGenerateColumns="false" AllowPaging="true"  OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="20"  CssClass="table">
                                <emptydatatemplate>
                                    <div class="text-center">No record found</div>
                                </emptydatatemplate>
                                <columns>
                                    <asp:BoundField HeaderText="Customer" DataField="customer" />
                                    <asp:BoundField HeaderText="Service" DataField="service" />
                                    <asp:BoundField HeaderText="Reference" DataField="reference" />
                                    <asp:BoundField HeaderText="Date" DataField="date" />
                                    <asp:BoundField HeaderText="Against" DataField="against" />
                                    <asp:BoundField HeaderText="Against Date" DataField="against_date" />
                                    <asp:BoundField HeaderText="Reference No." DataField="reference_no" />
                                </columns>
                            </asp:GridView>
                                     </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
        </div>
    </div>
</asp:Content>

