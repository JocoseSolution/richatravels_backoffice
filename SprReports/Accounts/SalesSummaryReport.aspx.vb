﻿
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Web.Services

Partial Class SprReports_Accounts_SalesSummaryReport
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Private ST As New SqlTransaction()
    Private CllInsSelectFlt As New clsInsertSelectedFlight()
    Dim AgencyDDLDS, grdds, fltds As New DataSet()
    Private sttusobj As New Status()
    Dim con As New SqlConnection()
    Dim PaxType As String
    Dim clsCorp As New ClsCorporate()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If

            If Not IsPostBack Then
                BindSalesExecutiveDetails()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Sub BindSalesExecutiveDetails()
        Try
            Dim FromDate As String
            Dim ToDate As String
            Dim ExecutId As String

            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)  'Date Format 2017-04-09 12:00:00 AM'
                FromDate = FromDate + " " + "00:00:00.001"
            End If

            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2) & "-" & Left((Request("To")).Split(" ")(0), 2)
                ToDate = ToDate & " " & "23:59:59.999"
            End If

            ExecutId = txt_ExecutiveId.Text

            Dim dtsaerch As New DataTable
            dtsaerch = STDom.BindSalesExecutiveDetails(FromDate, ToDate, ExecutId).Tables(0)
            execsummaryrept_grdview.DataSource = dtsaerch
            execsummaryrept_grdview.DataBind()

            txt_ExecutiveId.Text = ""
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            BindSalesExecutiveDetails()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
End Class
