﻿Imports System.Data.SqlClient
Imports System.Data
Imports YatraBilling

Partial Class SprReports_Accounts_UploadAdjustment
    Inherits System.Web.UI.Page
    Dim Counter As Integer
    Dim Type As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        If (Not IsPostBack) Then

            Dim con As New SqlConnection
            Dim adp As SqlDataAdapter
            Dim dt As New DataTable
            Counter = Convert.ToInt32(Request("Counter"))
            Type = Request("Type")
            If con.State = ConnectionState.Open Then con.Close()
            con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString

            If (Type.Trim.ToUpper() = "INSERT") Then
                div_adjdtl.Visible = False
                adp = New SqlDataAdapter("SP_GETCIFDETAILS_COUNTER", con)
                adp.SelectCommand.CommandType = CommandType.StoredProcedure
                adp.SelectCommand.Parameters.AddWithValue("@COUNTER", Counter)
                adp.Fill(dt)


                td_agentid.InnerText = dt.Rows(0)("AgentId").ToString()
                td_AgencyName.InnerText = dt.Rows(0)("AgencyName").ToString()
                lbl_Invoice.Text = dt.Rows(0)("InvoiceNo").ToString()
                lbl_Counter.Text = dt.Rows(0)("Counter").ToString()

            Else
                div_adj.Visible = False
                adp = New SqlDataAdapter("SP_GET_ADJUSTMENTDETAILS", con)
                adp.SelectCommand.CommandType = CommandType.StoredProcedure
                adp.SelectCommand.Parameters.AddWithValue("@COUNTER", Counter)
                adp.SelectCommand.Parameters.AddWithValue("@OPERATION", "SELECT")
                adp.Fill(dt)
                GridViewshow.DataSource = dt
                GridViewshow.DataBind()
            End If

        End If

    End Sub

    Protected Sub btn_adjamtClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_adjamt.Click
        Try
            Dim Narration As String = ""
            If (ddl_banklist.SelectedValue.ToUpper() <> "OTHER") Then
                Narration = txt_narration.Text.Trim()
            End If
            Dim con As New SqlConnection
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
            con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
            con.Open()
            Dim cmd As SqlCommand
            cmd = New SqlCommand("SP_INSERT_ADJUSTMENTDETAILS", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@INVOICENO", lbl_Invoice.Text.Trim)
            cmd.Parameters.AddWithValue("@AGENTID", td_agentid.InnerText.Trim)
            cmd.Parameters.AddWithValue("@AGENCYNAME", td_AgencyName.InnerText.Trim)
            cmd.Parameters.AddWithValue("@BANKNAME", ddl_banklist.SelectedItem.Text.Trim)
            cmd.Parameters.AddWithValue("@BANKCODE", ddl_banklist.SelectedValue)
            cmd.Parameters.AddWithValue("@AMOUNT", Convert.ToDecimal(txt_adj_amount.Text.Trim))
            cmd.Parameters.AddWithValue("@CIFCOUNTER", lbl_Counter.Text)
            cmd.Parameters.AddWithValue("@REMARK", txt_remark.Text.Trim)
            cmd.Parameters.AddWithValue("@UPLOADTYPE", ddl_utype.SelectedValue)
            cmd.Parameters.AddWithValue("@ACCOUNTID", Session("UID"))
            cmd.Parameters.AddWithValue("@NARRATION", Narration)
            Dim Status As Integer = cmd.ExecuteNonQuery()
            con.Close()


            If (Status > 0) Then
                Dim Inflow As New InFlowDetails()
                Inflow.Update(ddl_utype.SelectedValue, txt_remark.Text.Trim(), "", Session("UID"), "", Convert.ToInt32(lbl_Counter.Text.Trim))
                If ddl_banklist.SelectedValue.Trim.ToUpper <> "OTHER" Then
                    Try
                        Dim P_upload As New Party
                        P_upload.InsertUpload_Details(lbl_Invoice.Text.Trim, td_agentid.InnerText.Trim, "CREDIT", Convert.ToDecimal(txt_adj_amount.Text.Trim), Narration, ddl_banklist.SelectedValue)
                    Catch ex As Exception

                    End Try
                End If

                div_adjdtl.Visible = False
                tbl_Upload.Visible = False
                td_msg.Visible = True
                td_msg.InnerText = "INR. " & txt_adj_amount.Text.Trim & " adjusted to  " & td_agentid.InnerText & " (" & td_AgencyName.InnerText & ") account sucessfully"
            Else
                div_adjdtl.Visible = False
                tbl_Upload.Visible = False
                td_msg.Visible = True
                td_msg.InnerText = "Unable to adjust at this time . Please Try again"
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class
