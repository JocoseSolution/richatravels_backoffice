﻿Imports System
Imports System.Collections.Generic

Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Partial Class Reports_Accounts_AccountPanel
    Inherits System.Web.UI.Page


    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction
    Private objSql As New SqlTransactionNew
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        divFilter.Visible = False
        If Not IsPostBack Then
            Try


                If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing Then
                    grd_accdeposit.DataSource = STDom.DepositStatusDetails("Pending") ' upload.GetDepositProcessDetails("Pending")
                    grd_accdeposit.DataBind()
                    Dim DTUT As New DataTable
                    DTUT = STDom.GetUploadType().Tables(0)
                    RBL_Type.DataSource = DTUT
                    RBL_Type.DataTextField = "UploadCategoryText"
                    RBL_Type.DataValueField = "UploadCategory"
                    RBL_Type.DataBind()
                    RBL_Type.SelectedIndex = 0
                    Dim DTUC As New DataTable
                    DTUC = STDom.GetCategory("CA").Tables(0)
                    If DTUC.Rows.Count > 0 Then
                        ddl_Category.AppendDataBoundItems = True
                        ddl_Category.Items.Clear()
                        ddl_Category.Items.Insert(0, "--Select Category--")
                        ddl_Category.DataSource = DTUC
                        ddl_Category.DataTextField = "SubCategory"
                        ddl_Category.DataValueField = "GroupType"
                        ddl_Category.DataBind()

                    End If
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        End If


    End Sub
    Protected Sub grd_accdeposit_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        Try
            ID = e.CommandArgument.ToString()
            Dim DtDDetails As New DataTable
            DtDDetails = STDom.GetDepositDetailsByID(ID).Tables(0)
            Dim Status As String = DtDDetails.Rows(0)("Status").ToString
            Dim IDS As String = DtDDetails.Rows(0)("Counter").ToString
            Dim rw As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            Dim AgentID As Label = DirectCast(rw.FindControl("lbl_uid"), Label)
            Dim IsSent As New Boolean

            If e.CommandName = "accept" Then
                ID = e.CommandArgument.ToString()
                STDom.UpdateDepositDetails(ID, AgentID.Text, "InProcess", "Acc", Session("UID"), txt_Reject.Text.Replace("'", ""))
                'ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Sucessfully');", True)
                'td_Reject.Visible = False
                IsSent = SendMessageAcceptject(DtDDetails.Rows(0)("AgencyID").ToString, DtDDetails.Rows(0)("Amount").ToString, "Accepted")
                grd_accdeposit.DataSource = STDom.DepositStatusDetails("Pending")
                grd_accdeposit.DataBind()
            End If

            If e.CommandName = "reject" Then

                If IDS = ID AndAlso Status = "Confirm" Then
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Deposite amount is already confirm');", True)
                    grd_accdeposit.DataSource = STDom.DepositStatusDetails("Pending")
                    grd_accdeposit.DataBind()
                Else
                    Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
                    Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
                    gvr.BackColor = System.Drawing.Color.Yellow

                    td_Reject.Visible = True
                    ViewState("ID") = ID
                    ViewState("AgentID") = AgentID.Text
                    ViewState("UPAmount") = DtDDetails.Rows(0)("Amount").ToString
                End If



            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        Try
            td_Reject.Visible = False
            grd_accdeposit.DataSource = STDom.DepositStatusDetails("Pending")
            grd_accdeposit.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub grd_accdeposit_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)


    End Sub
    Protected Sub ddl_Category_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Category.SelectedIndexChanged
        Try
            BindGrid()
            txtSearch.Text = ""
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Private Sub BindGrid()
        Try
            Dim dtgrid As New DataTable
            dtgrid = STDom.DepositProcessDetails("Pending", ddl_Category.SelectedValue).Tables(0)
            grd_accdeposit.DataSource = dtgrid
            grd_accdeposit.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub
    Protected Sub RBL_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBL_Type.SelectedIndexChanged
        Try
            Dim DTUC As New DataTable
            DTUC = STDom.GetCategory(RBL_Type.SelectedValue).Tables(0)
            If DTUC.Rows.Count > 0 Then
                ddl_Category.AppendDataBoundItems = True
                ddl_Category.Items.Clear()
                ddl_Category.Items.Insert(0, "--Select Category--")
                ddl_Category.DataSource = DTUC
                ddl_Category.DataTextField = "SubCategory"
                ddl_Category.DataValueField = "GroupType"
                ddl_Category.DataBind()
                BindGrid()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Function SendMessageAcceptject(ByVal agencyId As String, ByVal amount As String, ByVal actionType As String) As Boolean
        Dim DT As DataTable
        Dim agencyName As String
        Dim agencyMobile As String
        DT = ST.GetAgencyDetails(agencyId).Tables(0)
        If DT.Rows.Count > 0 Then
            agencyName = DT.Rows(0)("Agency_Name").ToString
            agencyMobile = DT.Rows(0)("Mobile").ToString
        End If

        Dim agencysmstext As String = ""
        Dim adminsmstext As String = ""
        Dim smsAgencyStatus As String = ""
        Dim smsAdminStatus As String = ""
        Dim SmsCrd As DataTable
        Dim objSMSAPI As New SMSAPI.SMS
        Dim dtagentmob As New DataTable()

        SmsCrd = ST.SmsCredential(SMS.UPLOADCREDIT.ToString()).Tables(0)
        If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
            'Send Sms for agency
            agencysmstext = "Dear " + agencyName + ", Upload Request For Rs. " + amount + " has been " + actionType + " By Admin on " + DateTime.Now
            smsAgencyStatus = objSMSAPI.SendSmsForAnyService(agencyMobile, agencysmstext, SmsCrd)
            objSql.SmsLogDetails(agencyId, agencyMobile, agencysmstext, smsAgencyStatus)

            'Send Sms for admin
            adminsmstext = "Dear Admin, Upload request for Rs. " + amount + " has been " + actionType + " of Agency - " + agencyName + " (" + agencyId + ") by " + Convert.ToString(Session("UID")) + " on " + DateTime.Now
            smsAdminStatus = objSMSAPI.SendSmsForAnyService("9825326799", adminsmstext, SmsCrd)
            objSql.SmsLogDetails(Convert.ToString(Session("UID")), "9825326799", adminsmstext, smsAdminStatus)
        End If

        Return True

    End Function

    Private Sub SearchText()
        Try
            'If ddl_Category.SelectedValue = "--Select Category--" Then
            Dim dt As DataTable = STDom.DepositStatusDetails("Pending").Tables(0)
            Dim dv As New DataView(dt)
            Session("dv") = dv
            Dim SearchExpression As String = Nothing
            If Not [String].IsNullOrEmpty(txtSearch.Text) Then

                SearchExpression = String.Format("{0} '%{1}%'", grd_accdeposit.SortExpression, txtSearch.Text)
            End If
            dv.RowFilter = "AgencyName like" & SearchExpression
            grd_accdeposit.DataSource = dv
            grd_accdeposit.DataBind()
            'Else
            '    Dim dt As DataTable = STDom.DepositProcessDetails("Pending", ddl_Category.SelectedValue).Tables(0)
            '    Dim dv As New DataView(dt)
            '    Dim SearchExpression As String = Nothing
            '    If Not [String].IsNullOrEmpty(txtSearch.Text) Then

            '        SearchExpression = String.Format("{0} '%{1}%'", grd_accdeposit.SortExpression, txtSearch.Text)
            '    End If
            '    dv.RowFilter = "AgencyName like" & SearchExpression
            '    grd_accdeposit.DataSource = dv

            '    grd_accdeposit.DataBind()
            'End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Public Function ReplaceKeyWords(ByVal m As Match) As String

        Return "<span class='highlight'>" & Convert.ToString(m.Value) & "</span>"

    End Function
    Public Function Highlight(ByVal InputTxt As String) As String
        Dim Search_Str As String = txtSearch.Text.ToString()
        ' Setup the regular expression and add the Or operator.
        Dim RegExp As New Regex(Search_Str.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase)

        ' Highlight keywords by calling the 
        'delegate each time a keyword is found.
        Return RegExp.Replace(InputTxt, New MatchEvaluator(AddressOf ReplaceKeyWords))

        ' Set the RegExp to null.
        RegExp = Nothing

    End Function
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            SearchText()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click
        Try
            Dim IsSent As New Boolean
            STDom.UpdateDepositDetails(ViewState("ID"), ViewState("AgentID"), "Rejected", "Rej", Session("UID"), txt_Reject.Text.Replace("'", ""))

            IsSent = SendMessageAcceptject(ViewState("AgentID"), ViewState("UPAmount"), "Rejected")

            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Sucessfully');", True)
            td_Reject.Visible = False
            grd_accdeposit.DataSource = STDom.DepositStatusDetails("Pending")
            grd_accdeposit.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

End Class
