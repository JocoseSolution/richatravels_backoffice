﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="IntlSaleRegister.aspx.vb" Inherits="Reports_Accounts_IntlSaleRegister" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
       <link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language='javascript'>
        function callprint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'left=0,top=0,width=750,height=500,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write("<html><head><title>Ticket Details</title></head><body>" + prtContent.innerHTML + "</body></html>");
            prtContent.innerHTML = "";
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();

            prtContent.innerHTML = "";
            //prtContent.innerHTML = strOldOne;
        }
    </script>


    <%-- <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
       <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > International Sale Register</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PNR</label>
                                    <asp:TextBox ID="txt_PNR" runat="server" CssClass="form-control"></asp:TextBox>

                                </div>
                            </div>
                            

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">OrderId</label>
                                   <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"> Pax Name</label>
                                    <asp:TextBox ID="txt_PaxName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                           
           
                
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"> Ticket No</label>
                                    <asp:TextBox ID="txt_TktNo" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                           
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"> Airline </label>        
                                <asp:TextBox ID="txt_Airline" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>                           
                            <div class="col-md-3" id="spn_Projects" runat="server">
                                <div class="form-group" id="spn_Projects1" runat="server">
                                    <label for="exampleInputPassword1">Project Id </label>                                    
                                <asp:DropDownList ID="DropDownListProject" runat="server" CssClass="form-control">
                                </asp:DropDownList>                           
                                </div>
                            </div>                            
                             <div class="col-md-3" id="td_Agency" runat="server">
                                <div class="form-group" id="Div2" runat="server">
                                    <label for="exampleInputPassword1">Agency Name </label>                                    
                                <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />                           
                                </div>
                            </div>

                             <div class="col-md-3" id="divPaymentMode" runat="server">
                                    <label for="exampleInputEmail1">PaymentMode :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">                                 
                                   <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                   <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                  <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>                                                                
                                    </asp:DropDownList>
                                </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                     <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" />
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                   <asp:Button
                                    ID="btn_export" runat="server" Text="Export" CssClass="button buttonBlue" />
                                </div>
                            </div>                           
                        </div>

                       
               
                        <div class="row">
                               <div class="col-md-9">
                                   <span style="color: #FF0000">
                            * N.B: To get Today's booking without above parameter,do not fill any field, only
                                click on search your booking.</span>
                                   </div>
                                 </div>
                 
                  
   
   
                        <%--<tr>
                            <td colspan="4">
                                <table id="PrintVisible" runat="server" visible="false">
                                    <tr>
                                        <td>
                                            Invoice Pages
                                            <asp:TextBox ID="TextBoxPrintNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="button" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>--%>
            <div class="row" style="background-color:#fff; overflow-y:scroll; " runat="server">
    <div align="center">
        
        <table border="0" cellpadding="0" cellspacing="0" style="background-color:white;">
            <tr>
                <td align="left" style="padding-bottom: 5px">
                    <table id="PrintVisible" runat="server" visible="false">
                        <tr>
                            <td><b>Print Invoice Pages :</b>&nbsp;&nbsp;</td>
                            <td>
                                <asp:TextBox ID="TextBoxPrintNo" runat="server" Width="90px" Height="43px" CssClass="form-control"></asp:TextBox>&nbsp;&nbsp;</td>
                            <td style="padding-left:20px;">
                                &nbsp;&nbsp;&nbsp; <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="button buttonBlue" />
                                &nbsp;&nbsp;&nbsp;(Ex: 1-3 or 3-10)&nbsp;&nbsp; </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                            <%--<asp:GridView ID="grd_IntsaleRegis" runat="server" AllowPaging="true" AutoGenerateColumns="false" CssClass="table" GridLines="None"
                                 PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="ORDERID">
                                        <ItemTemplate>
                                            <a href='IntInvoiceDetails.aspx?OrderId=<%#Eval("OrderId") %>&amp;invno=<%#Eval("OrderId") %>&amp;tktno=<%#Eval("TicketNumber") %>&amp;AgentID=<%#Eval("UserId") %>'
                                                style="color: #004b91; font-size: 13px; font-weight: bold" target="_blank">
                                                <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                                &nbsp;(Invoice)</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField HeaderText="AgencyId">
                                        <ItemTemplate>

                                            <a href='CreditDebitNode.aspx?OrderId=<%#Eval("OrderID")%>&AgentId=<%#Eval("UserId")%>&TicketNo=<%#Eval("Ticketnumber")%>&TicketingCarier=<%#Eval("VC")%>&PNR=<%#Eval("GdsPnr")%>'
                                                                        rel="lyteframe" rev="width: 920px; height: 500px; overflow:hidden;"
                                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label>
                                                                    </a>                                          
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AGENCY&nbsp;NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sector0" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GDSPNR">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_gdspnr" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AIRLINE&nbsp;PNR">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_airpnrr" runat="server" Text='<%#Eval("AirlinePnr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SECTOR">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sector1" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VC">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sector2" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAX&nbsp;NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tittle" runat="server" Text='<%#Eval("title") %>'></asp:Label>
                                            &nbsp;<asp:Label ID="lbl_fname" runat="server" Text='<%#Eval("fname") %>'></asp:Label>
                                            &nbsp;<asp:Label ID="lbl_mname" runat="server" Text='<%#Eval("mname") %>'></asp:Label>&nbsp;<asp:Label
                                                ID="lbl_lname" runat="server" Text='<%#Eval("lname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAX&nbsp;TYPE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_paxtype" runat="server" Text='<%#Eval("Paxtype") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AIRLINE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAirLine" runat="server" Text='<%#Eval("AirLine") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TICKET&nbsp;NUMBER">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tkt" runat="server" Text='<%#Eval("Ticketnumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BASEFARE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_basefare" runat="server" Text='<%#Eval("basefare") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="YQ">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_yq" runat="server" Text='<%#Eval("YQ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TAX">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tottax" runat="server" Text='<%#Eval("TotalTax") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SERVICE&nbsp;TAX">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sertax" runat="server" Text='<%#Eval("servicetax") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TRANFEE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tranfee" runat="server" Text='<%#Eval("TranFee") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MGTFEE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tranfee" runat="server" Text='<%#Eval("MgtFee") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOTAL&nbsp;DISCOUNT">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_totdis" runat="server" Text='<%#Eval("TotalDiscount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TDS">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tds" runat="server" Text='<%#Eval("Tds") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOTAL&nbsp;FARE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_totfare" runat="server" Text='<%#Eval("totalfare") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NETFARE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_totbookcost" runat="server" Text='<%#Eval("totalafterdis") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CREATE&nbsp;DATE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAYMENT MODE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                       <asp:TemplateField HeaderText="Convenience Fee">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="CardNumber">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CardNumber" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>--%>
                            <asp:GridView ID="grd_IntsaleRegis" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                       CssClass="table" GridLines="None" PageSize="30" OnPageIndexChanging="grd_IntsaleRegis_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="TransactionId">
                                                <ItemTemplate>
                                                    <a href='IntInvoiceDetails.aspx?OrderId=<%#Eval("OrderId") %>&amp;invno=<%#Eval("OrderId") %>&amp;tktno=<%#Eval("TicketNumber") %>&amp;AgentID=<%#Eval("UserId") %>'
                                                        style="color: #004b91; font-size: 11px; font-weight: bold" target="_blank">
                                                        <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                                        &nbsp;(Invoice)</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                           
                                                            <asp:TemplateField HeaderText="AgencyId">
                                                                <ItemTemplate>
                                                                    <a href='CreditDebitNode.aspx?OrderId=<%#Eval("OrderID")%>&AgentId=<%#Eval("UserId")%>&TicketNo=<%#Eval("Ticketnumber")%>&TicketingCarier=<%#Eval("AirlineCode")%>&PNR=<%#Eval("GdsPnr")%>'
                                                                        rel="lyteframe" rev="width: 920px; height: 500px; overflow:hidden;"
                                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                        <asp:Label ID="lbl_AgentId" runat="server" Text='<%#Eval("AgencyID") %>'></asp:Label>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AgentCompany">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sector0" runat="server" Text='<%#Eval("AgentCompany")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="GDSPNR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_gdspnr" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AIRLINE&nbsp;PNR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_airpnrr" runat="server" Text='<%#Eval("AirlinePnr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SECTOR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sector1" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AirlineCode">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sector2" runat="server" Text='<%#Eval("AirlineCode")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PassengerName">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tittle" runat="server" Text='<%#Eval("PassengerName")%>'></asp:Label>
                                                                    <%--<asp:Label ID="lbl_tittle" runat="server" Text='<%#Eval("title") %>'></asp:Label>
                                                                    &nbsp;<asp:Label ID="lbl_fname" runat="server" Text='<%#Eval("fname") %>'></asp:Label>&nbsp;<asp:Label
                                                                        ID="lbl_mname" runat="server" Text='<%#Eval("mname") %>'></asp:Label>&nbsp;<asp:Label
                                                                            ID="lbl_lname" runat="server" Text='<%#Eval("lname") %>'></asp:Label>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PAX&nbsp;TYPE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_paxtype" runat="server" Text='<%#Eval("Paxtype") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="FlightNumber">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAirLine" runat="server" Text='<%#Eval("FlightNumber")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TICKET&nbsp;NUMBER">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tkt" runat="server" Text='<%#Eval("Ticketnumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="BASE FARE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_basefare" runat="server" Text='<%#Eval("basefare") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                             
                                                            <asp:TemplateField HeaderText="YQ">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_yq" runat="server" Text='<%#Eval("YQ") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TAX">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tottax" runat="server" Text='<%#Eval("Tax")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SERVICE&nbsp;TAX">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sertax" runat="server" Text='<%#Eval("servicetax") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TRANFEE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tranfee" runat="server" Text='<%#Eval("TransactionFee") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="MGTFEE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tranfee" runat="server" Text='<%#Eval("MgtFee") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total Commsion">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totdis" runat="server" Text='<%#Eval("Commsion")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TDS">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tds" runat="server" Text='<%#Eval("Tds") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="GrossFare">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totfare" runat="server" Text='<%#Eval("GrossFare")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="InvoiceTotal">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totbookcost" runat="server" Text='<%#Eval("InvoiceTotal")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="BookingDate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" Text='<%#Eval("BookingDate")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PAYMENT MODE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Convenience Fee">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>  
                                                             <asp:TemplateField HeaderText="GST No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_GstNo" runat="server" Text='<%#Eval("GstNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                             <asp:TemplateField HeaderText="Gst Company Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_GST_Company_Name" runat="server" Text='<%#Eval("GST_Company_Name")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>  															
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>
                </div>
        <div id="DivPrint" runat="server" visible="true"></div>
    </div>
                    </div>
                </div>
            </div>
        </div>
        
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>
