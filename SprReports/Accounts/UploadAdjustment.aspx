﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UploadAdjustment.aspx.vb" ValidateRequest="false"
    Inherits="SprReports_Accounts_UploadAdjustment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Adjestment</title>
    <style>
        input[type="text"], input[type="password"], select, radio, legend, fieldset, textarea
        {
            border: 1px solid #161946;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function validateDtl() {
            var amt = document.getElementById("txt_adj_amount").value;
            var banklist = document.getElementById("ddl_banklist").value;
            var rmk = document.getElementById("txt_remark").value;
            var Upload = document.getElementById("ddl_utype").value;

            if (amt == "") {
                alert("Please Enter Amount."); return false;
            }
            if (banklist == "--Select--") {
                alert("Please Select Bank."); return false;

            }
            if (banklist != "OTHER") {
                if (document.getElementById("txt_narration").value == "") {
                    alert("Please Enter Narration"); return false;
                }

            }
            if (rmk == "") {
                alert("Please Enter Remark."); return false;
            }
            if (Upload == "--Select--") {
                alert("Please Select Upload Type."); return false;

            }
            if (confirm("Are you sure??"))
                return true;
            return false;

        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UP" runat="server">
            <ContentTemplate>
                <div id="div_adj" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_Upload" runat="server">
                        <tr>
                            <td style="padding-top: 10px" align="center">
                                <fieldset style="border: thin solid #161946; padding-left: 10px; width: 60%;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="padding-right: 17px; font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                color: #161946;">
                                                Adjustment Infromation
                                                <asp:Label ID="lbl_Invoice" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lbl_Counter" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellpadding="10" cellspacing="10">
                                                    <tr>
                                                        <td>
                                                            Agent Id
                                                        </td>
                                                        <td id="td_agentid" runat="server">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Agency Name
                                                        </td>
                                                        <td id="td_AgencyName" runat="server">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Adjustment Amount
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt_adj_amount" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Bank List
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddl_banklist" runat="server" Width="100px">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                
                                                                <asp:ListItem Selected="True" Value="OTHER">OTHER</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Narration
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt_narration" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Remark
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt_remark" runat="server" TextMode="MultiLine" Width="300px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Upload Type
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddl_utype" runat="server" Width="100px">
                                                                <asp:ListItem Selected="True" Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btn_adjamt" runat="server" Text="Adjustment" OnClientClick="return validateDtl();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td id="td_msg" runat="server" visible="false" align="center" height="300px" class="SubHeading"
                                valign="middle">
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_adjdtl" runat="server">
                    <asp:GridView ID="GridViewshow" runat="server" AutoGenerateColumns="false" CssClass="mGrid">
                        <Columns>
                            <asp:TemplateField HeaderText="INVOICENO">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_INVOICENO" runat="server" Text='<%#Eval("INVOICENO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AGENTID">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AGENTID" runat="server" Text='<%#Eval("AGENTID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AGENCYNAME">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AGENCYNAME" runat="server" Text='<%#Eval("AGENCYNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BANKNAME">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_BANKNAME" runat="server" Text='<%#Eval("BANKNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BANKCODE">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_BANKCODE" runat="server" Text='<%#Eval("BANKCODE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AMOUNT">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AMOUNT" runat="server" Text='<%#Eval("AMOUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="REMARK">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_REMARK" runat="server" Text='<%#Eval("REMARK") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ADJUSTED BY">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ADJUSTED" runat="server" Text='<%#Eval("ACCOUNTID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UPDATEDDATE">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_UPDATEDDATE" runat="server" Text='<%#Eval("CREATEDDATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
            <ProgressTemplate>
                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                    padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                    z-index: 1000;">
                </div>
                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                    z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                    font-weight: bold; color: #000000">
                    Please Wait....<br />
                    <br />
                    <img alt="loading" src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    </form>
</body>
</html>
