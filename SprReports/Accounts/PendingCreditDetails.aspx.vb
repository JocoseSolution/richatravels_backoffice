﻿Imports System.Data

Partial Class SprReports_Accounts_PendingCreditDetails
    Inherits System.Web.UI.Page
    Private ST As New SqlTransaction()
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        Dim FromDate As String
        Dim ToDate As String
        If [String].IsNullOrEmpty(Request("From")) Then
            FromDate = ""
        Else

            FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
            FromDate = FromDate + " " + "12:00:00 AM"
        End If
        If [String].IsNullOrEmpty(Request("To")) Then
            ToDate = ""
        Else
            ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
            ToDate = ToDate & " " & "11:59:59 PM"
        End If


        Dim ds As New DataSet
        ds = ST.GetPendingCreditDetails(ddl_PaymentType.SelectedValue, FromDate, ToDate)
        Dim dt As New DataTable
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        dt = ds.Tables(0)

        Dim table As String = ""
        table = "<table    border='0' cellspacing='0'  cellpadding='0'>"
        table += "<tr>"
        table += "<td >"
        If (ddl_PaymentType.SelectedValue = "ALL") Then
            dt1 = ds.Tables(1)
            dt2 = ds.Tables(2)
            table = "<table    border='0' cellspacing='0'  cellpadding='0'>"
            table += "<tr>"
            table += "<td colspan='5' align='center' style='padding:15px'>"
            table += "<table    border='0' cellspacing='0'  cellpadding='0'>"
            table += "<tr>"
            table += "<td width='30px' height='15px' style='background-color: #FFFFCA'></td>"
            table += "<td width='160px' align='left' style='padding-left:10px'>3 Days Credit</td>"

            table += "<td width='30px' height='15px' style='background-color: #D7EBFF'></td>"
            table += "<td width='160px' align='left' style='padding-left:10px'>IATA Credit</td>"

            table += "<td width='30px' height='15px' style='background-color: #E1E1FF'></td>"
            table += "<td width='160px' align='left' style='padding-left:10px'>Weekly Credit</td>"
            table += "<td></td>"
            table += "</tr>"
            table += "</table>"
            table += "</td>"


            table += "</tr>"
            table += "<tr>"
            table += "<td width='80px' height='15px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Credit</td>"
            table += "<td width='80px'  style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agent ID</td>"
            table += "<td width='180px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agency Name</td>"
            table += "<td width='130px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agent Name</td>"
            table += "<td  width='100px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Mobile</td>"
            table += "<td width='160px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Email</td>"
            table += "<td width='120px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agency Type</td>"
            table += "<td width='160px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Sales Executive ID</td>"
            table += "<td width='120px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Credit Date</td>"

            table += "</tr>"
            For Each drdays As DataRow In dt.Rows
                table += "<tr style='background-color: #FFFFCA'>"
                table += "<td height='15px' style='padding: 10px;font-family: arial; font-size: 11px;'>" & drdays("Credit").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drdays("AgentID").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drdays("AgencyName").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drdays("AgentName").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drdays("Mobile").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & drdays("Email").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & drdays("AgencyType").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & drdays("SalesExecID").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & drdays("CreditDate").ToString & "</td>"
                table += "</tr>"
            Next
            For Each drIATA As DataRow In dt1.Rows
                table += "<tr style='background-color: #D7EBFF'>"
                table += "<td height='15px' style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("Credit").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("AgentID").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("AgencyName").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("AgentName").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("Mobile").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("Email").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("AgencyType").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & drIATA("SalesExecID").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drIATA("CreditDate").ToString & "</td>"
                table += "</tr>"
            Next
            For Each drWeekly As DataRow In dt2.Rows
                table += "<tr style='background-color: #E1E1FF'>"
                table += "<td height='15px' style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("Credit").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("AgentID").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("AgencyName").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("AgentName").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("Mobile").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("Email").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("AgencyType").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & drWeekly("SalesExecID").ToString & "</td>"
                table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & drWeekly("CreditDate").ToString & "</td>"
                table += "</tr>"
            Next

            table += "</table>"
        Else
            table = "<table    border='0' cellspacing='0'  cellpadding='0'>"


            If (dt.Rows.Count > 0) Then
                table += "<tr>"
                table += "<td width='80px' height='15px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Credit</td>"
                table += "<td width='80px'  style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agent ID</td>"
                table += "<td width='180px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agency Name</td>"
                table += "<td width='130px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agent Name</td>"
                table += "<td  width='100px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Mobile</td>"
                table += "<td width='160px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Email</td>"
                table += "<td width='120px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Agency Type</td>"
                table += "<td width='120px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Sales Executive ID</td>"
                table += "<td width='120px' style='padding:7px;background-color: #161946;color: #FFFFFF;font-family: arial; font-size: 12px; font-weight: bold;'>Credit Date</td>"
                table += "</tr>"
                For Each dr As DataRow In dt.Rows
                    table += "<tr>"
                    table += "<td height='15px' style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("Credit").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("AgentID").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("AgencyName").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("AgentName").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("Mobile").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("Email").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("AgencyType").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 10px';>" & dr("SalesExecID").ToString & "</td>"
                    table += "<td style='padding: 10px;font-family: arial; font-size: 11px;'>" & dr("CreditDate").ToString & "</td>"
                    table += "</tr>"
                Next
            End If

            table += "</table>"
        End If
        table += "</td>"
        table += "</tr>"
        table += "</table>"
        lbl_result.Text = table
    End Sub

    Protected Sub btn_word_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_word.Click
        Try
            Dim filename As String = ""
            filename = "CreditDetails.doc"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & filename & "")
            Response.Charset = ""
            Response.ContentType = "application/doc"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            lbl_result.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.[End]()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_excel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_excel.Click
        Try
            Dim filename As String = ""
            filename = "CreditDetails.xls"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & filename & "")

            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            lbl_result.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.[End]()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

End Class
