﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>






<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script type="text/javascript">
        $(document).ready(function () {
            var setinterval = setInterval("reloadpage()", 120000);
        })

        function reloadpage() {
            location.reload();
        }
    
    </script>
    <div class="row">
       
        <div  >
            <%--<div class="row">
                <div class="col-lg-12">

                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </li>
                    </ol>
                </div>
            </div>--%>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-white">Flight</h2>
                </div>
            </div>
            <div class="row">
                <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblTKTC" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Ticket Count</div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/ADMIN/QCTicketReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
                <%} %>

                 <%if (Session["User_Type"].ToString() != "SALES")
                          { %>

                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-hand-o-right fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblhold" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Hold</div>
                        </div>
                       
                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/HoldPNR/HoldPNRReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>

                        <a href="<%= ResolveUrl("~/SprReports/HoldPNR/DomHoldPNRRequest.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>                        
                    </div>
                </div>
                
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-ban fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblCancelReq" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Cancel Request</div>
                        </div>
                        
                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/Refund/CancellationReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/Refund/TktRptDom_RefundRequest.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>

                    </div>
                </div>
                <div class="col-lg-2 col-md-6" style="display:none;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lbltktreq" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Requested</div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/admin/RequestedRequest.aspx?type=F")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>


                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <%--<asp:Label ID="Label1" runat="server" Text=""></asp:Label>--%>
                                        <asp:Label ID="lblHoldByAgent" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>                           
                            <div>Hold By Agent</div>
                        </div>

                       <a href="<%= ResolveUrl("~/SprReports/holdpnr/HoldPnrReportNew.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblreissueReq" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Reissue Request</div>
                        </div>

                        
                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/Reissue/ReissueReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>

                        <a href="<%= ResolveUrl("~/SprReports/Reissue/TktRptDom_ReIssueRequest.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>

                        
                    </div>
                </div>

                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-hand-o-right fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblprehold" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Pre Hold</div>
                        </div>
                       
                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/HoldPNR/PreHold.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>

                        <a href="<%= ResolveUrl("~/SprReports/HoldPNR/PreHold.aspx.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>                        
                    </div>
                </div>
             <%} %>
            </div>


            <div class="row">
                <div class="col-lg-12">

                    <h2 class="text-white">Hotel</h2>
                </div>
            </div>
            <div class="row">
                <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                <div class="col-lg-2 col-md-6">                     
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblhtlcount" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Ticket Count</div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/Hotel/HtlBookingRpt.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
                  <%} %>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-hand-o-right fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblhtlhold" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Hold</div>
                        </div>

                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/Hotel/HoldHotelBooking.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>

                        <a href="<%= ResolveUrl("~/SprReports/Hotel/HoldHotelBooking.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>

                        
                    </div>
                </div>

                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-ban fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblhtlcancel" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Cancel Request</div>
                        </div>
                         
                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/Refund/HotelRefundReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>
                        <a href="<%= ResolveUrl("~/SprReports/Refund/HotelRefundReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>

                      
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblhtlrequest" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Requested</div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/admin/RequestedRequest.aspx?type=H")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-white">Bus</h2>
                </div>
            </div>
            <div class="row">
                <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblbscount" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Ticket Count</div>
                        </div>

                        <a href="<%= ResolveUrl("~/BS/BusReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>

                 <%} %>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-hand-o-right fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblbshold" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Hold</div>
                        </div>

                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/BS/BUSHoldPnrReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>

                        <a href="<%= ResolveUrl("~/BS/BUSHoldPnrReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-ban fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblbscancel" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Cancel Request</div>
                        </div>
                        <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                        <a href="<%= ResolveUrl("~/BS/BUSRefundReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <%} %>
                        <% else
                          { %>
                        <a href="<%= ResolveUrl("~/BS/BUSRefundReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <% }%>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblbsrequst" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>Requested</div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/admin/RequestedRequest.aspx?type=B")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>



                <div class="row">
                <div class="col-lg-12">
                      <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                    <h2 class="text-white">Rail</h2>
                       <%} %>
                </div>
            </div>
            <div class="row">
                <%if (Session["User_Type"].ToString() == "ADMIN")
                          { %>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <asp:Label Style="font-size:10.5px" ID="RailID" runat="server" Text=""></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div><asp:Label Style="font-size:10.5px" ID="RailCount" runat="server" Text=""></asp:Label></div>
                        </div>

                       
                        <a href="<%= ResolveUrl("~/SprReports/Accounts/IrctcLedger.aspx")%>"> 
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                            </a>
                     

                    </div>
                </div>
                <%} %>
                </div>





            <div class="row" style="visibility: hidden">
                <div class="col-md-3">

                    <asp:DataList ID="DataList1" runat="server" BackColor="Gray" BorderColor="#666666" BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2"
                        Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1" RepeatDirection="Horizontal"
                        Width="224px">
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle Font-Bold="True" Font-Size="Large" ForeColor="White"
                            HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#337ab7" />
                        <%--BackColor="#333333"--%>
                        <HeaderTemplate>
                            Ticket(s)
                        </HeaderTemplate>
                        <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
                        <ItemTemplate>
                            <b>Agency Name :</b>
                            <asp:Label ID="OrderId" runat="server" Text='<%# Eval("AgencyName") %>'></asp:Label>
                            <br />
                            <b>Sector :</b>
                            <asp:Label ID="Pnr" runat="server" Text='<%# Eval("sector") %>'></asp:Label>
                            <br />
                            <b>Amount:</b>
                            <asp:Label ID="CreateDate" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                            <br />
                            <b>BookingDate:</b>
                            <asp:Label ID="BookingDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                </div>






                <div class="col-md-3">

                    <asp:DataList ID="DataList2" runat="server" BackColor="Gray" BorderColor="#666666" BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2"
                        Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1" RepeatDirection="Horizontal"
                        Width="229px">
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle BackColor="#5cb85c" Font-Bold="True" Font-Size="Large" ForeColor="White"
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderTemplate>
                            Hold
                        </HeaderTemplate>
                        <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
                        <ItemTemplate>
                            <b>Agency Name :</b>
                            <asp:Label ID="OrderId" runat="server" Text='<%# Eval("AgencyName") %>'></asp:Label>
                            <br />
                            <b>Sector :</b>
                            <asp:Label ID="Pnr" runat="server" Text='<%# Eval("sector") %>'></asp:Label>
                            <br />
                            <b>Amount:</b>
                            <asp:Label ID="CreateDate" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                            <br />
                            <b>BookingDate:</b>
                            <asp:Label ID="BookingDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                </div>


                <div class="col-md-3">

                    <asp:DataList ID="DataList3" runat="server" BackColor="Gray" BorderColor="#666666" BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2"
                        Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1" RepeatDirection="Horizontal"
                        Width="224px">
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle BackColor="#f0ad4e" Font-Bold="True" Font-Size="Large" ForeColor="White"
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderTemplate>
                            Reissue Request
                        </HeaderTemplate>
                        <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
                        <ItemTemplate>
                            <b>Agency Name :</b>
                            <asp:Label ID="OrderId" runat="server" Text='<%# Eval("AgencyName") %>'></asp:Label>
                            <br />
                            <b>Sector :</b>
                            <asp:Label ID="Pnr" runat="server" Text='<%# Eval("sector") %>'></asp:Label>
                            <br />
                            <b>Amount:</b>
                            <asp:Label ID="CreateDate" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                            <br />
                            <b>BookingDate:</b>
                            <asp:Label ID="BookingDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                </div>


                <div class="col-md-3">

                    <asp:DataList ID="DataList4" runat="server" BackColor="Gray" BorderColor="#666666" BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2"
                        Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1" RepeatDirection="Horizontal"
                        Width="224px">
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle BackColor="#d9534f" Font-Bold="True" Font-Size="Large" ForeColor="White"
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderTemplate>
                            Cancel Request
                        </HeaderTemplate>
                        <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
                        <ItemTemplate>
                            <b>Agency Name:</b>
                            <asp:Label ID="OrderId" runat="server" Text='<%# Eval("AgencyName") %>'></asp:Label>
                            <br />
                            <b>Sector :</b>
                            <asp:Label ID="Pnr" runat="server" Text='<%# Eval("sector") %>'></asp:Label>
                            <br />
                            <b>Amount:</b>
                            <asp:Label ID="CreateDate" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                            <br />
                            <b>BookingDate:</b>
                            <asp:Label ID="BookingDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                </div>

            </div>
        </div>
    </div>
</asp:Content>

