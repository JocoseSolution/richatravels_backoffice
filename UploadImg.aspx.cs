﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class UploadImg : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindGrid();
        }

    }


    //protected void Upload(object sender, EventArgs e)
    //{
    //    //Extract Image File Name.
    //    string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);

    //    //Set the Image File Path.
    //    string filePath = "~/Uploads/" + fileName;  

    //    //Save the Image File in Folder.
    //    FileUpload1.PostedFile.SaveAs(Server.MapPath(filePath));

    //    string constrs = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    //    using (SqlConnection conn = new SqlConnection(constrs))
    //    {
    //        string sql = "INSERT INTO Files VALUES(@Name, @Path)";
    //        using (SqlCommand cmd = new SqlCommand(sql, conn))
    //        {
    //            cmd.Parameters.AddWithValue("@Name", fileName);
    //            cmd.Parameters.AddWithValue("@Path", filePath);
    //            conn.Open();
    //            cmd.ExecuteNonQuery();
    //            conn.Close();
    //        }
    //    }

    //    Response.Redirect(Request.Url.AbsoluteUri);
    //}



    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string ImgPath = string.Empty;
        string DbImgPath = string.Empty;

        try
        {
            if (FileUpload1.HasFile)
            {
                //ImgPath = @"C:\FWSD\Uploads\"+Guid.NewGuid() + FileUpload1.FileName;//(Server.MapPath("~/Uploads/") + Guid.NewGuid() + FileUpload1.FileName);

                string file_pan = string.Empty;
                Random r = new Random();
                int CID = r.Next(10, 50);

                string kkpath = "C:\\WEBSITES\\RichaPrepod\\B2B12052021\\B2B29FEB2020\\FlightPromotion\\" + "HImg_" + CID; //"E:\\Software\\QuickStartFile\\nolonger\\Bitbucket\\richatravels_richaprepod\\FlightPromotion\\" + "HImg_" + CID;
                kkpath = kkpath + ".jpg";
                FileUpload1.SaveAs(kkpath.ToString());

                string filepath_pan = Server.MapPath("/Uploads/" + "HImg_" + CID);
                filepath_pan = filepath_pan + ".jpg";
                FileUpload1.SaveAs(filepath_pan.ToString());
                file_pan = Path.GetFileName("HImg_" + CID + ".jpg");


                //ImgPath = (Server.MapPath("~/Uploads/") + Guid.NewGuid() + FileUpload1.FileName);
                //FileUpload1.SaveAs(ImgPath);

                //DbImgPath = filepath_pan.Substring(filepath_pan.LastIndexOf("\\"));
                //DbImgPath = DbImgPath.Insert(0, "Uploads");

                SqlCommand cmd = new SqlCommand("Insert into Tb_Images (Image_Path) values (@Image_Path)", con);
                cmd.Parameters.AddWithValue("@Image_Path", file_pan);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Connection = con;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                BindGrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Please select the image to upload');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Error occured : " + ex.Message.ToString() + "');", true);
        }
        finally
        {
            ImgPath = string.Empty;
            DbImgPath = string.Empty;
            con.Close();
        }
    }

    protected void BindGrid()
    {
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter();
        try
        {
            SqlCommand cmd = new SqlCommand("select * from Tb_Images", con);
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                grdImages.DataSource = dt;
                grdImages.DataBind();
            }
            else
            {
                grdImages.DataSource = null;
                grdImages.DataBind();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Error occured : " + ex.Message.ToString() + "');", true);
        }
        finally
        {
            dt.Clear();
            dt.Dispose();
            adp.Dispose();
        }
    }

    protected void grdImages_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string physicalPath = string.Empty;
        string imgPath = string.Empty;
        string finalPath = string.Empty;
        try
        {
            //Get the Image_Id from the DataKeyNames
            int imgId = Convert.ToInt32(grdImages.DataKeys[e.RowIndex].Value);
            SqlCommand cmd = new SqlCommand("delete from Tb_Images where Image_Id=@Image_Id", con);
            cmd.Parameters.AddWithValue("@Image_Id", imgId);
            cmd.CommandType = CommandType.Text;
            con.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            //Get the application physical path of the application
            physicalPath = HttpContext.Current.Request.MapPath(Request.ApplicationPath);
            //Get the Image path from the DataKeyNames
            imgPath = grdImages.DataKeys[e.RowIndex].Values["Image_Path"].ToString();
            //Create the complete path of the image
            finalPath = physicalPath + "Uploads\\" + imgPath;

            FileInfo file = new FileInfo(finalPath);
            if (file.Exists)//checking file exsits or not
            {
                file.Delete();//Delete the file
            }

            string kkpath = "C:\\WEBSITES\\RichaPrepod\\B2B12052021\\B2B29FEB2020\\FlightPromotion\\" + imgPath;//"E:\\Software\\QuickStartFile\\nolonger\\Bitbucket\\richatravels_richaprepod\\FlightPromotion\\" + imgPath;
            FileInfo file2 = new FileInfo(kkpath);
            if (file2.Exists)//checking file exsits or not
            {
                file2.Delete();//Delete the file
            }

            BindGrid();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Error occured : " + ex.Message.ToString() + "');", true);
        }
        finally
        {
            con.Close();
            physicalPath = string.Empty;
            imgPath = string.Empty;
            finalPath = string.Empty;
        }
    }

    protected void imgDownload_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton imgBtn = sender as ImageButton;
            GridViewRow gvrow = imgBtn.NamingContainer as GridViewRow;
            //Get the Image path from the DataKeyNames          
            string ImgPath = grdImages.DataKeys[gvrow.RowIndex].Values["Image_Path"].ToString();
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + ImgPath + "\"");
            Response.TransmitFile(Server.MapPath(ImgPath));
            Response.End();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Error occured : " + ex.Message.ToString() + "');", true);
        }
    }

    protected void grdImages_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdImages.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}