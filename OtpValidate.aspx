﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OtpValidate.aspx.vb" Inherits="OtpValidate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>

    </title>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="row">
    <div class="col-md-2">
        <label>Enter OTP:</label>
        <asp:TextBox runat="server" ID="txtotp" onkeypress="return onlyNumbers(event)" MaxLength="6"></asp:TextBox>
    </div>
        <div class="col-md-2">
            <asp:Button ID="btn_sumit" runat="server" Text="Verify" OnClick="btn_sumit_Click" CssClass="btn btn-success" />
        </div>
            <div class="col-md-2">
        <asp:Label ID="lvlval" runat="server"></asp:Label>
    </div>
    </div>
    </form>

    <script type="text/javascript">
        function onlyNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
        }
        </script>
</body>
</html>
