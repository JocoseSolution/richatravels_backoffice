﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FltResIntl.aspx.vb" Inherits="FltResIntl"
    MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/FltSearch.ascx" TagPrefix="uc1" TagName="FltSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <link href="../../Styles/flightsearch.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ResolveUrl("~/Styles/flightsearch.css")%>" rel="stylesheet" type="text/css" />
   <%-- <link href="../Styles/main.css" rel="stylesheet" />
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />--%>

    <style type="text/css">
         input[type=checkbox] {
   margin-top: 7px;
}</style>
    <div style="margin-top:50px;"></div>
    <div class="w100   ">
        <div id="MainSFR">
            <div id="MainSF">
               <%-- <div id="lftdv" class="hide" onclick="fltrclick1(this.id)">
                    <div class="collapse"><< Collapse</div>
                    <div class="collapse hide">>> Expand</div>
                </div>--%>
               
                <div class="w100">
                      <div id="DivLoadP" class="fltload">
                  
                    <img alt="loading" src="../../Images/fltloding.gif" />
                  </div>
                    <div class="fltbox1 auto" id="lftdv1">
                        <div id="fltrDiv">
                            <div class="clear1"></div>
                            <div class="row">
                          <div id="dsplm" style="width:150px;" >
                                    <input type="button" id="ModifySearch" value="Modify Search" onclick="DiplayMsearch1('DivMsearch');" class="pnday daybtn f10" />
                                <%--<img src="../images/modify.png" />&nbsp; <span class="t5">Modify Search</span>--%>
                            </div>
                            <div class="fade" id="DivMsearch">
                                <div style="width: 90%; padding: 1%; background: #f9f9f9;">
                                    <div style="float: right; cursor: pointer; padding: 5px 10px; box-shadow: 2px 1px 3px #000; background: #20313f; color: #fff; border: 2px solid #ccc; border-radius: 25px; float: right; position: relative; top: -20px; right: -20px;" onclick="DiplayMsearch1('DivMsearch');">x</div>
                                    <uc1:FltSearch runat="server" ID="FltSearch" />
                                </div>
                            </div>
                             <div class="large-2 medium-3 small-6 columns passenger"><button type="button" class="jplist-reset-btn cursorpointer f16" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="border: none; background: none;">
                                        Reset All Filters
                                        <img alt="" src="../../images/reset.png" style="position: relative; top: 3px;" />
                                    </button></div> </div>
                            <div class="clear"></div>
                            <hr />
                            
                            <%--<div class="bld f16">
                                <img src="../images/filter.png" />
                                Filter Search
                            </div>--%>
                            
                           
                            <div id="FilterLoad">
                            </div>
                            <div id="FilterBox">
                                <div class="jplist-panel">
                                    
                                    <div class="clear1">
                                    </div>
                                    <div id="flterTab" style="display:none">
                                        <div id="flterTabO" class="spn1">
                                            Outbound
                                        </div>
                                        <div class="lft w2">&nbsp;</div>
                                        <div id="flterTabR" class="spn">
                                            Inbound
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                     <div class="row passenger">
                                    <div class="large-2 medium-2 small-2 columns lft hide"  id="FltrPrice">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBP">Filter By Price</div>
                                        <div id="FBP1" class="w98 lft">
                                            <div class="clear2"></div>
                                            <div class="fo">
                                                <div class="clsone">
                                                    <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                        data-control-action="filter" data-path=".price">
                                                        <div class="clear1"></div>
                                                        <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                        </div>
                                                        <div class="clear1"></div>
                                                        <div class="lft w45">
                                                            <span class="lft">
                                                                <img src="../../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                            <span class="value lft" data-type="prev-value"></span>
                                                        </div>
                                                        <div class="rgt w45">
                                                            <span class="value rgt" data-type="next-value"></span>
                                                            <span class="rgt">
                                                                <img src="../../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                        data-path=".price" data-order="asc" data-type="number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fr">
                                                <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                    data-control-action="filter" data-path=".price">
                                                    <div class="clear1"></div>
                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                    </div>
                                                    <div class="clear1"></div>
                                                    <div class="lft w45">
                                                        <span class="lft">
                                                            <img src="../../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        <span class="value lft" data-type="prev-value"></span>
                                                    </div>
                                                    <div class="rgt w45">
                                                        <span class="value rgt" data-type="next-value"></span>
                                                        <span class="rgt">
                                                            <img src="../../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                    </div>
                                                </div>
                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                    data-path=".price" data-order="desc" data-type="number">
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    
                                    <div class="large-3 medium-3 small-3 lft" id="fltrTime">
                                        <div class="clear"></div>
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBDT">Filter By Departure Time</div>
                                        <div id="FBDT1" class="w98 lft">
                                            <div class="fo">
                                                <div class="jplist-range-slider" data-control-type="range-slider-Time" data-control-name="range-slider-Time"
                                                    data-control-action="filter" data-path=".deptime">
                                                    <div class="clear1"></div>
                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                    </div>
                                                    <div class="clear1"></div>
                                                    <div class="value lft" data-type="prev-value">
                                                    </div>
                                                    <div class="value rgt" data-type="next-value">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fr">
                                                <div class="jplist-range-slider" data-control-type="range-slider-TimeR" data-control-name="range-slider-TimeR"
                                                    data-control-action="filter" data-path=".deptime">
                                                    <div class="clear1"></div>
                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                    </div>
                                                    <div class="clear1"></div>
                                                    <div class="value lft" data-type="prev-value">
                                                    </div>
                                                    <div class="value rgt" data-type="next-value">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 small-2 lft">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBA">Filter By Airline</div>
                                        <div class="w98 lft" id="FBA1">
                                            <div class="clear2"></div>
                                            <div id="airlineFilter" class="fo" style="overflow: scroll; max-height: 70px; overflow-x: hidden; white-space: nowrap;"></div>
                                            <div id="airlineFilterR" class="fr" style="overflow: scroll; max-height: 70px; overflow-x: hidden; white-space: nowrap;"></div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div  class="large-2 medium-2 small-2 lft">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBS">Filter By Stops</div>
                                        <div class="w98 lft" id="FBS1">
                                            <div id="stopFlter" class="fo" style="overflow: scroll; max-height: 70px; overflow-x: hidden; white-space: nowrap;"></div>
                                            <div id="stopFlterR" class="fr" style="overflow: scroll; max-height: 70px; overflow-x: hidden; white-space: nowrap;"></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="large-3 medium-3 small-3 lft" style="display:none">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBFT">Filter By Fare Type</div>
                                        <div class="clsone" id="FBFT1">
                                            <div class="w98 lft">
                                                <div class="fo">
                                                    <div class="jplist-group"
                                                        data-control-type="RfndfilterO"
                                                        data-control-action="filter"
                                                        data-control-name="RfndfilteO"
                                                        data-path=".rfnd" data-logic="or">
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="Refundable" id="CheckboxR1" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top:3px">
                                                            <label for="CheckboxR1">Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="Non Refundable" id="CheckboxR2" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top:3px">
                                                            <label for="CheckboxR2">Non Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                                <div class="fr" >
                                                    <div class="jplist-group"
                                                        data-control-type="RfndfilterR"
                                                        data-control-action="filter"
                                                        data-control-name="RfndfilterR"
                                                        data-path=".rfnd" data-logic="or">
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="Refundable" id="Checkbox1" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top:3px">
                                                            <label for="CheckboxR1">Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="Non Refundable" id="Checkbox2" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top:3px">
                                                            <label for="CheckboxR2">Non Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fltbox2">
                        <div id="searchtext" class="clear1 passenger">
                            <div class="lft bld colormn">
                                <div id="divSearchText1">
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="w95 auto passenger">
                            <div class="lft padding1s cursorpointer clspMatrix" id="clspMatrix"></div>
                            <div class="lft w48" id="refinetitle"></div>
                        
                        <div class="w48 rgt passenger" id="prexnt">
                            <div class="rgt w40">
                                <input type="button" id="NextDay" value="Next Day" class="pnday f10" />
                                <img src="../../Styles/images/closeopen.png" />
                            </div>
                            
                            <div class="rgt w40">
                                <img src="../../Styles/images/closeopen2.png" />
                                <input type="button" id="PrevDay" value="Prev Day" class="pnday f10" />
                            </div>
                        </div></div>
                        <%--<div class="jplist-panel w5 rgt">
                            <div class="jplist-views"
                                data-control-type="views"
                                data-control-name="views"
                                data-control-action="views"
                                data-default="list-view">
                                <!-- data-default="list-view" -->
                                <button type="button" class="jplist-view list-view lft" style="background: url(../images/list-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in List View" data-type="list-view"></button>
                                <button type="button" class="jplist-view grid-view rgt" style="background: url(../images/grid-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in Grid View" data-type="grid-view"></button>
                            </div>
                        </div>--%>
                        <div class="clear"></div>
                        <div class="jplist-panel">
                            <div id="divMatrixRtfO" class="divMatrix"></div>
                            <div id="divMatrixRtfR" class="hide divMatrix"></div>
                            <div id="divMatrix"></div>
                            <div class="clear"></div>
                        </div>
                        <div id="RoundTripH" class="flightbox chkb">
                            <div class="nav-container">
                                
                                    <div class="jplist-panel">
                                        <div class="clear"></div>
                                        <div id="selctcntnt1" class="hide fltselct" >
                                            <div class="f16">Your Selection</div>
                                            <div id="selctcntnt" class="mauto">
                                                <div id="fltgo" class="w40 lft brdrtop">
                                                </div>
                                                <div class="w5 lft">&nbsp;</div>
                                                <div id="fltbk" class="w40 lft brdrtop">
                                                </div>
                                                <div id="fltbtn" class="w15 rgt">
                                                    <span class="f16 bld rgt" id="totalPay"></span>
                                                    <div class="clear"></div>
                                                    <input type="button" value="Request" class="button1 hide" id="FinalBook" />
                                                    <div id="Divproc" class="hide bld">
                                                        <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="w100 mauto bld bgmn1 colorwht">
                                            <div class="w50 padding1 lft">
                                                <div id="RTFTextFrom" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfFromNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="w45 padding1 lft">
                                                <div id="RTFTextTo" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfToNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="headerow hide">
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ1"
                                                    data-control-name="sortCITZ1"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirlineR"
                                                    data-control-name="sortAirlineR"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptimeR"
                                                    data-control-name="sortDeptimeR"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtimeR"
                                                    data-control-name="sortArrtimeR"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdurR"
                                                    data-control-name="sortTotdurR"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZR"
                                                    data-control-name="sortCITZR"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                               
                            </div>
                            <div  class="lft w51">
                                <div id="divFrom1" class="listO w100">
                                   <%-- <div class="dvsrc">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                </div>
                            </div>
                            <div class="rgt w50">
                                <div id="divTo1"  class="listR w100">
                                    <%--<div class="dvsrc textaligncenter">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>

                                </div>

                                <%--<div class="dvsrc textaligncenter">
                                    <img src="~/Images/fltloding.gif" />
                                </div>--%>
                            </div>
                        </div>
                        <div class="flightbox chkb" style="display: block;" id="onewayH">
                                  <div class="fltselct" id="INTLdivsection" style="display: none;">
                                    <div id="fltbtnO" class="w95 auto">
                                         <div class="f16 w30 lft bld">Your Selection</div>
                                        <div class="clear w100">
                                            <hr />
                                        </div>
                                        <div class="clear"></div>
                                        <div id="selctcntntO" class="mauto">
                                            <div class="w100">
                                                <div id="fltgoO" class="sec1 fltgo">
                                                </div>
                                            </div>
                                            <div class="w100">
                                                <div id="fltbkO" class="sec1 fltbk">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="clear"></div>
                                        <div class="w30 rgt">
                                            <input type="button" value="Request" id="FinalBookO" class="FinalBook" />
                                        </div>
                                    </div>
                                </div>
                             <div class="nav-container hide">
                                 <div class="nav">
                                    <div class="jplist-panel">
                                        <div class="headerow hide">
                                            <div class="w15 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Departure
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w24 lft">
                                                &nbsp;
                                            </div>
                                            <div class="w15 colorwht rgt srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ"
                                                    data-control-name="sortCITZ"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                               </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="mainDiv">
                                <div id="divResult">
                                    <div id="divFrom" class="list" style="width: 100%;">
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <%-- <div class="jplist-no-results jplist-hidden">
                                <div class='clear1'></div>
                                <div class='clear1'></div>
                                <div class='w90 mauto padding1 brdr'>
                                    <div class='clear1'></div>
                                    <div class='clear1'></div>
                                    <span class='vald f20'>Sorry, we could not find a match for your query. Please modify your search.</span> &nbsp;<span onclick='DiplayMsearch(this.id);' class='underlineitalic cursorpointer'>Modify Search</span><div class='clear'></div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="render">
        </div>
        <div class="clear">
        </div>
    </div>
    <%--<div id="fltselct" class="hide">
        <div class="clear1"></div>
        <div id="totalPay" class="f16">
            Your Selection
            <div class="clear1"></div>
        </div>
        <div id="selctcntnt" class="w70 mauto">
            <div class="clear1">
            </div>
            <div id="fltgo" class="w50 lft">
            </div>
            <div id="fltbk" class="w45 rgt">
            </div>
            <div id="fltbtn">
                <input type="button" value="Book" class="button1 hide" id="FinalBook" />
                <div id="Divproc" class="hide bld">
                    <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                </div>
            </div>
        </div>
    </div>--%></div>
    <div id="waitMessage" style="display: none;">
        <div style="text-align: center; z-index: 1001; background-color: #f9f9f9; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; border:5px solid #d1d1d1; border-radius:10px;">
            We are processing your request. Please wait....<br />
            <br />
            <img alt="loading" src="../../images/loadingAnim.gif" />
            <br />
            <div id="searchquery" style="padding-top: 15px">
            </div>
        </div>
    </div>
    <%-- <div id="divMail">
        <a href="#" class="topopup pop_button1" id="btnFullDetails">Mail All Result</a>
        <a href="#" class="topopup pop_button1" id="btnSendHtml">Mail Selected Result</a>
    </div>--%>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <%--<input type="button" class="pop_button" id="btnCancel" name="btnCancel" value="Cancel" />--%>
                        <input type="button" class="pop_button" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<div id="divabc">&nbsp;</div>--%>
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>
    <div id="ConfmingFlight" class="CfltFare hide">
        <div id="divLoadcf" class="CfltFare1">
        </div>
    </div>
    <div class="clear"></div>
    <a href="#toptop"><span class="toptop" style="position: fixed; bottom: 20px; right: 20px; cursor: pointer; padding: 5px 10px; background: #004b91; color: #fff;">Top</span></a>
    <div class="clear1"></div>
    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/")%>';
       <%-- var username = '<%= Session("User_Type") %>';
        alert(username);
        if(username="AGENT")
        {
            $("#fltselct").hide();
        }--%>
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/GroupBookingFlightResults.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>
    <%--   <script src="../Scripts/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
           
            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });
        });
       

    </script>
</asp:Content>
