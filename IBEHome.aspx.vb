﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class IBEHome
    Inherits System.Web.UI.Page
    Dim Email As String
    Dim Password As String
    Dim FirstName As String
    Dim LastName As String
    Dim AgencyName As String
    Dim Address As String
    Dim City As String
    Dim State As String
    Dim Country As String
    Dim agnttype As String
    Dim Zip As String
    Dim Phone As String
    Public Distr As String
    Public User_Type As String
    Dim ag_type As String
    Public dis_str
    Public Dis_Air
    Dim conn As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
    Dim Dist As String
    Dim credit_limit
    Dim Ex_Id As String
    Dim exe_dept As String
    Dim agent_type As String
    Dim agent_status As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("Login.aspx")
        End If
        Try

            If Not IsPostBack Then
                If Session("User_Type") = "AGENT" Or Session("User_Type") = "DIST" Then
                    Dim dtAg As New DataTable
                    Dim ST As New SqlTransaction
                    dtAg = ST.GetAgencyDetails(Session("UID")).Tables(0)
                    'If dtAg.Rows(0) Then
                    userid.InnerText = Session("UID")
                    Dim IsPWD As Boolean = If(String.IsNullOrEmpty(dtAg.Rows(0)("IsPWD").ToString()), False, Convert.ToBoolean(dtAg.Rows(0)("IsPWD")))
                    If IsPWD = False Then
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tmp", "<script type='text/javascript'>passchange();</script>", False)
                    End If

                End If



            End If
        Catch ex As Exception

        End Try


        Session("PNR") = "False"
        Session("Flag") = "0"
        Try
            User_Type = Session("User_Type").ToString.ToUpper
            If User_Type = "AGENT" Then
                AuthenticationMethod(Session("UID"), Session("User_Type"))
            ElseIf User_Type = "ADMIN" Then

            ElseIf User_Type = "DIST" Then
                AuthenticationMethod(Session("UID"), Session("User_Type"))
            ElseIf User_Type = "EXEC" Or User_Type = "ACC" Then

            End If

            If Session("UID") = "" Then
                Response.Redirect("Login.aspx")
            End If
        Catch ex As Exception

        End Try
        If (Request("Htl") <> "") Then

            If Request("Htl") = "H" Then
                IBE_CP.Visible = False
                HotelSearch.Visible = True
            Else
                HotelSearch.Visible = False
                IBE_CP.Visible = True
            End If
        Else
            HotelSearch.Visible = False
            IBE_CP.Visible = True
        End If


        Try
            Distr = Dist
            If Session("User_Type") = "DIST" Then
                di_dis()
            ElseIf Session("User_Type") = "AGENT" And Distr = UCase("SPRING") Then
                ag_dis(Distr)
            ElseIf Session("User_Type") = "AGENT" And Distr <> "SPRING" Then
                ag_dis(Distr)
            End If
        Catch ex As Exception

        End Try

        
        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "tmp", "<script type='text/javascript'>openDialog();</script>", False)
    End Sub
    'Protected Sub btn_resetpwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_resetpwd.Click
    '    Try


    '        Dim dtAg As New DataTable
    '        Dim ST As New SqlTransaction
    '        dtAg = ST.GetAgencyDetails(Session("UID")).Tables(0)
    '        If (dtAg.Rows(0)("PWD").ToString().ToUpper() = oldpwd.Text.Trim().ToUpper()) Then
    '            If dtAg.Rows(0)("PWD").ToString().ToUpper() = newpwd.Text.Trim().ToUpper() Then
    '                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Old and new password are same. Please try another password.');", True)
    '                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tmp", "<script type='text/javascript'>passchange();</script>", False)
    '            Else
    '                Dim STDom As New SqlTransactionDom()
    '                STDom.UpdateAgentProfile("PasswordChange", dtAg.Rows(0)("User_Id").ToString(), newpwd.Text.Trim(), "", "", "", "", "", "", "")
    '                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Your password has been changed sucessfully.');", True)
    '            End If

    '            'Try
    '            '    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Your password has been changed sucessfully.');", True)
    '            '    'FormsAuthentication.SignOut()
    '            '    'Session.Abandon()
    '            '    'Response.Redirect("~/Login.aspx")

    '            'Catch ex As Exception

    '            'End Try
    '            'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tmp", "<script type='text/javascript'>passchangeclose();</script>", False)
    '        Else
    '            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Old password is incorrect');", True)
    '            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tmp", "<script type='text/javascript'>passchange();</script>", False)
    '        End If
    '    Catch ex As Exception

    '    End Try

    'End Sub

    Public Sub ag_dis(ByVal Distr)
        Dim i As Integer
        'Dim dis_str As String

        Dim str
        dis_str = Nothing
        Dim Connection As New SqlConnection(conn)
        Connection.Open()
        If Session("User_Type") = "AGENT" And Distr <> UCase("SPRING") Then
            str = "select * from DAgent_CD where grade='" & Session("AGTY") & "' and distr='" & Replace(Distr, ",", "") & "'"
        Else
            str = "select * from Agent_CD where grade='" & Session("AGTY") & "'"
        End If

        Dim adp As SqlDataAdapter
        adp = New SqlDataAdapter(str, Connection)
        Dim dt As New DataTable
        adp.Fill(dt)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                dis_str = dis_str + CStr(dt.Rows(i)(2)) & "-" & CStr(dt.Rows(i)(1)) & "-" & CStr(dt.Rows(i)(3)) & "-" & CStr(dt.Rows(i)(4)) & "-" & CStr(dt.Rows(i)(5)) & "AD-"
            Next
            Dis_Air = Split(dis_str, "AD-")
            Session("Discount") = Dis_Air

        End If
    End Sub
    Public Sub di_dis()
        Dim i As Integer
        'Dim dis_str As String

        dis_str = Nothing
        Dim Connection As New SqlConnection(conn)
        Connection.Open()
        Dim str = "select * from Dist_CD where distri='" & Session("UID") & "'"
        Dim adp As SqlDataAdapter
        adp = New SqlDataAdapter(str, Connection)
        Dim dt As New DataTable
        adp.Fill(dt)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                dis_str = dis_str + CStr(dt.Rows(i)(1)) & "-" & CStr(dt.Rows(i)(2)) & "-" & CStr(dt.Rows(i)(3)) & "AD-"
            Next
            Dis_Air = Split(dis_str, "AD-")
            Session("Discount") = Dis_Air

        End If
    End Sub
    Private Sub Authentication_exe(ByVal UserName As String)
        Try
            Dim boolReturnValue As Boolean = False
            Dim Connection As New SqlConnection(conn)
            Connection.Open()
            Dim strSQL As String = "SELECT * FROM Execu"
            UserName = UserName
            Password = Password
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(strSQL, Connection)
            Dim dt As New DataTable
            adp.Fill(dt)
            Dim i As Integer
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If (LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString())) Then
                        Ex_Id = UCase((dt.Rows(i).Item("user_id")).ToString())
                        exe_dept = UCase((dt.Rows(i).Item("dept")).ToString())
                        boolReturnValue = True
                        Exit For
                    End If
                Next
            End If

        Catch
        End Try
    End Sub
    Private Sub AuthenticationMethod_admin(ByVal UserName As String)
        Try
            Dim boolReturnValue As Boolean = False
            Dim Connection As New SqlConnection(conn)
            Connection.Open()
            Dim strSQL As String = "SELECT * FROM ADMIN_b2b"
            UserName = UserName
            Password = Password
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(strSQL, Connection)
            Dim dt As New DataTable
            adp.Fill(dt)
            Dim i As Integer
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If (LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString())) Then
                        Email = UCase((dt.Rows(i).Item("user_id")).ToString())
                        FirstName = UCase((dt.Rows(i).Item("Fname")).ToString())
                        LastName = UCase((dt.Rows(i).Item("lname")).ToString())
                        Address = UCase((dt.Rows(i).Item("address")).ToString())
                        City = UCase((dt.Rows(i).Item("city")).ToString())
                        Zip = UCase((dt.Rows(i).Item("zipcode")).ToString())
                        Country = UCase((dt.Rows(i).Item("country")).ToString())
                        Phone = UCase((dt.Rows(i).Item("Mobileno")).ToString())
                        'Agnttype = UCase((dt.Rows(i).Item("AgentType")).ToString())
                        'AgencyName = UCase((dt.Rows(i).Item("company")).ToString())
                        'Dist = UCase((dt.Rows(i).Item("Distributor")).ToString())
                        boolReturnValue = True
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub AuthenticationMethod(ByVal UserName As String, ByVal User_Type As String)
        Try
            Dim strSQL
            Dim uid = UserName
            Dim pwd = Password
            Dim boolReturnValue As Boolean = False
            Dim Connection As New SqlConnection(conn)
            Connection.Open()
            If User_Type = "DIST" Then
                strSQL = "SELECT * FROM New_Regs where agent_Type='DA'"
            Else
                strSQL = "SELECT * FROM New_Regs where User_Id='" & UserName & "' and (agent_Type <> 'DA')"
            End If
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(strSQL, Connection)
            Dim dt As New DataTable
            adp.Fill(dt)
            Dim i As Integer
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString()) Then
                        Email = UCase((dt.Rows(i).Item("user_id")).ToString())
                        FirstName = UCase((dt.Rows(i).Item("Fname")).ToString())
                        LastName = UCase((dt.Rows(i).Item("lname")).ToString())
                        Address = UCase((dt.Rows(i).Item("address")).ToString())
                        City = UCase((dt.Rows(i).Item("city")).ToString())
                        Zip = UCase((dt.Rows(i).Item("zipcode")).ToString())
                        Country = UCase((dt.Rows(i).Item("country")).ToString())
                        Phone = UCase((dt.Rows(i).Item("Mobile")).ToString())
                        AgencyName = UCase((dt.Rows(i).Item("Agency_Name")).ToString())
                        Dist = UCase((dt.Rows(i).Item("Distr")).ToString())
                        credit_limit = UCase((dt.Rows(i).Item("Crd_Limit")).ToString())
                        agent_type = dt.Rows(i).Item("Agent_Type").ToString()
                        Session("agent_type") = agent_type
                        agent_status = dt.Rows(i).Item("Agent_Status").ToString()
                        Session("AgencyName") = AgencyName
                        Session("email_Agent") = Email
                        Session("Mobile") = Phone
                        Session("Address") = Address
                        Session("CL") = credit_limit
                        boolReturnValue = True
                        Exit For
                    End If
                Next
            End If

        Catch
        End Try
    End Sub
    'Private Function AuthenticationMethod_Dist(ByVal UserName As String, ByVal Password As String) As Boolean
    '    Dim boolReturnValue As Boolean = False
    '    Dim Connection As New SqlConnection(conn)
    '    Connection.Open()
    '    Dim strSQL As String = "SELECT * FROM distributor_agents"
    '    Dim adp As SqlDataAdapter
    '    adp = New SqlDataAdapter(strSQL, Connection)
    '    Dim dt As New DataTable
    '    adp.Fill(dt)
    '    Dim i As Integer
    '    If dt.Rows.Count > 0 Then
    '        For i = 0 To dt.Rows.Count - 1
    '            If (LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString())) And (LCase(Password) = LCase((dt.Rows(i).Item("user_pwd")).ToString())) Then
    '                Email = UCase((dt.Rows(i).Item("user_id")).ToString())
    '                FirstName = UCase((dt.Rows(i).Item("Fname")).ToString())
    '                LastName = UCase((dt.Rows(i).Item("lname")).ToString())
    '                Address = UCase((dt.Rows(i).Item("address")).ToString())
    '                City = UCase((dt.Rows(i).Item("city")).ToString())
    '                Zip = UCase((dt.Rows(i).Item("zipcode")).ToString())
    '                Country = UCase((dt.Rows(i).Item("country")).ToString())
    '                Phone = UCase((dt.Rows(i).Item("Mobileno")).ToString())
    '                Agnttype = UCase((dt.Rows(i).Item("AgentType")).ToString())
    '                AgencyName = UCase((dt.Rows(i).Item("company")).ToString())
    '                Dist = UCase((dt.Rows(i).Item("Distributor")).ToString())
    '                boolReturnValue = True
    '                Exit For
    '            End If
    '        Next
    '    End If
    '    Return boolReturnValue
    'End Function
End Class
