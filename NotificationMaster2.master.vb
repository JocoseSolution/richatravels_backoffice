﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Security
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Xml
Imports IPTracker

Partial Class NotificationMaster2
    Inherits System.Web.UI.MasterPage
    Private id As String
    Private usertype As String
    Private typeid As String
    Private ds As DataSet
    Private dsm As DataSet
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private adap As SqlDataAdapter
    Private det As New Details()
    Private dtm As DataTable
    Private servtype As String
    Public strPubMenu As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'CType((Page.Master.FindControl("lblcopyrt")), Label).Text = ConfigurationManager.AppSettings("footerval")
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("Login.aspx")
        End If
        'int role_id = 1;
        If (Page.IsPostBack = False) Then
            ShowMenu()
            getnotificationstatus()
        End If
        Dim Page_url As String = Request.Url.AbsolutePath
        Dim result As String = ""
        If Page_url = "/IBEHomeCheck.aspx" Then

            result = "valideuser"
        Else
            '' result = checkAuthorization()
        End If

        If result = "Invalideuser" Then
            Response.Redirect("Error.aspx")
        End If
        Try
            If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing Then
                Dim State As New StateCollection()
                Dim objIP As New IPDetails()
                State.SessionID = Session.SessionID
                State.Path = Request.CurrentExecutionFilePath
                State.Username = Session("UID").ToString() 'Page.User.Identity.Name
                State.VISTING_TIME = DateTime.Now.ToString()
                Dim objST As New SessionTrack()
                objST.Add(State, Request.CurrentExecutionFilePath)
            End If

        Catch ex As Exception

        End Try
        Try
            If Not IsPostBack Then
                If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing AndAlso Session("UserType") <> "" AndAlso Session("UserType") IsNot Nothing AndAlso Session("TypeID") <> "" AndAlso Session("TypeID") IsNot Nothing Then
                    id = Session("UID").ToString()
                    usertype = Session("UserType").ToString()
                    servtype = "Flight"
                    'div_Series.Visible = False
                    typeid = Session("TypeID").ToString()
                    If usertype = "AD" Then
                        'lblagency.Text = Session("ADMINLogin")
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "AC" Then
                        'lblagency.Text = "Accounts"
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "EC" Then
                        'lblagency.Text = Session("UID").ToString()
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "SE" Then
                        'lblagency.Text = Session("UID").ToString()
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "TA" Then
                        '' ds = det.AgencyInfo(id)
                        ''If ds.Tables(0).Rows.Count > 0 Then
                        'lblagency.Text = Session("AgencyName")
                        'lblCamt.Visible = True
                        ' ''''lblCamt.Text = " INR " & Convert.ToDouble(ds.Tables(0).Rows(0)("crd_limit").ToString())
                        ' ''td_AgencyID.InnerText = ds.Tables(0).Rows(0)("user_id").ToString()
                        'td_AgencyID.InnerText = Session("UID")

                        'lblagency.Text = Session("AgencyName")
                        'Session("AGTY") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
                        'Session("agent_type") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
                        'Session("MchntKeyITZ") = ds.Tables(0).Rows(0)("MerchantKey_ITZ").ToString().Trim()
                        ' ''''Session("ModeTypeITZ") = ds.Tables(0).Rows(0)("ModeType_ITZ").ToString().Trim()
                        'Session("_DCODE") = ds.Tables(0).Rows(0)("Decode_ITZ").ToString().Trim()
                        'Session("_SvcTypeITZ") = ds.Tables(0).Rows(0)("SvcType_ITZ").ToString().Trim()
                        'If ds.Tables(0).Rows(0)("Distr").ToString() <> "SPRING" Then
                        '    'div_ccpay.Visible = False
                        'End If
                        '' End If
                        'Marquee Message

                        'Try
                        '    dsm = det.GetMarquueemsg(servtype)
                        '    If dsm.Tables(0).Rows.Count > 0 Then
                        '        Dim msg As String = ""
                        '        For Each row As DataRow In dsm.Tables(0).Rows

                        '            msg += row("Message").ToString() & " ."
                        '        Next row

                        '        tdmarquee.InnerText = msg



                        '        'tdmarquee.InnerText = dsm.Tables(0).Rows(0)("Message").ToString()


                        '    End If
                        'Catch ex As Exception

                        'End Try





                        'BEGIN CHANGES FOR DISTR
                    ElseIf usertype = "DI" Then
                        'divflt.Visible = False
                        'divhtl.Visible = False
                        'div_Rail.Visible = False
                        'div_Bus.Visible = False
                        'div_Utility.Visible = False
                        'div_BillPayment.Visible = False
                        'div_Series.Visible = False
                        ds = det.AgencyInfo(id)
                        If ds.Tables(0).Rows.Count > 0 Then
                            'lblagency.Text = ds.Tables(0).Rows(0)("Agency_Name").ToString()
                            'lblCamt.Visible = True
                            'lblCamt.Text = " INR " & Convert.ToDouble(ds.Tables(0).Rows(0)("crd_limit").ToString())
                            'td_AgencyID.InnerText = ds.Tables(0).Rows(0)("user_id").ToString()
                            'lblagency.Text = Session("AgencyName")
                            ''Session("AGTY") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
                        End If
                        'END CHANGES FOR DISTR
                    End If

                    If Session("User_Type") = "ACC" Or Session("User_Type") = "SALES" Then
                        ' div_menu.Visible = False
                        'hypdeal.Visible = False
                        pending_li.Visible = False
                        reissue_li.Visible = False
                        refund_li.Visible = False

                    End If
                    If Session("User_Type") = "EXEC" Then
                        'div_Rail.Visible = False
                        'div_Utility.Visible = False
                        'div_Series.Visible = False
                    End If

                    If Session("User_Type") = "ADMIN" Then
                        ' hypdeal.Visible = False
                    End If
                    If typeid = "TA2" Then
                        'div_Rail.Visible = False
                        'divflt.Visible = False
                        'divhtl.Visible = False
                        'div_Series.Visible = False
                        'div_Utility.Visible = False
                        'hypdeal.Visible = False
                    End If

                    'If Session("User_Type") = "A" Or Session("User_Type") = "EXEC" Then
                    '    div_menu.Visible = False
                    'End If



                ElseIf Session("UID") Is Nothing AndAlso Session("UserType") Is Nothing AndAlso Session("TypeID") Is Nothing Then

                    Response.Redirect("~/Login.aspx?reason=Session TimeOut")
                End If
                ''  ShowMenu()
                'RowMenu.Visible = False

                If (Request.UserAgent.IndexOf("AppleWebKit") > 0) Then
                    Request.Browser.Adapters.Clear()
                End If


            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function checkAuthorization() As String
        Try

            ' Dim role_id As Integer = 3

            Dim role_id As Integer = Session("Role_id")


            Dim Page_url As String = "~" + Request.Url.AbsolutePath



            Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
            Dim con As New SqlConnection(constr)
            If con.State = ConnectionState.Closed Then
                con.Open()
            End If

            Dim cmd As New SqlCommand("CheckPageAuthorization_PP", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@pageurl", Page_url)
            cmd.Parameters.AddWithValue("@Role", role_id)

            Dim ret As String = ""
            ret = cmd.ExecuteScalar().ToString()
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
            '' Return "valideuser"
            Return ret
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Protected Sub lnklogout_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles lnklogout.Click
    '    Try
    '        FormsAuthentication.SignOut()
    '        Session.Abandon()
    '        Response.Redirect("~/Login.aspx")
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try

    'End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetNoStore()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub ShowMenu()
        Try
            'If Session("getMenuItem") = Nothing Then
            Dim dset As New DataSet
            adap = New SqlDataAdapter("getMenu", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure

            adap.SelectCommand.Parameters.AddWithValue("@roleid", Session("Role_Id"))
            adap.Fill(dset)




            Dim strmenu As String = "<div id='sidebar' style=''><ul style='display: block;'>"
            Dim i As Integer = 0
            For i = 0 To dset.Tables(0).Rows.Count - 1
                If dset.Tables(0).Rows(i)("Is_Parent_Page").ToString().Trim() = "Y" Then
                    strmenu = strmenu & "<li style='color:white;' ><a style='color:white;' >" & dset.Tables(0).Rows(i)("Page_name").ToString() & " </a>"
                    strmenu = strmenu & "<ul " & dset.Tables(0).Rows(i)("Page_name").ToString() & " style='overflow:auto;max-height:530px;'>"
                    For k = 0 To dset.Tables(0).Rows.Count - 1
                        If (dset.Tables(0).Rows(i)("page_id").ToString() = dset.Tables(0).Rows(k)("Root_page_id").ToString()) Then

                            strmenu = strmenu & "<li  class='submenu'> <a href=" & ResolveClientUrl("~" & dset.Tables(0).Rows(k)("Page_url")) & "> " & dset.Tables(0).Rows(k)("Page_name").ToString() & "</a></li>"
                        End If

                    Next
                    strmenu = strmenu & "</ul></li>"
                End If
            Next
            strmenu = strmenu & "</ul></div><div Style='Clear:Both'>"
            Session("getMenuItem") = strmenu
            ul_menu.InnerHtml = strmenu
            adap.Dispose()




            'Dim strmenu As String = " <aside class='navigation'><nav><ul class='nav luna-navs'><li class='nav-category'></li>"
            'Dim i As Integer = 0
            'For i = 0 To dset.Tables(0).Rows.Count - 1
            '    If dset.Tables(0).Rows(i)("Is_Parent_Page").ToString().Trim() = "Y" Then
            '        strmenu = strmenu & "<li class='filter1'><a  class='acordianfilter'> " & dset.Tables(0).Rows(i)("Page_name").ToString() & " <span class='sub-nav-icon'> <i class='fa fa-angle-down' aria-hidden='true'></i> </span> </a>"
            '        strmenu = strmenu & "<ul id=''" & dset.Tables(0).Rows(i)("Page_name").ToString() & " class='finner nav nav-second-level ' style='Display: none;'>"
            '        For k = 0 To dset.Tables(0).Rows.Count - 1
            '            If (dset.Tables(0).Rows(i)("page_id").ToString() = dset.Tables(0).Rows(k)("Root_page_id").ToString()) Then

            '                strmenu = strmenu & "<li> <a href=" & ResolveClientUrl("~" & dset.Tables(0).Rows(k)("Page_url")) & "><i class='fa fa-angle-down' aria-hidden='true'></i>&nbsp;&nbsp;&nbsp; " & dset.Tables(0).Rows(k)("Page_name").ToString() & "</a></li>"
            '            End If

            '        Next
            '        strmenu = strmenu & "</ul></li>"
            '    End If
            'Next
            'strmenu = strmenu & "</ul></nav></aside>"
            'Session("getMenuItem") = strmenu
            'Ul_Menu.InnerHtml = strmenu
            'adap.Dispose()
            'Else
            '    Ul_Menu.InnerHtml = Session("getMenuItem")

            'End If


            'Dim xmld As New XmlDataSource
            'xmld.ID = "XmlDataSource1"
            'xmld.EnableCaching = False
            'dset.DataSetName = "Menus"
            'dset.Tables(0).TableName = "abc"
            'Dim relation As New DataRelation("ParentChild", dset.Tables("abc").Columns("Page_ID"), dset.Tables("abc").Columns("PageParent_ID"), True)
            'relation.Nested = True
            'dset.Relations.Add(relation)
            'xmld.Data = dset.GetXml()
            'xmld.TransformFile = Server.MapPath("~/Transform.xslt")
            'xmld.XPath = "MenuItems/MenuItem"
            'Menu1.DataSource = xmld
            'Menu1.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Sub getnotificationstatus()
        Try
            Dim dset As New DataSet
            adap = New SqlDataAdapter("sp_getnotification_status", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.Fill(dset)
            Dim dt1 As DataTable = dset.Tables(0)
            Dim dt2 As DataTable = dset.Tables(1)
            Dim dt3 As DataTable = dset.Tables(2)
            Dim i As Integer = 0
            Dim notification As String = ""
            Dim notification_1 As String = ""
            Dim notification_2 As String = ""





            If (dt1.Rows.Count <> 0) Then

                noti_Counter.InnerText = dt1.Rows.Count
                If (dt1.Rows(0)("Status") = "InProcess" OrElse dt1.Rows(0)("Status") = "ConfirmByAgent" OrElse dt1.Rows(0)("Status") = "Confirm") Then
                    notification = notification & "<div class='row' style=' border-bottom:solid 1px rgba(100, 100, 100, .30);'>"

                    notification = notification & "<div class='col-sm-3'>SECTOR</div>"
                    notification = notification & "<div class='col-sm-3'>DATE</div>"
                    notification = notification & "<div class='col-sm-2'>STATUS</div>"
                    notification = notification & "<div class='col-sm-4'>ORDERID</div>"
                    notification = notification & "</div>"


                    For i = 0 To dt1.Rows.Count - 1
                        Dim formattedDate As String = dt1.Rows(i)("depdate").ToString().Substring(0, 2) + "-" + dt1.Rows(i)("depdate").ToString().Substring(2, 2) + "-" + dt1.Rows(i)("depdate").ToString().Substring(4, 2)
                        notification = notification & "<div class='row'>"
                        notification = notification & "<div class='col-sm-3'>" & dt1.Rows(i)("sector") & "</div>"
                        notification = notification & "<div class='col-sm-3'>" & formattedDate & "</div>"
                        If dt1.Rows(i)("Status") = "Confirm" Then
                            notification = notification & "<div class='col-sm-2 status_clr'>Pending</div>"
                            notification = notification & "<div class='col-sm-4'><a title='' href=" + ResolveClientUrl("~/DetailsPort/admin/Update_BookingOrder.aspx?OrderId=" & dt1.Rows(i)("orderid") & "&status=PNR%20ON%20HOLD&TransTD=") + " target='_blank'>" & dt1.Rows(i)("orderid") & "</a></div>"

                        ElseIf dt1.Rows(i)("Status") = "InProcess" Then
                            notification = notification & "<div class='col-sm-2 status_clr'>InProcess</div>"
                            notification = notification & "<div class='col-sm-4'><a title='' href=" + ResolveClientUrl("~/DetailsPort/admin/Update_BookingOrder.aspx?OrderId=" & dt1.Rows(i)("orderid") & "&status=InProcess&TransTD=") + " target='_blank'>" & dt1.Rows(i)("orderid") & "</a></div>"
                        Else

                            notification = notification & "<div class='col-sm-2 status_clr'>" & dt1.Rows(0)("Status") & "</div>"
                            notification = notification & "<div class='col-sm-4'><a title='' href=" + ResolveClientUrl("~/DetailsPort/admin/Update_BookingOrder.aspx?OrderId=" & dt1.Rows(i)("orderid") & "&status=" & dt1.Rows(i)("Status") & "&TransTD=") + " target='_blank'>" & dt1.Rows(i)("orderid") & "</a></div>"
                        End If


                        notification = notification & "</div>"




                    Next
                End If
            End If
            If (dt2.Rows.Count <> 0) Then
                noti_Counter1.InnerText = dt2.Rows.Count
                ''Refund
                If (dt2.Rows(0)("Status") = "Pending") Then
                    'notification_1 = notification_1 & "<div class='row' style=' border-bottom:solid 1px rgba(100, 100, 100, .30);'>"
                    notification_1 = notification_1 & "<div class='row'>"
                    notification_1 = notification_1 & "<div class='col-sm-3'>SECTOR</div>"
                    notification_1 = notification_1 & "<div class='col-sm-3'>DATE</div>"
                    notification_1 = notification_1 & "<div class='col-sm-4'>ORDERID</div>"
                    notification_1 = notification_1 & "</div>"
                    'notification_1 = notification_1 & "<div class='row'></div>"

                    For i = 0 To dt2.Rows.Count - 1

                        notification_1 = notification_1 & "<div class='row'>"
                        notification_1 = notification_1 & "<div class='col-sm-3'>" & dt2.Rows(i)("sector") & "</div>"
                        notification_1 = notification_1 & "<div class='col-sm-3'>" & dt2.Rows(i)("depdate") & "</div>"
                        notification_1 = notification_1 & "<div class='col-sm-4'><a title='' href=" + ResolveClientUrl("~/DetailsPort/admin/QCTicketReport.aspx?orderid=" & dt2.Rows(i)("orderid") & "") + ">" & dt2.Rows(i)("orderid") & "</a></div>"
                        notification_1 = notification_1 & "</div>"

                    Next
                End If
            End If
            If (dt3.Rows.Count <> 0) Then
                noti_Counter3.InnerText = dt3.Rows.Count
                ''Reissue
                If (dt3.Rows(0)("Status") = "Pending") Then
                    notification_2 = notification_2 & "<div class='row'>"
                    notification_2 = notification_2 & "<div class='col-sm-3'>SECTOR</div>"
                    notification_2 = notification_2 & "<div class='col-sm-3'>DATE</div>"
                    notification_2 = notification_2 & "<div class='col-sm-4'>ORDERID</div>"
                    notification_2 = notification_2 & "</div>"
                    notification_2 = notification_2 & "<div class='row'></div>"
                    For j = 0 To dt3.Rows.Count - 1
                        notification_2 = notification_2 & "<div class='row'>"
                        notification_2 = notification_2 & "<div class='col-sm-3'>" & dt3.Rows(j)("sector") & "</div>"
                        notification_2 = notification_2 & "<div class='col-sm-3'>" & dt3.Rows(j)("depdate") & "</div>"
                        notification_2 = notification_2 & "<div class='col-sm-4'><a title='' href=" + ResolveClientUrl("~/DetailsPort/admin/QCTicketReport.aspx?orderid=" & dt3.Rows(j)("orderid") & "") + ">" & dt3.Rows(j)("orderid") & "</a></div>"

                        notification_2 = notification_2 & "</div>"
                    Next
                End If
            End If

            div_notify_value1.InnerHtml = notification
            div_notify_value2.InnerHtml = notification_1
            div_notify_value3.InnerHtml = notification_2


        Catch ex As Exception

        End Try
    End Sub
End Class

