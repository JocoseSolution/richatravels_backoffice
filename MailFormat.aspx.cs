﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MailFormat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // string filepath = @"C:\Users\Devesh\Downloads\OFFICE_MEDICAL_CARD_SON.pdf";
        // int send = MailNotification("", "OFFICE_MEDICAL_CARD_SON", filepath, filepath);       
    }

    #region Send Mail
    public int SendMailApi(string ServiceType, string MailSubject, string Dis1, string Dis2, string Dis3, string Dis4, string Dis5)
    {
        int flag = 0;
        string mailHtml = string.Empty;
        try
        {
            #region Mail Body Html
            mailHtml += "<table>";
            mailHtml += "<tr><td>DateTime:</td>";
            mailHtml += "<td>" + System.DateTime.Now.ToString() + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription1:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis1 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription2:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis2 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription3:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis3 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription4:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis4 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription5:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis5 + "</br>Date Time" + System.DateTime.Now.ToString() + "</td>";
            mailHtml += "</tr>";
            mailHtml += "</table>";
            #endregion
            MailSubject = MailSubject + ",DateTime: " + System.DateTime.Now.ToString();
            flag = MailNotification(mailHtml, MailSubject, "", ServiceType);
            if (flag == 0)
            {
                string SendMailStatus = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SendMailStatus"]);
                System.IO.File.AppendAllText("C:\\CPN_SP\\UserDA_SendMailApi" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "ServiceType:" + ServiceType + Environment.NewLine + "MailSubject" + MailSubject + Environment.NewLine + "Dis1: " + Dis1 + Environment.NewLine + "Dis2: " + Dis2 + Environment.NewLine + "Dis3: " + Dis3 + Environment.NewLine + "Dis4: " + Dis4 + Environment.NewLine + "Dis5: " + Dis5 + Environment.NewLine + "SendMailEnable:" + SendMailStatus + Environment.NewLine + "---------------------" + Environment.NewLine);
            }
        }
        catch (Exception ex)
        {

        }
        return flag;
    }
    public int MailNotification_Old(string Emailbody, string subject, string AttachmentFile, string ServiceType)
    {
        int flag = 0;
        AttachmentFile = "";
        try
        {
            //body = "Test Mail Service Api";

            #region Send Mail
            string body = "<html><body><p>" + Emailbody + "</p></body></html>";
            //try
            //{
            //    body = System.IO.File.ReadAllText(@"D:\Mailbody.txt", System.Text.Encoding.UTF8);
            //}
            //catch (Exception exx)
            //{ }
            #region Set value Credential
            string SMTPCLIENT = "relay.galileo.co.in";
            string UserID = "auth@relay.galileo.co.in"; //"auth@relay.galileo.co.in";
            string Password = "}X7jw&amp;z)BGw=";//}X7jw&amp;z)BGw= //"z@14$>8*&}d#";
            string from = TxtFrom.Text; //Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailFrom"]);
            string To = TxtFrom.Text; //Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailTo"]);       
            subject = TxtSubject.Text;
            if (Convert.ToString(TxtBody.Text).ToLower().Contains(".txt"))//(  !string.IsNullOrEmpty(Convert.ToString(TxtBody.Text).Trim()))
            {
                try
                {
                    body = System.IO.File.ReadAllText(@"C:\Mail\" + Convert.ToString(TxtBody.Text), System.Text.Encoding.UTF8);
                }
                catch (Exception exx)
                { }
                // body = TxtBody.Text;
            }
            else
            {
                body = TxtBody.Text;
            }

            if (!string.IsNullOrEmpty(Convert.ToString(TxtAttchedFileName.Text).Trim()))
            {
                try
                {
                    AttachmentFile = @"C:\Mail\" + Convert.ToString(TxtAttchedFileName.Text);
                }
                catch (Exception ex)
                {
                    AttachmentFile = "";
                }

            }

            //string SMTPCLIENT = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SmtpServer"]);
            //string UserID = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailUserName"]);
            //string Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailPassword"]);
            //string from =Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailFrom"]);
            //string To = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailTo"]);
            //if (ServiceType.ToUpper() == "LOGIN")
            //{
            //    To = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailToLogin"]);
            //}
            string cc = "";// Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailCC"]);
            string bcc = "";//Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailBcc"]);
            string SendMailStatus = "true";// Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SendMailStatus"]);

            //To = "devesh.mailme@gmail.com";
            //from = "amindasingh@gmail.com";
            //body = "test";
            //subject = "test";
            #endregion
            if (SendMailStatus == "true")
            {
                #region Sending
                System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
                msgMail.To.Clear();
                foreach (var address in To.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    // mailMessage.To.Add(address);
                    msgMail.To.Add(new System.Net.Mail.MailAddress(address));
                }
                //msgMail.To.Add(new System.Net.Mail.MailAddress(To));
                //if (!string.IsNullOrEmpty(To))
                //{
                //    msgMail.To.Add(new System.Net.Mail.MailAddress(To));

                //}
                if (!string.IsNullOrEmpty(from))
                {
                    msgMail.From = new System.Net.Mail.MailAddress(from);
                }
                //else
                //{
                //    msgMail.From = new System.Net.Mail.MailAddress("noreply@itq.in");
                //}
                //if (!string.IsNullOrEmpty(bcc))
                //{
                //    msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
                //}
                //if (!string.IsNullOrEmpty(cc))
                //{
                //    msgMail.CC.Add(new System.Net.Mail.MailAddress(cc));
                //}
                if (!string.IsNullOrEmpty(AttachmentFile))
                {
                    msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
                }

                msgMail.Subject = subject;
                msgMail.IsBodyHtml = true;
                msgMail.Body = body;
                try
                {
                    objMail.Credentials = new System.Net.NetworkCredential(UserID, Password);
                    objMail.Host = SMTPCLIENT;
                    objMail.Send(msgMail);
                    flag = 1;

                }
                catch (Exception ex)
                {
                    flag = 0;
                    Label1.Text = "Ex.Message:" + Convert.ToString(ex.Message) + " ,Ex.StackTrace" + Convert.ToString(ex.StackTrace);
                    System.IO.File.AppendAllText("C:\\Mail\\SendMail1_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
                }

                #endregion
            }
            #endregion
        }
        catch (Exception ex)
        {
            flag = 0;
            System.IO.File.AppendAllText("C:\\Mail\\SendMail2_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
            System.IO.File.AppendAllText("C:\\Mail\\SendMail3_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
        }
        return flag;
    }

    #endregion

    #region Test Upload
    public void UploadFile()
    {
        try
        {
            if (TextBox1.Text == "2021")
            {
                if (FileUpload1.HasFile)
                {
                    FileUpload1.SaveAs(@"C:\Mail\" + FileUpload1.FileName);
                    Label1.Text = "File Uploaded: " + FileUpload1.FileName;
                }
                else
                {
                    Label1.Text = "No File Uploaded.";
                }
                if (FileUpload2.HasFile)
                {

                    FileUpload1.SaveAs(@"C:\Mail\" + FileUpload2.FileName);
                    Label2.Text = "File Uploaded: " + FileUpload2.FileName;
                }
                else
                {
                    Label2.Text = "No File Uploaded.";
                }
            }

        }
        catch (Exception ex)
        {
            Label3.Text = Convert.ToString(ex.Message) + Convert.ToString(ex.StackTrace);
        }

    }
    #endregion

    protected void TestSend_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text == "2021")
        {
            //FileUpload1.SaveAs(@"C:\Mail\" + FileUpload1.FileName);
            string filepath = "";
            int send = MailNotification("", "", filepath, filepath);
            if (send > 0)
                Label1.Text = "send";
            else
                Label1.Text = "Not sent";
        }
    }
    protected void TestUpload_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text == "2021")
        {
            UploadFile();
        }
    }

    private bool Check()
    {
        bool flag = false;

        return flag;
    }
    #region Read File From Directory
    private void ReadFileName()
    {
        try
        {
            string fileName = "";
            string[] pdfFiles = System.IO.Directory.GetFiles("C:\\Mail")
                                         .Select(System.IO.Path.GetFileName)
                                         .ToArray();
            if (pdfFiles != null && pdfFiles.Count() > 0)
            {
                int f = 1;
                for (int i = 0; i < pdfFiles.Count(); i++)
                {
                    //<span></span><br />
                    fileName += "<span>" + Convert.ToString(f) + "." + pdfFiles[i] + "</span><br />";
                    f++;
                    //Console.WriteLine(i);
                }
            }
            divFile.InnerHtml = fileName;
        }
        catch (Exception ex)
        { }
        string kk = "";
        //System.IO.Directory.GetFiles("C:\\Documents", "*.pdf")
    }

    protected void BtnFileShow_Click(object sender, EventArgs e)
    {
        ReadFileName();
    }
    #endregion

    public int MailNotification(string Emailbody, string subject, string AttachmentFile, string ServiceType)
    {
        int flag = 0;
        try
        {
            //body = "Test Mail Service Api";
            #region Send Mail   
            AttachmentFile = "";
            string body = "";// "<html><body><p>" + Emailbody + "</p></body></html>";
            #region Set value Credential 
            //string SMTPCLIENT = "relay.galileo.co.in";
            //string UserID = "auth@relay.galileo.co.in";
            //string Password = "Z@14$>8*&}d#";                   
            //string from = "noreply@flywidus.com";
            //string To = "Mohit.Kumar1@galileo.co.in";
            //string cc = "devesh.mailme@gmail.com";// "Ajit.Kumar@galileo.co.in";
            //string bcc = "Devesh.Singh@galileo.co.in";
            string SMTPCLIENT = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SmtpServer"]);
            string UserID = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailUserName"]);
            string Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailPassword"]);
            //string from = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailFrom"]);
            //string To = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailTo"]);

            string from = TxtFrom.Text; //Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailFrom"]);
            string To = TxtTo.Text; //Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailTo"]);       
            subject = TxtSubject.Text;

            //string cc = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailCC"]);
            //string bcc = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailBcc"]);
            //string SendMailStatus = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SendMailStatus"]);
            #endregion
            if (1 == 1)
            {
                #region Sending                         
                System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
                msgMail.To.Clear();
                foreach (var address in To.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    // mailMessage.To.Add(address);
                    msgMail.To.Add(new System.Net.Mail.MailAddress(address));
                }
                //msgMail.To.Add(new System.Net.Mail.MailAddress(To));                  
                if (!string.IsNullOrEmpty(from))
                {
                    msgMail.From = new System.Net.Mail.MailAddress(from);
                }

                //if (!string.IsNullOrEmpty(bcc))
                //{
                //    msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
                //}
                //if (!string.IsNullOrEmpty(cc))
                //{
                //    msgMail.CC.Add(new System.Net.Mail.MailAddress(cc));
                //}
                //if (!string.IsNullOrEmpty(AttachmentFile))
                //{
                //    msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
                //}

               
                if (Convert.ToString(TxtBody.Text).ToLower().Contains(".txt"))//(  !string.IsNullOrEmpty(Convert.ToString(TxtBody.Text).Trim()))
                {
                    try
                    {
                        body = System.IO.File.ReadAllText(@"C:\Mail\" + Convert.ToString(TxtBody.Text), System.Text.Encoding.UTF8);
                    }
                    catch (Exception exx)
                    { }
                    // body = TxtBody.Text;
                }
                else
                {
                    body = TxtBody.Text;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(TxtAttchedFileName.Text).Trim()))
                {
                    try
                    {
                        AttachmentFile = @"C:\Mail\" + Convert.ToString(TxtAttchedFileName.Text);
                    }
                    catch (Exception ex)
                    {
                        AttachmentFile = "";
                    }

                    if (!string.IsNullOrEmpty(AttachmentFile))
                    {
                        msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
                    }

                }

                msgMail.Subject = subject;
                msgMail.IsBodyHtml = true;
                msgMail.Body = body;
                try
                {
                    objMail.Credentials = new System.Net.NetworkCredential(UserID, Password);
                    objMail.Host = SMTPCLIENT;
                    objMail.Send(msgMail);
                    flag = 1;

                }
                catch (Exception ex)
                {
                    flag = 0;
                    System.IO.File.AppendAllText("C:\\Mail\\SendMail1_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
                }

                #endregion
            }
            #endregion
        }
        catch (Exception ex)
        {
            flag = 0;
            System.IO.File.AppendAllText("C:\\CPN_SP\\SendMail2_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
            System.IO.File.AppendAllText("C:\\CPN_SP\\SendMail2_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
        }
        return flag;
    }



}