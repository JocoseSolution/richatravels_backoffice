﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>


            <div id="OnewayTkt">
                <div style="clear: both;"></div>
                <table style="border: 2px solid #203240; font-family: arial; font-size: 10px; color: #000; line-height: 18px; text-align: left; width: 100%;">
                    <tbody>
                        <tr>
                            <td>
                                <table style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table style="width: 100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" style="width: 600px">
                                                                <img src="http://richatravels.com/BS/images/seatseller_logo.jpg" width="198" height="45" ></td>
                                                            <td align="right" style="width: 200px; font-family: arial; font-size: 12px">
                                                                <ul style="list-style-type: none; line-height: 20px;">
                                                                    <li>
                                                                        <h3 style="margin: 0;">BBI Travel</h3>
                                                                    </li>
                                                                    <li>Gurgaon,Beripeta</li>
                                                                    <li>Andhra Pradesh,India</li>
                                                                    <li>0/9999590749</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="color: #000; font-family: arial; font-size: 12px">
                                                                <h4 style="margin: 0; color: #000;">Bhubaneswar - Baripada</h4>
                                                                <span>(25 /05/ 2017)</span></td>
                                                            <td style="font-family: arial; font-size: 12px"><b>Bus Operator:</b></td>
                                                            <td style="font-family: arial; font-size: 12px">Das &amp; Das Travels</td>
                                                            <td style="font-family: arial; font-size: 12px"><b>Bus Operator Contact No. :</b></td>
                                                            <td style="font-family: arial; font-size: 12px">9437218291</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="font-family: arial; font-size: 12px"><b>PNR :</b></td>
                                                            <td style="font-family: arial; font-size: 12px">6JP468N</td>
                                                            <td style="font-family: arial; font-size: 12px">6JP468N</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-size: 12px">
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr style="background: #203240; color: #fff; font-weight: bold;">
                                            <td height="20px" style="font-family: arial; font-size: 12px">Passenger Name</td>
                                            <td style="font-family: arial; font-size: 12px; padding: 5px;">Seat Name</td>
                                            <td style="font-family: arial; font-size: 12px; padding: 5px;">Contact Number</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial; font-size: 12px">Paritosh Singh</td>
                                            <td style="font-family: arial; font-size: 12px">6</td>
                                            <td style="font-family: arial; font-size: 12px">9999590749</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr style="font-weight: bold; background: #203240; color: #fff">
                                            <td style="font-family: arial; font-size: 12px,width:150px; height: 20px">Bus Type</td>
                                            <td style="font-family: arial; font-size: 12px; width: 150px;">Reporting Time:</td>
                                            <td colspan="3" style="font-family: arial; font-size: 12px,">Boarding Point Address: </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-family: arial; font-size: 12px">Leyland A/C Seater/Sleeper Maharaja push back (2+2)</td>
                                            <td valign="top" style="font-family: arial; font-size: 12px">11:50 AM</td>
                                            <td valign="top" style="padding-right: 5px; width: 200px; font-family: arial; font-size: 12px"><b>Location : </b>Baramunda Bus Stand</td>
                                            <td valign="top" style="padding-right: 5px; width: 200px; font-family: arial; font-size: 12px"><b>Landmark : </b>Baramunda</td>
                                            <td valign="top" style="font-family: arial; font-size: 12px"><b>Address :</b> Baramunda,Bhubaneswar</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-family: arial; font-size: 12px"><b>Total Fare :</b>&nbsp; 300/-</td>
                                            <td style="font-family: arial; font-size: 12px">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-size: 12px">
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr>
                                            <td style="font-family: arial; font-size: 12px"><b>Terms &amp; Conditions :</b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-wrap: suppress; font-family: arial; font-size: 12px">SeatSeller only a bus ticket agent.It doestn't operate bus services of it's own.<br />
                                                In order to provide a comprehensive choice of bus operators departure times and prices to customers.
                                                <br />
                                                It has tied up with many bus operators.<br />
                                                SeatSeller advice to customers is to choice bus operator they are aware of and whose service they are comfortable with.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-size: 12px">
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr>
                                            <td valign="top" style="padding-right: 5px; width: 600px; text-wrap: suppress; font-family: arial; font-size: 12px;"><b>Seat Seller responsible  for :</b><p>1. Issuing a valid (a ticket that wiil be accepted by the bus operator) for<br />
                                                it's network of bus operator.<br />
                                                2. Providing refund &amp; support in the event of cancellation.<br />
                                                3. Providing customer support and information in case of any delays/inconvenence.</p>
                                                <p><b>SeatSeller not responsible for :</b></p>
                                                <p>1. The bus operator's bus not departing/reaching on time.<br />
                                                    2. The bus operator's employees being rude<br />
                                                    3. The bus operator's bus seats etc not being up to the customer's expectation.<br />
                                                    4. The bus operator canceling the trip due to unavoidable reasons.</p>
                                            </td>
                                            <td style="float: right; padding-right: 10px; vertical-align: top; width: 300px; font-family: arial; font-size: 12px">
                                                <table style="width: 100%; padding-right: 10px">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="padding-right: 10px; font-family: arial; font-size: 12px"><b>Cancellation Policy :</b> </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-family: arial; font-size: 12px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" style="font-family: arial; font-size: 12px;">Before 49 Hrs   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0%<br />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </form>
</body>
</html>
