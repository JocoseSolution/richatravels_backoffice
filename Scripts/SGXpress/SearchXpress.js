﻿
var SGHandler;
var SShiper;
var SConsignee;

$(document).ready(function () {
    SGHandler = new SGSearchHelper();
    SGHandler.BindEvents();

});
var SGSearchHelper = function () {
    //ctl00_ContentPlaceHolder1_IDAIRXPRESS_quantity
    this.XpressddlShipper = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlShipper");
    this.Xpressquantity = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_quantity");
    this.Xpressweight = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_weight");
    this.Xpresstxttype = $("ctl00_ContentPlaceHolder1_IDAIRXPRESS_#txttype");
    this.Xpresslength = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_length");
    this.Xpresswidth = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_width");
    this.Xpressheight = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_height");
    this.XpressProductname = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_Productname");
    this.XpressddlConsignee = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlConsignee");
    this.XpressTrackID = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_TrackID");
    this.Xpressbtnsubmit = $("ctl00_ContentPlaceHolder1_IDAIRXPRESS_#btnsubmit");
    this.XpressbtnPayment = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_btnPayment");
    this.XpressState = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownList1");
    this.XpressCity = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownList2");
    this.XpressAddBranch = $("#btnADDBranchID");

    this.Xconsignee_name = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_consignee_name");
    this.Xaddress = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_address");
    this.XState = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownList1 option:selected");
    this.XCity = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownList2 option:selected");
    this.XPostalCode = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_postal_code");
    this.XContactName = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_TextBox1");
    this.XLatitude = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_latitude");
    this.XLongitude = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_latitude");
    this.XEmail = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_BranchEmailid");

    this.Shipper_City = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_Shipper_City");
    this.ShipperPostalCode = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ShipperPostalCode");
    this.ShipperShipperState = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ShipperState");
    this.Shippercustomerstate = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_customerstate");
    this.BtnTaskCreateId = $("#BtnTaskCreateId");
}

SGSearchHelper.prototype.BindEvents = function () {
    var h = this;

    h.Xpressquantity.keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });
    h.Xpressweight.keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });
    h.Xpresslength.keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });
    h.Xpresswidth.keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });
    h.Xpresswidth.keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });


    h.XpressddlShipper.change(function () {
        $("#ddlConsigneeID").html("");
        $("#SelectShipperID").html("<div class=''> <h1>Please wait..</h1> <div class='progress'> <div class='progress-bar' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:70%'> <span class='sr-only'>70% Complete</span> </div> </div> </div>");
        var ShiperCode =   $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlShipper option:selected").text();
        var autoCity = UrlBase + "STG_TAG/XPressSvc.asmx/XpressCustomerlist";
        $.ajax({
            url: autoCity, //"AutoComplete.asmx/FetchCityList",
            data: "{ 'ShiperCode': '" + ShiperCode + "'}",
            dataType: "json", type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                debugger;
                var strres = "";
                var res = data.d;
                SShiper = res;
                if (res.status == "success") {
                    if (res.result.length != 0) {
                        strres += "<h3>Shipper Details</h3>";
                        strres += "<table class='table'>";
                        strres += "<tr>";
                        strres += "<th class='XpHw20'>";
                        strres += "Name";
                        strres += "</th>";
                        strres += "<th class='XpHw20'>";
                        strres += "Address";
                        strres += "</th>";
                        //strres += "<th>";
                        //strres += "ID";
                        //strres += "</th>";
                        strres += "<th class='XpHw20'>";
                        strres += "Airport Code";
                        strres += "</th>";


                        strres += "<th class='XpHw20'>";
                        strres += "Type";
                        strres += "</th>";
                        strres += "<th class='XpHw20'>";
                        strres += "Warehouse Code";
                        strres += "</th>";
                        strres += "</tr>";
                        for (var m = 0; m < res.result.length; m++) {
                            strres += "<tr>";
                            strres += "<td class='XpHw20'>";
                            strres += res.result[m].name;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += res.result[m].billing_address_1;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += res.result[m].airport_code;
                            strres += "</td>";
                            //strres += "<td>";
                            //strres += res.result[m].code;
                            //strres += "</td>";


                            strres += "<td class='XpHw20'>";
                            strres += res.result[m].type;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += res.result[m].warehouse_code;
                            strres += "</td>";
                            strres += "</tr>";
                        }

                        strres += "</table>";
                    }
                }
                $("#SelectShipperID").html(strres);
                var url = UrlBase + "STG_TAG/XPressSvc.asmx/XCustomerlist";
                $.ajax({
                    url: url, //"AutoComplete.asmx/FetchCityList",
                    data: "{ 'UserId': '" + ShiperCode + "'}",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        debugger;
                        var strres = "";
                        var res = data.d;
                       // SShiper = res;
                        strres += "<option value='Select Consignee'>Select Consignee</option>";
                        if (res.status == "success") {
                            if (res.result.length != 0) {  
                             
                                for (var m = 0; m < res.result.length; m++) {
                                    strres += "<option value='" + res.result[m].id +"'>" + res.result[m].address+"</option>";
                                }                             
                            }
                        }
                        $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlConsignee").html(strres);

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
                })
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
        })
    });
    h.XpressddlConsignee.change(function () {
        $("#ddlConsigneeID").html("<div class=''> <h1>Please wait..</h1> <div class='progress'> <div class='progress-bar' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:70%'> <span class='sr-only'>70% Complete</span> </div> </div> </div>");
        var autoCity = UrlBase + "STG_TAG/XPressSvc.asmx/XpressBrancheslist";
        debugger;
        var Valoft = "";
        Valoft = this.value;
        var ShiperCode = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlShipper option:selected").text();
        $.ajax({
            url: autoCity, //"AutoComplete.asmx/FetchCityList",
            data: "{ 'ShiperCode': '" + ShiperCode + "'}",
            dataType: "json", type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                debugger;
                var strres = "";
                var res = data.d;
                SConsignee = res;
                if (res.status == "success") {
                    if (res.result.length != 0) {

                        strres += "<h3>Consignee Details</h3>";
                        strres += "<table class='table'>";

                        var SSARR = null;
                        var SSRE = $(res.result).filter(function (a, b) {
                            if (b.id == Valoft) {
                                SSARR = b;
                            }

                        });
                        if (SSARR.length != 0) {
                            strres += "<tr>";
                            strres += "<th class='XpHw20'>";
                            strres += "ID";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "User Id";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Address";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "City";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "State";
                            strres += "</th>";
                            strres += "</tr>";

                            strres += "<tr>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.id;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.user_id;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.address;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.city;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.state;
                            strres += "</td>";
                            strres += "</tr>";



                            strres += "<tr>";
                            strres += "<th class='XpHw20'>";
                            strres += "Consignee Name";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Consignee Email";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Contact Number";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Country";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Postal Code";
                            strres += "</th>";
                            strres += "</tr>";
                            strres += "<tr>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.consignee_name;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.consignee_email;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.contact_number;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.country;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.postal_code;
                            strres += "</td>";
                            strres += "</tr>";

                            strres += "<tr>";
                            strres += "<th class='XpHw20'>";
                            strres += "IsActive";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Latitude";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Longitude";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Airport Code";
                            strres += "</th>";
                            strres += "<th class='XpHw20'>";
                            strres += "Real Airport Code";
                            strres += "</th>";
                            strres += "</tr>";
                            strres += "<tr>";
                            strres += "<td class='XpHw20'>";
                            if (SSARR.is_active == "1") {
                                strres += "Active";
                            }
                            else {
                                strres += "Deactive";
                            }

                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.latitude;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.longitude;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.airport_code;
                            strres += "</td>";
                            strres += "<td class='XpHw20'>";
                            strres += SSARR.real_airport_code;
                            strres += "</td>";
                            strres += "</tr>";


                        }

                        strres += "</table>";
                    }
                }
                $("#ddlConsigneeID").html(strres);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
        })
    });
    h.XpressState.change(function () {
        var autoCity = UrlBase + "STG_TAG/XPressSvc.asmx/XpressGetCity";
        debugger;
        var Valoft = "";
        Valoft = $("#" + this.id + " option:selected").text();
        $.ajax({
            url: autoCity, //"AutoComplete.asmx/FetchCityList",
            data: "{ 'state': '" + Valoft + "'}",
            dataType: "json", type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                debugger;
                var strres = "";
                var res = data.d;
                if (res.length != 0) {
                    // strres += "<option value='select city'>select city</option>"; 
                    for (var m = 0; m < res.length; m++) {
                        strres += "<option value='" + res[m].CityName + "'>" + res[m].CityName + "</option>";
                    }

                }
                h.XpressCity.append(strres);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
        })
    });

    h.XpressAddBranch.click(function () {
        var UserNmae = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownListBranches option:selected").text();
        var State = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownList1 option:selected").val();
        var City = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownList2 option:selected").text();
        if (h.Xconsignee_name.val() == "") {
            alert("pleae enter consignee name");
            h.Xconsignee_name.focus();
            return false;
        }
        else if (h.Xaddress.val() == "") {
            alert("pleae enter address");
            h.Xaddress.focus();
            return false;
        }
        else if (State == "" || State == "select state") {
            alert("pleae enter state");
            h.XState.focus();
            return false;
        }
        else if (City == "" || City == "select city") {
            alert("pleae enter city");
            h.XCity.focus();
            return false;
        }
        else if (h.XPostalCode.val() == "") {
            alert("pleae enter postal code");
            h.XPostalCode.focus();
            return false;
        }
        else if (h.XContactName.val() == "") {
            alert("pleae enter contact name");
            h.XContactName.focus();
            return false;
        }
        else if (h.XLatitude.val() == "") {
            alert("pleae enter latitude name");
            h.XLatitude.focus();
            return false;
        }
        else if (h.XLongitude.val() == "") {
            alert("pleae enter longitude name");
            h.XLongitude.focus();
            return false;
        }
        else if (h.XEmail.val() == "") {
            alert("pleae enter email id");
            h.XEmail.focus();
            return false;
        }
        var bbranchreq = {
            Id: "",
            user_id: "14105",
            address: h.Xaddress.val(),
            city: City,// h.XCity.val(),
            consignee_name: h.Xconsignee_name.val(),
            contact_number: h.XContactName.val(),
            latitude: h.XLatitude.val(),
            longitude: h.XLongitude.val(),
            state: State, //h.XState.val(),
            postal_code: h.XPostalCode.val(),
            is_active: "1",
            mode: "RWT",
            UserId: UserNmae,
            Email: h.XEmail.val()
        };
        var strdd = { objAddBranchrequest: bbranchreq };
        var n = UrlBase + "STG_TAG/XPressSvc.asmx/XAddBranch";
        $.ajax({
            url: n,
            type: "POST",
            data: JSON.stringify(strdd),
            dataType: "json",
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                debugger;
                var strres = "";
                var res = data.d;
                alert(res.message);
                $.ajax.stop()
            },
            error: function (e, t, n) {
                alert(t)
            }
        })

        //var autoCity = UrlBase + "STG_TAG/XPressSvc.asmx/XAddBranch";
        //$.ajax({
        //    url: autoCity, //"AutoComplete.asmx/FetchCityList",
        //    data: JSON.stringify(bbranchreq),
        //    dataType: "json", type: "POST",
        //    contentType: "application/json; charset=utf-8",
        //    success: function (data) {
        //        debugger;
        //        var strres = "";
        //        var res = data.d;
        //        alert(res.message);
        //        //$("#lable1").text = res.result.id.ToString()
        //        //Label1msg.Text = res.message.ToString()
        //    },
        //    error: function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
        //})

    });

    h.BtnTaskCreateId.click(function () {

        var ddlShipper = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlShipper option:selected").text();
        var ddlShipperVal = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlShipper option:selected").val();
        var ddlConsignee = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlConsignee option:selected").text();
        var commodity_code = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownListcommodity option:selected").val();
        var commodity_description = $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_DropDownListcommodity option:selected").text();
        
        if (ddlShipper == "" || ddlShipper=="Select Shipper") {
            alert("pleae select Shipper");
            $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlShipper").focus();
            return false;
        }

        if (h.Shipper_City.text() == "" ) {
            alert("pleae enter shipper city");
            h.Shipper_City.focus();
            return false;
        }
        if (h.ShipperPostalCode.text() == "") {
            alert("pleae enter shipper postal code");
            h.ShipperPostalCode.focus();
            return false;
        }
        var product = [];
        for (var M = 0; M < $(".element2").length; M++) {

            if ($($("#" + $(".element2")[M].id + " input")[0]).val() == "") {
                alert("pleae enter Quantity");
                $($("#" + $(".element2")[M].id + " input")[0]).focus();
                return false;
            }
            else if ($($("#" + $(".element2")[M].id + " input")[1]).val() == "") {
                alert("pleae enter Weight");
                $($("#" + $(".element2")[M].id + " input")[1]).focus();
                return false;
            }
            else if ($($("#" + $(".element2")[M].id + " input")[2]).val() == "") {
                alert("pleae enter Type");
                $($("#" + $(".element2")[M].id + " input")[2]).focus();
                return false;
            }
            else if ($($("#" + $(".element2")[M].id + " input")[3]).val() == "") {
                alert("pleae enter Length");
                $($("#" + $(".element2")[M].id + " input")[3]).focus();
                return false;
            }
            else if ($($("#" + $(".element2")[M].id + " input")[4]).val() == "") {
                alert("pleae enter Width");
                $($("#" + $(".element2")[M].id + " input")[4]).focus();
                return false;
            }
            else if ($($("#" + $(".element2")[M].id + " input")[5]).val() == "") {
                alert("pleae enter Height");
                $($("#" + $(".element2")[M].id + " input")[4]).focus();
                return false;
            }
            else if ($($("#" + $(".element2")[M].id + " input")[5]).val() == "") {
                alert("pleae enter Product Name");
                $($("#" + $(".element2")[M].id + " input")[4]).focus();
                return false;
            }

            product.push(
                {
                    quantity: $($("#" + $(".element2")[M].id + " input")[0]).val(),
                    weight: $($("#" + $(".element2")[M].id + " input")[1]).val(),
                    type: $($("#" + $(".element2")[M].id + " input")[2]).val(),
                    length: $($("#" + $(".element2")[M].id + " input")[3]).val(),
                    width: $($("#" + $(".element2")[M].id + " input")[4]).val(),
                    height: $($("#" + $(".element2")[M].id + " input")[5]).val(),
                    name: $($("#" + $(".element2")[M].id + " input")[6]).val(),
                }
            );
                //var id = $($("#" + $(".element2")[M].id + " input")[0])
        }

        if (ddlConsignee == "" || ddlConsignee == "Select Consignee") {
            alert("pleae select Consignee");
            $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlConsignee").focus();
            return false;
        }
        var SSSS = {};
        var MMM = $(SConsignee.result).filter(function (a, b) {
            if (b.id == $("#ctl00_ContentPlaceHolder1_IDAIRXPRESS_ddlConsignee option:selected").val()) {
                SSSS = b;
            }
            

        })
        var sgShiper = $(SShiper.result).filter(function (a, b) {
            return parseFloat(b.code) == parseFloat(ddlShipper);
        });
        var rrRequest = {
            branch_id: "",
            customer_address: SSSS.address,
            customer_city: SSSS.city,
            customer_state: SSSS.state,//ddlConsignee.split(',')[3],
            customer_pin_code: SSSS.postal_code,
            customer_country: SSSS.country,
            customer_name: SSSS.consignee_name,
            customer_contact: SSSS.contact_number,
            customer_code: ddlShipper,
            customer_warehouse_code: SSSS.airport_code,
            shipper_address: SShiper.result[0].billing_address_1,
            shipper_contact_number: SShiper.result[0].contact_number,
            shipper_city: SShiper.result[0].city,
            shipper_name: SShiper.result[0].name,
            shipper_country_code: SShiper.result[0].country,
            shipper_postal_code: SShiper.result[0].postal_code ,
            shipper_state: SShiper.result[0].state,
            products: product,
            mode: "RWT",
            commodity_code: commodity_code,
            commodity_description: commodity_description,
            UserName: ddlShipper,
            consignee_email: SSSS.consignee_email

        }
        var strdd = { objTaskcreaterequest: rrRequest };
        var n = UrlBase + "STG_TAG/XPressSvc.asmx/XCreateTask";
        $.ajax({
            url: n,
            type: "POST",
            data: JSON.stringify(strdd),
            dataType: "json",
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                debugger;
                var strres = "";
                var res = data.d;
                if (res.message.indexOf("Success") != -1) {
                    alert(res.message + " Track id is " + res.result.awb_number);
                    window.location.href = UrlBase + "STG_TAG/Result.aspx?TrackId=" + res.result.awb_number;
                }
                else {
                    alert(res.message);
                }
                
                $.ajax.stop()
            },
            error: function (e, t, n) {
                alert(t)
            }
        })


    });

   

}


$(document).ready(function () {
 
    // Add new element
    $(".add").click(function () {

        // Finding total number of elements added
        var total_element = $(".element2").length;

        // last <div> with element class id
        var lastid = $(".element2:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;

        var max = 5;
        // Check total number elements
        if (total_element < max) {
            // Adding new div container after last occurance of element class
            $(".element2:last").after("<div class='element2' id='div_" + nextindex + "'></div>");

            // Adding element to <div>
            var strlayout = "";
            strlayout += "<div>";
            strlayout += "<div class='col-md-12'>";
            strlayout += "<h4>Item " + nextindex + "</h4>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/stack.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_quantity' type='text' maxlength='10' id='IDAIRXPRESS_quantity_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Quantity'>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/stack.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_weight' type='text' maxlength='10' id='IDAIRXPRESS_weight_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Weight'>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/text.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_txttype' type='text' maxlength='100' id='IDAIRXPRESS_txttype_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Type'>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/Length.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_length' type='text' maxlength='10' id='IDAIRXPRESS_length_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Length'>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/width.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_width' type='text' maxlength='10' id='IDAIRXPRESS_width_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Width'>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/Height.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_height' type='text' maxlength='10' id='IDAIRXPRESS_height_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Height'>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='input-container'>";
            strlayout += "<i class='icon' aria-hidden='true'> <img src='../Images/XPRESS/Productname.png' style='width: 23px; margin-top: -6px;'></i>";
            strlayout += "<input name='IDAIRXPRESS_Productname' type='text' maxlength='10' id='IDAIRXPRESS_Productname_" + nextindex + "' class='input-field' minlenghth='1' placeholder='Product Name'>";
            strlayout += "</div>";
            strlayout += "</div>";

            strlayout += "<div class='col-md-6'>";
            strlayout += "<div class='' style='text-align:right'>";
            strlayout += "&nbsp;";
            strlayout += "<span id='remove_" + nextindex + "' class='remove'><img src='../Images/XPRESS/delete.png' style='width: 23px; margin-top: -6px;'></span>";
            strlayout += "</div>";
            strlayout += "</div>";
            strlayout += "<div class='clear'></div>";


            $("#div_" + nextindex).append(strlayout);

        }

    });

    // Remove element
    $('.container').on('click', '.remove', function () {

        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];

        // Remove <div> with id
        $("#div_" + deleteindex).remove();

    });
});