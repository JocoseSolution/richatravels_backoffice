﻿
var SGHandler;
var OBJRES;

$(document).ready(function () {
    SGHandler = new SGSearchHelper();
    SGHandler.BindEvents();

});
var SGSearchHelper = function () {
    this.Form = $("#ctl00_ContentPlaceHolder1_From");
    this.To = $("#ctl00_ContentPlaceHolder1_To");
    this.Xpressweight = $("#ctl00_ContentPlaceHolder1_txt_PNR");
    this.Xpresstxttype = $("ctl00_ContentPlaceHolder1_txt_OrderId");
    this.btn_result = $("#ctl00_ContentPlaceHolder1_btn_result");
    this.shipper = $("#ctl00_ContentPlaceHolder1_ddlShipper");
}
SGSearchHelper.prototype.BindEvents = function () {
    var h = this;
    var FromDate = h.Form.val();
    var ToDate = h.To.val();

    //var returnDate = h.hidtxtRetDate.val();
    //Date Picker Bind

    var dtPickerOptions = {
        numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-2y", showOtherMonths: true, selectOtherMonths: false
    };
    if (h.Form.length != 0) {
        h.Form.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", FromDate.substr(0, 10));
    }
    if (h.To.length != 0) {
        h.To.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", ToDate.substr(0, 10));
    }
    h.btn_result.click(function () {

        $("#ctl00_ContentPlaceHolder1_dtvs").html("<div class=''> <h1>Please wait..</h1> <div class='progress'> <div class='progress-bar' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:70%'> <span class='sr-only'>70% Complete</span> </div> </div> </div>");

        var shipername = $("#ctl00_ContentPlaceHolder1_ddlShipper option:selected").text();
        var fromdate = $("#ctl00_ContentPlaceHolder1_From").val();
        var todate = $("#ctl00_ContentPlaceHolder1_To").val();
        var awtno = $("#ctl00_ContentPlaceHolder1_txt_PNR").val();
        var ordeId = $("#ctl00_ContentPlaceHolder1_txt_OrderId").val();

        var billingReq = {
            shipername: shipername,
            fromdate: fromdate,
            todate: todate,// h.XCity.val(),
            awtno: awtno,
            ordeId: ordeId,
        };

        var n = UrlBase3 + "/XPressSvc.asmx/XBillingDetails";
        $.ajax({
            url: n,
            type: "POST",
            data:JSON.stringify( billingReq),
            dataType: "json",
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                debugger;
                var strres = "";
                var res = data.d;
                OBJRES = res;

                if (res.result.length != 0) {
                    CreateHtmlLayOut(res)
                }
                //  alert(res.message);
                $.ajax.stop()
            },
            error: function (e, t, n) {
                alert(t)
                $("#ctl00_ContentPlaceHolder1_dtvs").html("No Record founded");
            }
        })

    });

}
SGSearchHelper.prototype.UpdateRoundTripMininumDate = function (e, t) {
    SGSearchHelper.Form.datepicker("option", {
        minDate: e
    });
    SGSearchHelper.To.datepicker("option", { minDate: e });
};


function CreateHtmlLayOut(objGetTrackRequestResponse) {
    var strlayout2 = ""
    strlayout2 += "<h3> Status and Scan </h3> "
    strlayout2 += "<table class=table>"

    strlayout2 += "<tr>"
    strlayout2 += "<th>"
    strlayout2 += "Waybill Number"
    strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Created Date"
    strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Origin"
    strlayout2 += "</th>"

    strlayout2 += "<th>"
    strlayout2 += "Destination"
    strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Shipper ID"
    strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Billing Address"
    strlayout2 += "</th>"

    //strlayout2 += "<th>"
   // strlayout2 += "Total item"
   // strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Client Status list"
    strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Consignee Name"
    strlayout2 += "</th>"
    strlayout2 += "<th>"
    strlayout2 += "Amount"
    strlayout2 += "</th>"
    strlayout2 += "</tr>"
    if (objGetTrackRequestResponse.status != null) {
        for (var index = 0; index < objGetTrackRequestResponse.result.data.length; index++) {
            strlayout2 += "<tr>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].awb_number
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].created_at
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].origin_city
            strlayout2 += "</td>"

            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].destination_city
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].shipper_account_code.toString()
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].billing_address_1
            strlayout2 += "</td>"

            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].client_status_list[objGetTrackRequestResponse.result.data[index].client_status_list.length-1].status_name
            strlayout2 += "</td>"
            //strlayout2 += "<td>"
           // strlayout2 += objGetTrackRequestResponse.result.data[index].delvery_date
           // strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].name
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objGetTrackRequestResponse.result.data[index].rate
            strlayout2 += "</td>"
            strlayout2 += "</tr>"
        }
    }



    strlayout2 += "</table>"
    $("#ctl00_ContentPlaceHolder1_dtvs").html(strlayout2);
}
