﻿
var SHandler;
$(document).ready(function () {
    SHandler = new SearchHelper();
    SHandler.BindEvents();
});
var SearchHelper = function () {
    this.txtAgencyName = $("#txtAgencyName");
    this.hidtxtAgencyName = $("#hidtxtAgencyName");
    this.txtAirline = $("#txtAirline");
    this.hidtxtAirline = $("#hidtxtAirline");
    this.Form = $("#From");
    this.To = $("#To");
    this.DepartFrom = $("#DepartFrom");
    this.DepartTo = $("#DepartTo");
    this.acceptdate = $("#acceptdate");
    this.updatedate = $("#updatedate");
    this.requestdate = $("#requestdate");



    this.txtDepCity1 = $("#txtDepCity1");
    this.txtArrCity1 = $("#txtArrCity1");
    this.btnSearch = $("#btnSearch");
    this.hidtxtDepCity1 = $("#hidtxtDepCity1");
    this.hidtxtArrCity1 = $("#hidtxtArrCity1");
    this.hidAirline = $("#hidAirLine");


    //Bipin 12-12-2017
    this.txtStockistName = $("#ctl00_ContentPlaceHolder1_txtStockistName");
    this.hidtxtStockistName = $("#ctl00_ContentPlaceHolder1_hidtxtStockistName");
    this.TotxtStockistName = $("#ctl00_ContentPlaceHolder1_TotxtStockistName");
    this.TohidtxtStockistName = $("#ctl00_ContentPlaceHolder1_TohidtxtStockistName");

}

SearchHelper.prototype.BindEvents = function () {
 
    var h = this;
    if (h.txtAgencyName.length != 0) {
     
        var autoCity = UrlBase + "AgencySearch.asmx/FetchAgencyList";

        h.txtAgencyName.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: autoCity,
                    data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {
                        response($.map(data.d, function (item) {
                            var result = item.Agency_Name + "(" + item.User_Id + ")";
                            var hidresult = item.User_Id;
                            return { label: result, value: result, id: hidresult }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {
                h.hidtxtAgencyName.val(ui.item.id);
            }
        });
    }

    //FOR STOKIST 
    if (h.txtStockistName.length != 0) {

        var autoCity = UrlBase + "AgencySearch.asmx/FetchStokistList";

        h.txtStockistName.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: autoCity,
                    data: "{ 'prefix': '" + request.term + "','UserId': '" + "" + "', maxResults: 10 }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {
                        response($.map(data.d, function (item) {
                            var result = item.Agency_Name + "(" + item.User_Id + ")";
                            var hidresult = item.User_Id;
                            return { label: result, value: result, id: hidresult }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {
                h.hidtxtStockistName.val(ui.item.id);
                //h.BtnSearchStockist.trigger("click");
            }
        });
    }

    //for To StoKist
    if (h.TotxtStockistName.length != 0) {

        var autoCity = UrlBase + "AgencySearch.asmx/FetchStokistList";

        h.TotxtStockistName.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: autoCity,
                    data: "{ 'prefix': '" + request.term + "','UserId': '" + "" + "', maxResults: 10 }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {
                        response($.map(data.d, function (item) {
                            var result = item.Agency_Name + "(" + item.User_Id + ")";
                            var hidresult = item.User_Id;
                            return { label: result, value: result, id: hidresult }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {
                h.TotxtStockistName.val(ui.item.id);
            }
        });
    }


    //Devesh
    if (h.txtDepCity1.length != 0) {
        var i = UrlBase + "CitySearch.asmx/FetchCityList";
        h.txtDepCity1.autocomplete({
            source: function (e, t) {               
                $.ajax({
                    url: i,
                    data: "{ 'city': '" + e.term + "', maxResults: 10 }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (e) {
                        t($.map(e.d, function (e) {                           
                            var t = e.CityName + "(" + e.AirportCode + ")";
                            var n = e.AirportCode + "," + e.CountryCode;
                            return {
                                label: t,
                                value: t,
                                id: n
                            }
                        }))
                    },
                    error: function (e, t, n) {
                        alert(t)
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (t, n) {

                h.hidtxtDepCity1.val(n.item.id)
            }
        });
    }
    if (h.txtArrCity1.length != 0) {
        h.txtArrCity1.autocomplete({
            source: function (e, t) {
                $.ajax({
                    url: i,
                    data: "{ 'city': '" + e.term + "', maxResults: 10 }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (e) {
                        t($.map(e.d, function (e) {
                            var t = e.CityName + "(" + e.AirportCode + ")";
                            var n = e.AirportCode + "," + e.CountryCode;
                            return {
                                label: t,
                                value: t,
                                id: n
                            }
                        }))
                    },
                    error: function (e, t, n) {
                        alert(t)
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (t, n) {

                h.hidtxtArrCity1.val(n.item.id)
            }
        });
    }

    if (h.txtAirline.length != 0) {
        var s = UrlBase + "CitySearch.asmx/FetchAirlineList";
        h.txtAirline.autocomplete({
            source: function (e, t) {
                $.ajax({
                    url: s,
                    data: "{ 'airline': '" + e.term + "', maxResults: 10 }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (e) {
                        t($.map(e.d, function (e) {
                            var t = e.ALName + "(" + e.ALCode + ")";
                            var n = e.ALName + "," + e.ALCode;
                            return {
                                label: t,
                                value: t,
                                id: n
                            }
                        }))
                    },
                    error: function (e, t, n) {
                        alert(t)
                    }
                })
            },
            autoFocus: true,
            minLength: 2,
            select: function (t, n) {
                h.hidtxtAirline.val(n.item.id)
            }
        });
    }
    //Devesh End

    var FromDate = h.Form.val();
    var ToDate = h.To.val();
    var DepartFrom = h.DepartFrom.val();
    var DepartTo = h.DepartTo.val();
    var acceptdate = h.acceptdate.val();
    var updatedate = h.updatedate.val(); 
    var requestdate = h.requestdate.val();
  
    var dtPickerOptions = {
        numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-2y", showOtherMonths: true, selectOtherMonths: false
    };
    if (h.Form.length != 0) {
        h.Form.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", FromDate.substr(0, 10));
    }
    if (h.To.length != 0) {
        h.To.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", ToDate.substr(0, 10));
    }
    if (h.DepartFrom.length != 0) {
        h.DepartFrom.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", DepartFrom.substr(0, 10));
    }
    if (h.DepartTo.length != 0) {
        h.DepartTo.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", DepartTo.substr(0, 10));
    }

    if (h.acceptdate.length != 0) {
        h.acceptdate.datepicker(dtPickerOptions);
    }
    if (h.updatedate.length != 0) {
        h.updatedate.datepicker(dtPickerOptions);
    }
    if (h.requestdate.length != 0) {
        h.requestdate.datepicker(dtPickerOptions);
    }

    //Destination AutoComplete
    h.txtAgencyName.autocomplete({
        source: function (request, response) {
var autoCity = UrlBase + "AgencySearch.asmx/FetchAgencyList";
            $.ajax({
                url: autoCity,
                data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                dataType: "json", type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        var result = item.Agency_Name + "(" + item.User_Id + ")";
                        var hidresult = item.User_Id;
                        return { label: result, value: result, id: hidresult }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            })
        },
        autoFocus: true,
        minLength: 3,
        select: function (event, ui) {
            h.hidtxtAgencyName.val(ui.item.id);
        }
    });
}


function focusObj(obj) {
    if (obj.value == "Agency Name or ID") obj.value = "";

}

function blurObj(obj) {
    if (obj.value == "") obj.value = "Agency Name or ID";


}
