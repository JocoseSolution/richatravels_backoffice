﻿<%@ WebService Language="C#" Class="SateCityService" %>
using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[System.Web.Script.Services.ScriptService]
public class SateCityService  : System.Web.Services.WebService {
         string Con = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString.ToString();
    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }
    [WebMethod]
    public List<City> FetchGSTStateList(string cityCode)
    {
        List<City> objgstcity = new List<City>();
        try
        {
            DataSet CityDS = new DataSet();
            CityDS = GetStateList(cityCode);

            for (int i = 0; (i <= (CityDS.Tables[0].Rows.Count - 1)); i++)
            {
                objgstcity.Add(new City { ID = i, CityName = CityDS.Tables[0].Rows[i]["CITY"].ToString().Trim(), AirportCode = CityDS.Tables[0].Rows[i]["STATEID"].ToString().Trim(), ALCode = CityDS.Tables[0].Rows[i]["StateCode"].ToString().Trim() });
            }
        }
        catch(Exception ex)
        { }
        return objgstcity;

    }
     public DataSet GetStateList(string code) {
        DataSet gstStateDs=new DataSet();
        try{
            SqlDatabase DBHelper = new SqlDatabase(Con);
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "Usp_GetStateList_V1";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@param1", DbType.String, code);
            gstStateDs = DBHelper.ExecuteDataSet(cmd);
        }
        catch(Exception ex)
        {}
        return gstStateDs;
    }
}