﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SalesDashbord : System.Web.UI.Page
{
    private string User_Id = string.Empty;
    private string UserType = string.Empty;
    public string StrAirLine = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["User_Type"] != null)
        {
            this.UserType = Session["User_Type"].ToString();
            if (!string.IsNullOrEmpty(this.UserType))
            {
                if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
                {
                    this.User_Id = Session["UID"].ToString();
                    if (!Page.IsPostBack)
                    {
                        this.BindDashDetails("", "", "");
                    }
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    private void BindDashDetails(string fromDate, string toDate, string agencyId)
    {
        string salesId = "";
        if (this.UserType.ToLower().Trim() != "admin")
        {
            salesId = this.User_Id;
        }

        DataSet ds = new DataSet();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("sp_GetNewDashBoardDetails");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@FromDate", fromDate);
        cmd.Parameters.AddWithValue("@ToDate", toDate);
        cmd.Parameters.AddWithValue("@AgencyId", agencyId);
        cmd.Parameters.AddWithValue("@SalesExecId", salesId);
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.Connection = con;
		cmd.CommandTimeout=120;
        sda.SelectCommand = cmd;

        DataTable dtAir = new DataTable();
        DataTable dtRail = new DataTable();
        DataTable dtAirLine = new DataTable();
        DataTable dtPgUpload = new DataTable();
        DataTable dtCashInFlow = new DataTable();
        DataTable dtCashOutFlow = new DataTable();
        DataTable dtRefund = new DataTable();
        DataTable dtBus = new DataTable();
        DataTable dtHotel = new DataTable();
        DataTable dtAPISales = new DataTable();

        sda.Fill(ds);
        dtAir = ds.Tables[0];
        dtRail = ds.Tables[1];
        dtAirLine = ds.Tables[2];
        dtPgUpload = ds.Tables[3];
        dtCashInFlow = ds.Tables[4];
        dtCashOutFlow = ds.Tables[5];
        dtRefund = ds.Tables[6];
        dtBus = ds.Tables[7];
        dtHotel = ds.Tables[8];
        dtAPISales = ds.Tables[9];

        lblAirCount.Text = dtAir.Rows[0]["TotalAirCount"].ToString();
        lblAirAmount.Text = "₹ " + dtAir.Rows[0]["TotalAirBookingCost"].ToString();


        lblRailCount.Text = dtRail.Rows[0]["TotalRailCount"].ToString();
        lblRailAmount.Text = "₹ " + dtRail.Rows[0]["TotalRailCost"].ToString();

        lblPgUploadCount.Text = dtPgUpload.Rows[0]["TotalPGUploadCount"].ToString();
        lblPgUploadAmt.Text = "₹ " + dtPgUpload.Rows[0]["TotalPGUploadAmount"].ToString();

        lblCashInFlowCount.Text = dtCashInFlow.Rows[0]["TotalCashInFlowCount"].ToString();
        lblCashInFlowAmt.Text = "₹ " + dtCashInFlow.Rows[0]["TotalCashInFlowAmt"].ToString();

        lblCashOutFlowCount.Text = dtCashOutFlow.Rows[0]["TotalCashOutFlowCount"].ToString();
        lblCashOutFlowAmt.Text = "₹ " + dtCashOutFlow.Rows[0]["TotalCashOutFlowAmt"].ToString();

        lblRefundCount.Text = dtRefund.Rows[0]["TotalRefundCount"].ToString();
        lblRefundAmt.Text = "₹ " + dtRefund.Rows[0]["TotalRefundAmt"].ToString();

        lblBusCount.Text = dtBus.Rows[0]["TotalBusCount"].ToString();
        lblBusAmount.Text = "₹ " + dtBus.Rows[0]["TotalBusAmt"].ToString();

        lblHotelCount.Text = dtHotel.Rows[0]["TotalHotelCount"].ToString();
        lblHotelAmt.Text = "₹ " + (dtHotel.Rows[0]["TotalHotelAmt"].ToString() != "0" ? dtHotel.Rows[0]["TotalHotelAmt"].ToString() : "0.00");

        lblApiCount.Text = dtAPISales.Rows[0]["TotalApiSalesCount"].ToString();
        lblApiAmt.Text = "₹ " + dtAPISales.Rows[0]["TotalApiSalesCost"].ToString();

        StringBuilder dbAirLine = new StringBuilder();
        dbAirLine.Append("<table class='table table-bordered' style='width: 100% !important;'>");
        dbAirLine.Append("<thead><tr><th>Airlines</th><th>AirLine Code</th><th>Gross Sale</th><th>Comm. Passed</th><th>Net Sale</th></tr></thead>");
        dbAirLine.Append("<tbody>");
        if (dtAirLine.Rows.Count > 0)
        {
            double finalTotalNetSale = 0;
            double finalTotalSale = 0;
            double finalCommSale = 0;
            //double tTotalSale = 0;
            for (int i = 0; i < dtAirLine.Rows.Count; i++)
            {
                dbAirLine.Append("<tr>");
                dbAirLine.Append("<td>" + (!string.IsNullOrEmpty(dtAirLine.Rows[i]["AirlineName"].ToString()) ? dtAirLine.Rows[i]["AirlineName"].ToString() : "- - -") + "</td>");
                dbAirLine.Append("<td>" + dtAirLine.Rows[i]["vc"].ToString() + "</td>");
                dbAirLine.Append("<td>₹ " + dtAirLine.Rows[i]["TotalCost"].ToString() + "</td>");
                dbAirLine.Append("<td>₹ " + dtAirLine.Rows[i]["TotalComm"].ToString() + "</td>");
                dbAirLine.Append("<td>₹ " + dtAirLine.Rows[i]["NetSale"].ToString() + "</td>");
                dbAirLine.Append("</tr>");
                finalTotalNetSale = finalTotalNetSale + Convert.ToDouble(dtAirLine.Rows[i]["NetSale"].ToString());
                finalTotalSale = finalTotalSale + Convert.ToDouble(dtAirLine.Rows[i]["TotalCost"].ToString());
                finalCommSale = finalCommSale + Convert.ToDouble(dtAirLine.Rows[i]["TotalComm"].ToString());
                //tTotalSale = tTotalSale + Convert.ToDouble(dtAirLine.Rows[i]["TotalCost"].ToString());
            }
            //lblAirAmount.Text = "₹ " + tTotalSale.ToString();
            dbAirLine.Append("<tr>");
            dbAirLine.Append("<td colspan='2' style='text-align: center;'>Total</td><td>₹ " + finalTotalSale + "</td><td>₹ " + finalCommSale + "</td><td>₹ " + finalTotalNetSale + "</td>");
            dbAirLine.Append("</tr>");
        }
        else
        {
            dbAirLine.Append("<tr>");
            dbAirLine.Append("<td colspan='5' style='text-align: center;'>No such record found!</td>");
            dbAirLine.Append("</tr>");
        }
        dbAirLine.Append("</tbody>");
        dbAirLine.Append("</table>");

        StrAirLine = dbAirLine.ToString();
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        if (Session["User_Type"] != null)
        {
            string fromdate = ""; string todate = ""; string agencyId = "";
            if (!string.IsNullOrEmpty(Request["From"]))
            {
                fromdate = String.Format(Request["From"].Split('-')[2], 4) + "-" + String.Format(Request["From"].Split('-')[1], 2) + "-" + String.Format(Request["From"].Split('-')[0], 2) + " 00:00:00";
                hdnFromDate.Value = Request["From"];
            }
            if (!string.IsNullOrEmpty(Request["To"]))
            {
                todate = String.Format(Request["To"].Split('-')[2], 4) + "-" + String.Format(Request["To"].Split('-')[1], 2) + "-" + String.Format(Request["To"].Split('-')[0], 2);
                hdnToDate.Value = Request["To"];
            }
            if (!string.IsNullOrEmpty(Request["hidtxtAgencyName"]))
            {
                agencyId = Request["hidtxtAgencyName"].ToString();
                hdnAgency.Value = Request["txtAgencyName"] + "+" + Request["hidtxtAgencyName"];
            }

            BindDashDetails(fromdate, todate, agencyId);
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
}