﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Send : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Business.AirApiModelConverter obj = new Business.AirApiModelConverter();
            int ff = SendMailApi("LOGIN", "Authentication Failed-ValidateUser", "Authentication failed, database not connect to application server,Date Time: " + DateTime.Now, "Test1", "Test2", "Test3", "Test4");
        }
        catch (Exception ex1)
        { }
    }

    #region Send Mail
    public int SendMailApi(string ServiceType, string MailSubject, string Dis1, string Dis2, string Dis3, string Dis4, string Dis5)
    {
        int flag = 0;
        string mailHtml = string.Empty;
        try
        {
            #region Mail Body Html
            mailHtml += "<table>";
            mailHtml += "<tr><td>DateTime:</td>";
            mailHtml += "<td>" + System.DateTime.Now.ToString() + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription1:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis1 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription2:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis2 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription3:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis3 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription4:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis4 + "</td>";
            mailHtml += "</tr>";
            mailHtml += "<tr><td>Discription5:&nbsp;&nbsp;</td>";
            mailHtml += "<td>" + Dis5 + "</br>Date Time" + System.DateTime.Now.ToString() + "</td>";
            mailHtml += "</tr>";
            mailHtml += "</table>";
            #endregion
            MailSubject = MailSubject + ",DateTime: " + System.DateTime.Now.ToString();
            flag = MailNotification(mailHtml, MailSubject, "", ServiceType);
            if (flag == 0)
            {
                string SendMailStatus = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SendMailStatus"]);
                System.IO.File.AppendAllText("C:\\CPN_SP\\UserDA_SendMailApi" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "ServiceType:" + ServiceType + Environment.NewLine + "MailSubject" + MailSubject + Environment.NewLine + "Dis1: " + Dis1 + Environment.NewLine + "Dis2: " + Dis2 + Environment.NewLine + "Dis3: " + Dis3 + Environment.NewLine + "Dis4: " + Dis4 + Environment.NewLine + "Dis5: " + Dis5 + Environment.NewLine + "SendMailEnable:" + SendMailStatus + Environment.NewLine + "---------------------" + Environment.NewLine);
            }
        }
        catch (Exception ex)
        {

        }
        return flag;
    }
    public int MailNotification(string Emailbody, string subject, string AttachmentFile, string ServiceType)
    {
        int flag = 0;
        try
        {
            //body = "Test Mail Service Api";
            #region Send Mail               
            string body = "<html><body><p>" + Emailbody + "</p></body></html>";
            #region Set value Credential 
            //string SMTPCLIENT = "relay.galileo.co.in";
            //string UserID = "auth@relay.galileo.co.in";
            //string Password = "Z@14$>8*&}d#";                   
            //string from = "noreply@flywidus.com";
            //string To = "Mohit.Kumar1@galileo.co.in";
            //string cc = "devesh.mailme@gmail.com";// "Ajit.Kumar@galileo.co.in";
            //string bcc = "Devesh.Singh@galileo.co.in";
            string SMTPCLIENT = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SmtpServer"]);
            string UserID = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailUserName"]);
            string Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailPassword"]);
            string from = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailFrom"]);
            string To = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailTo"]);
            if (ServiceType.ToUpper() == "LOGIN")
            {
                To = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailToLogin"]);
            }
            string cc = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailCC"]);
            string bcc = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MailBcc"]);
            string SendMailStatus = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SendMailStatus"]);
            #endregion
            if (1==1)
            {
                #region Sending                         
                System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
                msgMail.To.Clear();
                foreach (var address in To.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    // mailMessage.To.Add(address);
                    msgMail.To.Add(new System.Net.Mail.MailAddress(address));
                }
                //msgMail.To.Add(new System.Net.Mail.MailAddress(To));                  
                if (!string.IsNullOrEmpty(from))
                {
                    msgMail.From = new System.Net.Mail.MailAddress(from);
                }
                else
                {
                    msgMail.From = new System.Net.Mail.MailAddress("noreply@itq.in");
                }
                if (!string.IsNullOrEmpty(bcc))
                {
                    msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
                }
                if (!string.IsNullOrEmpty(cc))
                {
                    msgMail.CC.Add(new System.Net.Mail.MailAddress(cc));
                }
                //if (!string.IsNullOrEmpty(AttachmentFile))
                //{
                //    msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
                //}

                msgMail.Subject = subject;
                msgMail.IsBodyHtml = true;
                msgMail.Body = body;
                try
                {
                    objMail.Credentials = new System.Net.NetworkCredential(UserID, Password);
                    objMail.Host = SMTPCLIENT;
                    objMail.Send(msgMail);
                    flag = 1;

                }
                catch (Exception ex)
                {
                    flag = 0;
                    System.IO.File.AppendAllText("C:\\Mail\\SendMail1_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
                }

                #endregion
            }
            #endregion
        }
        catch (Exception ex)
        {
            flag = 0;
            System.IO.File.AppendAllText("C:\\CPN_SP\\SendMail2_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
            System.IO.File.AppendAllText("C:\\CPN_SP\\SendMail2_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "DateTime:" + System.DateTime.Now.ToString() + Environment.NewLine + "Ex.Message:" + Convert.ToString(ex.Message) + Environment.NewLine + "Ex.StackTrace" + Convert.ToString(ex.StackTrace) + Environment.NewLine + "---------------------" + Environment.NewLine);
        }
        return flag;
    }

    #endregion
}