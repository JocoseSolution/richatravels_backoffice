﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RailPNR_IrctcMarkup : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    SqlDataAdapter adp = null;
    DataTable dt = null;
    SqlCommand cmd = null;
    SqlTransactionDom  STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        try
        {
            if (!IsPostBack)
            {
                ddl_type.AppendDataBoundItems = true;
                ddl_type.Items.Clear();

                ddl_type.DataSource = STDom.GetAllGroupType().Tables[0];
                ddl_type.DataTextField = "GroupType";
                ddl_type.DataValueField = "GroupType";
                ddl_type.DataBind();
                ddl_type.Items.Insert(0, new ListItem("GROUP TYPE", ""));






                BindGrid();
            }
        }
        catch (Exception ex)
        {

        }


    }

    public void BindGrid()
    {
        try
        {
            //if (ddl_ptype.SelectedValue != "Select Type")
            //{
            SqlDataAdapter sdr=new SqlDataAdapter("select * from IRCTC_AGENT_CHARGE",con);
            DataTable dt1=new DataTable();
            sdr.Fill(dt1);
            con.Open();
            cityGridview.DataSource = dt1;
            cityGridview.DataBind();
            con.Close();
            //}

        }
        catch (Exception ex)
        {
        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string message = "";
            cmd = new SqlCommand("Sp_insertrailcharges", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Type", ddl_type.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@Class", ddl_class.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@agent_charge", txt_agCharge.Value);
            cmd.Parameters.AddWithValue("@Pg_Charge", txt_pgcharge.Value);
            cmd.Parameters.AddWithValue("@updatedby", Session["UID"]);
            cmd.Parameters.AddWithValue("@Pg_ChargeonAC", ddlac_Charge.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@Isgst", ddlIsGst.SelectedItem.Value);
            cmd.Parameters.Add("@massage", SqlDbType.Char, 500);
            cmd.Parameters["@massage"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            message = (string)cmd.Parameters["@massage"].Value;
            cmd.ExecuteNonQuery();
            if (message.Trim()=="N")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record Already Exists.');", true);
            }
            con.Close();
            BindGrid();
            emptyfield();
        }
        catch (Exception ex)
        { }
    }
    //protected void cityGridview_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{

    //}
    //protected void cityGridview_RowEditing(object sender, GridViewEditEventArgs e)
    //{

    //}
    //protected void cityGridview_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{

    //}
    protected void cityGridview_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lbltype = (Label)(cityGridview.Rows[e.RowIndex].FindControl("lbltype"));
                Label lblclass = (Label)(cityGridview.Rows[e.RowIndex].FindControl("lblclass"));
                string  Type = lbltype.Text.Trim().ToString();
                string ClassType = lblclass.Text.Trim().ToString();
                //string IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                SqlCommand cmd = new SqlCommand("delete from IRCTC_AGENT_CHARGE where Type='" + Type + "' and Class='" + ClassType + "'", con);
                cmd.CommandType = CommandType.Text;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }

            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "Flight")
            clsErrorLog.LogInfo(ex);
        }

    }
    public void emptyfield()
    {
        txt_agCharge.Value = "";
        txt_pgcharge.Value = "";
    }
}