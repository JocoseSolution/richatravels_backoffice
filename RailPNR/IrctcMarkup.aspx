﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="IrctcMarkup.aspx.cs" Inherits="RailPNR_IrctcMarkup" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rail > IRCTC Markup</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Type :</label>
                                <asp:DropDownList CssClass="form-control" ID="ddl_type" runat="server" required="required">
                                </asp:DropDownList>
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Class :</label>
                                <asp:DropDownList CssClass="form-control" ID="ddl_class" runat="server">
                                    <asp:ListItem Value="AC">AC</asp:ListItem>
                                    <asp:ListItem Value="SL">SL</asp:ListItem>
                                    <asp:ListItem Value="CC">CC</asp:ListItem>
                                    <asp:ListItem Value="2S">2S</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Agent Charge:</label>
                             
                                    <input type="text" placeholder="Agent Charge" name="Agent_Charge" id="txt_agCharge" runat="server" class="form-control" required="required" onkeypress="return isNumberKey(event)"  />
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Pg Charge:</label>
                                <input type="text" placeholder="PG Charge" name="Pg_Charge" id="txt_pgcharge" runat="server" class="form-control" required="required" />
                            </div>
                                </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Pg Charge On AC>2000:</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlac_Charge" runat="server">
                                    <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>IsGst</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlIsGst" runat="server">
                                    <asp:ListItem Value="1" Selected="True">True</asp:ListItem>
                                    <asp:ListItem Value="0">False</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                            </div>

                            <div class="col-md-4" >
                                <div class="form-group">

                                    <asp:Button ID="btnSubmit" CssClass="button buttonBlue" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                </div>
                            </div>

                        </div>
                        <asp:GridView ID="cityGridview" runat="server" CssClass="table table-striped table-bordered table-hover" EmptyDataText="Sorry,no city found..!" AutoGenerateColumns="False" OnRowDeleting="cityGridview_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="Id">
                                    <ItemTemplate>

                                        <asp:Label ID="lblid" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>

                                        <asp:Label ID="lbltype" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Class">
                                    <ItemTemplate>
                                        <asp:Label ID="lblclass" runat="server" Text='<%# Eval("Class") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent Charge">
                                    <ItemTemplate>
                                        <asp:Label ID="lblagcharge" runat="server" Text='<%# Eval("Agent_Charge" ,"{0:n}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pg Charge">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpgcharge" runat="server" Text='<%# Eval("PG_Charge","{0:n}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pg Charge On AC>2000">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpgchargeONAC" runat="server" Text='<%# Eval("PG_ChargeONAC","{0:n}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ISGst">
                                    <ItemTemplate>
                                        <asp:Label ID="lblisgst" runat="server" Text='<%# Eval("isgst") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Action" ShowHeader="False">

                                    <ItemTemplate>
                                        &nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnLinkDelete" ImageUrl="~/Images/icons/Recycle_Bin_Full.png" Style="width: 23px;" runat="server" formnovalidate="" CommandName="Delete" Text="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
       <!--
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    //-->
    </script>
</asp:Content>

