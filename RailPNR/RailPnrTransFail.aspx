﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="RailPnrTransFail.aspx.cs" Inherits="RailPNR_RailPnrTransFail" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <%-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --%>
    <link rel="stylesheet" href="../CSS/RailCSS/importmsdn.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>  
<%--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />  --%>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>  
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>  

    <div class="container py_3">
        <div class="card">
            <div class="card_header bg_primary text_Uppercase text_white">
                <h5>Import Excel File</h5>
            </div>
            <div class="card_body">
                <button style="margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-plus-circle"></i>Import Excel  
                </button>
                <div class="modal fade" id="myModal" style="left: 50%;">
                    <div class="modal-dialog" style="margin-top: 208px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Import Excel File</h4>
                                <button type="button" class="close" data-dismiss="modal">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Choose excel file</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <asp:FileUpload ID="FileUpload1" CssClass="custom-file-input" runat="server" />
                                                    <label class="custom-file-label"></label>
                                                </div>
                                                <label id="filename"></label>
                                                <div class="input-group-append">
                                                    <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-outline-primary" Text="Upload" OnClick="btnUpload_Click" />
                                                </div>

                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <asp:Button ID="btnledger" runat="server" CssClass="btn btn-outline-primary" Text="Ledger" OnClick="btnledger_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <asp:Button ID="btnremove" runat="server" CssClass="btn btn-outline-primary" Text="Clear" OnClick="btnremove_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>
                    <asp:Label runat="server" ID="lblmsg"></asp:Label>
                </div>
                <asp:GridView ID="GridView1" HeaderStyle-CssClass="bg-primary text-white" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered">
                    <EmptyDataTemplate>
                        <div class="text-center">No record found</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="TRANSACTION_ID" DataField="TRANSACTION_ID" />
                        <asp:BoundField HeaderText="OPERATOR_TXN_ID" DataField="OPERATOR_TXN_ID" />
                            <asp:BoundField HeaderText="REFUND_AMOUNT" DataField="REFUND_AMOUNT" />
                            <asp:BoundField HeaderText="CLASS" DataField="CLASS" />
                            <asp:BoundField HeaderText="REFUND_DATE" DataField="REFUND_DATE" />
                            <asp:BoundField HeaderText="SUB_AGENT_USER_ID" DataField="SUB_AGENT_USER_ID" />
                            <asp:BoundField HeaderText="MASTER_USER_ID" DataField="MASTER_USER_ID" />
                            

                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

