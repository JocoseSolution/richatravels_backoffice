﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="RefundFailPNR.aspx.cs" Inherits="RailPNR_RefundFailPNR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%-- <link href="<%=ResolveUrl("~/CSS/main2.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/style.css")%>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>
        <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>
        <div class="row">
        
      <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rail Upload >Rail Refund Fail</h3>
                    </div>
                    <div class="panel-body">
                        
    <div class="row">

                                    <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">RESERVATION ID</label>
                                    <input type="text" name="From" id="texttransid" class="form-control" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-8 ">
                                <div class="w40 lft">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="button buttonBlue" Text="Search" OnClick="btnSearch_Click" />
                                </div>
                                <div class="w20 lft">&nbsp;</div>
                                <div class="w40 lft">
                                   <asp:Button ID="btnrefund" runat="server" CssClass="button buttonBlue" Text="Refund" OnClick="btnrefund_Click" />
                                </div>
                                                                <div class="w20 lft">&nbsp;</div>
                                <div class="w40 lft">
                                  <asp:Button ID="btnledger" runat="server" CssClass="button buttonBlue" Text="Refund All" OnClick="btnledger_Click" />
                                </div>
                                                                <div class="w20 lft">&nbsp;</div>
                                <div class="w40 lft">
                                    <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" OnClick="btn_export_Click" />
                                </div>
                            </div>
                        </div>
        
    </div>
                                                <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Count :</label>
                                    <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
    <div>
                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff;  overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                                            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        PageSize="30" OnPageIndexChanging="GridView1_PageIndexChanging">
       
            <EmptyDataTemplate>
                <div class="text-center">No record found</div>
            </EmptyDataTemplate>
            <Columns>
                <asp:BoundField HeaderText="ReferenceNo" DataField="ReferenceNo" />
                <asp:BoundField HeaderText="ReservationId" DataField="ReservationId" />
                <asp:BoundField HeaderText="BankTxnId" DataField="BankTxnId" />
                <asp:BoundField HeaderText="UserId" DataField="UserId" />
                <asp:BoundField HeaderText="AgencyId" DataField="AgencyId" />
                <asp:BoundField HeaderText="DistrId" DataField="DistrId" />
                <asp:BoundField HeaderText="AgencyName" DataField="AgencyName" />

                <asp:BoundField HeaderText="Type" DataField="Type" />
                <asp:BoundField HeaderText="TxnAmt" DataField="TxnAmt" />
                <asp:BoundField HeaderText="PGCharge" DataField="PGCharge" />
                <asp:BoundField HeaderText="AgentCharge" DataField="AgentCharge" />
                <asp:BoundField HeaderText="GstOnAgentCharge" DataField="GstOnAgentCharge" />
                <asp:BoundField HeaderText="TotalAmt" DataField="TotalAmt" />
                <asp:BoundField HeaderText="ChargesType" DataField="ChargesType" />

                <asp:BoundField HeaderText="Charges" DataField="Charges" />
                <asp:BoundField HeaderText="PGMin" DataField="PGMin" />
                <asp:BoundField HeaderText="PGMax" DataField="PGMax" />
                <asp:BoundField HeaderText="CreatedDate" DataField="CreatedDate" />
                <asp:BoundField HeaderText="TotalAmt" DataField="TotalAmt" />
                <asp:BoundField HeaderText="ChargesType" DataField="ChargesType" />



            </Columns>
        </asp:GridView>
                                    </ContentTemplate>
                                       </asp:UpdatePanel>
    </div>
                        </div>
                    </div>
                </div>
          </div>
            </div>
        <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

