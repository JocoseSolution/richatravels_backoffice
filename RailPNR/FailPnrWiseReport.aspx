﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="FailPnrWiseReport.aspx.vb" Inherits="RailPNR_FailPnrWiseReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <script type="text/javascript">
         $(document).ready(function () {
             $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
             $(".cd").click(function () {
                 $("#From").focus();
             }
            );
             $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
             $(".cd1").click(function () {
                 $("#To").focus();
             }
            );
         });
    </script>


       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="row">
       <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
        <div class="panel-heading">
                        <h3 class="panel-title">Rail > Fail PNR Report</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row" >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <%--<label for="exampleInputEmail1">From Date</label>--%>
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" readonly="readonly" class="form-control" id="dp1539064332191" />
                                   
                                </div>
                            </div>
                                  <div class="col-md-4">
                                <div class="form-group">
                                    <%--<label for="exampleInputEmail1">To Date</label>--%>
                                    <input type="text" placeholder="TO DATE" name="To" id="To" readonly="readonly" class="form-control" />
                                  
                                </div>
                            </div>
                           
                            <div class="col-md-4">
                                <div class="form-group" id="tr_Agency" runat="server">
                                    <%--<label for="exampleInputEmail1">Agency</label>--%>
                                    <span id="tr_AgencyName" runat="server">
                                        <input type="text" placeholder="AGENCY NAME" id="txtAgencyName" name="txtAgencyName" <%--onfocus="focusObj(this);"--%>
                                           <%-- onblur="blurObj(this);"--%> <%--defvalue="Agency Name or ID"--%> autocomplete="off" <%--value="Agency Name or ID"--%> class="form-control" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                                  
                                </div>
                            </div>


                              <div class="col-md-4">
                                <div class="form-group" id="Div2" runat="server">
                                
                                    <span id="Span1" runat="server">
                                        <input type="text" placeholder="DISTRIBUTOR" id="txtStockistName" name="txtStockistName"
                                           autocomplete="off" class="form-control" />
                                        <input type="hidden" id="hidtxtStockistName" name="hidtxtStockistName" value="" /></span>
                                 
                                </div>
                            </div>

                                                                     <div class="col-md-4">
                                <div class="form-group">
                                    <%--<label for="exampleInputEmail1">To Date</label>--%>
                                    <input type="text" placeholder="PNR" name="PNR" id="PNR" class="form-control" />
                                 
                                </div>
                            </div>


                             </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                               
                           <asp:Button ID="btn_search" runat="server" Text="Find" CssClass="button buttonBlue" />
                       </div>
                            </div>  
                                <div class="col-md-2">
                                <div class="form-group">
                                
                                   <asp:Button ID="btn_export" runat="server" Text="Export To Excel" CssClass="button buttonBlue"/>
                                </div>
                            </div> 
                        </div>
                        

                       



                        


                        <br />
                        <div id="Div1" class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">

                                <asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover" style="text-transform: uppercase;" GridLines="None" PageSize="30">

                                            <Columns>
                                                <asp:BoundField DataField="AgencyName" HeaderText="Agentid" />
                                                <%--<asp:BoundField DataField="ReferenceNo" HeaderText="ReferenceNo" />--%>
                                                <%--<asp:BoundField DataField="ReservationId" HeaderText="ReservationId" />--%>
                                                <%--<asp:BoundField DataField="tid" HeaderText="No of Pnr" />--%>
                                                <asp:BoundField DataField="DistrId" HeaderText="DistrID" />
                                                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                                <asp:BoundField DataField="pg" HeaderText="Pg charges"/>
                                                <asp:BoundField DataField="agcharge" HeaderText="Agent charges"  />
                                                <asp:BoundField DataField="Totalfare" HeaderText="Total Fare"  />
                                                <asp:BoundField DataField="gstcharge" HeaderText="GST Charge"  />
                                                <asp:BoundField DataField="ReservationId" HeaderText="PNR No."  />
                                                <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate"  />
                                                 <%--<asp:BoundField DataField="type" HeaderText="Agent Type"  />--%>
                                                <%-- <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" ></asp:BoundField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>
