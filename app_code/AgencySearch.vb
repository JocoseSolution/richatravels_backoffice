﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Security.Cryptography
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ScriptService()> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class AgencySearch
    Inherits System.Web.Services.WebService
    Dim SqlConnection As SqlConnection
    Dim SqlCommand As SqlCommand
    Dim SqlDataAdapter As SqlDataAdapter
    Dim DataTable As DataTable
    Dim DataSet As DataSet

    <WebMethod(EnableSession:=True)>
    Public Function FetchAgencyList(ByVal city As String) As List(Of City)
        Dim AgencyDS As DataSet
        Dim objSqlTrans As New SqlTransactionNew
        AgencyDS = objSqlTrans.GetAgencyList(city, HttpContext.Current.Session("UserType"), HttpContext.Current.Session("UID"))
        Dim Ag = New City()
        Dim fetchAgency = Ag.GetAgencyList(AgencyDS.Tables(0))
        Return fetchAgency.ToList()
        ' Return fetchAgency.ToArray()
    End Function
    <WebMethod()>
    Public Function GetCityFromAgentRegisterList(ByVal city As String) As List(Of City)
        Dim AgencyDS As DataSet
        Dim objSqlTrans As New SqlTransactionNew
        AgencyDS = objSqlTrans.GetCityFromAgentRegisterList(city)
        Dim Ag = New City()
        Dim fetchAgency = Ag.GetAgencyCityList(AgencyDS.Tables(0))
        Return fetchAgency.ToList()
        ' Return fetchAgency.ToArray()
    End Function
    <WebMethod()>
    Public Function GetExecutiveDetailList(ByVal exectid As String) As List(Of City)
        Dim AgencyDS As DataSet
        Dim objSqlTrans As New SqlTransactionNew
        AgencyDS = objSqlTrans.GetExecutiveDetailList(exectid)
        Dim Ag = New City()
        Dim fetchAgency = Ag.GetExecutiveDetailList(AgencyDS.Tables(0))
        Return fetchAgency.ToList()
        ' Return fetchAgency.ToArray()
    End Function
    <WebMethod()> _
   Public Function GetFlightDetails(ByVal agentID As String, ByVal date1 As String) As List(Of FltDetails)
        Dim AgencyDT As DataTable

        Dim objSqlTrans As New SqlTransactionDom
        Return objSqlTrans.GetFlightDetailsByAgentId(agentID, date1)

    End Function


    <WebMethod()> _
  Public Function GetFlightDetailsByDateForCal(ByVal agentID As String, ByVal date1 As String) As List(Of FltDetails)
        Dim AgencyDT As DataTable

        Dim objSqlTrans As New SqlTransactionDom
        Return objSqlTrans.GetFlightDetailsByDateForCal(agentID, date1)

    End Function

    '<WebMethod()> _
    'Public Function AgencyName(prefixText As String) As List(Of String)
    '    Dim dt As New DataTable()
    '    Dim constr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ToString()
    '    Dim con As New SqlConnection(constr)
    '    con.Open()
    '    Dim cmd As New SqlCommand("select * from agent_register where Agency_Name like @Agencyname+'%'", con)
    '    cmd.Parameters.AddWithValue("@Agencyname", prefixText)
    '    Dim adp As New SqlDataAdapter(cmd)
    '    adp.Fill(dt)
    '    Dim Agencyname__1 As New List(Of String)()
    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        Agencyname__1.Add(dt.Rows(i)(1).ToString())
    '    Next
    '    Return Agencyname__1
    'End Function

    <WebMethod()> _
     <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetCustomers(prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            Dim terms As List(Of String) = prefix.Split(","c).ToList()
            terms = terms.Select(Function(s) s.Trim()).ToList()

            'Extract the term to be searched from the list
            Dim searchTerm As String = terms.LastOrDefault().ToString().Trim()

            'Return if Search Term is empty
            If String.IsNullOrEmpty(searchTerm) Then
                Return New String(-1) {}
            End If

            'Populate the terms that need to be filtered out
            Dim excludeTerms As New List(Of String)()
            If terms.Count > 1 Then
                terms.RemoveAt(terms.Count - 1)
                excludeTerms = terms
            End If

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
            Using cmd As New SqlCommand()
                Dim query As String = "select top(10) Agency_Name  as Agency_Name, User_Id from agent_register where " + "User_Id like @SearchText + '%' or Agency_Name like @SearchText + '%' or  AgencyId like @SearchText + '%' "


                'Filter out the existing searched items
                If excludeTerms.Count > 0 Then
                    query += String.Format(" and User_Id not in ({0})", String.Join(",", excludeTerms.[Select](Function(s) "'" + s + "'").ToArray()))
                End If
                cmd.CommandText = query
                cmd.Parameters.AddWithValue("@SearchText", searchTerm)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("Agency_Name"), sdr("User_Id")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function

    <WebMethod()> _
    Public Function FetchStokistds(ByVal prefix As String, ByVal UserId As String) As DataSet
        SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        SqlConnection.Open()
        SqlCommand = New SqlCommand("ChangeStockList_DI", SqlConnection)
        SqlCommand.CommandType = CommandType.StoredProcedure
        SqlCommand.Parameters.AddWithValue("@UserId", UserId)
        SqlCommand.Parameters.AddWithValue("@PREFIX", prefix)
        If (UserId = "") Then
            SqlCommand.Parameters.AddWithValue("@Case", "All")
        Else
            SqlCommand.Parameters.AddWithValue("@Case", "")
        End If
        SqlDataAdapter = New SqlDataAdapter()
        SqlDataAdapter.SelectCommand = SqlCommand
        DataSet = New DataSet()
        SqlDataAdapter.Fill(DataSet)
        Return DataSet
    End Function
    <WebMethod()> _
    Public Function FetchStokistList(ByVal prefix As String, ByVal UserId As String) As List(Of Stockist)
        SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        SqlConnection.Open()
        SqlCommand = New SqlCommand("ChangeStockList_DI", SqlConnection)
        SqlCommand.CommandType = CommandType.StoredProcedure
        SqlCommand.Parameters.AddWithValue("@UserId", UserId)
        SqlCommand.Parameters.AddWithValue("@PREFIX", prefix)
        If (UserId = "") Then
            SqlCommand.Parameters.AddWithValue("@Case", "All")
        Else
            SqlCommand.Parameters.AddWithValue("@Case", "")
        End If
        SqlDataAdapter = New SqlDataAdapter()
        SqlDataAdapter.SelectCommand = SqlCommand
        DataTable = New DataTable()
        SqlDataAdapter.Fill(DataTable)
        Dim Ag = New Stockist()
        Dim fetchStockist = Ag.GetStockistList(DataTable)
        Return fetchStockist.ToList()
    End Function
    <WebMethod(EnableSession:=True)>
    Public Function DirectR1() As String


        Dim R1 As String = Encrypt(HttpContext.Current.Session("UID") & "-" & HttpContext.Current.Session("_PASSWORD") & "-" & DateTime.Now.ToString("yyMMddHHmmssff"))
        ''Server.Transfer("http://localhost:51310/FlightSearchResults?R1=" + R1)

        QueryKeyI(R1)
        Return "http://richatravels.in/fixedDeparture/FlightSearchResults?R1=" & R1
        '' Return "http://richatravels.in/fixedDeparture/FlightSearchResults?R1=" & R1

    End Function

    Protected Function QueryKeyI(ByVal QueryKey As String) As String
        Dim ReturnQUERY As String = ""
        Dim dt As DataTable = New DataTable()
        Dim ds As DataSet = New DataSet()
        Dim adp As SqlDataAdapter = New SqlDataAdapter()
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As SqlConnection = New SqlConnection(constr)
        Dim cmd As SqlCommand = New SqlCommand("SP_QueryKey")
        cmd.Connection = con
        con.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@QueryKey", QueryKey)
        cmd.Parameters.AddWithValue("@Action", "I")
        ReturnQUERY = Convert.ToString(cmd.ExecuteScalar())
        cmd.Dispose()
        Return ReturnQUERY
    End Function

    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99520"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
             &H65, &H64, &H76, &H65, &H64, &H65,
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function


End Class
