﻿<%@ WebService Language="C#" Class="HotelSearchs" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using HotelShared;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class HotelSearchs : System.Web.Services.WebService
{
    //Hotel City Search
    [WebMethod()]
    public List<HotelShared.CitySearch> HtlCityList(string city)
    {
        HotelDAL.HotelDA HTLST = new HotelDAL.HotelDA();
        List<HotelShared.CitySearch> citylist = new List<HotelShared.CitySearch>();
        try
        {
            citylist = HTLST.GetCityList(city);
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, ex.StackTrace);
        }
        return citylist;
    }
    //Hotel Name Search
    [WebMethod()]
    public List<HotelShared.CitySearch> HotelNameSearch(string HotelName, string city)
    {
        List<HotelShared.CitySearch> citylist = new List<HotelShared.CitySearch>();
        try
        {
            HotelDAL.HotelDA HTLST = new HotelDAL.HotelDA();
            citylist = HTLST.GetCityList2(city, HotelName);
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, ex.StackTrace);
        }
        return citylist;
    }
    //List<CitySearch> GetMarkupCityCountryList(string TripType, string city, string Country, List<CitySearch> CityList
    [WebMethod()]
    public List<HotelShared.CitySearch> HtlMrkupCityCountryList(string city, string Country)
    {
        HotelDAL.HotelDA HTLST = new HotelDAL.HotelDA();
        List <HotelShared.CitySearch > citylist = new List<HotelShared.CitySearch>();
        try
        {
            citylist = HTLST.GetMarkupCityCountryList( city, Country, citylist);
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, ex.StackTrace);
        }
        return citylist;
    }
   
    [WebMethod(EnableSession = true)]
    public List<HotelResult> GetHtlResult(string CheckInDate, string CheckOutDate, string HTLCity, string NoOfRooms, string TotalAdult, string TotalChild, string TotalAdultperrooms, string Totalchildperrooms, string childage, string HotelName, string starratings)
    {
        HotelSearch search = new HotelSearch(); HotelComposite HotelCompositResult = new HotelComposite();
        ArrayList adultsperroom = new ArrayList(); ArrayList childsperroom = new ArrayList();
        try
        {
            if (HttpContext.Current.Session["UID"] == null)
            {
                List<HotelResult> objHotellist = new List<HotelResult>();
                objHotellist.Add(new HotelResult { HtlError = "SessionExpired" });
                HotelCompositResult.Hotelresults = objHotellist;
            }
            else
            {
                //search.HTLCityList = HTLCity.Trim();
                //string[] CheckInsDate = CheckInDate.Split('/');
                //string[] CheckOutsDate = CheckOutDate.Split('/');
                //search.CheckInDate = CheckInsDate[2] + "-" + CheckInsDate[1] + "-" + CheckInsDate[0];
                //search.CheckOutDate = CheckOutsDate[2] + "-" + CheckOutsDate[1] + "-" + CheckOutsDate[0];
                //search.NoofRoom = Convert.ToInt32(NoOfRooms);
                //for (int aa = 0; aa < search.NoofRoom; aa++)
                //{
                //    adultsperroom.Add(TotalAdultperrooms.Split('_')[aa]);
                //    childsperroom.Add(Totalchildperrooms.Split('_')[aa]);
                //}
                //search.AdtPerRoom = adultsperroom;
                //search.ChdPerRoom = childsperroom;
                //search.TotAdt = Convert.ToInt32(TotalAdult);
                //search.TotChd = Convert.ToInt32(TotalChild);

                //int[,] chld_age = new int[search.NoofRoom + 1, 4];
                //try
                //{
                //    string[] RoomCount = childage.Split('#');
                //    for (int ii = 0; ii < search.NoofRoom; ii++)
                //    {
                //        if (childsperroom[ii] == null)
                //            childsperroom[ii] = 0;

                //        string[] agesArray1 = new string[4];

                //        if (Convert.ToInt32(childsperroom[ii]) != 0)
                //        {
                //            for (int tc = 0; tc <= Convert.ToInt32(childsperroom[ii]) - 1; tc++)
                //            {
                //                if (childsperroom[0] == null)
                //                {
                //                    break; // TODO: might not be correct. Was : Exit For rooms[0].child[0].age=3,rooms[0].child[1].age=4,rooms[1].child[0].age=5,rooms[1].child[1].age=6,
                //                }
                //                else
                //                {
                //                    for (int ag = 1; ag <= RoomCount.Length - 1; ag++)
                //                    {
                //                        string rn = RoomCount[ag].Substring(0, 1);

                //                        if (ii == Convert.ToInt32(rn))
                //                        {
                //                            string[] strarray = RoomCount[ag].Split('_');
                //                            agesArray1[tc] = strarray[tc];
                //                            chld_age[ii, tc] = Convert.ToInt32(agesArray1[tc].Substring(1, agesArray1[tc].Length - 1));
                //                            break; 
                //                        }

                //                    }
                //                    //if (RoomCount[ii] != "")
                //                    //{
                //                    //    string rn = RoomCount[ii].Substring(0, 1);

                //                    //    if (ii == Convert.ToInt32(rn))
                //                    //    {
                //                    //        string[] strarray = RoomCount[ii].Split('_');
                //                    //        agesArray1[tc] = strarray[tc];
                //                    //        chld_age[ii, tc] = Convert.ToInt32(agesArray1[tc].Substring(1, agesArray1[tc].Length - 1));
                //                    //    }
                //                    //}
                //                    //// string[] strarray1 = RoomCount[tc + ii].Split('_');
                //                    // if (RoomCount[tc + ii] == "")
                //                    // {
                //                    //     string[] strarray3 = RoomCount[tc +1].Split('_');
                //                    //     string rn = RoomCount[tc + 1].Substring(0, 1);
                //                    //     if (ii == Convert.ToInt32(rn))
                //                    //     {
                //                    //         agesArray1[tc] = strarray3[tc];
                //                    //         chld_age[ii, tc] = Convert.ToInt32(agesArray1[tc].Substring(1, agesArray1[tc].Length - 1));
                //                    //     }
                //                    // }
                //                    // else 
                //                    // {
                //                    //     string rn = RoomCount[tc].Substring(0, 1);
                //                    //     if (ii == Convert.ToInt32(rn))
                //                    //     {
                //                    //         string[] strarray = RoomCount[tc].Split('_');
                //                    //         agesArray1[tc] = strarray[tc];
                //                    //         chld_age[ii, tc] = Convert.ToInt32(agesArray1[tc].Substring(1, agesArray1[tc].Length - 1));
                //                    //     }
                //                    // }

                //                    //string[] strarray = RoomCount[ii].Split('_');
                //                    //agesArray1[tc] = strarray[tc];
                //                    //chld_age[ii, tc] = Convert.ToInt32(agesArray1[tc]);
                //                }
                //            }
                //        }
                //    }
                //    //string[] RoomCount = childage.Split('#');
                //    //for (int ii = 0; ii < RoomCount.Length; ii++)
                //    //{
                //    //    if (childsperroom[ii] == null)
                //    //        childsperroom[ii] = 0;

                //    //    string[] agesArray1 = new string[4];

                //    //    if (Convert.ToInt32(childsperroom[ii]) != 0)
                //    //    {
                //    //        for (int tc = 0; tc <= Convert.ToInt32(childsperroom[ii]) - 1; tc++)
                //    //        {
                //    //            if (childsperroom[0] == null)
                //    //            {
                //    //                break; // TODO: might not be correct. Was : Exit For rooms[0].child[0].age=3,rooms[0].child[1].age=4,rooms[1].child[0].age=5,rooms[1].child[1].age=6,
                //    //            }
                //    //            else
                //    //            {
                //    //                string[] strarray = RoomCount[ii].Split('_');
                //    //                agesArray1[tc] = strarray[tc];
                //    //                chld_age[ii, tc] = Convert.ToInt32(agesArray1[tc]);
                //    //            }
                //    //        }
                //    //    }
                //    //}
                //}
                //catch (Exception ex)
                //{
                //    HotelDAL.HotelDA.InsertHotelErrorLog(ex, "GetHtlResult");
                //    HotelDAL.HotelDA.InsertHotelErrorLog(ex, search.AgentID);
                //}
                //search.ChdAge = chld_age;
                //if (HotelName != null & !string.IsNullOrEmpty(HotelName))
                //    search.HotelName = HotelName;
                //else
                //    search.HotelName = "";
                //if ((starratings) != null & !string.IsNullOrEmpty(starratings))
                //    search.StarRating = Convert.ToInt32(starratings).ToString();
                //else
                //    search.StarRating = "0";
                //search.Sorting = "";
                //search.AgentID = HttpContext.Current.Session["UID"].ToString();
                ////SortOrder can have following values:TG_RANKING, PRICE, GUEST_RATING, STAR_RATING_ASCENDING, STAR_RATING_DESCENDING, DEALS
                //HotelBAL.HotelAvailabilitySearch objHtlReq = new HotelBAL.HotelAvailabilitySearch();
                //HotelCompositResult = objHtlReq.HotelAvailability(search);
                //HttpContext.Current.Session["HotelSearch"] = search;
                //HttpContext.Current.Session["SearchResult"] = HotelCompositResult.Hotelresults;
                //HttpContext.Current.Session["HtlList1"] = HotelCompositResult.Hotelresults;
                //HttpContext.Current.Session["OrgList"] = HotelCompositResult.HotelOrgList;
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "GetHtlResult");
        }
        // return HotelCompositResult;
        return HotelCompositResult.Hotelresults;
    }

    [WebMethod(EnableSession = true)]
    public HotelShared.RoomComposite SelectedHotelSearch(string HotelName, string HotelCode)
    {
        HotelShared.HotelSearch SearchDetails = new HotelShared.HotelSearch(); HotelShared.RoomComposite objRoonCombind = new HotelShared.RoomComposite();
        try
        {
            //if (Session["UID"] == null || Session["HotelSearch"] == null)
            //{
            //    List<RoomList> objRoomList = new List<RoomList>();
            //    objRoomList.Add(new RoomList { HtlError = "SessionExpired" });
            //    objRoonCombind.RoomDetails = objRoomList;
            //}
            //else
            //{
            //    SearchDetails = (HotelShared.HotelSearch)System.Web.HttpContext.Current.Session["HotelSearch"];
            //    string[] strhtl = HotelCode.Split('_');
            //    SearchDetails.HtlCode = strhtl[0];
            //    SearchDetails.HotelCityCode = strhtl[1];

            //    List<HotelShared.HotelResult> HtlList = new List<HotelShared.HotelResult>();
            //    HtlList = (List<HotelShared.HotelResult>)HttpContext.Current.Session["SearchResult"];
            //    HtlList = HtlList.Where(x => x.HotelCode == SearchDetails.HtlCode && x.HotelCityCode == SearchDetails.HotelCityCode).ToList();
            //    if ((HtlList.Count > 0))
            //    {
            //        SearchDetails.StarRating = HtlList[0].StarRating;
            //        SearchDetails.HotelName = HtlList[0].HotelName;
            //        SearchDetails.ThumbImage = HtlList[0].HotelThumbnailImg;
            //        SearchDetails.Provider = HtlList[0].Provider;
            //    }

            //    HotelBAL.HotelAvailabilitySearch htlresp = new HotelBAL.HotelAvailabilitySearch();
            //    objRoonCombind = htlresp.SelectedHotelResponse(SearchDetails);

            //    List<HotelShared.HotelResult> HtlList1 = new List<HotelShared.HotelResult>();
            //    if ((List<HotelShared.HotelResult>)Session["OrgList"] != null)
            //    {
            //        if ((((List<HotelShared.HotelResult>)Session["SearchResult"]).Count < ((List<HotelShared.HotelResult>)Session["OrgList"]).Count))
            //        {
            //            HtlList1 = (List<HotelShared.HotelResult>)Session["OrgList"];
            //            HtlList1 = HtlList1.Where(x => x.HotelName.ToUpper() == HotelName.ToUpper() && x.Provider != SearchDetails.Provider).ToList();
            //        }
            //    }

            //    List<RoomList> RoomDetals = null;
            //    if (HtlList1.Count > 0)
            //    {
            //        RoomComposite RommCompositResult1 = new RoomComposite(); RoomComposite RommCompositResult2 = new RoomComposite(); RoomComposite RommCompositResult3 = new RoomComposite();
            //        RoomComposite RommCompositResult4 = new RoomComposite(); RoomComposite RommCompositResult5 = new RoomComposite();
            //        for (int z = 0; z < HtlList1.Count; z++)
            //        {
            //            switch (HtlList1[z].Provider)
            //            {
            //                case "TG":
            //                    SearchDetails.Provider = "TG";
            //                    SearchDetails.HtlCode = HtlList1[z].HotelCode;
            //                    RommCompositResult1 = htlresp.SelectedHotelResponse(SearchDetails);
            //                    if (RommCompositResult1.RoomDetails.Count > 0)
            //                        RoomDetals = objRoonCombind.RoomDetails.Union(RommCompositResult1.RoomDetails).ToList();
            //                    else
            //                    {
            //                        HotelBAL.HotelSendMail_Log HtlLogObj2 = new HotelBAL.HotelSendMail_Log();
            //                        HtlLogObj2.SendEmail("vipul@richatravels.comm ", "TGRoom " + HotelName + "_" + SearchDetails.HtlCode, "ass6956", "");
            //                    }
            //                    break;
            //                case "GTA":
            //                    SearchDetails.Provider = "GTA";
            //                    SearchDetails.HtlCode = HtlList1[z].HotelCode;
            //                    SearchDetails.HotelCityCode = HtlList1[z].HotelCityCode;
            //                    RommCompositResult2 = htlresp.SelectedHotelResponse(SearchDetails);
            //                    if (RommCompositResult2.RoomDetails.Count > 0)
            //                        RoomDetals = objRoonCombind.RoomDetails.Union(RommCompositResult2.RoomDetails).ToList();
            //                    else
            //                    {
            //                        HotelBAL.HotelSendMail_Log HtlLogObj2 = new HotelBAL.HotelSendMail_Log();
            //                        HtlLogObj2.SendEmail("vipul@richatravels.comm ", "GTARoom " + HotelName + "_" + SearchDetails.HtlCode, "ass6956","");
            //                    }
            //                    break;
            //                case "RoomXML":
            //                    SearchDetails.Provider = "RoomXML";
            //                    SearchDetails.HtlCode = HtlList1[z].HotelCode;
            //                    SearchDetails.HotelCityCode = HtlList1[z].HotelCityCode;
            //                    RommCompositResult3 = htlresp.SelectedHotelResponse(SearchDetails);
            //                    if (RommCompositResult3.RoomDetails.Count > 0)
            //                        RoomDetals = objRoonCombind.RoomDetails.Union(RommCompositResult3.RoomDetails).ToList();
            //                    else
            //                    {
            //                        HotelBAL.HotelSendMail_Log HtlLogObj2 = new HotelBAL.HotelSendMail_Log();
            //                        HtlLogObj2.SendEmail("vipul@richatravels.comm ", "RoomXMLRoom " + HotelName + "_" + SearchDetails.HtlCode, "ass6956", "");
            //                    }
            //                    break;
            //                case "RZ":
            //                    SearchDetails.Provider = "RZ";
            //                    SearchDetails.HtlCode = HtlList1[z].HotelCode;
            //                    SearchDetails.HotelCityCode = HtlList1[z].HotelCityCode;
            //                    RommCompositResult5 = htlresp.SelectedHotelResponse(SearchDetails);
            //                    if (RommCompositResult5.RoomDetails.Count > 0)
            //                        RoomDetals = objRoonCombind.RoomDetails.Union(RommCompositResult5.RoomDetails).ToList();
            //                    else
            //                    {
            //                        HotelBAL.HotelSendMail_Log HtlLogObj2 = new HotelBAL.HotelSendMail_Log();
            //                        HtlLogObj2.SendEmail("vipul@richatravels.comm ", "RezNextRoom " + HotelName + "_" + SearchDetails.HtlCode, "ass6956", "");
            //                    }
            //                    break;
            //                case "EX":
            //                    SearchDetails.Provider = "EX";
            //                    SearchDetails.HtlCode = HtlList1[z].HotelCode;
            //                    SearchDetails.HotelCityCode = HtlList1[z].HotelCityCode;
            //                    RommCompositResult4 = htlresp.SelectedHotelResponse(SearchDetails);
            //                    if (RommCompositResult4.RoomDetails.Count > 0)
            //                        RoomDetals = objRoonCombind.RoomDetails.Union(RommCompositResult4.RoomDetails).ToList();
            //                    else
            //                    {
            //                        HotelBAL.HotelSendMail_Log HtlLogObj2 = new HotelBAL.HotelSendMail_Log();
            //                        HtlLogObj2.SendEmail("vipul@richatravels.comm ", "ExpediaRoom " + HotelName + "_" + SearchDetails.HtlCode, "ass6956","");
            //                    }
            //                    break;
            //            }
            //        }
            //        RoomDetals = RoomDetals.OrderBy(h => h.TotalRoomrate).ToList();
            //        objRoonCombind.RoomDetails = RoomDetals;
            //        Session["SelectedHotelDetail"] = objRoonCombind;

            //        if (RommCompositResult1 != null)
            //        {
            //            if (RommCompositResult1.SelectedHotelDetail != null)
            //            {
            //                objRoonCombind.SelectedHotelDetail.HotelDescription += RommCompositResult1.SelectedHotelDetail.HotelDescription;
            //                objRoonCombind.SelectedHotelDetail.HotelImage += RommCompositResult1.SelectedHotelDetail.HotelImage;
            //                objRoonCombind.SelectedHotelDetail.Attraction += RommCompositResult1.SelectedHotelDetail.Attraction;
            //                objRoonCombind.SelectedHotelDetail.RoomAmenities += RommCompositResult1.SelectedHotelDetail.RoomAmenities;
            //                objRoonCombind.SelectedHotelDetail.HotelAmenities += RommCompositResult1.SelectedHotelDetail.HotelAmenities;
            //            }
            //        }
            //        if (RommCompositResult2 != null)
            //        {
            //            if (RommCompositResult2.SelectedHotelDetail != null)
            //            {
            //                objRoonCombind.SelectedHotelDetail.HotelDescription += RommCompositResult2.SelectedHotelDetail.HotelDescription;
            //                objRoonCombind.SelectedHotelDetail.HotelImage += RommCompositResult2.SelectedHotelDetail.HotelImage;
            //                objRoonCombind.SelectedHotelDetail.Attraction += RommCompositResult2.SelectedHotelDetail.Attraction;
            //                objRoonCombind.SelectedHotelDetail.RoomAmenities += RommCompositResult2.SelectedHotelDetail.RoomAmenities;
            //                objRoonCombind.SelectedHotelDetail.HotelAmenities += RommCompositResult2.SelectedHotelDetail.HotelAmenities;
            //            }
            //        }
            //        if (RommCompositResult3 != null)
            //        {
            //            if (RommCompositResult3.SelectedHotelDetail != null)
            //            {
            //                objRoonCombind.SelectedHotelDetail.HotelDescription += RommCompositResult3.SelectedHotelDetail.HotelDescription;
            //                objRoonCombind.SelectedHotelDetail.HotelImage += RommCompositResult3.SelectedHotelDetail.HotelImage;
            //                objRoonCombind.SelectedHotelDetail.Attraction += RommCompositResult3.SelectedHotelDetail.Attraction;
            //                objRoonCombind.SelectedHotelDetail.RoomAmenities += RommCompositResult3.SelectedHotelDetail.RoomAmenities;
            //                objRoonCombind.SelectedHotelDetail.HotelAmenities += RommCompositResult3.SelectedHotelDetail.HotelAmenities;
            //            }
            //        }
            //        if (RommCompositResult4 != null)
            //        {
            //            if (RommCompositResult4.SelectedHotelDetail != null)
            //            {
            //                objRoonCombind.SelectedHotelDetail.HotelDescription += RommCompositResult4.SelectedHotelDetail.HotelDescription;
            //                objRoonCombind.SelectedHotelDetail.HotelImage += RommCompositResult4.SelectedHotelDetail.HotelImage;
            //                objRoonCombind.SelectedHotelDetail.Attraction += RommCompositResult4.SelectedHotelDetail.Attraction;
            //                objRoonCombind.SelectedHotelDetail.RoomAmenities += RommCompositResult4.SelectedHotelDetail.RoomAmenities;
            //                objRoonCombind.SelectedHotelDetail.HotelAmenities += RommCompositResult4.SelectedHotelDetail.HotelAmenities;
            //            }
            //        }
            //        if (RommCompositResult5 != null)
            //        {
            //            if (RommCompositResult5.SelectedHotelDetail != null)
            //            {
            //                objRoonCombind.SelectedHotelDetail.HotelDescription += RommCompositResult5.SelectedHotelDetail.HotelDescription;
            //                objRoonCombind.SelectedHotelDetail.HotelImage += RommCompositResult5.SelectedHotelDetail.HotelImage;
            //                objRoonCombind.SelectedHotelDetail.Attraction += RommCompositResult5.SelectedHotelDetail.Attraction;
            //                objRoonCombind.SelectedHotelDetail.RoomAmenities += RommCompositResult5.SelectedHotelDetail.RoomAmenities;
            //                objRoonCombind.SelectedHotelDetail.HotelAmenities += RommCompositResult5.SelectedHotelDetail.HotelAmenities;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        RoomDetals = objRoonCombind.RoomDetails.OrderBy(x => x.TotalRoomrate).ToList();
            //        objRoonCombind.RoomDetails = RoomDetals;

            //        Session["SelectedHotelDetail"] = objRoonCombind;
            //        if (SearchDetails.Provider == "RoomXML")
            //        {
            //            SearchDetails.HotelContactNo = objRoonCombind.SelectedHotelDetail.HotelContactNo;
            //        }
            //        if (SearchDetails.Provider == "GTA")
            //        {
            //            SearchDetails.HotelContactNo = objRoonCombind.SelectedHotelDetail.HotelContactNo;
            //            //Show No of Room Updated
            //            if (SearchDetails.ExtraRoom > 0)
            //            {
            //                //   ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "ShowRoomAlertMassage('" + Htlsearch.HotelName + " can not alowed 4 Adult and Child  in One Room, So we added one more " + Htlsearch.ExtraRoomType + ".');", true);
            //                //  htlrmslbl.Text = (Htlsearch.NoofRoom + Htlsearch.ExtraRoom).ToString();
            //            }
            //            //Show No of Cot requested
            //            if (SearchDetails.ExtraCot != "0")
            //            {
            //                // cots.Visible = true;
            //                // cots1.Text = SearchDetails.ExtraCot;
            //                // ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "ShowAlertMassage();", true);

            //            }
            //        }
            //    }
            //    SearchDetails.HotelName = HtlList[0].HotelName;
            //    Session["HotelSearcDetailss"] = SearchDetails;
            //}
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SelectedHotelSearch_" + HotelName + "_" + HotelCode);
        }
        return objRoonCombind;
    }

    [WebMethod(EnableSession = true)]
    public string CancPolicy(string HotelCode, string CityCode, string Provider)
    {
        string Policys = "";
        HotelSearch HotelDetails = new HotelSearch();
        try
        {
            if (Session["UID"] == null || Session["HotelSearch"] == null)
                Policys = "SessionExpired";
            else
            {
                //HotelDetails = (HotelSearch)HttpContext.Current.Session["HotelSearch"];

                //if (Provider == "TG")
                //{
                //    HotelDetails.HotelName = HotelCode;
                //    HotelDetails.HtlRoomCode = CityCode;

                //    HotelBAL.TGHotelResponse htlresp = new HotelBAL.TGHotelResponse();
                //    Policys = htlresp.TGCancellationPolicy(HotelDetails);
                //}
                //else if (Provider == "RoomXML")
                //{
                //    HotelBAL.RoomXMLHotelResponse Roomxmlobj = new HotelBAL.RoomXMLHotelResponse();
                //    Policys = Roomxmlobj.RoomXmlCancellationpolicy(HotelDetails, HotelCode, CityCode);
                //}
                //else if (Provider == "EX")
                //{
                //    HotelBAL.EXHotelResponse ObjCalncelPolicy = new HotelBAL.EXHotelResponse();
                //    HotelDetails.HtlCode = HotelCode;
                //    HotelDetails.HtlRoomCode = CityCode;//CityCode is Used to pass Room RateKey
                //    HotelDetails.Provider = Provider;
                //    Policys = ObjCalncelPolicy.EXCancellationPolicy(HotelDetails);
                //}
                //else if (Provider == "RZ")
                //{
                //    HotelBAL.RezNextHotelResponse ObjCalncelPolicy = new HotelBAL.RezNextHotelResponse();
                //    HotelDetails.HtlCode = HotelCode;
                //    HotelDetails.HtlRoomCode = CityCode;
                //    HotelDetails.Provider = Provider;
                //    Policys = ObjCalncelPolicy.RezNextCancellationPolicy(HotelDetails);
                //}
                //else
                //{
                //    XDocument documen = XDocument.Parse(HotelDetails.GTA_HotelSearchRes);
                //    var HotelsPlicy = (from Hotel in documen.Descendants("HotelDetails").Descendants("Hotel")
                //                       where Hotel.Element("City").Attribute("Code").Value == CityCode && Hotel.Element("Item").Attribute("Code").Value == HotelCode
                //                       select new { CanPolicy = Hotel.Element("RoomCategories").Element("RoomCategory") }).ToList();
                //    HotelBAL.GTAHotelResponse GTAobj = new HotelBAL.GTAHotelResponse();
                //    Policys = GTAobj.SetHotelPolicy(HotelsPlicy[0].CanPolicy, HotelDetails);
                //}
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HtlResult_Cancpolicy");
            Policys = ex.Message;
        }
        return HttpUtility.HtmlDecode(Policys);
    }
    [WebMethod(EnableSession = true)]
    public string HotelCashback_Room(string HotelPrice, string Star, string AgentMarkup)
    {
        StringBuilder str = new StringBuilder("");
        HotelSearch HotelDetails = new HotelSearch();
        try
        {
            //if (Session["UID"] == null || Session["HotelSearch"] == null)
            //    str.Append("SessionExpired");
            //else
            //{
            //    HotelDetails = (HotelSearch)HttpContext.Current.Session["HotelSearch"];
            //    string AgencyType = "";
            //    if (HttpContext.Current.Session["User_Type"].ToString() == "AGENT")
            //        AgencyType = HttpContext.Current.Session["AGTY"].ToString();
            //    else
            //        AgencyType = HttpContext.Current.Session["User_Type"].ToString();
            //    decimal NetPrices = Convert.ToDecimal(HotelPrice) - Convert.ToDecimal(AgentMarkup);
            //    HotelDAL.HotelDA HTLST = new HotelDAL.HotelDA();
            //    DataSet commisionds = new DataSet();
            //    DataTable commisiondt = new DataTable();
            //    commisionds = HTLST.GetDetailsPageCommision(HotelDetails.Country, HotelDetails.SearchCity, Star, HotelDetails.AgentID, AgencyType, NetPrices);
            //    commisiondt = commisionds.Tables[0];
            //    str.Append("<div align='center' style='font-size:13px;line-height:130%;width:250px;'>");
            //    str.Append("<div class='clear'></div>");
            //    str.Append("<div class='lft'>Commision : </div><div class='lft'>&nbsp;&nbsp;- <img src='Images/htlrs.png' class='pagingSize' /> " + commisiondt.Rows[0]["CommisionAmt"].ToString() + "</div>");
            //    str.Append("<div class='clear'></div>");
            //    str.Append("<div class='lft'>Markups : </div><div class='lft'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - <img src='Images/htlrs.png' class='pagingSize' /> " + AgentMarkup + "</div>");
            //    str.Append("<div class='clear'></div>");
            //    str.Append("<div class='lft bld'>Price To Agent:</div><div class='lft bld'>&nbsp;&nbsp;<img src='Images/htlrs.png' class='pagingSize' /> " + commisiondt.Rows[0]["TotalAmount"].ToString() + "</div>");
            //    str.Append("<div class='clear'></div>");
            //    str.Append("</div>");
            //}
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HtlResult_HotelCashback");
        }
        return str.ToString();
    }

    [WebMethod()]
    public string RecentBookedHotel()
    {
        StringBuilder strhtl = new StringBuilder("");
        try
        {
            HotelDAL.HotelDA HTLST = new HotelDAL.HotelDA();
            DataSet HotelDT = new DataSet();
            HotelDT = HTLST.RecentBookedHotel();
            if (HotelDT != null)
            {
                DataTable htldt = HotelDT.Tables[0];
                if (htldt.Rows.Count > 0)
                {
                    strhtl.Append("<div>");
                    strhtl.Append("<div class='f16 bld'>Recently Booked Hotels</div><div class='clear1'></div>");
                    for (int i = 0; i < htldt.Rows.Count; i++)
                    {
                        string redirctURL = "", starImg = "";
                        string[] checkin = htldt.Rows[i]["CheckIN"].ToString().Split('-');
                        string[] checkOut = htldt.Rows[i]["CheckOut"].ToString().Split('-');
                       if (htldt.Rows[i]["Provider"].ToString() != "RoomXML")
                            redirctURL = "HtlResult.aspx?htlcitylist=" + htldt.Rows[i]["City"].ToString() + "," + htldt.Rows[i]["cityCode"].ToString() + "," + htldt.Rows[i]["Country"].ToString() + "," + htldt.Rows[i]["CountryCode"].ToString() + ",CITY," + htldt.Rows[i]["cityCode"].ToString() + "&htlcheckin=" + checkin[2] + "/" + checkin[1] + "/" + checkin[0] + "&htlcheckout=" + checkOut[2] + "/" + checkOut[1] + "/" + checkOut[0] + "&numRoom=1&rooms[0].adult=1&rooms[0].children=0&TotAdt=1&TotChd=0&htlCity=" + htldt.Rows[i]["City"].ToString() + "&Nights=1&htlname=" + htldt.Rows[i]["HtlCode"].ToString() + "&htlstar=0&Guest=1_0_";
                       else if (htldt.Rows[i]["Provider"].ToString() != "GTA")
                           redirctURL = "HtlResult.aspx?htlcitylist=" + htldt.Rows[i]["City"].ToString() + "," + htldt.Rows[i]["cityCode"].ToString() + "," + htldt.Rows[i]["Country"].ToString() + "," + htldt.Rows[i]["CountryCode"].ToString() + ",CITY,&htlcheckin=" + checkin[2] + "/" + checkin[1] + "/" + checkin[0] + "&htlcheckout=" + checkOut[2] + "/" + checkOut[1] + "/" + checkOut[0] + "&numRoom=1&rooms[0].adult=1&rooms[0].children=0&TotAdt=1&TotChd=0&htlCity=" + htldt.Rows[i]["City"].ToString() + "&Nights=1&htlname=" + htldt.Rows[i]["HtlName"].ToString() + "&htlstar=0&Guest=1_0_"; 
                       else 
                           redirctURL = "HtlResult.aspx?htlcitylist=" + htldt.Rows[i]["City"].ToString() + "," + htldt.Rows[i]["cityCode"].ToString() + "," + htldt.Rows[i]["Country"].ToString() + "," + htldt.Rows[i]["CountryCode"].ToString() + ",CITY,&htlcheckin=" + checkin[2] + "/" + checkin[1] + "/" + checkin[0] + "&htlcheckout=" + checkOut[2] + "/" + checkOut[1] + "/" + checkOut[0] + "&numRoom=1&rooms[0].adult=1&rooms[0].children=0&TotAdt=1&TotChd=0&htlCity=" + htldt.Rows[i]["City"].ToString() + "&Nights=1&htlname=" + htldt.Rows[i]["HtlCode"].ToString() + "&htlstar=0&Guest=1_0_";
                       //else if (htldt.Rows[i]["Provider"].ToString() != "EX")
                       //     redirctURL = "HtlResult.aspx?htlcitylist=" + htldt.Rows[i]["City"].ToString() + "," + htldt.Rows[i]["cityCode"].ToString() + "," + htldt.Rows[i]["Country"].ToString() + "," + htldt.Rows[i]["CountryCode"].ToString() + ",CITY,&htlcheckin=" + checkin[2] + "/" + checkin[1] + "/" + checkin[0] + "&htlcheckout=" + checkOut[2] + "/" + checkOut[1] + "/" + checkOut[0] + "&numRoom=1&rooms[0].adult=1&rooms[0].children=0&TotAdt=1&TotChd=0&htlCity=" + htldt.Rows[i]["City"].ToString() + "&Nights=1&htlname=" + htldt.Rows[i]["HtlCode"].ToString() + "&htlstar=0&Guest=1_0_";
                       // else if (htldt.Rows[i]["Provider"].ToString() != "RZ")
                       //     redirctURL = "HtlResult.aspx?htlcitylist=" + htldt.Rows[i]["City"].ToString() + "," + htldt.Rows[i]["cityCode"].ToString() + "," + htldt.Rows[i]["Country"].ToString() + "," + htldt.Rows[i]["CountryCode"].ToString() + ",CITY,&htlcheckin=" + checkin[2] + "/" + checkin[1] + "/" + checkin[0] + "&htlcheckout=" + checkOut[2] + "/" + checkOut[1] + "/" + checkOut[0] + "&numRoom=1&rooms[0].adult=1&rooms[0].children=0&TotAdt=1&TotChd=0&htlCity=" + htldt.Rows[i]["City"].ToString() + "&Nights=1&htlname=" + htldt.Rows[i]["HtlCode"].ToString() + "&htlstar=0&Guest=1_0_";
                        
                        string[] starratings = htldt.Rows[i]["StarRating"].ToString().Split('.');
                        for (var st = 0; st < Convert.ToInt32(starratings[0]); st++)
                        {
                            starImg += " <img src='Images/star.png' alt='Hotel Rating'/> ";
                        }
                        if (starratings.Length > 1)
                        {
                            if (starratings[1] == "5")
                                starImg += "<img src='Images/star_cut.png' alt='Hotel Rating'/>";
                        }
                        
                        strhtl.Append("<div class='clear1'></div>");
                        strhtl.Append("<div><a href='" + redirctURL + "' class='' >");
                        strhtl.Append("<div class='lft w24'><img src='" + htldt.Rows[i]["HtlImage"].ToString() + "' class='hights60 w98'/></div>");
                        strhtl.Append("<div class='w75 rgt'>");
                        strhtl.Append("<div class='lft bld'>" + htldt.Rows[i]["HtlName"].ToString() + "</div>");
                        strhtl.Append("<div class='clear'></div>");
                        strhtl.Append("<div class='lft'>" + htldt.Rows[i]["City"].ToString() + ", " + htldt.Rows[i]["Country"].ToString() + "</div>");
                        strhtl.Append("<div class='clear'></div>");
                        strhtl.Append("<div class='lft'>" + starImg + "</div>");
                        strhtl.Append("<div class='clear'></div>");
                        strhtl.Append("</div></a></div>");
                        strhtl.Append("<div class='clear'></div></div><hr />");
                    }
                    strhtl.Append("</div>");
                }
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RecentBookedHotel");
        }
        return strhtl.ToString();
    }

    [WebMethod(EnableSession = true)]
    public string HotelCashback(string HotelPrice, string Star)
    {
        StringBuilder str = new StringBuilder();
        try
        {
            if (Session["UID"] == null)
                str.Append("SessionExpired");
            else
            {
                //HotelSearch HotelDetails = new HotelSearch();
                //HotelDetails = (HotelSearch)HttpContext.Current.Session["HotelSearch"];
                //string AgencyType = "";
                //if (HttpContext.Current.Session["User_Type"].ToString() == "AGENT")
                //    AgencyType = HttpContext.Current.Session["AGTY"].ToString();
                //else
                //    AgencyType = HttpContext.Current.Session["User_Type"].ToString();

                //decimal hotelrate = Convert.ToDecimal(HotelPrice.Trim());

                //List<HotelResult> cashbak = new List<HotelResult>();
                //cashbak = (List<HotelResult>)HttpContext.Current.Session["SearchResult"];
                //cashbak = cashbak.Where(x => x.hotelPrice == hotelrate).ToList();
                ////Commision Calculation
                //HotelDAL.HotelDA HTLST = new HotelDAL.HotelDA();
                //DataTable commisiondt = HTLST.GetDetailsPageCommision(HotelDetails.Country, HotelDetails.SearchCity, Star, HotelDetails.AgentID, AgencyType, cashbak[0].hotelPrice - cashbak[0].AgtMrk).Tables[0];
                //str.Append("<table style='background-color:#fff;float:right;z-index:1;padding:11px;font-size:12px; overflow:visible;border-radius: 4px; line-height:130%;border:thin solid #333;width:200px;'>");
                //str.Append("<tr><td class='bld'>Total : </td><td> <img src='Images/htlrs.png' class='pagingSize' /> " + hotelrate.ToString() + "</td></tr>");
                //str.Append("<tr><td class='bld'>Commision : </td><td>- <img src='Images/htlrs.png' class='pagingSize' /> " + commisiondt.Rows[0]["CommisionAmt"].ToString() + "</td></tr>");
                //str.Append("<tr><td class='bld'>Markups : </td><td>- <img src='Images/htlrs.png' class='pagingSize' /> " + cashbak[0].AgtMrk.ToString() + "</td></tr>");
                //str.Append("<tr><td class='bld'>Net Cost : </td><td> <img src='Images/htlrs.png' class='pagingSize' /> " + commisiondt.Rows[0]["TotalAmount"].ToString() + "</td></tr>");
                //str.Append("</table>");
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HtlResult_HotelCashback");
            str.Append("");
        }
        return str.ToString();
    }



    [WebMethod(EnableSession = true)]
    public string RoomXMLCancellationPolicy(string Quadid)
    {
        string Policys = "";
        HotelSearch HotelDetails = new HotelSearch();
        try
        {
            //if (Session["UID"] == null || Session["HotelSearcDetailss"] == null)
            //    Policys = "SessionExpired";
            //else
            //{
            //    HotelDetails = (HotelSearch)HttpContext.Current.Session["HotelSearcDetailss"];
            //    HotelBAL.RoomXMLHotelResponse Roomxmlobj = new HotelBAL.RoomXMLHotelResponse();
            //    Policys = Roomxmlobj.RoomXmlCancellationPolicy_ForRoom(HotelDetails, Quadid);

            //}
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RoomXMLCancellationPolicy");
        }
        return Policys;
    }














    [WebMethod()]
    public string HotelServices(string HotelCode)
    {
        string Hotelinclution = "";
        try
        {
            HotelDAL.HotelDA objDa = new HotelDAL.HotelDA();
            Hotelinclution = SetHotelService_TG(objDa.GetHotelServices(HotelCode, "property"));
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelServices");
        }
        return Hotelinclution;
    }










    protected string SetHotelService_TG(DataTable HtlServices)
    {
        string InclImg = ""; int i = 0, j = 0, k = 0, l = 0, m = 0, n = 0;
        try
        {
            foreach (DataRow Services in HtlServices.Rows)
            {
                switch (Services["Amenity_id"].ToString().Trim())
                {
                    case "01":
                    case "344":
                        if (n == 0)
                        {
                            InclImg += "<img src='../Hotel/Images/Facility/travel_desk.png' title='Tagency Help Desk' style='margin:2px 2px; 0 2px' />";
                            n = 0;
                        }
                        break;
                    case "03":
                    case "04":
                        if (i == 0)
                        {
                            InclImg += "<img src='../Hotel/Images/Facility/Airport_transfer.png' title='Car rental facilities' style='margin:2px 2px; 0 2px' />";
                            i = 1;
                        }
                        break;
                    case "08":
                        InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Banquet_hall' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "09":
                        InclImg += "<img src='../Hotel/Images/Facility/bar.png' title='Mini bar' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "11":
                        InclImg += "<img src='../Hotel/Images/Facility/beauty.png' title='Beauty parlour' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "22":
                        InclImg += "<img src='../Hotel/Images/Facility/babysitting.png' title='Baby sitting' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "24":
                        InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Business centre' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "26":
                        InclImg += "<img src='../Hotel/Images/Facility/Phone.png' title='Direct dial phone' style='margin:0 2px' />";
                        break;
                    case "30":
                    case "32":
                        InclImg += "<img src='../Hotel/Images/Facility/breakfast.png' title='Tea/Coffee' style='margin:0 2px' />";
                        break;
                    case "31":
                    case "44":
                    case "65":
                        if (j == 0)
                        {
                            InclImg += "<img src='../Hotel/Images/Facility/lobby.png' title='Lobby' style='margin:2px 2px; 0 2px' />";
                            j = 1;
                        }
                        break;
                    case "40":
                        InclImg += "<img src='../Hotel/Images/Facility/elevator.png' title='Lifts' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "52":
                        InclImg += "<img src='../Hotel/Images/Facility/health_club.png' title='Gymnasium' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "54":
                    case "55":
                    case "56":
                    case "57":
                    case "58":
                        if (k == 0)
                        {
                            InclImg += "<img src='../Hotel/Images/Facility/Internet.png' title='Internet' style='margin:2px 2px; 0 2px' />";
                            k = 1;
                        }
                        break;
                    case "59":
                        InclImg += "<img src='../Hotel/Images/Facility/laundary.png' title='Laundry facilities' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "71":
                    case "72":
                    case "73":
                    case "74":
                    case "75":
                        if (l == 0)
                        {
                            InclImg += "<img src='../Hotel/Images/Facility/Parking.png' title='Parking' style='margin:2px;' />";
                            l = 1;
                        }
                        break;
                    case "88":
                        InclImg += "<img src='../Hotel/Images/Facility/sauna.png' title='Sauna' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "122":
                    case "360":
                        InclImg += "<img src='../Hotel/Images/Facility/AC.png' title='AC' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "126":
                    case "325":
                    case "131":
                        if (m == 0)
                        {
                            InclImg += "<img src='../Hotel/Images/Facility/TV.png' title='TV' style='margin:2px 2px; 0 2px' />";
                            m = 1;
                        }
                        break;
                    case "334":
                        InclImg += "<img src='../Hotel/Images/Facility/handicap.png' title='Disabled facilities' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "338":
                        InclImg += "<img src='../Hotel/Images/Facility/golf.png' title='Golf' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "345":
                        InclImg = "<img src='../Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' style='margin:2px 2px; 0 2px' />";
                        break;
                    case "355":
                        InclImg += "<img src='../Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' style='margin:2px 2px; 0 2px' />";
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetHotelService_TG");
        }
        return InclImg;
    }
    [WebMethod(EnableSession = true)]
    public int HotelPriceEnquiry(string HotelName)
    {
        HotelSearch HotelDetails = new HotelSearch(); int i = 0;
        try
        {
            if (Session["UID"] != null && Session["HotelSearch"] != null)
            {
                HotelDetails = (HotelSearch)HttpContext.Current.Session["HotelSearch"];
                HotelBAL.HotelSendMail_Log objHtlEnquery = new HotelBAL.HotelSendMail_Log();
                HotelDetails.HotelName = HotelName;
                i = objHtlEnquery.HotelEnquiryEmail(HotelDetails);
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelPriceEnquiry");
        }
        return i;
    }

    public void exporttexttoexel()
    {
        List<string> lstColumnNames = new List<string>();
        List<List<string>> lstRowData = new List<List<string>>();
        try
        {
            System.IO.StreamReader file =
             new System.IO.StreamReader("D:\\ActivePropertyList.txt");
            string line = "";
            int counter = 0;
            while ((line = file.ReadLine()) != null)
            {
                if (counter == 0)
                {
                    lstColumnNames.AddRange(line.Split('|'));
                }
                else
                {
                    List<string> tempRowData = new List<string>();
                    tempRowData.AddRange(line.Split('|'));
                    lstRowData.Add(tempRowData);
                }
                counter++;
            }
            System.IO.TextWriter tw = new System.IO.StreamWriter("D:\\Expedia.xlsx");

            if (lstColumnNames.Count != 0)
            {
                string temp = "";
                foreach (string str in lstColumnNames)
                {
                    if (temp != "")
                    {
                        temp += ";";
                    }
                    temp += str;
                }
                tw.WriteLine(temp);


                foreach (List<string> lstRow in lstRowData)
                {
                    temp = "";

                    foreach (string str in lstRow)
                    {
                        if (temp != "")
                        {
                            temp += ";";
                        }
                        temp += str;
                    }
                    tw.WriteLine(temp);
                }
                tw.Close();
            }
        }
        catch (Exception ex)
        {
            string a = "";
        
        }
    }
}

