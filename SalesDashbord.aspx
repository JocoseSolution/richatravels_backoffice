﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="SalesDashbord.aspx.cs" Inherits="SalesDashbord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>

    <style type="text/css">
        .huge {
            font-size: 20px !important;
        }

        .table th {
            font-size: 11px;
            text-align: left;
            text-transform: uppercase;
            background: #d9534f !important;
            color: #fff;
        }

        .table td:first-child, .table--flip tbody tr:first-child {
            background-image: none !important;
        }

        .table td:last-child, .table--flip tbody tr:last-child {
            background-image: none !important;
        }

        .table tbody {
            color: #fff !important;
            background: #337ab7;
        }
    </style>
    <div class="row form-group" style="margin-top: 2px; padding: 11px 0px;">
        <div class="col-sm-3" style="margin-top: 6px;">
            <div class="form-group" id="divFromDate">
                <input type="text" name="From" id="From" class="form-control" placeholder="dd/mm/yyyy" />
                <asp:HiddenField ID="hdnFromDate" runat="server" />
            </div>
        </div>
        <div class="col-sm-3" style="margin-top: 6px;">
            <div class="form-group" id="divToDate">
                <input type="text" name="To" id="To" class="form-control" placeholder="dd/mm/yyyy" />
                <asp:HiddenField ID="hdnToDate" runat="server" />
            </div>
        </div>
        <div class="col-sm-3" style="margin-top: 6px;">
            <div class="form-group" id="td_Agency" runat="server">
                <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                    onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                <input type="hidden" id="hidtxtAgencyName" class="form-control" name="hidtxtAgencyName" value="" />
                <asp:HiddenField ID="hdnAgency" runat="server" />
            </div>
        </div>
        <div class="col-sm-3" style="margin-top: 6px;">
            <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" Style="margin-top: -4px;" OnClick="btn_result_Click" />
        </div>
    </div>
    <div class="row form-group">
        <div class="col-lg-2 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-8">
                            <p style="font-size: 11px; font-weight: bold;">Ticket Count</p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <div class="huge">
                                <asp:Label ID="lblAirCount" runat="server"></asp:Label>
                            </div>

                        </div>
                    </div>
                    <div>
                        <asp:Label ID="lblAirAmount" runat="server"></asp:Label>
                    </div>
                </div>

                <a href="<%= ResolveUrl("~/SprReports/ADMIN/QCTicketReport.aspx")%>">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>

            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <p style="font-size: 11px; font-weight: bold;">Cash In-flow</p>
                        </div>
                        <div class="col-xs-3 text-right">
                            <div class="huge">
                                <asp:Label ID="lblCashInFlowCount" runat="server"></asp:Label>
                            </div>

                        </div>
                    </div>
                    <div><asp:Label ID="lblCashInFlowAmt" runat="server"></asp:Label></div>
                </div>
                <a href="<%= ResolveUrl("~/SprReports/Accounts/CashInFlow.aspx")%>">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <p style="font-size: 10px; font-weight: bold;">Cash Out-flow</p>
                        </div>
                        <div class="col-xs-3 text-right">
                            <div class="huge">
                                 <asp:Label ID="lblCashOutFlowCount" runat="server"></asp:Label>
                            </div>

                        </div>
                    </div>
                    <div> <asp:Label ID="lblCashOutFlowAmt" runat="server"></asp:Label></div>
                </div>
                <a href="<%= ResolveUrl("~/SprReports/Accounts/CashInFlow.aspx")%>">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <p style="font-size: 10px; font-weight: bold;">Refunded TKT</p>
                        </div>
                        <div class="col-xs-3 text-right">
                            <div class="huge">
                                <asp:Label ID="lblRefundCount" runat="server"></asp:Label>
                            </div>

                        </div>
                    </div>
                    <div><asp:Label ID="lblRefundAmt" runat="server"></asp:Label></div>
                </div>

                <a href="<%= ResolveUrl("~/SprReports/Refund/CancellationReport.aspx")%>">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>

            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <p style="font-size: 11px; font-weight: bold;">P.G Uploads</p>
                        </div>
                        <div class="col-xs-3 text-right">
                            <div class="huge">
                                <asp:Label ID="lblPgUploadCount" runat="server"></asp:Label>
                            </div>

                        </div>
                    </div>
                    <div><asp:Label ID="lblPgUploadAmt" runat="server"></asp:Label></div>
                </div>
                <a href="<%= ResolveUrl("~/SprReports/Reissue/ReissueReport.aspx")%>">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9">
                            <p style="font-size: 11px; font-weight: bold;">API Sales</p>
                        </div>
                        <div class="col-xs-3 text-right">
                            <div class="huge">
                                <asp:Label ID="lblApiCount" runat="server">0</asp:Label>
                            </div>

                        </div>
                    </div>
                    <div><asp:Label ID="lblApiAmt" runat="server">0.00</asp:Label></div>
                </div>

                <a href="<%= ResolveUrl("~/SprReports/HoldPNR/PreHold.aspx")%>">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>

            </div>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-sm-10">
            <%=StrAirLine %>
        </div>
        <div class="col-sm-2">
            <div class="row">
                <div class="col-lg-12 col-md-6">
                    <div class="panel panel-primary" style="width: 111%;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-8">
                                    <p style="font-size: 11px; font-weight: bold;">Rail</p>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblRailCount" runat="server"></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div>
                                <asp:Label ID="lblRailAmount" runat="server"></asp:Label>
                            </div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/Accounts/IrctcLedger.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="col-lg-12 col-md-6">
                    <div class="panel panel-primary" style="width: 111%;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-8">
                                    <p style="font-size: 11px; font-weight: bold;">Hotel</p>
                                </div>
                                <div class="col-xs-3 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblHotelCount" runat="server"></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div><asp:Label ID="lblHotelAmt" runat="server"></asp:Label></div>
                        </div>

                        <a href="<%= ResolveUrl("~/SprReports/Hotel/HtlBookingRpt.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="col-lg-12 col-md-6">
                    <div class="panel panel-primary" style="width: 111%;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-8">
                                    <p style="font-size: 11px; font-weight: bold;">Bus</p>
                                </div>
                                <div class="col-xs-3 text-right">
                                    <div class="huge">
                                        <asp:Label ID="lblBusCount" runat="server"></asp:Label>
                                    </div>

                                </div>
                            </div>
                            <div><asp:Label ID="lblBusAmount" runat="server"></asp:Label></div>
                        </div>

                        <a href="<%= ResolveUrl("~/BS/BusReport.aspx")%>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        var hdnFromDate = '<%=hdnFromDate.ClientID%>';
        $("#From").val($("#" + hdnFromDate).val());
        var hdnToDate = '<%=hdnToDate.ClientID%>';
        $("#To").val($("#" + hdnToDate).val());

        var hdnAgency = '<%=hdnAgency.ClientID%>';
        if ($("#" + hdnAgency).val() != "") {
            debugger;
            var agencySpilit = $("#" + hdnAgency).val().split("+");
            $("#txtAgencyName").val(agencySpilit[0]);
            $("#hidtxtAgencyName").val(agencySpilit[1]);
        }
    </script>
</asp:Content>

